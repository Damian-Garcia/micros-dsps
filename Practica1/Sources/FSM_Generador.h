/*
 * FSM_Generador.h
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia Y Erick :)
 */

#ifndef SOURCES_FSM_GENERADOR_H_
#define SOURCES_FSM_GENERADOR_H_
#include "DataTypeDefinitions.h"
#include "MK64F12.h"
#include "DAC.h"
#include "PIT.h"
#include "Eventos.h"
#include "NVIC.h"
#include "GPIO.h"
#include "PinConfiguration.h"

/*!
 * Structure of a state on the machine. Each state knows the next state (depending on the input) and also
 * has an action to perform in the current state.
 */
typedef enum
{
	FSM_GN_IDLE,
	FSM_GN_IDLE_ACTIVE,
	FSM_GN_SQUARE,
	FSM_GN_SQUARE_NEXT,
	FSM_GN_SINE,
	FSM_GN_SINE_NEXT,
	FSM_GN_TRIANGLE,
	FSM_GN_TRIANGLE_NEXT,
	FSM_GN_MAX_ENUM
}FSM_GeneratorState;

typedef void(*fptrAction)(uint16_t*);

typedef struct
{
	uint16_t* actionArgument; //Indicates which is going to be the paramter of the action
	LedValue ledStates[LED_VALUE_MAX_ENUM];
	fptrAction Accion; //Receive the argument that is going to modify
	FSM_GeneratorState nextEstate[GENERADOR_MAX_ENUM];
}FsmGenStateType;

void FSM_Generator_Config();
void FSM_GeneratorEvaluate(Evento_Generador event);


#endif /* SOURCES_FSM_GENERADOR_H_ */
