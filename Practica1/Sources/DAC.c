/*
 * DAC.c
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia Y Erick Ortega =)
 */
#include "DAC.h"

BooleanType DAC_clockGatingDisable(DAC_name dacName) {
	switch (dacName)
	/** Selecting the DAC for clock Disabling*/
	{
	case DAC_0: /** DAC 0 is selected*/
		SIM_SCGC2 &= ~(SIM_SCGC2_DAC0_MASK);
		break;
	case DAC_1: /** DAC 1 is selected*/
		SIM_SCGC2 &= ~(SIM_SCGC2_DAC1_MASK);
		break;
	default: /**If doesn't exist the option*/
		return (FALSE);
	} // end switch
	/**Successful configuration*/
	return (TRUE);
} // end function

BooleanType DAC_clockGatingEnable(DAC_name dacName) {
	switch (dacName)
	/** Selecting the DAC for clock enabling*/
	{
	case DAC_0: /** DAC 0 is selected*/
		SIM_SCGC2 |= SIM_SCGC2_DAC0_MASK;
		break;
	case DAC_1: /** DAC 1 is selected*/
		SIM_SCGC2 |= SIM_SCGC2_DAC1_MASK;
		break;
	default: /**If doesn't exist the option*/
		return (FALSE);
	} // end switch
	/**Successful configuration*/
	return (TRUE);
} // end function

void DAC_setDACx_DATnL(DAC_name dacName, uint8_t index, uint8_t value) {
	switch (dacName)
	/** Selecting the DAC for clock enabling*/
	{
	case DAC_0: /** DAC 0 is selected*/
		DAC0_DATL(index) = value;
		break;
	case DAC_1: /** DAC 1 is selected*/
		DAC1_DATL(index) = value;
		break;
	default:
		break;
	} // end switch
}
void DAC_setDACx_DATnH(DAC_name dacName, uint8_t index, uint8_t value) {
	switch (dacName)
	/** Selecting the DAC for clock enabling*/
	{
	case DAC_0: /** DAC 0 is selected*/
		DAC0_DATH(index) = value;
		break;
	case DAC_1: /** DAC 1 is selected*/
		DAC1_DATH(index) = value;
		break;
	default:
		break;
	} // end switch
}
void DAC_setDACx_DATnValue(DAC_name dacName, uint8_t index, uint16_t value) {
	uint8_t lowValue = (value & (0xFF));
	DAC_setDACx_DATnL(dacName, index, lowValue);
	uint8_t highValue = (value & (0x0F00)) >> 8;
	DAC_setDACx_DATnH(dacName, index, highValue);
}
BooleanType DACx_SRreadTopPositionFlag(DAC_name dacName) //returns DACBFRPTF
{
	switch (dacName) {
	case DAC_0:
		return ((DAC0_SR) & DAC_SR_DACBFRPTF_MASK) >> 1;
		break;
	case DAC_1:
		return ((DAC1_SR) & DAC_SR_DACBFRPTF_MASK) >> 1;
		break;
	default:
		return FALSE;
		break;
	}
}
BooleanType DACx_SRreadBottomPositionFlag(DAC_name dacName) //returns DACBFRPBF
{
	switch (dacName) {
	case DAC_0:
		return (DAC0_SR) & DAC_SR_DACBFRPBF_MASK;
		break;
	case DAC_1:
		return (DAC1_SR) & DAC_SR_DACBFRPBF_MASK;
		break;
	default:
		return FALSE;
		break;
	}
}
void DACx_SRclearTopPositionFlag(DAC_name dacName) {
	switch (dacName) {
	case DAC_0:
		DAC0_SR &= ~(DAC_SR_DACBFRPTF_MASK);
		break;
	case DAC_1:
		DAC1_SR &= ~(DAC_SR_DACBFRPTF_MASK);
		break;
	default:
		break;
	}
}
void DACx_SRclearBottomPositionFlag(DAC_name dacName) {
	switch (dacName) {
	case DAC_0:
		DAC0_SR &= ~(DAC_SR_DACBFRPBF_MASK);
		break;
	case DAC_1:
		DAC1_SR &= ~(DAC_SR_DACBFRPBF_MASK);
		break;
	default:
		break;
	}
}
void DAC_ControlRegisterZeroConfig(DAC_ConfigurationC0 dacConfigC0) {
	switch (dacConfigC0.dacName) {
	case (DAC_0):
		DAC0_C0 =
				DAC_C0_DACRFS(
						dacConfigC0.dacReference) | DAC_C0_DACTRGSEL(dacConfigC0.dacTrigger)| DAC_C0_LPEN(dacConfigC0.dacPower)| DAC_C0_DACBTIEN(dacConfigC0.topPtrInterrupt)| DAC_C0_DACBBIEN(dacConfigC0.bottomPtrInterrupt);
		//DAC0_C0 =DAC_C0_DACRFS(dacConfigC0.dacReference) | DAC_C0_DACTRGSEL(dacConfigC0.dacTrigger)| DAC_C0_LPEN(dacConfigC0.dacPower);
		break;
	case (DAC_1):
		DAC1_C0 =
				DAC_C0_DACRFS(
						dacConfigC0.dacReference) | DAC_C0_DACTRGSEL(dacConfigC0.dacTrigger)| DAC_C0_LPEN(dacConfigC0.dacPower)| DAC_C0_DACBTIEN(dacConfigC0.topPtrInterrupt)| DAC_C0_DACBBIEN(dacConfigC0.bottomPtrInterrupt);
		break;
	default:
		break;
	}
}
void DAC_EnableDACx(DAC_name dacName) {
	switch (dacName) {
	case DAC_0:
		DAC0_C0 |= DAC_C0_DACEN(DAC_ENABLE);
		break;
	case DAC_1:
		DAC1_C0 |= DAC_C0_DACEN(DAC_ENABLE);
		break;
	default:
		break;
	}
}
void DAC_DisableDACx(DAC_name dacName) {
	switch (dacName) {
	case DAC_0:
		DAC0_C0 &= ~(DAC_C0_DACEN(DAC_ENABLE));
		break;
	case DAC_1:
		DAC1_C0 &= ~(DAC_C0_DACEN(DAC_ENABLE));
		break;
	default:
		break;
	}
}
void DAC_EnableRegister(DAC_name dacName, DAC_status dacStatus) {
	switch (dacStatus) {
	case DAC_DISABLE:
		DAC_DisableDACx(dacName);
		break;
	case DAC_ENABLE:
		DAC_EnableDACx(dacName);
		break;
	default:
		break;
	}
}
void DAC_SoftTriggerValidDACx(DAC_name dacName) {
	switch (dacName) {
	case DAC_0:
		DAC0_C0 &= ~(DAC_C0_DACSWTRG(DAC_VALIDTRG));
		break;
	case DAC_1:
		DAC1_C0 &= ~(DAC_C0_DACSWTRG(DAC_VALIDTRG));
		break;
	default:
		break;
	}
}
void DAC_SoftTriggerInvalidDACx(DAC_name dacName) {
	switch (dacName) {
	case DAC_0:
		DAC0_C0 &= ~(DAC_C0_DACSWTRG(DAC_VALIDTRG));
		break;
	case DAC_1:
		DAC1_C0 &= ~(DAC_C0_DACSWTRG(DAC_VALIDTRG));
		break;
	default:
		break;
	}
}
void DAC_SoftwareTriggerRegister(DAC_name dacName, DAC_status dacStatus) {
	switch (dacStatus) {
	case DAC_INVALIDTRG:
		DAC_SoftTriggerInvalidDACx(dacName);
		break;
	case DAC_VALIDTRG:
		DAC_SoftTriggerValidDACx(dacName);
		break;
	default:
		break;
	}
}
void DAC_ControlRegisterOneConfig(DAC_ConfigurationC1 dacConfigC1) {
	switch (dacConfigC1.dacName) {
	case DAC_0:
		DAC0_C1 =
				DAC_C1_DMAEN(
						dacConfigC1.dacDmaStatus) | DAC_C1_DACBFMD(dacConfigC1.dacBufferMode)
						| DAC_C1_DACBFEN(dacConfigC1.dacBufferEnable);
		break;
	case DAC_1:
		DAC0_C1 =
				DAC_C1_DMAEN(
						dacConfigC1.dacDmaStatus) | DAC_C1_DACBFMD(dacConfigC1.dacBufferMode)
						| DAC_C1_DACBFEN(dacConfigC1.dacBufferEnable);
		break;
	default:
		break;
	}
}
