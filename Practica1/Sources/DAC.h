/**
	 \file PIT.h
	 \brief
	 This is the header file for the DAC driver.
	 It contains public functions to interface with the DAC of the FRDM K64.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	9/28/2016
 */

#ifndef SOURCES_DAC_H_
#define SOURCES_DAC_H_
#include "MK64F12.h"
#include "DataTypeDefinitions.h"

/*!
 * \typedef DAC_name
 * \brief Enum type to select the one of the two DACs
 */
typedef enum
{
	DAC_0, /*!< Definition to select DAC 0*/
	DAC_1, /*!< Definition to select DAC 1 */
} DAC_name;

/*!
 * \typedef DAC_reference
 * \brief Enum type to select the voltage reference
 */
typedef enum
{
	DAC_REF1, /*!< VREF_OUT reference (not conected) 0*/
	DAC_REF2, /*!< VDDA (3.3 Volts) 1 */
}DAC_referenceType;

/*!
 * \typedef DAC_triggerType
 * \brief Enum type to select trigger mode
 */
typedef enum
{
	DAC_HARDWARETRIGGER, /*!< Hardware Trigger 0*/
	DAC_SOFTWARETRIGGER, /*!< Software Trigger DAC 1 */
}DAC_triggerType;

/*!
 * \typedef DAC_PowerType
 * \brief Enum type to select the power mode
 */
typedef enum
{
	DAC_HIPWR, /*!< High Power Mode*/
	DAC_LWPWR, /*!< Low Power Mode */
}DAC_PowerType;

/*!
 * \typedef DAC_status
 * \brief Enum type to select the status of the DAC
 */
typedef enum
{
	DAC_DISABLE, /*!< Definition to select DAC 0*/
	DAC_ENABLE, /*!< Definition to select DAC 1 */
}DAC_status;

/*!
 * \typedef DAC_PowerType
 * \brief Enum type to select if the software trigger is valid
 */
typedef enum
{
  DAC_INVALIDTRG,	/*!< Soft trigger is invalid*/
  DAC_VALIDTRG,		/*!< Soft trigger is valid 0*/
}DAC_softwareTrigger;

/*! \struct DAC_Configuration_C0
 * 	\typedef DAC_ConfigurationC0
 * 	\brief Structure type for configuring the C0 register
 */
typedef struct DAC_Configuration_C0 {
	DAC_name dacName;					/*!< Selected DAC  */
	DAC_referenceType dacReference;		/*!< Voltage reference */
	DAC_triggerType dacTrigger;			/*!< Trigger mode */
	DAC_PowerType dacPower;				/*!< Power mode */
	DAC_status topPtrInterrupt;			/*!< */
	DAC_status bottomPtrInterrupt;
} DAC_ConfigurationC0;

/*!
 * \typedef DAC_BufferMode
 * \brief Enum type to select the operating mode of the DAC
 */
typedef enum
{
  DAC_NORMAL,
  DAC_ONETIMESCAN,
}DAC_BufferMode;

/*!
 * \typedef DAC_ConfigurationC1
 * \enum DAC_ConfigurationC1
 * \brief Enum type to select the operating mode of the DAC
 */
typedef struct DAC_Configuration_C1 {
	DAC_name dacName;
	DAC_BufferMode dacBufferMode;
	DAC_status dacDmaStatus;
	DAC_status dacBufferEnable;
} DAC_ConfigurationC1;

/*!
 * \fn BooleanType DAC_ClockGatingDisable(DAC_name dacName)
 * @param dacName DAC to be disabled
 * @return TRUE if it was possible to disable the selected DAC
 * \todo implement
 */
BooleanType DAC_ClockGatingDisable(DAC_name dacName);

/*!
 * \fn BooleanType DAC_clockGatingEnable(DAC_name dacName)
 * @param dacName DAC to be enabled
 * @return TRUE if it was possible to enable the selected DAC
 */
BooleanType DAC_clockGatingEnable(DAC_name dacName);

/*!
 * \fn void DAC_setDACx_DATnL(DAC_name dacName, uint8_t index, uint8_t value)
 * \brief sets the 8 LSB of the 12 bit value to be converted
 * @param dacName DAC to be written
 * @param index	Offset in the data buffer
 * @param value Data low value to be converted
 */
void DAC_setDACx_DATnL(DAC_name dacName, uint8_t index, uint8_t value);

/*!
 * \fn void DAC_setDACx_DATnH(DAC_name dacName, uint8_t index, uint8_t value)
 * \brief sets the 4 MSB of the 12 bit value to be converted
 * @param dacName DAC to be written
 * @param index	Offset in the data buffer
 * @param value Data high value to be converted
 */
void DAC_setDACx_DATnH(DAC_name dacName, uint8_t index, uint8_t value);

/*!
 * \fn void DAC_setDACx_DATnValue(DAC_name dacName, uint8_t index, uint16_t value)
 * \brief This functions sets the 12 bits of the value to be converted
 * @param dacName
 * @param index
 * @param value
 */
void DAC_setDACx_DATnValue(DAC_name dacName, uint8_t index, uint16_t value);

/*!
 * \fn BooleanType DACx_SRreadTopPositionFlag(DAC_name dacName)
 * \brief Returns the state of the Top Position Flag
 * @param dacName DAC Top Position flag to be read
 * @return	TRUE if the top position flag is at the top of the buffer, FALSE otherwise
 */
BooleanType DACx_SRreadTopPositionFlag(DAC_name dacName);

/*!
 * \fn BooleanType DACx_SRreadBottomPositionFlag(DAC_name dacName
 * \brief Returns the state of the Bottom Position Flag
 * @param dacName DAC Bottom Position flag to be read
 * @return	TRUE if the bottom position flag is at the bottom of the buffer, FALSE otherwise
 */
BooleanType DACx_SRreadBottomPositionFlag(DAC_name dacName);

/*!
 * \fn void DACx_SRclearTopPositionFlag(DAC_name dacName)
 * \brief clears the corresponding Top Position Flag
 * @param dacName DAC top position flag to be cleared
 */
void DACx_SRclearTopPositionFlag(DAC_name dacName);

/*!
 * \fn void DACx_SRclearBottomPositionFlag(DAC_name dacName)
 * \brief clears the corresponding Bottom Position Flag
 * @param dacName DAC bottom position flag to be cleared
 */
void DACx_SRclearBottomPositionFlag(DAC_name dacName);

/*!
 * \fn void DAC_ControlRegisterZeroConfig(DAC_ConfigurationC0 dacConfigC0)
 * \brief This function configures the DACx_C0 from a given DAC_ConfigurationC0 variable
 * @param dacConfigC0 Configuration structure to utilize
 */
void DAC_ControlRegisterZeroConfig(DAC_ConfigurationC0 dacConfigC0);

/*!
 * \fn void DAC_EnableDACx(DAC_name dacName)
 * \brief Enables the DAC generation operation
 * @param dacName DAC select value
 */
void DAC_EnableDACx(DAC_name dacName);

/*!
 * \fn void DAC_DisableDACx(DAC_name dacName)
 * \brief Disables the DAC conversions.
 * @param dacName DAC select value
 */
void DAC_DisableDACx(DAC_name dacName);

/*!
 * \fn void DAC_EnableRegister(DAC_name dacName, DAC_status dacStatus)
 * \brief Sets the status of a DAC to a given value
 * @param dacName	DAC select value
 * @param dacStatus	Status value, it can be DAC_ENABLED or DAC_DISABLED
 */
void DAC_EnableRegister(DAC_name dacName, DAC_status dacStatus);

/*!
 * \fn void DAC_SoftTriggerValidDACx(DAC_name dacName)
 * \brief Sets software triggers of a DAC to be valid
 * @param dacName	Selected DAC
 */
void DAC_SoftTriggerValidDACx(DAC_name dacName);

/*!
 * \fn void DAC_SoftTriggerInvalidDACx(DAC_name dacName);
 * \brief Sets software triggers of a DAC to be invalid
 * @param dacNam Selected DAC
 */
void DAC_SoftTriggerInvalidDACx(DAC_name dacName);

/*!
 * \fn void DAC_SoftwareTriggerRegister(DAC_name dacName, DAC_status dacStatus)
 * \brief Sets the software triggers of a DAC to be valid or invalid
 * @param dacName	DAC select value
 * @param dacStatus	Status value, it can be DAC_ENABLED(valid) or DAC_DISABLED(invadid)
 */
void DAC_SoftwareTriggerRegister(DAC_name dacName, DAC_status dacStatus);

/*!
 * \fn void DAC_ControlRegisterOneConfig(DAC_ConfigurationC1 dacConfigC1)
 * \brief This function configures the DACx_C1 from a given DAC_ConfigurationC1 variable.
 * @param dacConfigC1 Configuration structure to utilize.
 */
void DAC_ControlRegisterOneConfig(DAC_ConfigurationC1 dacConfigC1);

/*!
 * \fn void DAC_ControlRegisterOneConfig(DAC_ConfigurationC1 dacConfigC1)
 * \brief This function returns the current index of the DataBuffer pointer of a DAC
 * @param dacConfigC1 The selected DAC
 */
uint8_t DAC_getDACBufferPointer(DAC_name dacName);

/*!
 * \fn void DAC_setDACBufferPointer(DAC_name dacName, uint8_t bfrPtr)
 * \brief This function sets the current index of the DataBuffer pointer of a DAC.
 * @param dacName The selected DAC.
 * @param bfrPtr The new index value.
 */
void DAC_setDACBufferPointer(DAC_name dacName, uint8_t bfrPtr);

/*!
 * \fn void DAC_setBufferUpperLimit(DAC_name dacName, uint8_t bfrPtrLimit)
 * \brief This function sets the upper limit of the DataBuffer in a specific DAC
 * @param dacName The selected DAC
 * @param bfrPtrLimit The new limit value
 */
void DAC_setBufferUpperLimit(DAC_name dacName, uint8_t bfrPtrLimit);

#endif /* SOURCES_DAC_H_ */
