/**
 \file FSM_LED_ERROR.h
 \brief
 This is the header file for the LED ERROR FSM
 It contains the State Machine enumeration,the structure for
 containing states, and a way to interface it.
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	9/28/2016
 */

#ifndef SOURCES_FSM_LED_ERROR_H_
#define SOURCES_FSM_LED_ERROR_H_

#include "Eventos.h"

typedef void(*action) (void);

/*! \typedef Estado_FSM_LED_Error
 *	\brief Type enum for the LED ERROR FSM
*/
typedef enum{
	FSM_LED_ERROR_IDLE,
	FSM_LED_ERROR_ACTIVAR1,
	FSM_LED_ERROR_ESPERAR1,
	FSM_LED_ERROR_APAGAR1,
	FSM_LED_ERROR_ESPERAR2,
	FSM_LED_ERROR_ACTIVAR2,
	FSM_LED_ERROR_ESPERAR3,
	FSM_LED_ERROR_APAGAR2,
	FSM_LED_ERROR_MAX_ENUM
}Estado_FSM_LED_Error;

/*! \typedef FSM_LED_ERROR_ESTADO
	\brief This struct type contains a state for the LED ERROR FSM
	The struct has a pointer to function to execute depending on the current even
	It also contains a pointer to an array containing the next state, depending on the current event
 */
typedef struct{
	action a;
	const Estado_FSM_LED_Error *nextList;
}FSM_LED_Error_Estado;

/*!
 * \fn void FSM_LED_Error_init(void)
 * \brief This function configures the initial state of the FSM.
 *	This function sets the initial state to IDLE, configures the PIT_3 timer and
 *	enables its interrupt.
 */
void FSM_LED_Error_init(void);

/*!
 * \fn FSM_LED_Error_evaluar(Evento_LED)
 * \brief This function evaluates the current event of the LED ERRROR FSM.
 *	This function sets the initial state to IDLE, configures the PIT_3 timer and
 *	enables its interrupt.
 */
void FSM_LED_Error_evaluar(Evento_LED);

#endif /* SOURCES_FSM_LED_ERROR_H_ */
