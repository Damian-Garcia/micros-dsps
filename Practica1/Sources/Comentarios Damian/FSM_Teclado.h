/**
 \file FSM_Teclado.h
 \brief
 This is the header file for the Keyboard logic
 It contains private functions to change the state of the execution depending on the current event and state.
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	9/28/2016
 */

#ifndef SOURCES_FSM_TECLADO_H_
#define SOURCES_FSM_TECLADO_H_

#include "Eventos.h"
#include "TecladoMatricial.h"

/*!
 * \typedef FSM_TecladoPassType
 * \brief Enum type for selecting the current process password
 */
typedef enum
{
	PASS_MASTER,//!< PASS_MASTER
	PASS_A,     //!< PASS_Motor
	PASS_B,     //!< PASS_Genarator
	PASS_MAX    //!< Enumerator Max value
}FSM_TecladoPassType;

/*!
 * \typedef FSM_TecladoState
 * \brief Enum type for the Teclado FSM
 */
typedef enum
{
	FSM_TECLADO_IDLE,
	FSM_TECLADO_GET4,
	FSM_TECLADO_SELECTOPTION,
	FSM_TECLADO_CORRECT,
	FSM_TECLADO_INCORRECT
}FSM_TecladoState;

/*!
 * \typedef FsmTecladoStateType
 * \brief Struct Type for containing the description of a state.
 */
typedef struct
{
	FSM_TecladoState currentState; ///<Indicates which is going to be the parameter of the action
	FSM_TecladoState(*fptrAction)(Evento_Teclado e); ///<Receive the argument that is going to modify
}FsmTecladoStateType;

/*!
 * \fn void FSM_Teclado_init()
 * \brief This function configures the state machine to its initial state
 */
void FSM_Teclado_init();

/*!
 * \fn void FSM_Teclado_Evaluar(Evento_Teclado e)
 * \brief This function evaluates the new event in the current state.
 */
void FSM_Teclado_Evaluar(Evento_Teclado e);

#endif /* SOURCES_FSM_TECLADO_H_ */
