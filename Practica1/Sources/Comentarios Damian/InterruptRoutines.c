/**
 \file InterruptRoutines.c
 \brief
 This is the source file for the IRQs of Practice2
 It contains all the implementation for the FSMs control signaling
 There is not a header file for this source, as it is only neccesary in the linking process.
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	9/28/2016
 */
#include "MK64F12.h"
#include "GPIO.h"
#include "Eventos.h"
#include "PIT.h"
#include "NVIC.h"



/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function is used as a small delay for debouncing
 	 \param[in]  Integer value.
 */
static void delay(uint16 delay) {
	volatile uint16 counter;

	for (counter = delay; counter > 0; counter--) {
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 IRQ for the SW3 button. It adds a start event to the Generator State Machine.
 */
void PORTA_IRQHandler() {
	if (FALSE == GPIO_readPIN(GPIOA,4)) {
		GPIO_clearInterrupt(GPIOA);
		addEventoGenerador(GENERADOR_SW3);
		delay(1000);
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Currently empty interrupt, it only cleans the interrupt flag.
 */
void PORTB_IRQHandler() {
	GPIO_clearInterrupt(GPIOB);
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 IRQ for the SW2 button. It adds a start event to the Generator State Machine.
 */
void PORTC_IRQHandler() {
	/*
	 * Solo realiza el add del evento del motor en caso de que haya sido el pin del switch 2
	 */
	if (FALSE == GPIO_readPIN(GPIOC, 4)) {

		GPIO_clearInterrupt(GPIOC);
		addEventoMotor(MOTOR_SW2);
		delay(1000);
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function decodes a key from the Matrix Keyboard.
 	 \param[in]  Value of the ports, in a single variable.
 	 \return The event corresponding to the current key.

 */
Evento_Teclado DecoData(uint32_t data) {
	switch (data) {
	case 0x77: //01110111
		return TECLADO_1; //0x1;
		break;
	case 0xb7: //10110111
		return TECLADO_4; //0x4;
		break;
	case 0xd7:
		return TECLADO_7; //0x7;
		break;
	case 0xe7:
		return TECLADO_E; //0xe;
		break;
	case 0x7b: //01110111
		return TECLADO_2; //0x2;
		break;
	case 0xbb: //10110111
		return TECLADO_5; //0x5;
		break;
	case 0xdb:
		return TECLADO_8; //0x8;
		break;
	case 0xeb:
		return TECLADO_0; //0x0;
		break;
	case 0x7d: //01110111
		return TECLADO_3; //0x3;
		break;
	case 0xbd: //10110111
		return TECLADO_6; //0x6;
		break;
	case 0xdd:
		return TECLADO_9; //0x9;
		break;
	case 0xed:
		return TECLADO_F; //0xf;
		break;
	case 0x7e: //01110111 //01110110
		return TECLADO_A; //0xA;
		break;
	case 0xbe: //10110111
		return TECLADO_B; //0xB;
		break;
	case 0xde:
		return TECLADO_C; //0xC;
		break;
	case 0xee:
		return TECLADO_D; //0xD;
		break;
	default:
		return TECLADO_NOP;
		break;
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function is used as a delay for debouncing the keyboard.
 	 \param[in]  Integer value.
 */
static void tms(unsigned int ms) {
	volatile unsigned char i, k;
	volatile unsigned int ms2;
	ms2 = ms;
	while (ms2) {
		for (i = 0; i <= 140; i++) {
			k++;
		}
		ms2--;
		k++;
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 IRQ for the Matrix Keyboard POIRT. It decodes the data in the port and adds to
 	 	 Matrix Keyboard the decoded key as an event.
 */
void PORTD_IRQHandler() {
	NVIC_disableInterrupt(PORTD_IRQ);
	if (!isKeyPressed()) {
		uint32_t data = (GPIOD_PDIR & 0xF) << 4 | (GPIOC_PDIR & 0xF);
		tms(2000);
		addEventoTeclado(DecoData(data));
	}
	NVIC_enableInterruptAndPriotity(PORTD_IRQ, PRIORITY_2);
	GPIO_clearInterrupt(GPIOD);
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Currently empty interrupt, it only cleans the interrupt flag.
 */
void PORTE_IRQHandler() {
	GPIO_clearInterrupt(GPIOE);
}
