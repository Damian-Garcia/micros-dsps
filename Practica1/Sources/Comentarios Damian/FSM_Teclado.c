/*
 * FSM_Teclado.c
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#include "FSM_Teclado.h"

static FSM_TecladoPassType currentPassType;// Stores the current value of the pass type which helps on decisions
static uint8 passArray[PASS_MAX][4]={{1,2,3,4}, {4,5,6,7}, {7,8,9,0}};
static uint8 passEntered[4];

static uint8 index;

static FSM_TecladoState actualState;

FSM_TecladoState idleNextStateCalculator(Evento_Teclado e);
FSM_TecladoState get4NextStateCalculator(Evento_Teclado e);
FSM_TecladoState optionSelectNextStateCalculator(Evento_Teclado e);
FSM_TecladoState correctNextStateCalculator(Evento_Teclado e);
FSM_TecladoState incorrectNextStateCalculator(Evento_Teclado e);

const FsmTecladoStateType FSMteclado[5]=
 {
	 {FSM_TECLADO_IDLE,idleNextStateCalculator},
	 {FSM_TECLADO_GET4,get4NextStateCalculator},
	 {FSM_TECLADO_SELECTOPTION,optionSelectNextStateCalculator},
	 {FSM_TECLADO_CORRECT,correctNextStateCalculator},
	 {FSM_TECLADO_INCORRECT,incorrectNextStateCalculator},
 };

void FSM_Teclado_init()
{
	for(int i = 0; i<4; i++)
	{
		passEntered[i] = 0;
	}
	index = 0;
	actualState = FSM_TECLADO_IDLE;
	currentPassType = PASS_MASTER;

}
void FSM_Teclado_Evaluar(Evento_Teclado e)
{
	if((e != TECLADO_NOP) || (actualState == FSM_TECLADO_CORRECT) ||(actualState == FSM_TECLADO_INCORRECT)  )// Permite ejecutar una accion solo cuando se presiono una tecla
	{
		actualState = FSMteclado[actualState].fptrAction(e);
	}
}

FSM_TecladoState idleNextStateCalculator(Evento_Teclado e)
{
	clearRgbLeds(BLUE);
	//Solamente entramos cuando se presiono alguna tecla
	index  = 0; // reiniciamos el valor de index
	passEntered[index++] = e - 1;
	currentPassType = PASS_MASTER; // Seleccionamos tipo PASS_MASTER
	return FSM_TECLADO_GET4;
}

FSM_TecladoState get4NextStateCalculator(Evento_Teclado e)
{
	setRgbLeds(BLUE);
	//Solamente llegamos aqui cuando se presiono una tecla diferente de nop
	passEntered[index++] = e - 1;
	if(index == 4)//Si ya llegamos al ultimo elemento de la contrasena
	{
		int error = 0;
		for(int i=0 ; i < 4 ; i++)
		{
			if(passEntered[i] != passArray[currentPassType][i])
			{
				error = 1;
				break;
			}
		}
		return (error==1)? FSM_TECLADO_INCORRECT : FSM_TECLADO_CORRECT;
	}
	return FSM_TECLADO_GET4;
}

FSM_TecladoState optionSelectNextStateCalculator(Evento_Teclado e)
{
	clearRgbLeds(BLUE);
	index  = 0;
	switch (e)
	{
		case TECLADO_A:
			currentPassType = PASS_A;
			return FSM_TECLADO_GET4;
			break;
		case TECLADO_B:
			currentPassType = PASS_B;
			return FSM_TECLADO_GET4;
			break;
		default:
			return FSM_TECLADO_SELECTOPTION;
			break;
	}
}

FSM_TecladoState correctNextStateCalculator(Evento_Teclado e)
{
	addEventoLEDInicio(LED_EVENT_ACTIVAR);
	switch (currentPassType)
	{
	case PASS_MASTER:
		break;
	case PASS_A:
		addEventoMotor(MOTOR_ACTIVAR);
		break;
	case PASS_B:
		addEventoGenerador(GENERADOR_ACTIVAR);
		break;
	default:
		break;
	}
	return FSM_TECLADO_SELECTOPTION;
}

FSM_TecladoState incorrectNextStateCalculator(Evento_Teclado e)
{
	addEventoLEDError(LED_EVENT_ACTIVAR);
	if(currentPassType == PASS_MASTER)
	{
		return FSM_TECLADO_IDLE;
	}
	return FSM_TECLADO_SELECTOPTION;
}

