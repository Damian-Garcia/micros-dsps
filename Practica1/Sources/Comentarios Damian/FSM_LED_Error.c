/*
 * FSM_LED_Error.c
 *
 *  Created on: Sep 25, 2016
 *      Author: Damian Garcia
 */
#include "FSM_LED_Error.h"
#include "NVIC.h"
#include "DataTypeDefinitions.h"
#include "PinConfiguration.h"
#include "PIT.h"

static Estado_FSM_LED_Error actualError;

static void LED_Error_idle();
static void LED_Error_activar1();
static void LED_Error_esperar1();
static void LED_Error_apagar1();
static void LED_Error_esperar2();
static void LED_Error_activar2();
static void LED_Error_esperar3();
static void LED_Error_apagar2();

static const Estado_FSM_LED_Error FSM_LED_Error_Transiciones[FSM_LED_ERROR_MAX_ENUM][LED_MAX_ENUM] =
{	//NOP						//ACTIVAR					//TICK
	{FSM_LED_ERROR_IDLE, 		FSM_LED_ERROR_ACTIVAR1,		FSM_LED_ERROR_IDLE},//Idle
	{FSM_LED_ERROR_ESPERAR1,	FSM_LED_ERROR_ACTIVAR1,		FSM_LED_ERROR_ESPERAR1}, 	//Activar1
	{FSM_LED_ERROR_ESPERAR1,	FSM_LED_ERROR_ACTIVAR1, 	FSM_LED_ERROR_APAGAR1},	//Esperar1
	{FSM_LED_ERROR_ESPERAR2,	FSM_LED_ERROR_ACTIVAR1,		FSM_LED_ERROR_ESPERAR2},	// Apagar1
	{FSM_LED_ERROR_ESPERAR2,	FSM_LED_ERROR_ACTIVAR1,		FSM_LED_ERROR_ACTIVAR2},	// Esperar2
	{FSM_LED_ERROR_ESPERAR3,	FSM_LED_ERROR_ACTIVAR1,		FSM_LED_ERROR_ESPERAR3},	// Activar2
	{FSM_LED_ERROR_ESPERAR3,	FSM_LED_ERROR_ACTIVAR1,		FSM_LED_ERROR_APAGAR2},	// Esperar3
	{FSM_LED_ERROR_IDLE,		FSM_LED_ERROR_ACTIVAR1, 	FSM_LED_ERROR_IDLE},// Apagar2
};

static const FSM_LED_Error_Estado FSM_LED_Error_Tabla[FSM_LED_ERROR_MAX_ENUM] =
{
	{ LED_Error_idle, FSM_LED_Error_Transiciones[FSM_LED_ERROR_IDLE] }, // Idle
	{ LED_Error_activar1, FSM_LED_Error_Transiciones[FSM_LED_ERROR_ACTIVAR1] }, // Activar1
	{ LED_Error_esperar1, FSM_LED_Error_Transiciones[FSM_LED_ERROR_ESPERAR1] }, // Esperar1
	{ LED_Error_apagar1, FSM_LED_Error_Transiciones[FSM_LED_ERROR_APAGAR1] }, // Apagar1
	{ LED_Error_esperar2, FSM_LED_Error_Transiciones[FSM_LED_ERROR_ESPERAR2] }, // Esperar2
	{ LED_Error_activar2, FSM_LED_Error_Transiciones[FSM_LED_ERROR_ACTIVAR2] }, // Activar2
	{ LED_Error_esperar3, FSM_LED_Error_Transiciones[FSM_LED_ERROR_ESPERAR3] }, // Esperar3
	{ LED_Error_apagar2, FSM_LED_Error_Transiciones[FSM_LED_ERROR_APAGAR2] }, // Apagar2
};

void FSM_LED_Error_init(void) {
	actualError = FSM_LED_ERROR_IDLE;

	PIT_TimerType pit = PIT_3;
	PIT_ClockGatingEnable(); //Inicializamos el clock gating del PIT
	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE };
	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_CH3_IRQ, PRIORITY_5);
	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
}

void FSM_LED_Error_evaluar(Evento_LED e) {
	Estado_FSM_LED_Error siguiente =
	FSM_LED_Error_Tabla[actualError].nextList[e];
	actualError = siguiente;
	FSM_LED_Error_Tabla[actualError].a();
}

void LED_Error_idle() {

}

void LED_Error_activar1() {
	setTecladoLedError(LED_VALUE_ON);
	float sysClock = 21000000;
	PIT_delay(PIT_3, sysClock, 1);
}
void LED_Error_esperar1() {

}

void LED_Error_apagar1() {
	setTecladoLedError(LED_VALUE_OFF);
	float sysClock = 21000000;
	PIT_delay(PIT_3, sysClock, 1);
}

void LED_Error_esperar2() {

}

void LED_Error_activar2() {
	setTecladoLedError(LED_VALUE_ON);
	float sysClock = 21000000;
	PIT_delay(PIT_3, sysClock, 1);
}

void LED_Error_esperar3() {
}

void LED_Error_apagar2() {
	setTecladoLedError(LED_VALUE_OFF);
}
