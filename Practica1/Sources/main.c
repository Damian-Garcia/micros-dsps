/**
 \file main.c
 \brief
 This is the main file of the Practica2 project
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	9/28/2016
 */
#include "MK64F12.h"
#include "FSM_Generador.h"
#include "PinConfiguration.h"
#include "FSM_Motor.h"
#include "TecladoMatricial.h"
#include "Practica2.h"

/*!
 * The main function calls the initial configurations once and evaluates the run function forever
 * @return 0 (It will not return anything)
 */
int main(void)
{
	practica2_init();

	for (;;) {
    	practica2_run();
    }
    return 0;
}
