/**
 \file
 \brief
 This is the source file for the GPIO device driver for Kinetis K64.
 It contains all the implementation for configuration functions and runtime functions.
 i.e., this is the application programming interface (API) for the GPIO peripheral.
 \author J. Luis Pizano Escalante, luispizano@iteso.mx
 \date	7/09/2014
 */
#include "MK64F12.h"
#include "GPIO.h"
#include "Eventos.h"
#include "PIT.h"
#include "NVIC.h"

static myData32 dataPortE = { 0 };
static myData32 dataPortB = { 0 };
static myData32 dataPortC = { 0 };
static myData32 dataPortD = { 0 };
static myData32 dataPortA = { 0 };

struct GPIO_Interrupt_flags {
	uint8 FlagPortA :1;
	uint8 FlagPortB :1;
	uint8 FlagPortC :1;
	uint8 FlagPortD :1;
	uint8 FlagPortE :1;
} GPIO_InterruptFlags;

void GPIO_clearInterrupt(GPIO_portNameType portName) {
	switch (portName)/** Selecting the GPIO for clock enabling*/
	{
	case GPIOA: /** GPIO A is selected*/
		PORTA_ISFR = 0xFFFFFFFF;
		break;
	case GPIOB: /** GPIO B is selected*/
		PORTB_ISFR = 0xFFFFFFFF;
		break;
	case GPIOC: /** GPIO C is selected*/
		PORTC_ISFR = 0xFFFFFFFF;
		break;
	case GPIOD: /** GPIO D is selected*/
		PORTD_ISFR = 0xFFFFFFFF;
		break;
	default: /** GPIO E is selected*/
		PORTE_ISFR = 0xFFFFFFFF;
		break;
	} // end switch
}

uint8 GPIO_clockGating(GPIO_portNameType portName) {
	/*!
	 * \dot
	 digraph G {
	 main -> parse -> execute;
	 main -> init;
	 main -> cleanup;
	 execute -> make_string;
	 execute -> printf
	 init -> make_string;
	 main -> printf;
	 execute -> compare;
	 }
	 \enddot
	 */
	switch (portName)/** Selecting the GPIO for clock enabling*/
	{
	case GPIOA: /** GPIO A is selected*/
		SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTA; /** Bit 9 of SIM_SCGC5 is  set*/
		break;
	case GPIOB: /** GPIO B is selected*/
		SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTB; /** Bit 10 of SIM_SCGC5 is set*/
		break;
	case GPIOC: /** GPIO C is selected*/
		SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTC; /** Bit 11 of SIM_SCGC5 is set*/
		break;
	case GPIOD: /** GPIO D is selected*/
		SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTD; /** Bit 12 of SIM_SCGC5 is set*/
		break;
	case GPIOE: /** GPIO E is selected*/
		SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTE; /** Bit 13 of SIM_SCGC5 is set*/
		break;
	default: /**If doesn't exist the option*/
		return (FALSE);
	} // end switch
	/**Successful configuration*/
	return (TRUE);
} // end function

uint8 GPIO_pinControlRegister(GPIO_portNameType portName, uint8 pin,
		const GPIO_pinControlRegisterType* pinControlRegister) {
	switch (portName) {
	case GPIOA:/** GPIO A is selected*/
		PORTA_PCR(pin) = *pinControlRegister;
		break;
	case GPIOB:/** GPIO B is selected*/
		PORTB_PCR(pin) = *pinControlRegister;
		break;
	case GPIOC:/** GPIO C is selected*/
		PORTC_PCR(pin) = *pinControlRegister;
		break;
	case GPIOD:/** GPIO D is selected*/
		PORTD_PCR(pin) = *pinControlRegister;
		break;
	case GPIOE: /** GPIO E is selected*/
		PORTE_PCR(pin) = *pinControlRegister;
		break;
	default:/**If doesn't exist the option*/
		return (FALSE);
		break;

	}
	/**Successful configuration*/
	return (TRUE);
}

void GPIO_writePORT(GPIO_portNameType portName, uint32 Data) {
	switch (portName) {
	case GPIOA:
		GPIOA_PDOR = Data;
		break;
	case GPIOB:
		GPIOB_PDOR = Data;
		break;
	case GPIOC:
		GPIOC_PDOR = Data;
		break;
	case GPIOD:
		GPIOD_PDOR = Data;
		break;
	case GPIOE:
		GPIOE_PDOR = Data;
		break;
	default:
		break;
	}
}

uint32 GPIO_readPORT(GPIO_portNameType portName) {
	uint32 portValue = 0;
	switch (portName) {
	case GPIOA:
		portValue = GPIOA_PDIR;
		break;
	case GPIOB:
		portValue = GPIOB_PDIR;
		break;
	case GPIOC:
		portValue = GPIOC_PDIR;
		break;
	case GPIOD:
		portValue = GPIOD_PDIR;
		break;
	case GPIOE:
		portValue = GPIOE_PDIR;
		break;
	default:
		portValue = 0;
		break;
	}
	return portValue;
}

uint8 GPIO_readPIN(GPIO_portNameType portName, uint8 pin) {
	uint32 mask, portValue;
	mask = 1 << pin;
	switch (portName) {
	case GPIOA:
		portValue = GPIOA_PDIR;
		break;
	case GPIOB:
		portValue = GPIOB_PDIR;
		break;
	case GPIOC:
		portValue = GPIOC_PDIR;
		break;
	case GPIOD:
		portValue = GPIOD_PDIR;
		break;
	case GPIOE:
		portValue = GPIOE_PDIR;
		break;
	default:
		portValue = 0;
		break;
	}
	portValue &= mask;
	return (portValue) ? BIT_ON : BIT_OFF;
}

void GPIO_setPIN(GPIO_portNameType portName, uint8 pin) {
	uint32 valor = (1 << pin);
	switch (portName) {
	case GPIOA:
		GPIOA_PSOR |= valor;
		break;
	case GPIOB:
		GPIOB_PSOR |= valor;
		break;
	case GPIOC:
		GPIOC_PSOR |= valor;
		break;
	case GPIOD:
		GPIOD_PSOR |= valor;
		break;
	case GPIOE:
		GPIOE_PSOR |= valor;
		break;
	default:
		break;
	}
}

void GPIO_clearPIN(GPIO_portNameType portName, uint8 pin) {
	uint32 clear;
	if (pin > BIT31) {
		return;
	}
	clear = (1 << pin);
	switch (portName) {
	case GPIOA:
		GPIOA_PCOR |= clear;
		break;
	case GPIOB:
		GPIOB_PCOR |= clear;
		break;
	case GPIOC:
		GPIOC_PCOR |= clear;
		break;
	case GPIOD:
		GPIOD_PCOR |= clear;
		break;
	case GPIOE:
		GPIOE_PCOR |= clear;
		break;

	}
}

void GPIO_tooglePIN(GPIO_portNameType portName, uint8 pin) {
	uint32 valor;
	valor = (1 << pin);
	switch (portName) {
	case GPIOA:
		GPIOA_PTOR |= valor;
		break;
	case GPIOB:
		GPIOB_PTOR |= valor;
		break;
	case GPIOC:
		GPIOC_PTOR |= valor;
		break;
	case GPIOD:
		GPIOD_PTOR |= valor;
		break;
	case GPIOE:
		GPIOE_PTOR |= valor;
		break;
	default:
		break;
	}
}

void GPIO_dataDirectionPORT(GPIO_portNameType portName, uint32 direction) {
	uint32 valor = (direction == GPIO_INPUT) ? 0 : 0xFFFFFFFF;
	switch (portName) {
	case GPIOA:
		GPIOA_PDDR = valor;
		break;
	case GPIOB:
		GPIOB_PDDR = valor;
		break;
	case GPIOC:
		GPIOC_PDDR = valor;
		break;
	case GPIOD:
		GPIOD_PDDR = valor;
		break;
	case GPIOE:
		GPIOE_PDDR = valor;
		break;
	default:
		break;
	}

}

void GPIO_dataDirectionPIN(GPIO_portNameType portName, uint8 State, uint8 pin) {
	uint32 valor = (1 << pin);
	if (State == GPIO_INPUT) {
		switch (portName) {
		case GPIOA:
			GPIOA_PDDR &= ~valor;
			break;
		case GPIOB:
			GPIOB_PDDR &= ~valor;
			break;
		case GPIOC:
			GPIOC_PDDR &= ~valor;
			break;
		case GPIOD:
			GPIOD_PDDR &= ~valor;
			break;
		case GPIOE:
			GPIOE_PDDR &= ~valor;
			break;
		default:
			break;
		}
	} else if (State == GPIO_OUTPUT) {
		switch (portName) {
		case GPIOA:
			GPIOA_PDDR |= valor;
			break;
		case GPIOB:
			GPIOB_PDDR |= valor;
			break;
		case GPIOC:
			GPIOC_PDDR |= valor;
			break;
		case GPIOD:
			GPIOD_PDDR |= valor;
			break;
		case GPIOE:
			GPIOE_PDDR |= valor;
			break;
		default:
			break;
		}
	}
}

void GPIO_writePIN(GPIO_portNameType portName, uint8 value, uint8 pin) {
	if (value == 0) {
		GPIO_clearPIN(portName, pin);
	} else if (value == 1) {
		GPIO_setPIN(portName, pin);
	}
}

