/*
 * Practica2.h
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_PRACTICA2_H_
#define SOURCES_PRACTICA2_H_

#include "DataTypeDefinitions.h"

/*!
 * \fn void practica2_init();
 * \brief This function calls the setup methos of all the modules used.
 */
void practica2_init();

/*!
 * 	\fn void practica2_init();
 * 	\brief This functions calls the evaluate methods of all the State machines in the current state
 */
void practica2_run();

#endif /* SOURCES_PRACTICA2_H_ */
