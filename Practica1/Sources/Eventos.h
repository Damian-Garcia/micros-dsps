/*
 * Eventos.h
 *
 *  Created on: Sep 23, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_EVENTOS_H_
#define SOURCES_EVENTOS_H_

#include "DataTypeDefinitions.h"

typedef enum {
	TECLADO_NOP,
	TECLADO_0,
	TECLADO_1,
	TECLADO_2,
	TECLADO_3,
	TECLADO_4,
	TECLADO_5,
	TECLADO_6,
	TECLADO_7,
	TECLADO_8,
	TECLADO_9,
	TECLADO_A,
	TECLADO_B,
	TECLADO_C,
	TECLADO_D,
	TECLADO_E,
	TECLADO_F,
	TECLADO_MAX_ENUM
} Evento_Teclado;

typedef enum{
	MAT_KEYBOARD_NOP,
	MAT_KEYBOARD_TICK,
	MAT_KEYBOARD_MAX_ENUM
} Evento_MatKeyboard;

typedef enum{
	MOTOR_NOP,
	MOTOR_ACTIVAR,
	MOTOR_SW2,
	MOTOR_TICK,
	MOTOR_MAX_ENUM
}Evento_Motor;

typedef enum{
	GENERADOR_NOP,
	GENERADOR_ACTIVAR,
	GENERADOR_SW3,
	GENERADOR_TICK,
	GENERADOR_MAX_ENUM
}Evento_Generador;

typedef enum{
	LED_EVENT_NOP,
	LED_EVENT_ACTIVAR,
	LED_TICK,
	LED_MAX_ENUM
}Evento_LED;

uint8 isKeyPressed();

void eventos_init();
Evento_MatKeyboard getEventoMatKeyboard(void);
Evento_Teclado getEventoTeclado(void);
Evento_Motor getEventoMotor(void);
Evento_Generador getEventoGenerador(void);
Evento_LED getEventoLEDError(void);
Evento_LED getEventoLEDInicio(void);

void addEventoMatKeyboard(Evento_MatKeyboard e);
void addEventoTeclado(Evento_Teclado e);
void addEventoMotor(Evento_Motor);
void addEventoGenerador(Evento_Generador);
void addEventoLEDError(Evento_LED);
void addEventoLEDInicio(Evento_LED);

#endif /* SOURCES_EVENTOS_H_ */
