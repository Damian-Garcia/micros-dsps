/*
 * FSM_Generador.c
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia y Erick Ortega Prudencio
 */
#include "FSM_Generador.h"
#define RESOLUTION 64
DAC_name dac = DAC_0;
static uint16_t genCounter;

static uint16_t squareValue[RESOLUTION]={[0 ... 31]=4095, [32 ... (RESOLUTION-1)]=0};
static uint16_t sineValue[RESOLUTION]={2048,2252,2454,2652,2843,3027,3201,3364,3514,3649,3768,3870,3954,4019,4064,4090,4095,4080,4044,3989,3914,3821,3710,3583,3441,3284,3116,2936,2748,2553,2353,2150,1946,1743,1543,1348,1160,980,812,655,513,386,275,182,107,52,16,1,6,32,77,142,226,328,447,582,732,895,1069,1253,1444,1642,1844,2048};
static uint16_t triangularValue[RESOLUTION]={0,131,263,395,528,660,792,924,1056,1188,1320,1452,1585,1717,1849,1981,2113,2245,2377,2509,2642,2774,2906,3038,3170,3302,3434,3566,3699,3831,3963,4095,4095,3963,3831,3699,3566,3434,3302,3170,3038,2906,2774,2642,2509,2377,2245,2113,1981,1849,1717,1585,1452,1320,1188,1056,924,792,660,528,395,263,131,0};
static uint16_t actualState;

static void FSM_Generator_StartProcess(uint16_t* nothing);
static void FSM_Generator_VoltageOutput(uint16_t* array);
static void FSM_Generator_NoAction(uint16_t * nothing);
static void FSM_Generator_IncrementCounter(uint16_t* counter);
static void FSM_Generator_SetLeds(LedValue* ledState);


static const FsmGenStateType FSMGenMoore[FSM_GN_MAX_ENUM]=
{
{squareValue,{LED_VALUE_OFF, LED_VALUE_OFF},FSM_Generator_NoAction,{FSM_GN_IDLE, FSM_GN_IDLE_ACTIVE, FSM_GN_IDLE, FSM_GN_IDLE}},//FSM_GN_IDLE
{squareValue,{LED_VALUE_OFF, LED_VALUE_OFF},FSM_Generator_StartProcess,{FSM_GN_IDLE_ACTIVE, FSM_GN_IDLE, FSM_GN_SQUARE, FSM_GN_IDLE_ACTIVE}},//FSM_GN_IDLE_ACTIVE
{squareValue,{LED_VALUE_ON, LED_VALUE_OFF},FSM_Generator_VoltageOutput,{FSM_GN_SQUARE, FSM_GN_IDLE,FSM_GN_SINE,FSM_GN_SQUARE_NEXT}},//FSM_GN_SQUARE
{ &genCounter,{LED_VALUE_ON, LED_VALUE_OFF},FSM_Generator_IncrementCounter,{ FSM_GN_SQUARE, FSM_GN_IDLE, FSM_GN_SINE, FSM_GN_SQUARE }},//FSM_GN_SQUARE_NEXT
{sineValue,{LED_VALUE_OFF, LED_VALUE_ON},FSM_Generator_VoltageOutput,{FSM_GN_SINE, FSM_GN_IDLE, FSM_GN_TRIANGLE,FSM_GN_SINE_NEXT}},//FSM_GN_SINE
{&genCounter,{LED_VALUE_OFF, LED_VALUE_ON},FSM_Generator_IncrementCounter,{ FSM_GN_SINE, FSM_GN_IDLE, FSM_GN_TRIANGLE, FSM_GN_SINE }},//FSM_GN_SINE_NEXT
{triangularValue,{LED_VALUE_OFF, LED_VALUE_OFF},FSM_Generator_VoltageOutput,{ FSM_GN_TRIANGLE, FSM_GN_IDLE, FSM_GN_SQUARE, FSM_GN_TRIANGLE_NEXT}},//FSM_GN_TRIANGLE
{&genCounter,{LED_VALUE_OFF, LED_VALUE_OFF},FSM_Generator_IncrementCounter,{ FSM_GN_TRIANGLE, FSM_GN_IDLE, FSM_GN_SQUARE, FSM_GN_TRIANGLE }}//FSM_GN_TRIANGLE_NEXT
};

void FSM_Generator_StartProcess(uint16_t* nothing)
{
	PIT_TimerType pit = PIT_0;
	DAC_clockGatingEnable(DAC_0);// Inicializamos el DAC 0
	PIT_ClockGatingEnable(); //Inicializamos el clock gating del PIT
	float sysClock = 21000000;
	float period = 0.003125;
	PIT_setTimeDelay(pit, sysClock, period);
	DAC_ConfigurationC0 dacConfigC0 = { dac, DAC_REF2, DAC_SOFTWARETRIGGER, DAC_HIPWR, DAC_DISABLE, DAC_DISABLE};
	DAC_ConfigurationC1 dacConfigC1 = { dac, DAC_NORMAL, DAC_DISABLE, DAC_DISABLE };
	DAC_ControlRegisterZeroConfig(dacConfigC0);
	DAC_ControlRegisterOneConfig(dacConfigC1);
	DAC_setDACx_DATnValue(dac,0,0);
	DAC_EnableDACx(dac);
	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE};
	PIT_TCRConfig pitTCRConfig= { pit, NOTCHAINED, PIT_ENABLE, PIT_ENABLE};
	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_CH0_IRQ, PRIORITY_1);
	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
	PIT_configureTimmerControlRegister(pitTCRConfig);
}

void FSM_Generator_VoltageOutput(uint16_t* array)
{
	DAC_setDACx_DATnValue(dac,0,array[genCounter]);

}

void FSM_Generator_NoAction(uint16_t * nothing)
{
	return;
}

void FSM_Generator_IncrementCounter(uint16_t* counter)
{
	genCounter = (genCounter>=(RESOLUTION-1))? 0:(genCounter+1);
}

void FSM_Generator_Config()
{
	genCounter = 0;
	actualState = FSM_GN_IDLE;// Set actual state to IDLE
	sw3InterruptInitialize();
}

void FSM_GeneratorEvaluate(Evento_Generador event)//FSM_GeneratorLEDState* ledState
{
	if((actualState != FSM_GN_IDLE))// && (actualState!=FSM_GN_IDLE_ACTIVE)
	{
		setRgbLeds(GREEN);
	}
	else
	{
		clearRgbLeds(GREEN);
	}
	uint16_t* value = (FSMGenMoore[actualState].actionArgument);
	FSMGenMoore[actualState].Accion(value);
	LedValue ledState[2]={FSMGenMoore[actualState].ledStates[0],FSMGenMoore[actualState].ledStates[1]};
	FSM_Generator_SetLeds(ledState);
	actualState = FSMGenMoore[actualState].nextEstate[event];
}

void FSM_Generator_SetLeds(LedValue* ledState)
{
	setOutputLedValues(FSM_GEN_OUT_LEDS, ledState[0], ledState[1]);
}
