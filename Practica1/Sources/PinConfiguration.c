/*
 * PinConfiguration.c
 *
 *  Created on: Sep 25, 2016
 *      Author: operi
 */

#ifndef SOURCES_PINCONFIGURATION_C_
#define SOURCES_PINCONFIGURATION_C_
#include "PinConfiguration.h"

void sw2InterruptInitialize()
{
	GPIO_clockGating(GPIOC);
	GPIO_pinControlRegisterType portC_PCR6 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_dataDirectionPIN(GPIOC,GPIO_INPUT,6);
	GPIO_pinControlRegister(GPIOC, 6, &portC_PCR6);
	NVIC_enableInterruptAndPriotity(PORTC_IRQ, PRIORITY_4);
	EnableInterrupts;
}

/*
 * Inicializacion del switch 3 para poder detectarlo
 */
void sw3InterruptInitialize()
{
	GPIO_clockGating(GPIOA);
	GPIO_pinControlRegisterType portA_PCR4 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_dataDirectionPIN(GPIOA,GPIO_INPUT,4);
	GPIO_pinControlRegister(GPIOA, 4, &portA_PCR4);
	NVIC_enableInterruptAndPriotity(PORTA_IRQ, PRIORITY_4);
	EnableInterrupts;
}

void portDInterruptInitialize()//Puede que no sea necesaria para nada
{
	GPIO_clockGating(GPIOD);
	NVIC_enableInterruptAndPriotity(PORTD_IRQ, PRIORITY_2);
	EnableInterrupts;
}

/*
 * This function sets the specified pins as outputs.
 */
void rgbLedsInitialization()
{
	GPIO_clockGating(RGB_GREEN_PORT);
	GPIO_pinControlRegisterType pcrRGBgreen = GPIO_MUX1;
	GPIO_pinControlRegister(RGB_GREEN_PORT, RGB_GREEN_PIN_LED, &pcrRGBgreen);
	GPIO_setPIN(RGB_GREEN_PORT, RGB_GREEN_PIN_LED);
	GPIO_dataDirectionPIN(RGB_GREEN_PORT,GPIO_OUTPUT,RGB_GREEN_PIN_LED);

	GPIO_clockGating(RGB_RED_PORT);
	GPIO_pinControlRegisterType pcrRGBred = GPIO_MUX1;
	GPIO_pinControlRegister(RGB_RED_PORT, RGB_RED_PIN_LED, &pcrRGBred);
	GPIO_setPIN(RGB_RED_PORT, RGB_RED_PIN_LED);
	GPIO_dataDirectionPIN(RGB_RED_PORT,GPIO_OUTPUT,RGB_RED_PIN_LED);

	GPIO_clockGating(RGB_BLUE_PORT);
	GPIO_pinControlRegisterType pcrRGBblue = GPIO_MUX1;
	GPIO_pinControlRegister(RGB_BLUE_PORT, RGB_BLUE_PIN_LED, &pcrRGBblue);
	GPIO_setPIN(RGB_BLUE_PORT, RGB_BLUE_PIN_LED);
	GPIO_dataDirectionPIN(RGB_BLUE_PORT,GPIO_OUTPUT,RGB_BLUE_PIN_LED);
}

void setRgbLeds(RGB_LED_Color color)
{
	switch(color)
	{
	case RED:
		GPIO_clearPIN(RGB_RED_PORT, RGB_RED_PIN_LED);
		break;
	case BLUE:
		GPIO_clearPIN(RGB_BLUE_PORT, RGB_BLUE_PIN_LED);
		break;
	case GREEN:
		GPIO_clearPIN(RGB_GREEN_PORT, RGB_GREEN_PIN_LED);
		break;
	default:
		break;
	}
}

void clearRgbLeds(RGB_LED_Color color)
{
	switch(color)
	{
	case RED:
		GPIO_setPIN(RGB_RED_PORT, RGB_RED_PIN_LED);
		break;
	case BLUE:
		GPIO_setPIN(RGB_BLUE_PORT, RGB_BLUE_PIN_LED);
		break;
	case GREEN:
		GPIO_setPIN(RGB_GREEN_PORT, RGB_GREEN_PIN_LED);
		break;
	default:
		break;
	}
}

void matricialKeyboardPinsInitialization()
{
	GPIO_clockGating(FSM_MAT_PORT_C0);
	GPIO_pinControlRegisterType pcrFSM_MatC0 = GPIO_MUX1;
	GPIO_pinControlRegister(FSM_MAT_PORT_C0, FSM_MAT_PIN_C0, &pcrFSM_MatC0);
	GPIO_setPIN(FSM_MAT_PORT_C0, FSM_MAT_PIN_C0);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_C0,GPIO_OUTPUT,FSM_MAT_PIN_C0);

	GPIO_clockGating(FSM_MAT_PORT_C1);
	GPIO_pinControlRegisterType pcrFSM_MatC1 = GPIO_MUX1;
	GPIO_pinControlRegister(FSM_MAT_PORT_C1, FSM_MAT_PIN_C1, &pcrFSM_MatC1);
	GPIO_setPIN(FSM_MAT_PORT_C1, FSM_MAT_PIN_C1);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_C1,GPIO_OUTPUT,FSM_MAT_PIN_C1);

	GPIO_clockGating(FSM_MAT_PORT_C2);
	GPIO_pinControlRegisterType pcrFSM_MatC2 = GPIO_MUX1;
	GPIO_pinControlRegister(FSM_MAT_PORT_C2, FSM_MAT_PIN_C2, &pcrFSM_MatC2);
	GPIO_setPIN(FSM_MAT_PORT_C2, FSM_MAT_PIN_C2);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_C2,GPIO_OUTPUT,FSM_MAT_PIN_C2);

	GPIO_clockGating(FSM_MAT_PORT_C3);
	GPIO_pinControlRegisterType pcrFSM_MatC3 = GPIO_MUX1;
	GPIO_pinControlRegister(FSM_MAT_PORT_C3, FSM_MAT_PIN_C3, &pcrFSM_MatC3);
	GPIO_clearPIN(FSM_MAT_PORT_C3, FSM_MAT_PIN_C3);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_C3,GPIO_OUTPUT,FSM_MAT_PIN_C3);

	GPIO_clockGating(FSM_MAT_PORT_R0);
	GPIO_pinControlRegisterType pcrFSM_MatR0 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_MAT_PORT_R0, FSM_MAT_PIN_R0, &pcrFSM_MatR0);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_R0,GPIO_INPUT,FSM_MAT_PIN_R0);

	GPIO_clockGating(FSM_MAT_PORT_R1);
	GPIO_pinControlRegisterType pcrFSM_MatR1 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_MAT_PORT_R1, FSM_MAT_PIN_R1, &pcrFSM_MatR1);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_R1,GPIO_INPUT,FSM_MAT_PIN_R1);

	GPIO_clockGating(FSM_MAT_PORT_R2);
	GPIO_pinControlRegisterType pcrFSM_MatR2 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_MAT_PORT_R2, FSM_MAT_PIN_R2, &pcrFSM_MatR2);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_R2,GPIO_INPUT,FSM_MAT_PIN_R2);

	GPIO_clockGating(FSM_MAT_PORT_R3);
	GPIO_pinControlRegisterType pcrFSM_MatR3 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_MAT_PORT_R3, FSM_MAT_PIN_R3, &pcrFSM_MatR3);
	GPIO_dataDirectionPIN(FSM_MAT_PORT_R3,GPIO_INPUT,FSM_MAT_PIN_R3);

	NVIC_enableInterruptAndPriotity(PORTD_IRQ, PRIORITY_2);
	EnableInterrupts;
}

void tecladoOutputPinsInitialization()
{
	GPIO_clockGating(FSM_TECLADO_PORT_LEDINICIO);
	GPIO_pinControlRegisterType pcrFSM_TecladoLedInicio = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_TECLADO_PORT_LEDINICIO, FSM_TECLADO_PIN_LEDINICIO, &pcrFSM_TecladoLedInicio);
	GPIO_setPIN(FSM_TECLADO_PORT_LEDINICIO, FSM_TECLADO_PIN_LEDINICIO);
	GPIO_dataDirectionPIN(FSM_TECLADO_PORT_LEDINICIO,GPIO_OUTPUT,FSM_TECLADO_PIN_LEDINICIO);

	GPIO_clockGating(FSM_TECLADO_PORT_LEDERROR);
	GPIO_pinControlRegisterType pcrFSM_TecladoLedError = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_TECLADO_PORT_LEDERROR, FSM_TECLADO_PIN_LEDERROR, &pcrFSM_TecladoLedError);
	GPIO_setPIN(FSM_TECLADO_PORT_LEDERROR, FSM_TECLADO_PIN_LEDERROR);
	GPIO_dataDirectionPIN(FSM_TECLADO_PORT_LEDERROR,GPIO_OUTPUT,FSM_TECLADO_PIN_LEDERROR);
}
void outputPinsInitialization()
{
	/*
	 * Configuration for the generator led pins
	 */
	GPIO_clockGating(FSM_GEN_PORT_LED0);
	GPIO_pinControlRegisterType pcrFSM_GenLed0 = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_GEN_PORT_LED0, FSM_GEN_PIN_LED0, &pcrFSM_GenLed0);
	GPIO_setPIN(FSM_GEN_PORT_LED0, FSM_GEN_PIN_LED0);
	GPIO_dataDirectionPIN(FSM_GEN_PORT_LED0,GPIO_OUTPUT,FSM_GEN_PIN_LED0);

	GPIO_clockGating(FSM_GEN_PORT_LED1);
	GPIO_pinControlRegisterType pcrFSM_GenLed1 = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_GEN_PORT_LED1, FSM_GEN_PIN_LED1, &pcrFSM_GenLed1);
	GPIO_setPIN(FSM_GEN_PORT_LED1, FSM_GEN_PIN_LED1);
	GPIO_dataDirectionPIN(FSM_GEN_PORT_LED1,GPIO_OUTPUT,FSM_GEN_PIN_LED1);
/*
 * Initialization of the motor pins
 */
	GPIO_clockGating(FSM_MOT_PORT_LED0);
	GPIO_pinControlRegisterType pcrFSM_MotLed0 = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_MOT_PORT_LED0, FSM_MOT_PIN_LED0, &pcrFSM_MotLed0);
	GPIO_setPIN(FSM_MOT_PORT_LED0, FSM_MOT_PIN_LED0);
	GPIO_dataDirectionPIN(FSM_MOT_PORT_LED0,GPIO_OUTPUT,FSM_MOT_PIN_LED0);

	GPIO_clockGating(FSM_MOT_PORT_LED1);
	GPIO_pinControlRegisterType pcrFSM_MotLed1 = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_MOT_PORT_LED1, FSM_MOT_PIN_LED1, &pcrFSM_MotLed1);
	GPIO_setPIN(FSM_MOT_PORT_LED0, FSM_MOT_PIN_LED1);
	GPIO_dataDirectionPIN(FSM_MOT_PORT_LED1,GPIO_OUTPUT,FSM_MOT_PIN_LED1);

	GPIO_clockGating(FSM_MOT_PORT_OUT);
	GPIO_pinControlRegisterType pcrFSM_MotOut = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_MOT_PORT_OUT, FSM_MOT_PIN_OUT, &pcrFSM_MotOut);
	GPIO_clearPIN(FSM_MOT_PORT_OUT, FSM_MOT_PIN_OUT); // El motor es activo en alto
	GPIO_dataDirectionPIN(FSM_MOT_PORT_OUT,GPIO_OUTPUT,FSM_MOT_PIN_OUT);
}

void setTecladoLedError(LedValue ledErrorVal)
{
	GPIO_writePIN(FSM_TECLADO_PORT_LEDERROR, ledErrorVal, FSM_TECLADO_PIN_LEDERROR);
}

void setTecladoLedInicio(LedValue ledInicioVal)
{
	GPIO_writePIN(FSM_TECLADO_PORT_LEDINICIO, ledInicioVal, FSM_TECLADO_PIN_LEDINICIO);
}

void setOutputLedValues(FSM_OutputLed outputLeds, LedValue led0val, LedValue led1val)
{
	switch(outputLeds)
	{
	case FSM_GEN_OUT_LEDS:
		GPIO_writePIN(FSM_GEN_PORT_LED0, led0val, FSM_GEN_PIN_LED0);
		GPIO_writePIN(FSM_GEN_PORT_LED0, led1val, FSM_GEN_PIN_LED1);
		break;
	case FSM_MOTOR_OUT_LEDS:
		GPIO_writePIN(FSM_MOT_PORT_LED0, led0val, FSM_MOT_PIN_LED0);
		GPIO_writePIN(FSM_MOT_PORT_LED0, led1val, FSM_MOT_PIN_LED1);
		break;
	default:
		break;
	}
}

void setOutputMotorValue(FSM_MotorOutputValue value)
{
	GPIO_writePIN(FSM_MOT_PORT_OUT, value, FSM_MOT_PIN_OUT);
}

#endif /* SOURCES_PINCONFIGURATION_C_ */
