/*
 * Eventos.c
 *
 *  Created on: Sep 23, 2016
 *      Author: Damian Garcia
 */

#include "Eventos.h"

struct {
	uint8 Iniciar :1;
	uint8 SW2 :1;
	uint8 Tick :1;
} Eventos_Motor;

struct {
	uint8 Iniciar :1;
	uint8 SW3 :1;
	uint8 Tick :1;
} Eventos_Generador;

struct {
	uint8 Activar :1;
	uint8 Tick :1;
} Eventos_LEDError;

struct {
	uint8 Activar :1;
	uint8 Tick :1;
} Eventos_LEDInicio;

struct {
	uint8 Tick:1;
} Eventos_MATKeyboard;

struct {
	uint8 teclaPresionada:1;
	Evento_Teclado tecla:5;
} Eventos_Teclado;

void eventos_init(void) {
	Eventos_Motor.Iniciar = 0;
	Eventos_Motor.SW2 = 0;
	Eventos_Motor.Tick = 0;

	Eventos_Generador.Iniciar = 0;
	Eventos_Generador.SW3 = 0;
	Eventos_Generador.Tick = 0;

	Eventos_MATKeyboard.Tick = 0;
}

uint8 isKeyPressed(){
	if(Eventos_Teclado.teclaPresionada){
		return TRUE;
	}else{
		return FALSE;
	}
};

Evento_Teclado getEventoTeclado(void) {
	if(Eventos_Teclado.teclaPresionada){
		Eventos_Teclado.teclaPresionada = 0;
		return Eventos_Teclado.tecla;
	}
	return TECLADO_NOP;

}
Evento_MatKeyboard getEventoMatKeyboard(void) {
	if (Eventos_MATKeyboard.Tick) {
		Eventos_MATKeyboard.Tick = 0;
		return MAT_KEYBOARD_TICK;
	} else {
		return MAT_KEYBOARD_NOP;
	}
}
Evento_Motor getEventoMotor(void) {
	if (Eventos_Motor.Iniciar) {
		Eventos_Motor.Iniciar = 0;
		return MOTOR_ACTIVAR;
	} else if (Eventos_Motor.SW2) {
		Eventos_Motor.SW2 = 0;
		return MOTOR_SW2;
	} else if (Eventos_Motor.Tick) {
		Eventos_Motor.Tick = 0;
		return MOTOR_TICK;
	} else {
		return MOTOR_NOP;
	}
}

Evento_Generador getEventoGenerador(void) {
	if (Eventos_Generador.Iniciar) {
		Eventos_Generador.Iniciar = 0;
		return GENERADOR_ACTIVAR;
	} else if (Eventos_Generador.SW3) {
		Eventos_Generador.SW3 = 0;
		return GENERADOR_SW3;
	} else if (Eventos_Generador.Tick) {
		Eventos_Generador.Tick = 0;
		return GENERADOR_TICK;
	} else {
		return GENERADOR_NOP;
	}
}

Evento_LED getEventoLEDError(void) {
	if (Eventos_LEDError.Activar == 1) {
		Eventos_LEDError.Activar = 0;
		return LED_EVENT_ACTIVAR;
	} else if (Eventos_LEDError.Tick == 1) {
		Eventos_LEDError.Tick = 0;
		return LED_TICK;
	} else {
		return LED_EVENT_NOP;
	}
}

Evento_LED getEventoLEDInicio(void) {
	if (Eventos_LEDInicio.Activar == 1) {
		Eventos_LEDInicio.Activar = 0;
		return LED_EVENT_ACTIVAR;
	} else if (Eventos_LEDInicio.Tick == 1) {
		Eventos_LEDInicio.Tick = 0;
		return LED_TICK;
	} else {
		return LED_EVENT_NOP;
	}
}

void addEventoMatKeyboard(Evento_MatKeyboard e) {
	if (e == MAT_KEYBOARD_TICK) {
		Eventos_MATKeyboard.Tick = 1;
	}
}

void addEventoTeclado(Evento_Teclado e) {
	Eventos_Teclado.teclaPresionada = 1;
	Eventos_Teclado.tecla = e;
}

void addEventoMotor(Evento_Motor e) {
	if (e == MOTOR_ACTIVAR) {
		Eventos_Motor.Iniciar = 1;
	} else if (e == MOTOR_SW2) {
		Eventos_Motor.SW2 = 1;
	} else if (e == MOTOR_TICK) {
		Eventos_Motor.Tick = 1;
	}
}

void addEventoGenerador(Evento_Generador e) {
	if (e == GENERADOR_ACTIVAR) {
		Eventos_Generador.Iniciar = 1;
	} else if (e == GENERADOR_SW3) {
		Eventos_Generador.SW3 = 1;
	} else if (e == GENERADOR_TICK) {
		Eventos_Generador.Tick = 1;
	}
}

void addEventoLEDError(Evento_LED e) {
	if (e == LED_EVENT_ACTIVAR) {
		Eventos_LEDError.Activar = 1;
	} else {
		Eventos_LEDError.Tick = 1;
	}
}

void addEventoLEDInicio(Evento_LED e) {
	if (e == LED_EVENT_ACTIVAR) {
		Eventos_LEDInicio.Activar = 1;
	} else {
		Eventos_LEDInicio.Tick = 1;
	}
}
