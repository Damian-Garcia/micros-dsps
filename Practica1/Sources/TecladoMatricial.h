/*
 * TecladoMatricial.h
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_TECLADOMATRICIAL_H_
#define SOURCES_TECLADOMATRICIAL_H_

#include "Eventos.h"
#include "DataTypeDefinitions.h"
#include "MK64F12.h"
#include "PIT.h"
#include "Eventos.h"
#include "NVIC.h"
#include "GPIO.h"
#include "PinConfiguration.h"
#define PIT_MAT_KEYBOARD PIT_2

typedef enum
{
	C3_COLD,
	C2_COLD,
	C1_COLD,
	C0_COLD,
	FSM_MATKEYBOAR_MAX_ENUM
}FSM_MATKEYBOARD_STATE;

typedef struct
{
	FSM_MATKEYBOARD_STATE state;
	void(*fptrAction)(FSM_MATKEYBOARD_STATE state);//Receive the argument that is going to modify
	FSM_MATKEYBOARD_STATE nextState[2];
}FSM_MooreMatKeyboard;

void FSM_MatKeyboardInitialize();
void FSM_MatKeyboardEvaluate(Evento_MatKeyboard event);

Evento_Teclado decoData(uint32_t data);

#endif /* SOURCES_TECLADOMATRICIAL_H_ */
