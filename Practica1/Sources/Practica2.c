#include "Practica2.h"
#include "FSM_Motor.h"
#include "FSM_Teclado.h"
#include "Eventos.h"
#include "FSM_LED_Error.h"
#include "FSM_LED_Inicio.h"
#include "FSM_Generador.h"
#include "PinConfiguration.h"
#include "FSM_Motor.h"
#include "TecladoMatricial.h"


void practica2_init(void){
	eventos_init();
	rgbLedsInitialization();
	FSM_MatKeyboardInitialize();
	tecladoOutputPinsInitialization();
	outputPinsInitialization();
	FSM_LED_Error_init();
	FSM_LED_Inicio_init();
	FSM_Teclado_init();
	FSM_Generator_Config();
	FSM_MotorInitialize();
}

void practica2_run(){
	Evento_Teclado nuevoEventoTeclado;
	Evento_Generador nuevoEventoGenerador;
	Evento_Motor nuevoEventoMotor;
	Evento_LED nuevoEventoLEDError;
	Evento_LED nuevoEventoLEDInicio;

	FSM_MatKeyboardEvaluate(getEventoMatKeyboard());

	nuevoEventoTeclado = getEventoTeclado();
	FSM_Teclado_Evaluar(nuevoEventoTeclado);

	nuevoEventoLEDError = getEventoLEDError();
	FSM_LED_Error_evaluar(nuevoEventoLEDError);

	nuevoEventoLEDInicio = getEventoLEDInicio();
	FSM_LED_Inicio_evaluar(nuevoEventoLEDInicio);

	nuevoEventoMotor = getEventoMotor();
	FSM_MotorEvaluate(nuevoEventoMotor);

	nuevoEventoGenerador = getEventoGenerador();
	FSM_GeneratorEvaluate(nuevoEventoGenerador);
}
