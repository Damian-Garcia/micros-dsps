/*
 * Teclado_FSM.h
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_FSM_MOTOR_
#define SOURCES_FSM_MOTOR_

#include "DataTypeDefinitions.h"
#include "MK64F12.h"
#include "FSM_Motor.h"
#include "PIT.h"
#include "Eventos.h"
#include "NVIC.h"
#include "GPIO.h"
#include "PinConfiguration.h"
/*
 * Structure of a state on the machine. Each state knows the next state (depending on the input) and also
 * has an action to perform in the current state.
 */

typedef enum
{
	FSM_MOT_IDLE,
	FSM_MOT_IDLE_ACTIVE,
	FSM_MOT_ACTIVE_1,
	FSM_MOT_WAIT_1,
	FSM_MOT_ACTIVE_2,
	FSM_MOT_WAIT_2,
	FSM_MOT_ACTIVE_3,
	FSM_MOT_WAIT_3,
	FSM_MOT_MAX_ENUM
}FSM_MotorState;

typedef struct
{
	FSM_MotorOutputValue actionArgument; //Indicates which is going to be the paramter of the action
	uint16_t delay;
	LedValue ledStates[2];
	void(*fptrAction)(uint16_t, FSM_MotorOutputValue);//Receive the argument that is going to modify
	FSM_MotorState nextEstate[4];
}FsmMotStateType;


void FSM_MotorInitialize();
void FSM_MotorEvaluate(Evento_Motor event);

#endif /* SOURCES_FSM_MOTOR_ */
