/*
 * FSM_LED_Inicio.c
 *
 *  Created on: Sep 25, 2016
 *      Author: Damian Garcia
 */

#include "FSM_LED_Inicio.h"
#include "NVIC.h"
#include "DataTypeDefinitions.h"
#include "PinConfiguration.h"
#include "PIT.h"

Estado_FSM_LED_Inicio actualInicio;

void LED_Inicio_idle();
void LED_Inicio_activar1();
void LED_Inicio_esperar1();
void LED_Inicio_apagar1();
void LED_Inicio_esperar2();
void LED_Inicio_activar2();
void LED_Inicio_esperar3();
void LED_Inicio_apagar2();


static const Estado_FSM_LED_Inicio FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_MAX_ENUM][LED_MAX_ENUM] = {
	{FSM_LED_INICIO_IDLE, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_IDLE},			//Idle
	{FSM_LED_INICIO_ESPERAR1, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_ESPERAR1}, 	//Activar1
	{FSM_LED_INICIO_ESPERAR1, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_APAGAR1},		//Esperar1
	{FSM_LED_INICIO_ESPERAR2, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_ESPERAR2},	// Apagar1
	{FSM_LED_INICIO_ESPERAR2, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_ACTIVAR2},	// Esperar2
	{FSM_LED_INICIO_ESPERAR3, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_ESPERAR3},	// Activar2
	{FSM_LED_INICIO_ESPERAR3, FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_APAGAR2},	// Esperar3
	{FSM_LED_INICIO_IDLE,	FSM_LED_INICIO_ACTIVAR1, FSM_LED_INICIO_IDLE},	// Apagar2
};

static const FSM_LED_Inicio_Estado FSM_LED_Inicio_Tabla[FSM_LED_INICIO_MAX_ENUM] = {
	{LED_Inicio_idle, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_IDLE]}, // Idle
	{LED_Inicio_activar1, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_ACTIVAR1]}, // Activar1
	{LED_Inicio_esperar1, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_ESPERAR1]}, // Esperar1
	{LED_Inicio_apagar1, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_APAGAR1]}, // Apagar1
	{LED_Inicio_esperar2, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_ESPERAR2]}, // Esperar2
	{LED_Inicio_activar2, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_ACTIVAR2]}, // Activar2
	{LED_Inicio_esperar3, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_ESPERAR3]}, // Esperar3
	{LED_Inicio_apagar2, FSM_LED_Inicio_Transiciones[FSM_LED_INICIO_APAGAR2]}, // Apagar2
};




void FSM_LED_Inicio_init(void){
	actualInicio= FSM_LED_INICIO_IDLE;

	PIT_TimerType pit = PIT_3;
	PIT_ClockGatingEnable(); //Inicializamos el clock gating del PIT
	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE};
	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_CH3_IRQ, PRIORITY_5);
	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
}

void FSM_LED_Inicio_evaluar(Evento_LED e){
	if(e == LED_EVENT_ACTIVAR ){
		int a = 0;
	}
	actualInicio = FSM_LED_Inicio_Tabla[actualInicio].nextList[e];
	FSM_LED_Inicio_Tabla[actualInicio].a();
}

void LED_Inicio_idle(){

}

void LED_Inicio_activar1(){
	 setTecladoLedInicio(LED_VALUE_ON);
	float sysClock = 21000000;
	PIT_delay(PIT_3,sysClock, 1);
}

 void LED_Inicio_esperar1(){

}

void LED_Inicio_apagar1(){
	 setTecladoLedInicio(LED_VALUE_OFF);
		float sysClock = 21000000;
		PIT_delay(PIT_3,sysClock, 1);
}

void LED_Inicio_esperar2(){

}

void LED_Inicio_activar2(){
	 setTecladoLedInicio(LED_VALUE_ON);
	float sysClock = 21000000;
	PIT_delay(PIT_3,sysClock, 1);
}

void LED_Inicio_esperar3(){

}

void LED_Inicio_apagar2(){
	 setTecladoLedInicio(LED_VALUE_OFF);
}
