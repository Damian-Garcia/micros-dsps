/*
 * TecladoMatricial.c
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#include "TecladoMatricial.h"

static uint16_t actualState;
void FSM_MatKeyboardAction();


const FSM_MooreMatKeyboard FSM_MatMoore[FSM_MATKEYBOAR_MAX_ENUM]=
 {
	{C3_COLD, 0, {C3_COLD, C2_COLD}},//FSM_GN_IDLE
	{C2_COLD, 0, {C2_COLD, C1_COLD}},//FSM_GN_IDLE
	{C1_COLD, 0, {C1_COLD, C0_COLD}},//FSM_GN_IDLE
 	{C0_COLD, 0, {C0_COLD, C3_COLD}},//FSM_GN_IDLE
 };

void FSM_MatKeyboardInitialize()
{
	/*
	 * Inicializacion del pit que se encargara de la realizacion del barrido por siempre
	 * Tambien llamara a la funciond e inicializacion de los pines de salida
	 */
	matricialKeyboardPinsInitialization();
	actualState = C3_COLD;
	PIT_TimerType pit = PIT_MAT_KEYBOARD;
	PIT_ClockGatingEnable(); //Inicializamos el clock gating del PIT
	float sysClock = 21000000;
	float period = 0.05;
	PIT_setTimeDelay(pit, sysClock, period);
	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE};
	PIT_TCRConfig pitTCRConfig= { pit, NOTCHAINED, PIT_ENABLE, PIT_ENABLE};
	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_CH2_IRQ, PRIORITY_2);
	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
	PIT_configureTimmerControlRegister(pitTCRConfig);
	GPIO_setPIN(FSM_MAT_PORT_C2, FSM_MAT_PIN_C2);
	GPIO_clearPIN(FSM_MAT_PORT_C3, FSM_MAT_PIN_C3);
	GPIO_setPIN(FSM_MAT_PORT_C1, FSM_MAT_PIN_C1);
	GPIO_setPIN(FSM_MAT_PORT_C0, FSM_MAT_PIN_C0);
}

void FSM_MatKeyboardAction()
{
	switch(actualState)
	{
	case C3_COLD:
		GPIOC_PDOR = (GPIOC_PDOR & (~0x1))|0x8;
		break;
	case C2_COLD:
		GPIOC_PDOR = (GPIOC_PDOR & (~0x2))|0x1;
		break;
	case C1_COLD:
		GPIOC_PDOR = (GPIOC_PDOR & (~0x4))|0x2;
		break;
	case C0_COLD:
		GPIOC_PDOR = (GPIOC_PDOR & (~0x8))|0x4;
		break;
	}
}

void FSM_MatKeyboardEvaluate(Evento_MatKeyboard event)
{
	FSM_MatKeyboardAction();
	actualState = FSM_MatMoore[actualState].nextState[event];
	/*
	 * En el puerto donde se decida colocar la interrupcion del teclado matricial
	 * se realizara la lectura del dato completo
	 */
}

Evento_Teclado decoData(uint32_t data) {
	switch (data) {
	case 0x77: //01110111
		return TECLADO_1; //0x1;
		break;
	case 0xb7: //10110111
		return TECLADO_4; //0x4;
		break;
	case 0xd7:
		return TECLADO_7; //0x7;
		break;
	case 0xe7:
		return TECLADO_E; //0xe;
		break;
	case 0x7b: //01110111
		return TECLADO_2; //0x2;
		break;
	case 0xbb: //10110111
		return TECLADO_5; //0x5;
		break;
	case 0xdb:
		return TECLADO_8; //0x8;
		break;
	case 0xeb:
		return TECLADO_0; //0x0;
		break;
	case 0x7d: //01110111
		return TECLADO_3; //0x3;
		break;
	case 0xbd: //10110111
		return TECLADO_6; //0x6;
		break;
	case 0xdd:
		return TECLADO_9; //0x9;
		break;
	case 0xed:
		return TECLADO_F; //0xf;
		break;
	case 0x7e: //01110111 /
		return TECLADO_A; //0xA;
		break;
	case 0xbe: //10110111
		return TECLADO_B; //0xB;
		break;
	case 0xde:
		return TECLADO_C; //0xC;
		break;
	case 0xee:
		return TECLADO_D; //0xD;
		break;
	default:
		return TECLADO_NOP;
		break;
	}
}
