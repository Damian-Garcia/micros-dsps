/*
 * PinConfiguration.h
 *
 *  Created on: Sep 25, 2016
 *      Author: operi
 */

#ifndef SOURCES_PINCONFIGURATION_H_
#define SOURCES_PINCONFIGURATION_H_
#include "MK64F12.h"
#include "NVIC.h"
#include "DataTypeDefinitions.h"
#include "GPIO.h"
#include "PIT.h"

#define FSM_TECLADO_PORT_LEDINICIO GPIOB
#define FSM_TECLADO_PIN_LEDINICIO 18
#define FSM_TECLADO_PORT_LEDERROR GPIOB
#define FSM_TECLADO_PIN_LEDERROR 19

#define RGB_GREEN_PORT GPIOE
#define RGB_GREEN_PIN_LED 26
#define RGB_RED_PORT GPIOB
#define RGB_RED_PIN_LED 22
#define RGB_BLUE_PORT GPIOB
#define RGB_BLUE_PIN_LED 21


#define FSM_GEN_PORT_LED0 GPIOE
#define FSM_GEN_PIN_LED0 24
#define FSM_GEN_PORT_LED1 GPIOE
#define FSM_GEN_PIN_LED1 25

#define FSM_MOT_PORT_LED0 GPIOB
#define FSM_MOT_PIN_LED0 3
#define FSM_MOT_PORT_LED1 GPIOB
#define FSM_MOT_PIN_LED1 10
#define FSM_MOT_PORT_OUT GPIOB
#define FSM_MOT_PIN_OUT 11

#define FSM_MAT_PORT_C0 GPIOC
#define FSM_MAT_PIN_C0 3
#define FSM_MAT_PORT_C1 GPIOC
#define FSM_MAT_PIN_C1 2
#define FSM_MAT_PORT_C2 GPIOC
#define FSM_MAT_PIN_C2 1
#define FSM_MAT_PORT_C3 GPIOC
#define FSM_MAT_PIN_C3 0

#define FSM_MAT_PORT_R0 GPIOD
#define FSM_MAT_PIN_R0 3
#define FSM_MAT_PORT_R1 GPIOD
#define FSM_MAT_PIN_R1 2
#define FSM_MAT_PORT_R2 GPIOD
#define FSM_MAT_PIN_R2 1
#define FSM_MAT_PORT_R3 GPIOD
#define FSM_MAT_PIN_R3 0

typedef enum
{
	RED,
	GREEN,
	BLUE
}RGB_LED_Color;

typedef enum
{
	FSM_GEN_OUT_LEDS,
	FSM_MOTOR_OUT_LEDS
}FSM_OutputLed;

typedef enum
{
	MOTOR_DISABLED,
	MOTOR_ENABLED
}FSM_MotorOutputValue;

typedef enum
{
	LED_VALUE_ON,
	LED_VALUE_OFF,
	LED_VALUE_MAX_ENUM
}LedValue;

void rgbLedsInitialization();
void clearRgbLeds(RGB_LED_Color color);
void setRgbLeds(RGB_LED_Color color);
void sw2InterruptInitialize();
void sw3InterruptInitialize();
void tecladoOutputPinsInitialization();
void setTecladoLedInicio(LedValue ledInicioVal);
void setTecladoLedError(LedValue ledErrorVal);
void matricialKeyboardPinsInitialization();
void outputPinsInitialization();
void setOutputLedValues(FSM_OutputLed outputLeds, LedValue led0val, LedValue led1val);
void setOutputMotorValue(FSM_MotorOutputValue value);

#endif /* SOURCES_PINCONFIGURATION_H_ */
