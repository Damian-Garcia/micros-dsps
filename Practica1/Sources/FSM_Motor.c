#include "FSM_Motor.h"
#define PIT_MOTOR PIT_1
#define PIT_MOTOR_IRQ PIT_CH1_IRQ

typedef uint8_t state_ExecutionFlags;
static state_ExecutionFlags executionFlags[8] = {0,0,0,0,0,0,0,0};
static uint16_t actualState;
static uint16_t beforeState;

static void FSM_Motor_StartProcess(uint16_t delay, FSM_MotorOutputValue value);
static void FSM_Motor_PitConfigDelay(uint16_t delay);
static void FSM_Motor_NoAction(uint16_t delay, FSM_MotorOutputValue value);
static void FSM_Motor_Action(uint16_t delay, FSM_MotorOutputValue value);
static void FSM_Motor_SetOutput(FSM_MotorOutputValue value);
static void FSM_Motor_SetLeds(LedValue* ledState);

static const FsmMotStateType FSMMotMoore[FSM_MOT_MAX_ENUM] =
{
	{MOTOR_DISABLED,0,{LED_VALUE_OFF, LED_VALUE_OFF},FSM_Motor_NoAction,{FSM_MOT_IDLE, FSM_MOT_IDLE_ACTIVE, FSM_MOT_IDLE, FSM_MOT_IDLE}},//FSM_GN_IDLE
	{MOTOR_DISABLED,0,{LED_VALUE_OFF, LED_VALUE_OFF},FSM_Motor_StartProcess,{FSM_MOT_IDLE_ACTIVE, FSM_MOT_IDLE, FSM_MOT_ACTIVE_1, FSM_MOT_IDLE_ACTIVE}},//FSM_GN_IDLE
	{MOTOR_ENABLED,1,{LED_VALUE_ON, LED_VALUE_OFF},FSM_Motor_Action,{FSM_MOT_ACTIVE_1, FSM_MOT_IDLE, FSM_MOT_ACTIVE_3, FSM_MOT_WAIT_1}},//FSM_GN_IDLE
	{MOTOR_DISABLED,1,{LED_VALUE_ON, LED_VALUE_OFF},FSM_Motor_Action,{FSM_MOT_WAIT_1, FSM_MOT_IDLE, FSM_MOT_ACTIVE_3, FSM_MOT_ACTIVE_2}},//FSM_GN_IDLE
	{MOTOR_ENABLED,3,{LED_VALUE_ON, LED_VALUE_OFF},FSM_Motor_Action,{FSM_MOT_ACTIVE_2, FSM_MOT_IDLE, FSM_MOT_ACTIVE_3, FSM_MOT_WAIT_2}},//FSM_GN_IDLE
	{MOTOR_DISABLED,1,{LED_VALUE_ON, LED_VALUE_OFF},FSM_Motor_Action,{FSM_MOT_WAIT_2, FSM_MOT_IDLE, FSM_MOT_ACTIVE_3, FSM_MOT_ACTIVE_1}},//FSM_GN_IDLE
	{MOTOR_ENABLED,4,{LED_VALUE_OFF, LED_VALUE_ON},FSM_Motor_Action,{FSM_MOT_ACTIVE_3, FSM_MOT_IDLE, FSM_MOT_IDLE_ACTIVE, FSM_MOT_WAIT_3}},//FSM_GN_IDLE
	{MOTOR_DISABLED,4,{LED_VALUE_OFF, LED_VALUE_ON},FSM_Motor_Action,{FSM_MOT_WAIT_3, FSM_MOT_IDLE, FSM_MOT_IDLE_ACTIVE, FSM_MOT_ACTIVE_3}},//FSM_GN_IDLE
};

void FSM_Motor_StartProcess(uint16_t delay, FSM_MotorOutputValue value)
{
/*
 * Solo hacer este proceso una sola vez cada vez que se entra a este estado
 */
	if(beforeState == FSM_MOT_IDLE)
	{
	PIT_TimerType pit = PIT_MOTOR;
	PIT_ClockGatingEnable(); //Inicializamos el clock gating del PIT

	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE};
	PIT_TCRConfig pitTCRConfig= { pit, NOTCHAINED, PIT_ENABLE, PIT_ENABLE};
	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_MOTOR_IRQ, PRIORITY_5);
	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
	}
	setOutputMotorValue(value);

	//PIT_configureTimmerControlRegister(pitTCRConfig);
}
void FSM_MotorPitConfigDelay(uint16_t delay)
{

	if((PIT_isEnabled(PIT_MOTOR)== 0 )|| (beforeState != actualState))//Se reconfigura el pit si ocurrio un cambio de estado o si el delay ya paso
	{
		float sysClock = 21000000;
		PIT_delay(PIT_MOTOR,sysClock, delay);
	}
}
void FSM_Motor_NoAction(uint16_t delay, FSM_MotorOutputValue value)
{

	setOutputMotorValue(value);
}
void FSM_Motor_Action(uint16_t delay, FSM_MotorOutputValue value)
{
	FSM_MotorPitConfigDelay(delay);
	FSM_Motor_SetOutput(value);
}
void FSM_Motor_SetOutput(FSM_MotorOutputValue value)
{
	setOutputMotorValue(value);
}
void FSM_Motor_SetLeds(LedValue* ledState)
{
	setOutputLedValues(FSM_MOTOR_OUT_LEDS, ledState[0], ledState[1]);
}
void FSM_MotorInitialize()
{
	/*
	 * Inicializar el pin de salida del motor y los pines de salida de los leds del motor
	 * Inicializar Los switches que utiliza el motor Hacer lo mismo con el generador
	 * Colocar las salidas del motor en desactivado y los leds en su estado predeterminado
	 */
	actualState = FSM_MOT_IDLE;// Set actual state to IDLE
	beforeState = FSM_MOT_IDLE;
	sw2InterruptInitialize();
}
void FSM_MotorEvaluate(Evento_Motor event)//FSM_GeneratorLEDState* ledState
{
	if((actualState != FSM_MOT_IDLE))
	{
		setRgbLeds(RED);
	}
	else
	{
		clearRgbLeds(RED);
	}
	uint16_t delay = (FSMMotMoore[actualState].delay);
	FSM_MotorOutputValue valorNuevo = FSMMotMoore[actualState].actionArgument;
	FSMMotMoore[actualState].fptrAction(delay,valorNuevo);

	LedValue ledState[2]={FSMMotMoore[actualState].ledStates[0],FSMMotMoore[actualState].ledStates[1]};
	FSM_Motor_SetLeds(ledState);
	beforeState = actualState;
	actualState = FSMMotMoore[actualState].nextEstate[event];
}
