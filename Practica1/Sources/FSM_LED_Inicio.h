/**
 \file FSM_LED_INICIO.h
 \brief
 This is the header file for the LED INICIO FSM
 It contains the State Machine enumeration,the structure for
 containing states, and a way to interface it.
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	9/28/2016
 */
#ifndef SOURCES_FSM_LED_INICIO_H_
#define SOURCES_FSM_LED_INICIO_H_

#include "Eventos.h"

/*! \typedef action
 * 	\brief pointer to void function type
 */
typedef void(*action) (void);

/*! \typedef Estado_FSM_LED_Inicio
 *	\brief Type enum for the LED INICIO FSM
*/
typedef enum{
	FSM_LED_INICIO_IDLE,
	FSM_LED_INICIO_ACTIVAR1,
	FSM_LED_INICIO_ESPERAR1,
	FSM_LED_INICIO_APAGAR1,
	FSM_LED_INICIO_ESPERAR2,
	FSM_LED_INICIO_ACTIVAR2,
	FSM_LED_INICIO_ESPERAR3,
	FSM_LED_INICIO_APAGAR2,
	FSM_LED_INICIO_MAX_ENUM
}Estado_FSM_LED_Inicio;

/*! \typedef FSM_LED_Inicio_Estado
	\brief This struct type contains a state for the LED ERROR FSM
	The struct contains an action to execute depending on the current event
	It also contains a pointer to an array containing the next state, depending on the current event
 */
typedef struct{
	action a;
	const Estado_FSM_LED_Inicio *nextList;
}FSM_LED_Inicio_Estado;

/*!
 * \fn void FSM_LED_Inicio_init(void)
 * \brief This function configures the initial state of the FSM.
 *	This function sets the initial state to IDLE, configures the PIT_3 timer and
 *	enables its interrupt.
 */
void FSM_LED_Inicio_init(void);

/*!
 * \fn void FSM_LED_Inicio_init(void)
 * \brief This function configures the initial state of the FSM.
 *	This function sets the initial state to IDLE, configures the PIT_3 timer and
 *	enables its interrupt.
 */
void FSM_LED_Inicio_evaluar(Evento_LED);

#endif /* SOURCES_FSM_LED_INICIO_H_ */
