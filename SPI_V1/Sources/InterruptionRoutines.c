/*
 * InterruptionRoutines.c
 *
 *  Created on: Oct 1, 2016
 *      Author: operi
 */

#include "MK64F12.h"
#include "GPIO.h"
#include "PIT.h"
#include "NVIC.h"

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 IRQ for the SW3 button. It adds a start event to the Generator State Machine.
 */
void PORTA_IRQHandler() {
	if (FALSE == GPIO_readPIN(GPIOA,4)) {
		GPIO_clearInterrupt(GPIOA);
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Currently empty interrupt, it only cleans the interrupt flag.
 */
void PORTB_IRQHandler() {
	GPIO_clearInterrupt(GPIOB);
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 IRQ for the SW2 button. It adds a start event to the Generator State Machine.
 */
void PORTC_IRQHandler() {
	/*
	 * Solo realiza el add del evento del motor en caso de que haya sido el pin del switch 2
	 */
	if (FALSE == GPIO_readPIN(GPIOC, 4)) {

		GPIO_clearInterrupt(GPIOC);
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function is used as a delay for debouncing the keyboard.
 	 \param[in]  ms value.
 */
static void tms(unsigned int ms) {
	volatile unsigned char i, k;
	volatile unsigned int ms2;
	ms2 = ms;
	while (ms2) {
		for (i = 0; i <= 140; i++) {
			k++;
		}
		ms2--;
		k++;
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 IRQ for the Matrix Keyboard POIRT. It decodes the data in the port and adds to
 	 	 Matrix Keyboard the decoded key as an event.
 */
void PORTD_IRQHandler() {

}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Currently empty interrupt, it only cleans the interrupt flag.
 */
void PORTE_IRQHandler() {

}

