/**	 \file FSM_Control.c
	 \brief
	 	 This is the source file for the user input processing and system control
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 */

#include "FSM_Control.h"
#include "UARTDriver.h"
#include "Formatter.h"
///used for debugging
#include <stdio.h>
#include "CMP.h"
#include "FSMPWMCntrl.h"
typedef void (*action)(void);
#define NULL ((void *) 0)

/// local input buffer
uint8 buffer[1024];
/// local len of the string
uint8 len=0;

///commonly used escape code
uint8 escape_borrarPantalla[] = "\033[2J\033[0;0H";
/**
 * main menu message.
 */
uint8 mensaje_menuPrincipal[] = "Menu Principal\r\n1.-Cambio de Frecuencia de muestreor\r\n"
		"2.-Cambio de frecuencia de salidar\r\n"
		"3.-Cambiod de voltaje minimo de la fuente\r\n"
		"4.-Activar\r\n"
		"5.-Desactivar\r\n";
/**
 * Menu messages
 */
uint8 mensaje_menu1[] = "Introduzca una frecuencia portadora valida \r\nO presione escape para cancelar r\n";
uint8 mensaje_menu2[] = "Introduzca una frecuencia de salida valida \r\nO presione escape para cancelar \r\n";
uint8 mensaje_menu3[] = "Introduce el nuevo voltaje minimo \r\n";

/**
 * \brief Adds, if possible, a char to the buffer.
 * @param c
 */
void addBuffer(uint8 c){
	if(len<1024)
		buffer[len++] = c;
}

/**
 * \brief enum type for the possible Menus.
 */
typedef enum{
	principal,           //!< principal
	error_principal,     //!< error_principal
	f_muestreo,          //!< f_muestreo
	error_f_muestreo,    //!< error_f_muestreo
	f_senal,             //!< f_senal
	error_f_senal,       //!< error_f_senal
	voltaje_fuente,      //!< voltaje_fuente
	error_voltaje_fuente,//!< error_voltaje_fuente
	activar,             //!< activar
	apagar,              //!< apagar
}Menu_stateEnum;

/**
 * \brief enum type for the possible input FSM states
 */
typedef enum{
	E_idle,          //!< E_idle
	E_wait,          //!< E_wait
	E_store,         //!< E_store
	E_eval,          //!< E_eval
	E_erase,         //!< E_erase
	E_escape,        //!< E_escape
	Estados_MAX_ENUM,//!< Estados_MAX_ENUM
}Estados_Control_State;

/**
 * \brief FSM state encapsulation
 */
typedef struct {
	action a;
	Estados_Control_State *nextList;
}Estado_type;

///current menu variable
Menu_stateEnum currentMenu;
///current control state variable
Estados_Control_State currentControl;

///Transition list for the wait state.
Estados_Control_State waitNextList[]={
	E_wait, //nop
	E_store, //char
	E_eval,
	E_erase,
	E_escape,
};

/**
 * \brief Comparator IRQ handler. Its invoked when the voltage level is lower than the threshold
 */
void CMP0_IRQHandler(){
	if(CMP0_SCR & CMP_SCR_CFF_MASK){
		UART_putString(UART_4,"\e[48;5;1m" );
		UART_putString(UART_4,escape_borrarPantalla);

		UART_putString(UART_4, "La fuente de alimentacion ha bajado demasiado\r\n");
		UART_putString(UART_4,mensaje_menuPrincipal);

	}else{
		UART_putString(UART_4, "La fuente de alimentacion se ha restablecido\r\n");
	}

	//CMP0_SCR |= (CMP_SCR_CFR_MASK | CMP_SCR_CFF_MASK);

}

/**
 *\brief Evaluates the input when main menu is the current menu.
 * \return Menu_stateEnum
 */
Menu_stateEnum evaluarEntradaPrincipal(){
	len = 0;
	switch(buffer[0]){
	case '1':
		return f_muestreo;
		break;
	case '2':
		return f_senal;
		break;
	case '3':
		return voltaje_fuente;
		break;
	case '4':
		return activar;
	case '5':
		return apagar;
	default:
		return error_principal;
	}
}

/**
 * \brief Evaluates the input and parses it to select it as the new carrier frequency.
 * @return sint32
 */
sint32 evaluarEntradaF_Muestreo(){
	uint32 res = parseAsciiToUnsignedInt(buffer, len);
	len = 0;

	double tempOutputFrec = FSM_PWM_ControlGetOutputFrequency();
	double tempResolution = ((double)res)/tempOutputFrec;


	if((tempResolution<4) || (tempResolution > MAX_RES))
		return -1;

	FSM_PWM_ControlSetCarrierFrequency((double)res);

	printf("%d", res);
	return res;
}

/**
 * \brief Evaluates the input and parses it to select it as the new output frequency.
 * \return sint32
 */
sint32 evaluarEntradaF_Senal(){
	uint32 res = parseAsciiToUnsignedInt(buffer, len);
	len = 0;
	double tempCarrierFrec = FSM_PWM_ControlGetCarrierFrequency();
	double tempResolution = tempCarrierFrec/((double)res);

	if((tempResolution<4) || (tempResolution > MAX_RES))
		return -1;

	FSM_PWM_ControlSetOutputFrequency((double) res);

	printf("%d \r\n", res);
	return res;
}


/**
 * \brief Evaluates the input and parses it to select it as the new threshold voltage.
 * \return sint32
 */
sint32 evaluarEntradaVoltajeFuente(){
	sint32 res = parseAsciiToUnsignedInt(buffer, len);
	len = 0;

	if(res<=4)
		return -1;
	printf("%d \r\n", res);
	CMP_InterruptEnable(CMP_0, FALSE, FALSE);
	CMP_DAC_enable(CMP_0, FALSE);
	CMP_DAC_setValue(CMP_0,  res/11.0);
	CMP_DAC_enable(CMP_0, TRUE);
	CMP_clearFallingFlag(CMP_0);
	CMP_clearRisingFlag(CMP_0);
	CMP_InterruptEnable(CMP_0, TRUE, TRUE);

	return res;
}

/// Initial state
void idle(){

	UART_putString(UART_4,"\e[48;5;1m" );
	UART_putString(UART_4,escape_borrarPantalla);

	UART_putString(UART_4,mensaje_menuPrincipal);
	currentControl = E_wait;
}

/// No user input ocurred.
void wait(){

}

/**
 * An enter was received. Input must be processed by the right menu
 */
void eval(){
	switch(currentMenu){
	case principal:

		switch(evaluarEntradaPrincipal()){
		case principal:
			break;
		case error_principal:

			break;
		case f_muestreo:
			UART_putString(UART_4,"\e[48;5;1m" );
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menu1);
			currentMenu = f_muestreo;
			break;
		case error_f_muestreo:

			break;
		case f_senal:
			UART_putString(UART_4,"\e[48;5;1m" );
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menu2);
			currentMenu = f_senal;
			break;
		case error_f_senal:

			break;
		case voltaje_fuente:
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menu3);
			currentMenu = voltaje_fuente;

			break;
		case error_voltaje_fuente:

			break;
		case activar:
			//f(CMP_comparatorOutput(CMP_0)){
				UART_putString(UART_4,"\e[48;5;2m" );
				UART_putString(UART_4,escape_borrarPantalla);
				UART_putString(UART_4,mensaje_menuPrincipal);
				addEventPWMCntrl(EVENT_PWM_CTRL_RUN);
				currentMenu = principal;
/*			}else{
				UART_putString(UART_4,escape_borrarPantalla);
				UART_putString(UART_4,"La fuente no tiene suficiente nivel de voltaje\r\n");
				UART_putString(UART_4,mensaje_menuPrincipal);
				addEventPWMCntrl(EVENT_PWM_CTRL_STOP);
			}*/
			break;
		case apagar:
			UART_putString(UART_4,"\e[48;5;1m " );
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menuPrincipal);
			addEventPWMCntrl(EVENT_PWM_CTRL_STOP);
			break;
		}

	break;
	case error_principal:

		break;

	case f_muestreo:
		if(evaluarEntradaF_Muestreo() > 0){
			UART_putString(UART_4,"\r\nLa frecuencia de la portadora ha cambiado\r\n");
			addEventPWMCntrl(EVENT_PWM_CTRL_CONFIG);
		}else{
			UART_putString(UART_4,"\r\nEsa frecuencia no es valida\r\n");
		}
		break;

	case error_f_muestreo:

		break;

	case f_senal:
		if(evaluarEntradaF_Senal() > 0){
			UART_putString(UART_4,"\r\nLa frecuencia de la moduladora ha cambiado\r\n");
			addEventPWMCntrl(EVENT_PWM_CTRL_CONFIG);
		}else{
			UART_putString(UART_4,"\r\nEsa frecuencia no es valida\r\n");
		}
		break;
	case error_f_senal:

		break;
	case voltaje_fuente:
		if(evaluarEntradaVoltajeFuente()>0){
			UART_putString(UART_4,"\r\nEl voltaje de threshold ha cambiado\r\n");
		}else{
			UART_putString(UART_4,"\r\nEl voltaje no es valido\r\n");
		}
		break;
	case error_voltaje_fuente:

		break;
	case activar:

		break;
	}
	currentControl = E_wait;
}

/**
 * Normal character was received, must be stored at the buffer.
 */
void store(){
	uint8 c;
	c = UART_getMailboxContent(UART_4);
	buffer[len++] = c;
	UART_putChar(UART_4, c);
	currentControl = E_wait;
}


/**
 * Backspace was received, a character from the terminal must be erased.
 */
void erase(){
	if(len>0){
		len--;
		UART_putChar(UART_4,'\b');
		UART_putChar(UART_4, ' ');
		UART_putChar(UART_4,'\b');
	}
	currentControl = E_wait;
}
/**
 * Escape code was received , execution must retur to parent menu.
 */
void scape(){
	len = 0;
	switch(currentMenu){
		case principal:
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menuPrincipal);
			currentMenu = principal;
			break;
		case error_principal:

			break;

		case f_muestreo:
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menuPrincipal);
			currentMenu = principal;

			break;

		case error_f_muestreo:
			break;

		case f_senal:
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menuPrincipal);
			currentMenu = principal;
			break;
		case error_f_senal:

			break;
		case voltaje_fuente:
			UART_putString(UART_4,escape_borrarPantalla);
			UART_putString(UART_4,mensaje_menuPrincipal);
			currentMenu = principal;
			break;
		case error_voltaje_fuente:

			break;
		}
		currentControl = E_wait;
}
///Idle state next state transitions.
const Estados_Control_State idleNextList[] = {E_idle, E_idle, E_idle, E_idle, E_idle, E_idle};

/// Transition table
const Estado_type estados[Estados_MAX_ENUM] = {
	{idle, idleNextList},
	{wait, waitNextList},
	{store, NULL},
	{eval, NULL},
	{erase, NULL},
	{scape, NULL},

};

///Sets the initial Control FSM to the initial idle state
void FSM_Control_init(){
	currentControl = E_idle;
	currentMenu = principal;
}

/// Evaluates the last user input event.
void FSM_Control_eval(FSM_Control_Event evt){
	currentControl = estados[currentControl].nextList[evt];
	estados[currentControl].a();
	//printf("%d %d\r\n", currentControl, currentMenu);
}
