/*
 * UART.c
 *
 *  Created on: Oct 11, 2016
 *      Author: operi
 */
#include "UART.h"

//
////
///********************************************************************************************/
///********************************************************************************************/
///********************************************************************************************/
///*!
// 	 \brief	 It configures the UART to be used
// 	 \param[in]  uartChannel indicates which UART will be used.
// 	 \param[in]  systemClk indicates the MCU frequency.
// 	 \param[in]  baudRate sets the baud rate to transmit.
// 	 \return void
// */
//void UART_init(UART_ChannelType uartChannel, uint32 systemClk, UART_BaudRateType baudRate)
//{
//	uint32 sbr = systemClk/(16*baudRate);
//	float brfd = systemClk/(16*baudRate)-sbr;
//	uint32 brfa = (brfd*32.0);
//	SIM_SCGC4 |= SIM_SCGC4_UART0_MASK;
//	UART0_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
//	UART0_BDH = UART_BDH_SBR((sbr>>8));
//	UART0_BDL = UART_BDH_SBR((0xFF&sbr));
//	UART0_C4 |= UART_C4_BRFA(brfa);
//	UART0_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
//
//}
//
///********************************************************************************************/
///********************************************************************************************/
///********************************************************************************************/
///*!
// 	 \brief	 enables the RX UART interrupt). This function should include the next sentence:
// 	 while (!(UART0_S1 & UART_S1_RDRF_MASK)). It is to guaranty that the incoming data is complete
// 	 when reception register is read. For more details see chapter 52 in the kinetis reference manual.
// 	 \param[in]  uartChannel indicates the UART channel.
// 	 \return void
// */
//void UART0_interruptEnable(UART_ChannelType uartChannel)
//{
//	UART0_C2 |= UART_C2_RIE_MASK;
//}
//
///********************************************************************************************/
///********************************************************************************************/
///********************************************************************************************/
///*!
// 	 \brief	 It sends one character through the serial port. This function should include the next sentence:
// 	 while(!(UART0_S1 & UART_S1_TC_MASK)). It is to guaranty that before to try to transmit a byte, the previous
// 	 one was transmitted. In other word, to avoid to transmit data while the UART is busy transmitting information.
// 	 \param[in]  uartChannel indicates the UART channel.
// 	 \param[in]  character to be transmitted.
// 	 \return void
// */
//
//void UART_putChar (UART_ChannelType uartChannel, uint8 character)
//{
//	while(!(UART0_S1 & UART_S1_TC_MASK));
//	UART0_D  = character;
//}
///********************************************************************************************/
///********************************************************************************************/
///********************************************************************************************/
///*!
// 	 \brief	 It sends a string character through the serial port.
// 	 \param[in]  uartChannel indicates the UART channel.
// 	 \param[in]  string pointer to the string to be transmitted.
// 	 \return void
// */
//void UART_putString(UART_ChannelType uartChannel, sint8* string)
//{
//	sint8 * puntero = string;
//	while(*puntero)
//	{
//		UART_putChar(uartChannel, *puntero);
//		puntero++;
//	}
//}
