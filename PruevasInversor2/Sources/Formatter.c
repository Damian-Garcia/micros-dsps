/**	 \file Formatter.c
	 \brief
	 	 Este es el archivo fuente del formatter que contiene las distintas funciones
	 	 que se utilizan para darle formato a las variables que son mostradas en pantalla,
	 	 recibidas de la UART, enviadas a la UART, etc...
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	11/2/2016
 */
#include "Formatter.h"

uint32 tenPower(uint8 n){
	int r = 1;
	for(;n;n--){
		r *= 10;
	}
	return r;
}

uint8 integerlength(uint32 number){
	uint8 digits;
	for (digits = 0; number ;number /= 10, digits++);
	return (digits>0)? digits : 1;
}

void formatUnsignedInteger32(uint32 number, uint8 *buffer, sint8 digits) {
	uint8 i = 0;
	if(digits < 0){
		digits = integerlength(number);
	}

	for (i = 0; i < digits; i++) {
		buffer[digits - i - 1] = (number % 10) + '0';
		number /= 10;
	}
	buffer[digits] = '\0';
}

void formatDouble(double number, uint8 *buffer, uint8 decimalDigits) {
	uint32 integerPart;
	integerPart = number;
	uint8 digits = integerlength(integerPart);
	formatUnsignedInteger32(number, buffer, digits);
	buffer[digits] = '.';
	number -= integerPart;
	number *= tenPower(decimalDigits);
	uint32 decimalPart = number;
	formatUnsignedInteger32(number, buffer+digits+1, decimalDigits);
}
void formatBCDint(uint32 number, uint8 * buffer, sint8 digits){
	uint8 i = 0;
	if(digits < 0){
		digits = integerlength(number);
	}
	uint32 mask = 0xF;
	for (i = 0; i < digits; i++) {
		buffer[digits - i - 1] = (number & mask) + '0';
		number = number>>4;
	}
	buffer[digits] = '\0';
}
uint16 convertAsciiAddressToHexadecimal(uint8* buffer)//La posicion mas baja del buffer contiene el dato mas significativo
{
	uint16 resultado = 0;
	uint8 temp = 0;
	for(int i=0; i<4;i++)
	{
		temp = convertAsciiDigitToBinary(buffer[i]);
		resultado = resultado | temp<<(12-i*4);
	}
	return resultado;
	//return ((convertAsciiToBinary(buffer[0])<<12) | (convertAsciiToBinary(buffer[1])<<8) | (convertAsciiToBinary(buffer[2])<<4) | (convertAsciiToBinary(buffer[3])<<0));
}
uint16 convertAsciiAddressToHexadecimalBCDWithNumberOfDigits(uint8* buffer, uint8 digits)//La posicion mas baja del buffer contiene el dato mas significativo
{
	uint16 resultado = 0;
	uint8 temp = 0;
	for(int i=0; i<digits;i++)
	{
		temp = convertAsciiDigitToBinary(buffer[i]);
		resultado = resultado | temp<<(((12-4*(4-digits))-i*4));
	}
	return resultado;
	//return ((convertAsciiToBinary(buffer[0])<<12) | (convertAsciiToBinary(buffer[1])<<8) | (convertAsciiToBinary(buffer[2])<<4) | (convertAsciiToBinary(buffer[3])<<0));
}
uint16 convertAsciiBufferToOneUnsignedInt(uint8* buffer, uint8 digits)//La posicion mas baja del buffer contiene el dato mas significativo
{
	uint16 resultado = 0;
	uint8 temp = 0;
	for(int i=0; i<digits;i++)
	{
		temp = convertAsciiDigitToBinary(buffer[i]);
		resultado = resultado | temp<<(((12-4*(4-digits))-i*4));
	}
	return convertBCDtoBinary(resultado);
	//return ((convertAsciiToBinary(buffer[0])<<12) | (convertAsciiToBinary(buffer[1])<<8) | (convertAsciiToBinary(buffer[2])<<4) | (convertAsciiToBinary(buffer[3])<<0));
}
uint8 convertAsciiDigitToBinary(uint8 value)
{
	if(value >0x39)
	{
		return value - 55;
	}
	else
	{
		return value-0x30;
	}
}

BooleanType isDigit(uint8 c){
	if('0'>c || '9' < c){
		return FALSE;
	}else{
		return TRUE;
	}
}

sint32 parseAsciiToUnsignedInt(uint8 *buffer, uint8 digits){
	uint32 i = 0;
	uint32 acum = 0;
	for(i= 0; i<digits; i++){
		if(!isDigit(buffer[i])){
			return -1;
		}
		acum *= 10;
		acum += buffer[i]-'0';
	}
	return acum;
}

