/**
	\file 
	\brief 
		This is the source file of FlexTimer.
		In this file the FlexTimer is configured in overflow mode and other useful modes
	\author Erick Ortega y Damian Garcia.
	\date	11/2/2016
 */
#ifndef FLEXTIMER_H_
#define FLEXTIMER_H_

#include "MK64F12.h"
/*! \typedef FTM_NameType
 *	\brief Contains the constants used to refer to a specific flex timer
*/
typedef enum
{
	FTM_0,
	FTM_1,
	FTM_2,
	FTM_3,
	FTM_MAX
}FTM_NameType;
/*! \typedef FTM_BooleanType
 *	\brief Constants used to enable or disable a feature of the flex timer
*/
typedef enum
{
	FTM_DISABLE,
	FTM_ENABLE
}FTM_BooleanType;
/*! \typedef FTM_CounterTypePWMS
 *	\brief Constants used to refer to the different counter types.
*/
typedef enum
{
	FTM_COUNTER_UP,
	FTM_COUNTER_DOWN
}FTM_CounterTypePWMS;

/*! \typedef FTM_TypeCLKsource
 *	\brief Constants used to refer to the different clocks available for the flex timer.
*/
typedef enum
{
	FTM_NO_CLK,
	FTM_SYSTEM_CLK,
	FTM_FIXED_FREC_CLK,
	FTM_EXT_CLK
}FTM_TypeCLKsource;
/*! \typedef FTM_TypePrescalerFactor
 *	\brief Constants used to refer to the different prescalers available for the flex timers.
*/
typedef enum
{
	FTM_DIVIDED_1,
	FTM_DIVIDED_2,
	FTM_DIVIDED_4,
	FTM_DIVIDED_8,
	FTM_DIVIDED_16,
	FTM_DIVIDED_32,
	FTM_DIVIDED_64,
	FTM_DIVIDED_128
}FTM_TypePrescalerFactor;
/*! \typedef FTM_TypeFaultControl
 *	\brief Constants used to refer to the different fault controls available for the flex timmers.
*/
typedef enum
{
	FTM_FAULT_CONTROL_DISABLED,
	FTM_FALUT_CONTROL_ENABLED_EVEN_CHANNELS,
	FTM_FAULT_CONTROL_ENABLED_ALL_CHANNELS_MANUAL_CLEARING,
	FTM_FAULT_CONTROL_ENABLED_ALL_CHANNELS_AUTOMATIC_CLEARING
}FTM_TypeFaultControl;

/*! \typedef FTM_TypePWMSync
 *	\brief Constants used to refer to the different pwm sync modes available.
*/
typedef enum
{
	FTM_NO_RESTRICTIONS,
	FTM_SFT_TRIGGER_ONLY_MOD_AND_CNV_SYNC_HARDWARE_OUTMASK_AND_FTMCOUNTERSYNC
}FTM_TypePWMSync;

/*! \typedef FTM_TypeAvailableRegisters
 *	\brief Constants used to define the registers used on the flex timer.
*/
typedef enum
{
	FTM_ONLY_TPM_COMPATIBLE_REGISTERS_AVAILABLE,
	FTM_ALL_FTM_SPECIFIC_REGISTERS_AVAILABLE
}FTM_TypeAvailableRegisters;

/*! \typedef FTM_TypeChannel
 *	\brief Constants used to define the different channels available of the flex timer.
*/
typedef enum
{
	FTM_CH0,
	FTM_CH1,
	FTM_CH2,
	FTM_CH3,
	FTM_CH4,
	FTM_CH5,
	FTM_CH6,
	FTM_CH7

}FTM_TypeChannel;

/*! \typedef FTM_OverflowInterruptB
 *	\brief Constant type used to define the enable or disable of the overflow interrupt of a flex timer.
*/
typedef const FTM_BooleanType FTM_OverflowInterruptB;

/*! \typedef FTM_FaultIEB
 *	\brief Constant type used to define the enable or disable of the fault interrupt of a flex timer.
*/
typedef const FTM_BooleanType FTM_FaultIEB;

/*! \typedef FTM_CaptureTestEB
 *	\brief Constant type used to define the enable or disable of the capture test of a flex timer.
*/
typedef const FTM_BooleanType FTM_CaptureTestEB;

/*! \typedef FTM_WriteProtectionDisableE
 *	\brief Constant type used to define the enable or disable of the write protection of a flex timer.
*/
typedef const FTM_BooleanType FTM_WriteProtectionDisableE;

/*! \typedef FTM_WriteProtectionDisableE
 *	\brief Constant type used to define the enable or disable of the write protection of a flex timer.
*/
typedef const FTM_BooleanType FTM_ChannelIEB;

/*! \typedef FTM_ModVal
 *	\brief Type used to define the value of the mod register of the flex timer.
*/
typedef uint16_t FTM_ModVal;

/*! \typedef FTM_InitChOut
 *	\brief Type used to define the initial value of the channel out.
*/
typedef uint8_t FTM_InitChOut;

/*! \typedef FTM_MSB_ModeSelect
 *	\brief Type used to define the msb bit of the flex timer.
*/
typedef uint8_t FTM_MSB_ModeSelect;
/*! \typedef FTM_MSA_ModeSelect
 *	\brief Type used to define the msa bit of the flex timer.
*/
typedef uint8_t FTM_MSA_ModeSelect;
/*! \typedef FTM_ELSB_EdgeLevelSelect
 *	\brief Type used to define the elsb bit of the flex timer.
*/
typedef uint8_t FTM_ELSB_EdgeLevelSelect;
/*! \typedef FTM_ELSA_EdgeLevelSelect
 *	\brief Type used to define the elsa bit of the flex timer.
*/
typedef uint8_t FTM_ELSA_EdgeLevelSelect;
/*! \typedef FTM_DMA
 *	\brief Type used to define the direct memory access register of the flex timer.
*/
typedef uint8_t FTM_DMA;

/*! \typedef FTM_TypeCv
 *	\brief Type used to define the type of value of the current value register of the flex timer.
*/
typedef uint16_t FTM_TypeCv;

/*! \typedef FTM_OverflowConfiguration
 *	\brief Structure used to configure a flex timer as an overflow.
*/
typedef struct FTM_Overflow_Configuration
{
	FTM_NameType ftmName;
	FTM_TypeCLKsource sourceClk;
	FTM_TypePrescalerFactor prescaleFactor;
	FTM_ModVal modVal;
}FTM_OverflowConfiguration;

/*! \typedef FTM_InputCapture_Configuration
 *	\brief Structure used to configure a flex timer in the input capture mode.
*/
typedef struct FTM_InputCapture_Configuration
{
	FTM_NameType ftmName;
	FTM_TypeCLKsource sourceClk;
	FTM_TypePrescalerFactor prescaleFactor;
	FTM_ModVal modVal;
	FTM_TypeChannel channelType;
	FTM_ChannelIEB channelIE;
 	FTM_MSB_ModeSelect msbModeSelect;
	FTM_MSA_ModeSelect msaModeSelect;
	FTM_ELSB_EdgeLevelSelect elsb;
	FTM_ELSA_EdgeLevelSelect elsa;
	FTM_TypeCv cvType;
}FTM_InputCaptureConfiguration;

/*! \typedef FTM_PWMConfiguration
 *	\brief Structure used to configure a flex timer in the pwm mode.
*/
typedef struct FTM_PWM_Configuration
{
	FTM_NameType ftmName;
	FTM_TypeCLKsource sourceClk;
	FTM_TypePrescalerFactor prescaleFactor;
	FTM_ModVal modVal;
	FTM_TypeChannel channelType;
	FTM_ChannelIEB channelIE;
 	FTM_MSB_ModeSelect msbModeSelect;
	FTM_MSA_ModeSelect msaModeSelect;
	FTM_ELSB_EdgeLevelSelect elsb;
	FTM_ELSA_EdgeLevelSelect elsa;
	FTM_TypeCv cvType;
}FTM_PWMConfiguration;

/*! \typedef FTM_PWMConfiguration
 *	\brief Structure used to configure a flex timer in the pwm mode.
*/
typedef struct FTM_PWM_CenterAlligned_Configuration
{
	FTM_NameType ftmName;
	FTM_TypeCLKsource sourceClk;
	FTM_TypePrescalerFactor prescaleFactor;
	FTM_ModVal modVal;
	FTM_ChannelIEB channelIE;
 	FTM_MSB_ModeSelect msbModeSelect;
	FTM_MSA_ModeSelect msaModeSelect;
	FTM_ELSB_EdgeLevelSelect elsb;
	FTM_ELSA_EdgeLevelSelect elsa;
	FTM_TypeCv cvType;
	uint8_t deatTime;
}FTM_PWMcenterStruct;


/*! \typedef FTM_PWMConfiguration
 *	\brief Structure used to configure almost all the refisters of a flex timer.
*/
typedef struct FTM_General_Configuration
{
	/*
	 * Configuration of the sc register
	 */
	FTM_NameType ftmName;
	FTM_OverflowInterruptB overflowInterruptB;
	FTM_CounterTypePWMS counterType;
	FTM_TypeCLKsource sourceClk;
	FTM_TypePrescalerFactor prescaleFactor;
	/*
	 * Configuration of the MOD register
	 */
	FTM_ModVal modVal;
	/*
	 * Configuration of the mode
	 */
	FTM_FaultIEB faultieInterruptE;
	FTM_TypeFaultControl faultTypeControl;
	FTM_CaptureTestEB captureTestE;
	FTM_TypePWMSync pwmSyncType;
	FTM_WriteProtectionDisableE writeProtectionDisableE;
	FTM_InitChOut initChOut;
	FTM_TypeAvailableRegisters availableRegistersType;

	/*
	 * CnSC configuration
	 */
	FTM_TypeChannel channelType;//Used to configure the channel n status and control
	FTM_ChannelIEB channelIE;
 	FTM_MSB_ModeSelect msbModeSelect;
	FTM_MSA_ModeSelect msaModeSelect;
	FTM_ELSB_EdgeLevelSelect elsb;
	FTM_ELSA_EdgeLevelSelect elsa;
	FTM_DMA ftmDma;

	FTM_TypeCv cvType;
}FTM_GeneralConfiguration;


/*!
 	 \brief This function enables the channel interrupt of the specified channel.
*/
void FTM_SetChannelInterruptEnable(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_ChannelIEB channelIE);

/*!
 	 \brief This function configures the flex timer MSB and MSA bits, which define the mode of the flex timer.
*/
void FTM_SetMSB_MSA_Mode(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_MSB_ModeSelect msbModeSelect, FTM_MSA_ModeSelect msaModeSelect);

/*!
 	 \brief This function configures the flex timer MSB and MSA bits, which define the mode of the flex timer.
*/
void FTM_SetEdgeLevelSelect(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_ELSB_EdgeLevelSelect elsb, FTM_ELSA_EdgeLevelSelect elsa);

/*!
 	 \brief This function configures the DMA of the specified flex timer.
*/
void FTM_SetDMA(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_DMA ftmDma);


/*!
 	 \brief This function configures the value in the channel value of the specified flex timer.
*/
void FTM_SetCVvalue(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_TypeCv cvType);


/*!
 	 \brief This function updates the channel value of the specified channel.
*/
void FTM_updateCHValue(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_TypeCv cvType);


/*!
 	 \brief This function returns the current value inside the channel value register
 	 of the specified flex timer.
*/
uint16_t FTM_getCHValue(FTM_NameType ftmName, FTM_TypeChannel channelType);


/*!
 	 \brief This function configures the faultie interrupt enable.
*/
void FTM_SetFaultieIE(FTM_NameType ftmName,FTM_FaultIEB faultieInterruptE);


/*!
 	 \brief This function configures the fault control type of the specified flex timer.
*/
void FTM_SetTypeFaultControl(FTM_NameType ftmName, FTM_TypeFaultControl faultTypeControl);


/*!
 	 \brief This function configures the capture test of the flex timer.
*/
void FTM_SetCaptureTestEnable(FTM_NameType ftmName, FTM_CaptureTestEB captureTestE);


/*!
 	 \brief This function configures the pwm sync mode of the flex timer..
*/
void FTM_SetTypePWMSync(FTM_NameType ftmName,FTM_TypePWMSync pwmSyncType);


/*!
 	 \brief This function configures the write enable protection of the flex timer.
*/
void FTM_SetWriteProtectionDisableEnable(FTM_NameType ftmName, FTM_WriteProtectionDisableE writeProtectionDisableE);

/*!
 	 \brief This function configures the initial channel output value of the specified flex timer.
*/
void FTM_SetInitChannelOutput(FTM_NameType ftmName, FTM_InitChOut initChOut);

/*!
 	 \brief This function selects the available registers of the specified channel
*/
void FTM_SetTypeAvailableRegisters(FTM_NameType ftmName, FTM_TypeAvailableRegisters availableRegistersType);
/*!
 	 \brief This function configures the mod register of the specified flex timer.
*/
void FTM_SetModValue(FTM_NameType ftmName, FTM_ModVal modVal);
/*!
 	 \brief This function enables or disables the timmer overflow interruption.
*/
void FTM_SetOverflowInterrput(FTM_NameType ftmName, FTM_OverflowInterruptB overflowInterruptB);
/*!
 	 \brief This function configures the counter type of the pwm mode of the specified flex timer.
*/
void FTM_SetCounterTypePWMS(FTM_NameType ftmName, FTM_CounterTypePWMS counterType);
/*!
 	 \brief This function selects the clock source of the specified flex timer.
*/
void FTM_SetTypeClockSource(FTM_NameType ftmName, FTM_TypeCLKsource sourceClk);
/*!
 	 \brief This function selects the prescaler of the specified flex timer.
*/
void FTM_SetTypePrescalerFactor(FTM_NameType ftmName,FTM_TypePrescalerFactor prescaleFactor);
/*!
 	 \brief This function enables or disables the clock gating of the specified flex timer.
*/
void FTM_ClockGatingEnable(FTM_NameType ftmName);
/*!
 	 \brief This function selects the clock source of the specified flex timer.
*/

//void FTM0_ISR(void);

/*!
 	 \brief This function initialize the specified flex timer with the received general flex timer structure.
*/
void FlexTimer_InitGeneralFlex(const FTM_GeneralConfiguration* flexTimerConfiguration);
/*!
 	 \brief This function initialize the specified flex timer in a pwm mode.
*/
void FlexTimer_InitPWM(const FTM_PWMConfiguration * flexTimerConfiguration);
/*!
 	 \brief This function initialize the specified flex timer in the input capture mode.
*/
void FlexTimer_InitInputCaptureMode(const FTM_InputCaptureConfiguration* flexTimerConfiguration);
/*!
 	 \brief This function initialize the specified flex timer in the output compare mode.
*/
void FlexTimer_OutputCompareInit();
/*!
 	 \brief This function initialize the specified flex timer in the normal overflow mode.
*/
void FlexTimer_InitOverflow(const FTM_OverflowConfiguration* flexTimerConfiguration);
void FlexTimer_InitPWMcenterAlligned();

#endif /* FLEXTIMER_H_ */
