/**	 \file FSM_Control.h
	 \brief
	 	 This is the header file for the user input processing and system control
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 */
#ifndef FSM_CONTROL_H_
#define FSM_CONTROL_H_

#include "DataTypeDefinitions.h"
#include "Eventos.h"

/**
 * \brief This function initiates the system state to a correct value, beginning the system operation
 */
void FSM_Control_init();
/**
 * \brief Evaluates the last event, to generate the next FSM state, depending on the input value and the current state.
 * @param
 */
void FSM_Control_eval(FSM_Control_Event);

#endif /* FSM_CONTROL_H_ */
