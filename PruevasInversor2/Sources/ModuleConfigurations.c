/*
 * ModuleConfigurations.c
 *
 *  Created on: Nov 1, 2016
 *      Author: operi
 */


#include "ModuleConfigurations.h"
#include "MCG.h"
#include "EEPROM24LC256.h"
#include "RTC7940M.h"
#include "PIT.h"
#include "SPI.h"
#include "LCDNokia5110.h"

#define CLK_FREQ_HZ 50000000  /* CLKIN0 frequency */
#define SLOW_IRC_FREQ 32768	/*This is the approximate value for the slow irc*/
#define FAST_IRC_FREQ 4000000 /*This is the approximate value for the fast irc*/
#define EXTERNAL_CLOCK 0 /*It defines an external clock*/
#define PLL_ENABLE 1 /**PLL is enabled*/
#define PLL_DISABLE 0 /**PLL is disabled*/
#define CRYSTAL_OSC 1  /*It defines an crystal oscillator*/
#define LOW_POWER 0     /* Set the oscillator for low power mode */
#define SLOW_IRC 0 		/* Set the slow IRC */
#define CLK0_TYPE 0     /* Crystal or canned oscillator clock input */
#define PLL0_PRDIV 25    /* PLL predivider value */
#define PLL0_VDIV 30    /* PLL multiplier value*/



//Estructura de inicializacion para la configuracion de la hora.
dateStruct date=
{
	0x00,//segundos
	0x20,//minutos
	0x11,//horas
	0x21,//dia
	0x07,//mes
	0x16,//anio
	0x20,//anio
	FALSE,//AMPMFORMAT
	FALSE//AM O PM
};

uartStruct uartScreenConfigStruct =
{
		UART_SCREEN_CHANNEL,
		60000000,//21000000,
		BD_9600
};
static uartStruct uartBluetoothConfigStruct =
{
		UART_BLUETOOTH_CHANNEL,
		21000000,
		BD_9600
};


const SPI_ConfigType SPI_Config = { SPI_DISABLE_FIFO,
		SPI_LOW_POLARITY,
		SPI_LOW_PHASE,
		SPI_MSB,
		SPI_0,
		SPI_MASTER,
		GPIO_MUX2,
		SPI_BAUD_RATE_6, //SPI_BAUD_RATE_2,
		SPI_FSIZE_8,
		{ GPIOD, BIT1, BIT2 } };

/*
 * Funcion que servira para la decodificacion de los distintos botones que pueden ser
 * presionados.
 */
//void PORTC_IRQHandler(){
//	GPIO_clearInterrupt(GPIOC);
//	NVIC_DisableIRQ(PORTC_IRQn);
//	uint32 data = GPIO_readPORT(GPIOC);
//	NVIC_EnableIRQ(PORTC_IRQn);
//	data = (~data)&0x3F;
//	if(data == 1){
//		addEventoBoton(EVT_BTN_0);
//	}else if(data == 2){
//		addEventoBoton(EVT_BTN_1);
//	}else if(data == 4){
//		addEventoBoton(EVT_BTN_2);
//	}else if(data == 8){
//		addEventoBoton(EVT_BTN_3);
//	}else if(data == 16){
//		addEventoBoton(EVT_BTN_4);
//	}else if(data == 32){
//		addEventoBoton(EVT_BTN_5);
//	}else{
//
//	}
//	PIT_setEnable(PIT_2);
//
//}
void PinConfiguration_initializeI2CModules()
{
	PIT_ClockGatingEnable();
	NVIC_enableInterruptAndPriotity(PIT_CH0_IRQ, PRIORITY_12);
	EnableInterrupts;

	GPIO_clockGating(I2C_MEM_SDA_PORT);
	GPIO_pinControlRegisterType pcr = GPIO_MUX5;
	GPIO_pinControlRegister(I2C_MEM_SDA_PORT, i2C_MEM_SDA_PIN, &pcr);
	GPIO_clockGating(I2C_MEM_SCL_PORT);
	GPIO_pinControlRegister(I2C_MEM_SCL_PORT, i2C_MEM_SCL_PIN, &pcr);

	//Configuracion del modulo I2C
	configureMemoryConfiguration();
	RTC_initializeDate(&date);


}
void PinConfiguration_initializeUartModules()
{
	/*
	 * Configuracion de los pines de la pantalla (transmisor y receptor)
	 */

	GPIO_clockGating(UART_SCREEN_TX_PORT);
	GPIO_pinControlRegisterType pcrScreen = GPIO_MUX3;
	GPIO_pinControlRegister(UART_SCREEN_TX_PORT, UART_SCREEN_TX_PIN, &pcrScreen);
	GPIO_pinControlRegister(UART_SCREEN_RX_PORT, UART_SCREEN_RX_PIN, &pcrScreen);
	/*
	 * Configuracion de los pines del bluetooth (transmisor y receptor)
	 */

	GPIO_clockGating(UART_BLUETOOTH_TX_PORT);
	GPIO_pinControlRegisterType pcrBluetooth = GPIO_MUX3;
	GPIO_pinControlRegister(UART_BLUETOOTH_TX_PORT, UART_BLUETOOTH_TX_PIN, &pcrBluetooth);
	GPIO_pinControlRegister(UART_BLUETOOTH_RX_PORT, UART_BLUETOOTH_RX_PIN, &pcrBluetooth);
	/*
	 * Inicializacion de los modulos de UART
	 */
	UART_init(&uartScreenConfigStruct);
	UART_init(&uartBluetoothConfigStruct);

}

void initializeMCGHighFrequence()
{
    int mcg_clk_hz;
    mcg_clk_hz = fei_fbi(SLOW_IRC_FREQ, SLOW_IRC);//64 Hz 32768
    mcg_clk_hz = fbi_fbe(CLK_FREQ_HZ, LOW_POWER, EXTERNAL_CLOCK);//98 KHz --> 50MHZ
    mcg_clk_hz = fbe_pbe(CLK_FREQ_HZ, PLL0_PRDIV, PLL0_VDIV);//98 KHz --> 50 MHz PLL
    mcg_clk_hz = pbe_pee(CLK_FREQ_HZ);//117.18Hz
}
void PinConfig_BtnInputPinsInitialization()
{
	GPIO_clockGating(FSM_PORT_BTN0);
	GPIO_pinControlRegisterType pcrFSM_Btn0 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN0, FSM_PIN_BTN0, &pcrFSM_Btn0);
	GPIO_dataDirectionPIN(FSM_PORT_BTN0,GPIO_INPUT,FSM_PIN_BTN0);

	GPIO_clockGating(FSM_PORT_BTN1);
	GPIO_pinControlRegisterType pcrFSM_Btn1 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN1, FSM_PIN_BTN1, &pcrFSM_Btn1);
	GPIO_dataDirectionPIN(FSM_PORT_BTN1,GPIO_INPUT,FSM_PIN_BTN1);

	GPIO_clockGating(FSM_PORT_BTN2);
	GPIO_pinControlRegisterType pcrFSM_Btn2 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN2, FSM_PIN_BTN2, &pcrFSM_Btn2);
	GPIO_dataDirectionPIN(FSM_PORT_BTN2,GPIO_INPUT,FSM_PIN_BTN2);

	GPIO_clockGating(FSM_PORT_BTN3);
	GPIO_pinControlRegisterType pcrFSM_Btn3 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN3, FSM_PIN_BTN3, &pcrFSM_Btn3);
	GPIO_dataDirectionPIN(FSM_PORT_BTN3,GPIO_INPUT,FSM_PIN_BTN3);

	GPIO_clockGating(FSM_PORT_BTN4);
	GPIO_pinControlRegisterType pcrFSM_Btn4 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN4, FSM_PIN_BTN4, &pcrFSM_Btn4);
	GPIO_dataDirectionPIN(FSM_PORT_BTN4,GPIO_INPUT,FSM_PIN_BTN4);

	GPIO_clockGating(FSM_PORT_BTN5);
	GPIO_pinControlRegisterType pcrFSM_Btn5 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN5, FSM_PIN_BTN5, &pcrFSM_Btn5);
	GPIO_dataDirectionPIN(FSM_PORT_BTN5,GPIO_INPUT,FSM_PIN_BTN5);

}
void PinConfig_BtnInterruptPortInitialization()
{
	NVIC_enableInterruptAndPriotity(FSM_PORT_BTN5, PRIORITY_5);
	/*
	 * Configuracion del PIT timer para evitar los rebotes cuando
	 * se presiono un boton.
	 * No estoy seguro de que esto este correcto.
	 */
	float sysClock = 60000000;
	float period = 0.5;
	PIT_ClockGatingEnable();
	PIT_setTimeDelay(BTNPIT, sysClock, period);
	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE };
	PIT_TCRConfig pitTCRConfig2 = { BTNPIT, NOTCHAINED, PIT_ENABLE, PIT_DISABLE };
	NVIC_enableInterruptAndPriotity(PIT_CH2_IRQ, PRIORITY_12);
	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
}
void PinConfig_ScreenInitialization()
{
	SPI_init(&SPI_Config);
	LCDNokia_init();
}
/*
 * Estas definiciones de funciones no seran necesarias para la realizacion
 */
//void Bluetooth_PutScreen(sint8* string)
//{
//	UART_putString(UART_BLUETOOTH_CHANNEL, string);
//}
//void Computer_PutScreen(sint8* string)
//{
//	UART_putString(UART_SCREEN_CHANNEL, string);
//}
//void Bluetooth_getMailboxFlag()
//{
//
//}
//void Computer_getMailboxFlag()
//{
//
//}
//void Bluetooth_getMailboxContent()
//{
//
//}
//void Computer_getMailboxContent()
//{
//
//}
