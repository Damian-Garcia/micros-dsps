/**	 \file Eventos.h
	 \brief
	 	 This is the header file for the event definition and handling.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 */

#ifndef SOURCES_EVENTOS_H_
#define SOURCES_EVENTOS_H_

#include "DataTypeDefinitions.h"

/**
 * \brief Enum type for the possible user events
 */
typedef enum FSM_Control_Event{
	EVT_FSM_CONTROL_NOP,      //!< EVT_FSM_CONTROL_NOP
	EVT_FSM_CONTROL_CHAR,     //!< EVT_FSM_CONTROL_CHAR
	EVT_FSM_CONTROL_ENTER,    //!< EVT_FSM_CONTROL_ENTER
	EVT_FSM_CONTROL_BACKSPACE,//!< EVT_FSM_CONTROL_BACKSPACE
	EVT_FSM_CONTROL_ESC,      //!< EVT_FSM_CONTROL_ESC
	EVT_FSM_CONTROL_RESTART,  //!< EVT_FSM_CONTROL_RESTART
}FSM_Control_Event;

/**
 * \brief adds an event to be processed by the system
 * \param FSM_Control_Event
 */
void addEvent(FSM_Control_Event);
/**
 * \brief decodes a character as a user input event.
 * \param uint8
 * \return FSM_Control_Event
 */
FSM_Control_Event decodeControlEvent(uint8);
/**
 * \brief pops the last event to process it
 * @return FSM_Control_Event
 */
FSM_Control_Event getEvent();

/**
 * \brief Enum type for the possible control events
 */
typedef enum{
	EVENT_PWM_CTRL_NOP,
	EVENT_PWM_CTRL_RUN,
	EVENT_PWM_CTRL_STOP,
	EVENT_PWM_CTRL_CONFIG,
	EVENT_PWM_CTRL_MAX_ENUM
}Event_PWM_Cntrl;

/**
 * \brief Initiates the control events signals.
 */
void eventos_init(void);
/**
 * \brief pops the last control event to process it
 * \return Event_PWM_Cntrl
 */
Event_PWM_Cntrl getEventPWMCntrl();
/**
 * \brief adds a control event to be later processed by the system.
 * \param e
 */
void addEventPWMCntrl(Event_PWM_Cntrl e);

#endif /* SOURCES_EVENTOS_H_ */
