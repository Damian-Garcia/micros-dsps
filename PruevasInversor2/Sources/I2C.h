/**
	\file
	\brief
		This is the source file for the I2C device driver.
		i.e., this is the application programming interface (API) for the GPIO peripheral.
	\author Erick Ortega and Damian Garcia Serrano.
	\date	11/2/2016
 */

#ifndef SOURCES_I2C_H_
#define SOURCES_I2C_H_

#include "MK64F12.h"
#include "DataTypeDefinitions.h"
#include "PIT.h"

/** Constant that represent the clock enable for GPIO A */
#define I2C0_CLOCK_GATING 0x40

/*
 * Define used to refer to the pit timer which is gonna be used to detect
 * the disconnection of a I2C circuit communication.
 */
#define I2CPIT PIT_0

/*! \typedef I2C_ChannelType
 *	\brief  This enum define the UART port to be used.
 */
typedef enum {I2C_0, I2C_1, I2C_2}I2C_ChannelType;


/*! \typedef I2C_MODE
 *	\brief  Enumerator to refer to the different roles that a I2C module can take.
 */
typedef enum {I2C_SLAVE, I2C_MASTER}I2C_MODE;

/*! \typedef I2C_COM_MODE
 *	\brief   Enumerator that refers to the different communication modes that the I2C can be.
 */
typedef enum {I2C_RX_MODE, I2C_TX_MODE}I2C_COM_MODE;

/*! \struct GPIOForSPIType
 *	\brief Type for GPIO configuration that activates the GPIO for SPI
 */
typedef struct I2c_Structure
{
	I2C_ChannelType channel;
	I2C_MODE mode;//Modo del I2C
	I2C_COM_MODE comMode;
	BooleanType interruptEnable;//Habilitacion o deshabilitacion de la interrupcion
	uint32 systemClock;//Reloj del sistema
	uint32 baudRate;//Baundrate a seleccionar
	uint8 clockratePrescaler;//Prescaler del reloj
	uint8 multiplierFactor;//Factor de multiplicacion.
}I2CStructure;

/********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 Indicates the status of the bus regardless of slave or master mode. Internally checks the busy bit int the
  	 	 I2Cx_S register. This bit is set when a START signal is detected and cleared when a STOP signal is detected.
  	 \return This function returns a 0 logic if the bus is idle and returns 1 logic if the bus is busy.

  */
 uint8 I2C_busy(I2C_ChannelType currentChannel);
 /********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 It selects between master or slave mode.
  	 \param[in] masterOrSlave If == 1 master mode, if == 0 slave mode.
  	 \return void

  */
 void I2C_MST_OrSLV_Mode(I2C_ChannelType currentChannel, I2C_MODE mode);
 /********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 It selects between transmitter mode or receiver mode.
  	 \param[in] txOrRx If == 1 transmitter mode, if == 0 slave mode.
  	 \return void

  */
 void I2C_TX_RX_Mode(I2C_ChannelType currentChannel, I2C_COM_MODE comMode);
 /********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 It generates the Not ACKnowledge that is needed when the master reads data.
  	 \return void

  */
 void I2C_NACK(I2C_ChannelType currentChannel);
 /********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 It generates a repeated start that is needed when master reads data.
  	 \return void

  */
 void I2C_repeted_Start(I2C_ChannelType currentChannel);
 /********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 It writes the data to be transmitted into the transmission buffer. When you want to
  	 	 write a value into the buffer you need to use this sentence I2C0_D = data. Avoid to use
  	 	 masks like this I2C0_D |= data.
  	 \return void

  */
void I2C_write_Byte(I2C_ChannelType currentChannel, uint8 byte);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 It reads data from the receiving buffer.
 	 \return void

 */
uint8  I2C_read_Byte(I2C_ChannelType currentChannel);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 It makes the processor to wait until the transfer of the data is completed. If the transfer is not done by the time that is configured on the pit
 	 	 delay function then it assumes that the memory is disconnected or that there is an error in the connection.

 */
void I2C_wait(I2C_ChannelType currentChannel);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Indicates if the acknowledge was received.
 	 \return This function returns a 0 logic if the acknowledge was received and returns 1 logic if the acknowledge was not received.

 */
BooleanType I2C_get_ACK(I2C_ChannelType currentChannel);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Generates the start signal. When MST bit is changed from 0 to 1, a START signal is generated
 	 	 on the bus and master mode is selected. Also, inside the function the I2C is
 	 	 change to transmitter mode.
 	 \return void

 */
void I2C_start(I2C_ChannelType currentChannel);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Generates the stop signal. When this bit changes from 1 to 0, a STOP signal is generated
 	 	 and the mode of operation changes from master to slave. Also, inside the function the I2C is
 	 	 change to receiver mode.
 	 \return void

 */
void I2C_stop(I2C_ChannelType currentChannel);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Receives the value that is going to be written on the multiplier factor of the I2C configuration registers.
 	 \return void

 */
void I2C_setMultiplierFactor(I2C_ChannelType currentChannel, uint8 multiplierFactor);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 Sets the clock Rate value that is going to be written in the F register of The i2c module.
 	 \return void
 */
void I2C_setClockRate(I2C_ChannelType currentChannel, uint8 clockRatePrescaler);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 It enables the specified I2C channel so that the module is activated.
 	 \return void
 */

void I2C_setEnableI2C(I2C_ChannelType currentChannel, BooleanType enable);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 It enables the the interrupt of the specified I2C module.
 	 \return void
 */
void I2C_setInterruptEnable(I2C_ChannelType currentChannel, BooleanType interruptEnable);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 It activates the clock gating of the specified channel of the I2C module. This clockgating depends on the timer. and without it the module cannot be used.
 	 \return void
 */
void I2C_clockGating(I2C_ChannelType currentChannel);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 It sets the FACK bit register so that this mode of fast acknowledge is activated.
 	 \return void
 */
void I2C_setFack(I2C_ChannelType currentChannel);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 This module attempt to clear the SRW flag.
 	 \return void
 */
void I2C_clearSRW(I2C_ChannelType currentChannel);

/********************************************************************************************/
 /********************************************************************************************/
 /********************************************************************************************/
 /*!
  	 \brief
  	 	 Configures the I2C module with the structure that it receives as an input. The variables that are connected inside the structure are the following contain the information
  	 	 regarding the baudrate, the current system clock and the channel that is going to be used.
  	 \return void
  */
void I2C_initAsMaster(I2CStructure* i2CStruct);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief
 	 	 This function makes a delay of the microprocessor. The delay is used to let the microcontroller finish writting or reading data from the I2C connected device.
 	 \return void
 */
void tms(unsigned int ms) ;


#endif /* SOURCES_I2C_H_ */
