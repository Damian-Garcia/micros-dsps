/**	 \file Eventos.c
	 \brief
	 	 This is the source file for the event definition and handling.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 */

#include "Eventos.h"

FSM_Control_Event current = EVT_FSM_CONTROL_NOP;

void addEvent(FSM_Control_Event evt){
	current =  evt;
}

FSM_Control_Event decodeControlEvent(uint8 c){
	switch(c){
	case '\r':
		return EVT_FSM_CONTROL_ENTER;
	case '\e':
		return EVT_FSM_CONTROL_ESC;
	case '\b':
		return EVT_FSM_CONTROL_BACKSPACE;
	default:
		if('z'>= c && 'a'<=c)
			return EVT_FSM_CONTROL_CHAR;
		else if( 'Z'>= c && 'A'<=c )
			return EVT_FSM_CONTROL_CHAR;
		else if( '9' >= c && '0' <= c)
			return EVT_FSM_CONTROL_CHAR;
		else
			return EVT_FSM_CONTROL_NOP;
	}
}

FSM_Control_Event getEvent(){
	FSM_Control_Event returnValue = current;
	current = EVT_FSM_CONTROL_NOP;
	return returnValue;
}

struct {
	uint8 run : 1;
	uint8 stop: 1;
	uint8 config:1;
} Event_Struct_PwmCntrl;


void eventos_init(void)
{
	Event_Struct_PwmCntrl.config = 0;
	Event_Struct_PwmCntrl.run = 0;
	Event_Struct_PwmCntrl.stop = 0;
}

Event_PWM_Cntrl getEventPWMCntrl()
{
	if(Event_Struct_PwmCntrl.stop)
	{
		Event_Struct_PwmCntrl.stop = 0;
		return EVENT_PWM_CTRL_STOP;
	}
	else if(Event_Struct_PwmCntrl.run)
	{
		Event_Struct_PwmCntrl.run = 0;
		return EVENT_PWM_CTRL_RUN;
	}
	else if(Event_Struct_PwmCntrl.config)
	{
		Event_Struct_PwmCntrl.config = 0;
		return EVENT_PWM_CTRL_CONFIG;
	}
	return EVENT_PWM_CTRL_NOP;
}


void addEventPWMCntrl(Event_PWM_Cntrl e)
{
	if(EVENT_PWM_CTRL_RUN == e)
	{
		Event_Struct_PwmCntrl.run = 1;
	}
	else if(EVENT_PWM_CTRL_STOP == e)
	{
		Event_Struct_PwmCntrl.stop = 1;
	}
	else if(EVENT_PWM_CTRL_CONFIG == e)
	{
		Event_Struct_PwmCntrl.config = 1;
	}
}

