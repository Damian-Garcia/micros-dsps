
/**	 \file EEPROM24LC256.h
	 \brief
	 	 Este es el archivo de cabecera del device driver de la memoria
	 	 que se encuentra conectada a la tarjeta Kinetis de desarrollo
	 	 por medio del protocolo de comunicacion serial I2C.
	 \date	11/2/2016
 */
#ifndef SOURCES_EEPROM24LC256_H_
#define SOURCES_EEPROM24LC256_H_

#include "I2C.h"

/*!
 * \brief Retorna verdadero si ocurrio un error.
 */
BooleanType getErrorMemoryFlag();
/*!
 * \brief Escribe un dato en la posicion de memoria requerida.
 */
void writeByteToMemoryAddress(uint8 byteData, uint16 memoryAddress);
/*!
 * \brief Escribe el contenido de un arreglo en la memoria.
 */
void writeStreamofBytesToMemoryAddress(uint8* buffer, uint16 startAddress, uint8 size);
/*!
 * \brief Recibe una direccion en especifico y retorna el contenido de esa direccion
 * Este contenido normalmente es el valor en ascii
 */
uint8 readByteFromMemoryAddress( uint16 memoryAddress);
/*!
 * \brief Inicializa el modulo de I2C para poder comunicarse con la memoria.
 */
void configureMemoryConfiguration();
/*!
 * \brief Recibe un apuntador a un arreglo y posteriormente rellena ese arreglo desde la posicion cero
 * hasta la ultima posicion con el contenido de la memoria (que son caracteres en ascii normalmente).
 */
void readStreamOfBytesFromMemoryToArray(uint8* buffer, uint16 startAddress, uint8 size);

#endif /* SOURCES_EEPROM24LC256_H_ */
