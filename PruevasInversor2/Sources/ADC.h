/*
 * ADC.h
 *
 *  Created on: Oct 8, 2016
 *      Author: operi
 */

#ifndef SOURCES_ADC_H_
#define SOURCES_ADC_H_
#include "MK64F12.h"
#include "DataTypeDefinitions.h"
#include "GPIO.h"

/*! These constants are used to select an specific adc*/
typedef enum
{ADC_0, /*!< Definition to select ADC 0 */
 ADC_1, /*!< Definition to select ADC 1 */
 ADC_MAX /*!< Definition to select ADC 2 */
} ADC_NameType;
/*! \typedef ADC_Mode
 *	\brief This enum contains the constants that specifie the mode of the adc.
*/
typedef enum
{
	ADC_SINGLE_MODE,
	ADC_DIFFERENTIAL_MODE
} ADC_Mode;
/*! \typedef ADC_BOOLTYPE
 *	\brief This enum contains the constants used to enable or disbale features of the adc.
*/
typedef enum
{
	ADC_DISABLE,
	ADC_ENABLE
}ADC_BOOLTYPE;

/*! \typedef ADC_DivRatioType
 *	\brief This enum contains the constants used to select the div ratio of the adc.
*/
typedef enum
{
	ADC_DIVRATIO_1,
	ADC_DIVRATIO_2,
	ADC_DIVRATIO_4,
	ADC_DIVRATIO_8
}ADC_DivRatioType;

/*! \typedef ADC_SampleType
 *	\brief This enum contains the constants used to select sample type
*/
typedef enum
{
	ADC_SHORT_SAMPLE,
	ADC_LONG_SAMPLE
}ADC_SampleType;
/*! \typedef ADC_ResolutionMode
 *	\brief Constants of the resolution mode of the adc.
*/
typedef enum
{
	ADC_S_8BIT_DIFF_9BIT,
	ADC_S_12BIT_DIFF_13BIT,
	ADC_S_10BIT_DIFF_11BIT,
	ADC_S_16BIT_DIFF_16BIT
}ADC_ResolutionMode;
/*! \typedef ADC_ClockSource
 *	\brief Constants of source clocks of the adcs.
*/
typedef enum
{
	ADC_BUSCLK,
	ADC_ALTCLK2,
	ADC_ALTCLK1,
	ADC_ADACK,
}ADC_ClockSource;

/*! \typedef ADC_ConversionInterrupt
 *	\brief Constants to specify the conversion interrupt boolean.
*/
typedef const ADC_BOOLTYPE ADC_ConversionInterrupt;
/*! \typedef ADC_LowPowerConfiguration
 *	\brief Constants to specify the low powerc configuration boolean.
*/
typedef const ADC_BOOLTYPE ADC_LowPowerConfiguration;
/*! \typedef ADC_AsyncOutputClock
 *	\brief Constants to specify the async output clock configuration boolean.
*/
typedef const ADC_BOOLTYPE ADC_AsyncOutputClock;
/*! \typedef ADC_CompareFunction
 *	\brief Constants to specify the compare function configuration boolean.
*/
typedef const ADC_BOOLTYPE ADC_CompareFunction;
/*! \typedef ADC_CompareFunctionRange
 *	\brief Constants to specify the compare function configuration boolean.
*/
typedef const ADC_BOOLTYPE ADC_CompareFunctionRange;
/*! \typedef ADC_HardwareAverageEnable
 *	\brief Constants to specify the hardware average configuration boolean.
*/
typedef const ADC_BOOLTYPE ADC_HardwareAverageEnable;
/*! \typedef ADC_DMA
 *	\brief Constants to specify the dma configuration boolean.
*/
typedef const ADC_BOOLTYPE ADC_DMA;
/*! \typedef ADC_Resolution
 *	\brief Constants to specify the resolution of the adc.
*/
typedef const uint32 ADC_Resolution;
/*! \typedef ADC_ChannelType
 *	\brief Constants to specify the channel type of the adc.
*/
typedef const uint8 ADC_ChannelType;

/*! \typedef ADC_MuxType
 *	\brief Constants of the mux types of the adc
*/
typedef enum
{
	ADC_ADXXA,
	ADC_ADXXB
}ADC_MuxType;

/*! \typedef ADC_ConversionTriggerSelectType
 *	\brief Constants of the triggers of the adc
*/
typedef enum
{
	ADC_SOFTWARETRIGGER,
	ADC_HARDWARETRIGGER
}ADC_ConversionTriggerSelectType;

/*! \typedef ADC_CompareFunctionGreaterThan
 *	\brief Constants of the compare functions of the adc.
*/
typedef enum
{
	ADC_LESSTHANTHRESHOLD_NOTINCLUSIVE,
	ADC_GREATEROREQUALTHRESHOLD_INCLUSIVE
}ADC_CompareFunctionGreaterThan;

/*! \typedef ADC_VoltageRefType
 *	\brief Constants of the sources of voltages for the adc.
*/
typedef enum
{
	ADC_VOLTAGE_DEFAULT,
	ADC_VOLTAGE_ALT
}ADC_VoltageRefType;

/*! \typedef ADC_ContinuousConvertType
 *	\brief Constants of the conversion types of the adc.
*/
typedef enum
{
	ADC_ONE_CONVERSION,
	ADC_CONTINUOUS_CONVERSIONS
}ADC_ContinuousConvertType;

/*! \typedef ADC_HardwareAverageSelectType
 *	\brief Constants of the hardware average sample.
*/
typedef enum
{
	SAMPLE_4,
	SAMPLE_8,
	SAMPLE_16,
	SAMPLE_32
}ADC_HardwareAverageSelectType;

/*! \typedef ADC_Config
 *	\brief Structure used to configure the adc registers.
*/
typedef struct ADC_Configuration
{
	ADC_NameType adcName;
	ADC_ConversionInterrupt convInterrupt;
	ADC_Mode adcMode;
	ADC_ChannelType adcChannel;
	ADC_LowPowerConfiguration lwpConfig;
	ADC_DivRatioType divRatio;
	ADC_SampleType sampleType;
	ADC_ResolutionMode resMode;
	ADC_ClockSource clockSrc;
	ADC_MuxType muxType;
	ADC_AsyncOutputClock asyncOutputClk;
	ADC_ConversionTriggerSelectType convTriggerSelect;
	ADC_CompareFunction compareFunction;
	ADC_CompareFunctionGreaterThan functionGreaterThanConfig;
	ADC_CompareFunctionRange functionRange;
	ADC_DMA adcDma;
	ADC_VoltageRefType voltageRef;
	ADC_ContinuousConvertType continuousConvert;
	ADC_HardwareAverageEnable hardwareAverage;
	ADC_HardwareAverageSelectType hardwareAverageSelect;
}ADC_Config;
/*!
 	 \brief This function configures the hardware average sample number of the specified adc.
*/
void ADC_SetHardwareAverageSampleNumber(ADC_NameType adcName, ADC_HardwareAverageSelectType hardwareAverageSelect);
/*!
 	 \brief This function enables the hardware average value of the specified adc.
*/
void ADC_SetHardwareAverageEnable(ADC_NameType adcName, ADC_HardwareAverageEnable hardwareAverage);
/*!
 	 \brief This function configures the conversion type of the specified adc.
*/
void ADC_SetContinuousConvertType(ADC_NameType adcName, ADC_ContinuousConvertType continuousConvert);
/*!
 	 \brief This function configures the the voltage reference of the specified adc.
*/
void ADC_SetVoltageRefType(ADC_NameType adcName, ADC_VoltageRefType voltageRef);
/*!
 	 \brief This function configures the DMA of the specified adc.
*/
void ADC_SetDMA(ADC_NameType adcName, ADC_DMA adcDma);
/*!
 	 \brief This function configures the DMA of the specified adc.
*/
void ADC_SetFunctionRange(ADC_NameType adcName, ADC_CompareFunctionRange functionRange);
/*!
 	 \brief This function configures the adc compare function.
*/
void ADC_SetCompareFunctionGreaterThan(ADC_NameType adcName, ADC_CompareFunctionGreaterThan functionGreaterThanConfig);
/*!
 	 \brief This function enables or disable the compare funciton of the adc.
*/
void ADC_SetCompareFunction(ADC_NameType adcName, ADC_CompareFunction compareFunction);
/*!
 	 \brief This function enables or disables the conversion complete of the adc.
*/
void ADC_SetConversionTriggerSelect(ADC_NameType adcName, ADC_ConversionTriggerSelectType convTriggerSelect);
/*!
 	 \brief This function configures the asynchronous output clock mode for the adc.
*/
void ADC_SetAsyncOutputClock(ADC_NameType adcName, ADC_AsyncOutputClock asyncOutputClk);
/*!
 	 \brief This function copnfigures the mux type of the adc.
*/
void ADC_SetMuxType(ADC_NameType adcName, ADC_MuxType muxType);
/*!
 	 \brief This function selects the channel that is going to be used.
*/
void ADC_ChannelSelect(ADC_NameType adcName, ADC_ChannelType adcChannel);
/*!
 	 \brief This function enable or disable the conversion complete interrupt.
*/
void ADC_ConfigConversionInterrupt(ADC_NameType adcName, ADC_ConversionInterrupt convInterrupt);
/*!
 	 \brief This function selects the mode that is going to be used of the adc.
*/
void ADC_ModeSelect(ADC_NameType adcName, ADC_Mode adcMode);
/*!
 	 \brief This function selects the source clock that is going to be used.
*/
void ADC_ClockSourceSelection(ADC_NameType adcName, ADC_ClockSource clockSrc);
/*!
 	 \brief This function selects the resolution of the adc that is going to be used.
*/
void ADC_SetResolution(ADC_NameType adcName, ADC_ResolutionMode resMode);
/*!
 	 \brief This function selects the sample type that is going to be used.
*/
void ADC_SetSampleType(ADC_NameType adcName, ADC_SampleType sampleType);
/*!
 	 \brief This function selects the div ratio that is going to be used.
*/
void ADC_SetDivRatio(ADC_NameType adcName, ADC_DivRatioType divRatio);
/*!
 	 \brief This function selects the power mode that is going to be used.
*/
void ADC_SetLowPowerConfiguration(ADC_NameType adcName, ADC_LowPowerConfiguration lwpConfig);
/*!
 	 \brief This function initialize the adc selected with the values contained in the structure received.
*/
void ADC_initializeAdc(const ADC_Config* adcConfig);
/*!
 	 \brief This function disables the clock gating of the adc.
*/
void ADC_ClockGatingDisable(ADC_NameType adcName);
/*!
 	 \brief This function enables the clock gating of the adc.
*/
void ADC_clockGatingEnable(ADC_NameType adcName);
/*!
 	 \brief This function configures the read mode of the specified adc.
*/
void ADC_enableSingleRead(ADC_ChannelType adcChannel);
/*!
 	 \brief This function store the current adc value on the static variable adc0value.
 	 It stores teh value after making the necessary convertions to obtain the temperature equivalent.
*/
void ADC_writeAdc0Value(unsigned int value);
/*!
 	 \brief This function returns the adc0value stored in the static variable.
*/
double ADC_getAdc0Value();
/*!
 	 \brief This function store the current adc value on the static variable adc0value.
*/
unsigned int ADC_readUnsignedData(ADC_NameType adcName, ADC_ChannelType adcChannel);
/*!
 	 \brief This function read the data of the adc when it is signed.
*/
signed int ADC_readSignedData(ADC_NameType adcName);
#endif /* SOURCES_ADC_H_ */
