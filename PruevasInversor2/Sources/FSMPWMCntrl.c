/*
 * FSMPWMCntrl.c
 *
 *  Created on: Nov 16, 2016
 *      Author: operi
 */

#include "FlexTimer.h"
#include "FSMPWMCntrl.h"
#include "DataTypeDefinitions.h"
#include "GPIO.h"
#include "NVIC.h"

#define FSM_PWM_PREESCALER FTM_DIVIDED_128//Current static preescaler value.


/*
 * Lookup table parameters
 */
#define TABLE_SIZE 900

/*
 * Variables of the FSM
 */

static float freqSource = 60000000.0;//21000000.0;//Definition of the source frequency of the flex timer module.
static float preescalerValue = 128.0;
static BooleanType modifyingMod = FALSE;

static FSM_PWMCntrlState actualState;
static BooleanType runProcess = FALSE;//Bit that indicates whenever the inverter is already running

/*
 * Input action elements of each state.
 */

static double resolution = 0;
static double resHalfWave = 0;
static uint32 currentModValue = 0;//This value gets update every time the frequency is pdated
typedef enum
{
	FREQUENCY_CARRIER,
	FREQUENCY_OUTPUT,
	FSM_PWM_FREC_ARRAY_MAX_ENUM
}FSM_PWM_FREC_ARRAY_ENUM;

static double frequencyConfiguration[FSM_PWM_FREC_ARRAY_MAX_ENUM] = {0};// = {frequencyCarrier, frequencyOutput}

/*
 * Data structure
 */
typedef struct DutyCycle_Struct
{
	uint32 array[MAX_RES];
	uint32 size;
	uint32 currentPositionIndex;
	BooleanType descending;
}DutyCycleStruct;

DutyCycleStruct dutyCycleStruct ={
		{0}, MAX_RES, 0, FALSE
};

/*
 * Prototypes of the methods of the FSM
 */

void FSM_PWM_Control_NoAction();
void FSM_PWM_Control_Run_Process();
void FSM_PWM_Control_ConfigureParameters();
void FSM_PWM_Control_Stop_Prucess();
float sineWithDegrees(float degree);


static const FsmPWMCntrlType FSM_PWMCntrl[FSM_PWM_CNTRL_MAX_ENUM] =
{
	//                                                               NOP                  RUN                STOP                CONFIG
	{FSM_PWM_Control_Run_Process, FSM_PWM_CNTRL_RUN,            {FSM_PWM_CNTRL_RUN, FSM_PWM_CNTRL_RUN, FSM_PWM_CNTRL_STOP, FSM_PWM_CNTRL_CONFIG}}, // RUN state
	{FSM_PWM_Control_Stop_Prucess, FSM_PWM_CNTRL_STOP,          {FSM_PWM_CNTRL_STOP, FSM_PWM_CNTRL_RUN, FSM_PWM_CNTRL_STOP, FSM_PWM_CNTRL_CONFIG}}, // STOP state
	{FSM_PWM_Control_ConfigureParameters, FSM_PWM_CNTRL_CONFIG, {FSM_PWM_CNTRL_STOP, FSM_PWM_CNTRL_RUN, FSM_PWM_CNTRL_STOP, FSM_PWM_CNTRL_STOP}}, // Config State
};
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function is used as a delay for debouncing the keyboard.
 	 \param[in]  ms value.
 */
static void tms(unsigned int ms) {
	volatile unsigned char i, k;
	volatile unsigned int ms2;
	ms2 = ms;
	while (ms2) {
		for (i = 0; i <= 140; i++) {
			k++;
		}
		ms2--;
		k++;
	}
}



void PinConfig_PWMOutputPinInitialization()
{
	// todo write initialization of output pins routine.
	/*
	 *
	 */
	GPIO_clockGating(FSM_PWM_A_PORT_OUT);
	GPIO_pinControlRegisterType pcrFSM_pwm = GPIO_MUX4;
	GPIO_pinControlRegister(FSM_PWM_A_PORT_OUT, FSM_PWM_A_PIN_OUT, &pcrFSM_pwm);
	GPIO_clockGating(FSM_PWM_B_PORT_OUT);
	GPIO_pinControlRegister(FSM_PWM_B_PORT_OUT, FSM_PWM_B_PIN_OUT, &pcrFSM_pwm);

	GPIO_clockGating(GPIOD);
	GPIO_pinControlRegisterType pcrFSM_pines = GPIO_MUX1;
	GPIO_setPIN(GPIOD, 1);
	GPIO_setPIN(GPIOD, 2);
	GPIO_pinControlRegister(GPIOD, 1, &pcrFSM_pines);
	GPIO_pinControlRegister(GPIOD, 2, &pcrFSM_pines);
	GPIO_dataDirectionPIN(GPIOD,GPIO_OUTPUT,1);
	GPIO_dataDirectionPIN(GPIOD,GPIO_OUTPUT,2);
	GPIO_setPIN(GPIOD, 1);
	GPIO_setPIN(GPIOD, 2);

}
void FlexTimerFastConfig_updateSampleFrequency()
{
	/*
	 * Calculation of the new mod value based on the new frequency value
	 */
//	modifyingMod = TRUE;
	float modPreciseValue = freqSource/(preescalerValue*2.0f*frequencyConfiguration[FREQUENCY_CARRIER]);
	uint16 modValue = (uint16) modPreciseValue;
	currentModValue = modValue;
	modifyingMod = FALSE;
	NVIC_disableInterrupt(FTM0_IRQ);
	FTM0_CNT = 0;
	FTM0_C0V = 0xFF;
	FTM0_MOD = FTM_MOD_MOD(modValue);
	dutyCycleStruct.currentPositionIndex = 0;
	NVIC_enableInterruptAndPriotity(FTM0_IRQ,PRIORITY_1);
	FTM0_SYNC|=FTM_SYNC_SWSYNC_MASK;

	while((FTM0_SYNC & FTM_SYNC_SWSYNC_MASK) == 0);


	//tms(1000);
}
void FTM0_IRQHandler()//Para el PWM. En teoria solo entrara aqui cuando el flex timer se encuentre corriendo.
{
//	if(modifyingMod == FALSE)
//	{
		dutyCycleStruct.currentPositionIndex++;
		if((dutyCycleStruct.size) ==dutyCycleStruct.currentPositionIndex)//Si llegamos al Limite superior del arreglo
		{
			dutyCycleStruct.currentPositionIndex=0;
		}

		FTM0_C0V = dutyCycleStruct.array[dutyCycleStruct.currentPositionIndex];
		FTM0_SC &= ~FTM_SC_TOF_MASK;// Se apaga al final para evitar volver a entrar en cuanto se salga del flex timer y para acelerar la reconfiguracion del flex timer.
		FTM0_SYNC|=FTM_SYNC_SWSYNC_MASK;
//	}
//	else
//	{
//		float modPreciseValue = freqSource/(preescalerValue*2.0f*frequencyConfiguration[FREQUENCY_CARRIER]);
//		uint16 modValue = (uint16) modPreciseValue;
//		currentModValue = modValue;
//		modifyingMod = FALSE;
//		FTM0_C0V = 0x0;
//		FTM0_MOD = FTM_MOD_MOD(modValue);
//		FTM0_SC &= ~FTM_SC_TOF_MASK;// Se apaga al final para evitar volver a entrar en cuanto se salga del flex timer y para acelerar la reconfiguracion del flex timer.
//		FTM0_SYNC|=FTM_SYNC_SWSYNC_MASK;
//
//	}
}
/*
 * Para la realizacion de la configuracion primero es necesario realizar la escritura de la frecuencia de carrier y la frecuencia de output.
 * Es necesario que ambas sean pares.
 */
void FSM_PWM_Control_ConfigureParameters()//Configuration for a almost symetric sine with repetitive values at top and bottom.
{
	GPIO_setPIN(GPIOD, 1);
	GPIO_setPIN(GPIOD, 2);
	//FlexTimer_InitPWMcenterAlligned();// Configure the PWM as center alligned and two opposite pwm signals on the FTM0 channel. It doesn't start the PWM.
	//FTM_SetTypeClockSource(FTM_0,FTM_NO_CLK);
	float periodCarrier = 1.0f/(frequencyConfiguration[FREQUENCY_CARRIER]);
	float periodOutput = 1.0f/(frequencyConfiguration[FREQUENCY_OUTPUT]);
	FTM_SetTypeClockSource(FTM_0,FTM_SYSTEM_CLK);
	FlexTimerFastConfig_updateSampleFrequency();//Updates the mod value.
	FTM_SetTypeClockSource(FTM_0,FTM_NO_CLK);
	float maxDutyCycleValue = currentModValue;

	resolution = frequencyConfiguration[FREQUENCY_CARRIER] / (2.0* frequencyConfiguration[FREQUENCY_OUTPUT]);// (periodOutput/2.0f)/(periodCarrier);
	resHalfWave = frequencyConfiguration[FREQUENCY_CARRIER] / (4.0* frequencyConfiguration[FREQUENCY_OUTPUT]);//resolution / 2.0f;
	/*
	 * Generation of the pwm duty cycle values based on the angular position of the sine wave of reference.
	 * Positive part of the sinusoidal cycle.
	 */
	double stepSize = 90/(resHalfWave-1);
	double currentValue = 0.0;
	uint32 i = 0;
	do
	{
		dutyCycleStruct.array[i] = ((uint32)((maxDutyCycleValue - (maxDutyCycleValue/2.0))*sineWithDegrees(currentValue)+(maxDutyCycleValue/2.0))); //getNewModValue()*sineWithDegrees(currentValue);//Se agrega el * maximo size del registro de duty cycle.
		i++;
		currentValue+= stepSize;
	}while(currentValue <= 90.0);
	for(int k=i-1;k>=0;k--, i++)
	{
		dutyCycleStruct.array[i]= dutyCycleStruct.array[k];
	}
	/*
	 * Generation of the pwm duty cycle values based on the angular position of the sine wave of reference.
	 * Negative part of the sinusoidal cycle.
	 */
	uint32 tempi = i;
	stepSize = 90/(resHalfWave-1);
	currentValue = 0.0;
	do
	{
		dutyCycleStruct.array[i] = ((uint32)((maxDutyCycleValue - (maxDutyCycleValue/2.0))*(-1.0)*sineWithDegrees(currentValue)+(maxDutyCycleValue/2.0))); //getNewModValue()*sineWithDegrees(currentValue);//Se agrega el * maximo size del registro de duty cycle.
		i++;
		currentValue+= stepSize;
	}while(currentValue <= 90.0);
	for(int k=i-1;k>=tempi;k--, i++)
	{
		dutyCycleStruct.array[i]= dutyCycleStruct.array[k];
	}

	dutyCycleStruct.size = i;
	// It doesn't activate the pwm after doing the configuration. The behaivour is going to be determined by the FSM of the PWM. FTM_SetTypeClockSource(FTM_0,FTM_SYSTEM_CLK);
}
void FSM_PWM_Control_Run_Process()
{
	if(FALSE == runProcess)
	{
		GPIO_clearPIN(GPIOD, 1);
		GPIO_clearPIN(GPIOD, 2);
		runProcess = TRUE;
//		tms(5000);

		/*
		 * todo encendido del flex timer y configuracion del mismo
		 */
		FTM_SetTypeClockSource(FTM_0,FTM_SYSTEM_CLK);
//		tms(5000);
	}
}
/*
 * Se encarga de realizar la desactivacion del flex timer. y de quitarlo del NVIC. Esto solo lo hace cuando el timer se encuentra activado.
 */
void FSM_PWM_Control_Stop_Prucess()
{
	if(TRUE == runProcess)
	{
		runProcess = FALSE;
		GPIO_setPIN(GPIOD, 1);
		GPIO_setPIN(GPIOD, 2);
		/*
		 * todo apagado del flex timer y configuracion del mismo
		 */
		FTM_SetTypeClockSource(FTM_0,FTM_NO_CLK);
	}
}


float sineWithDegrees(float degree) // Receives degrees and returns the sine of the angle received.
{
	uint16 index = (uint16) (degree*10) - 1;
	float value;
	static const float lookup[TABLE_SIZE] =
	{
			0.0f,0.0017473f,0.0034945f,0.0052418f,0.006989f,0.0087362f,0.010483f,0.012231f
			,0.013978f,0.015725f,0.017472f,0.019219f,0.020966f,0.022713f
			,0.024459f,0.026206f,0.027953f,0.029699f,0.031446f,0.033192f
			,0.034938f,0.036684f,0.03843f,0.040176f,0.041922f,0.043668f
			,0.045413f,0.047159f,0.048904f,0.050649f,0.052394f,0.054139f
			,0.055884f,0.057628f,0.059372f,0.061116f,0.06286f,0.064604f
			,0.066348f,0.068091f,0.069834f,0.071577f,0.07332f,0.075062f
			,0.076804f,0.078546f,0.080288f,0.082029f,0.083771f,0.085512f
			,0.087252f,0.088993f,0.090733f,0.092473f,0.094213f,0.095952f
			,0.097691f,0.09943f,0.10117f,0.10291f,0.10464f,0.10638f
			,0.10812f,0.10986f,0.11159f,0.11333f,0.11506f,0.1168f
			,0.11854f,0.12027f,0.122f,0.12374f,0.12547f,0.12721f
			,0.12894f,0.13067f,0.1324f,0.13413f,0.13587f,0.1376f
			,0.13933f,0.14106f,0.14279f,0.14452f,0.14624f,0.14797f
			,0.1497f,0.15143f,0.15315f,0.15488f,0.15661f,0.15833f
			,0.16006f,0.16178f,0.16351f,0.16523f,0.16695f,0.16868f
			,0.1704f,0.17212f,0.17384f,0.17556f,0.17728f,0.179f
			,0.18072f,0.18244f,0.18415f,0.18587f,0.18759f,0.1893f
			,0.19102f,0.19273f,0.19445f,0.19616f,0.19787f,0.19959f
			,0.2013f,0.20301f,0.20472f,0.20643f,0.20814f,0.20985f
			,0.21156f,0.21326f,0.21497f,0.21668f,0.21838f,0.22009f
			,0.22179f,0.22349f,0.2252f,0.2269f,0.2286f,0.2303f
			,0.232f,0.2337f,0.2354f,0.2371f,0.23879f,0.24049f
			,0.24219f,0.24388f,0.24557f,0.24727f,0.24896f,0.25065f
			,0.25234f,0.25403f,0.25572f,0.25741f,0.2591f,0.26079f
			,0.26247f,0.26416f,0.26584f,0.26753f,0.26921f,0.27089f
			,0.27258f,0.27426f,0.27594f,0.27761f,0.27929f,0.28097f
			,0.28265f,0.28432f,0.286f,0.28767f,0.28934f,0.29102f
			,0.29269f,0.29436f,0.29603f,0.2977f,0.29936f,0.30103f
			,0.3027f,0.30436f,0.30602f,0.30769f,0.30935f,0.31101f
			,0.31267f,0.31433f,0.31599f,0.31765f,0.3193f,0.32096f
			,0.32261f,0.32426f,0.32592f,0.32757f,0.32922f,0.33087f
			,0.33252f,0.33416f,0.33581f,0.33746f,0.3391f,0.34074f
			,0.34238f,0.34403f,0.34567f,0.34731f,0.34894f,0.35058f
			,0.35222f,0.35385f,0.35548f,0.35712f,0.35875f,0.36038f
			,0.36201f,0.36364f,0.36526f,0.36689f,0.36851f,0.37014f
			,0.37176f,0.37338f,0.375f,0.37662f,0.37824f,0.37986f
			,0.38147f,0.38309f,0.3847f,0.38631f,0.38792f,0.38953f
			,0.39114f,0.39275f,0.39436f,0.39596f,0.39756f,0.39917f
			,0.40077f,0.40237f,0.40397f,0.40557f,0.40716f,0.40876f
			,0.41035f,0.41194f,0.41354f,0.41513f,0.41671f,0.4183f
			,0.41989f,0.42147f,0.42306f,0.42464f,0.42622f,0.4278f
			,0.42938f,0.43096f,0.43253f,0.43411f,0.43568f,0.43725f
			,0.43882f,0.44039f,0.44196f,0.44353f,0.44509f,0.44666f
			,0.44822f,0.44978f,0.45134f,0.4529f,0.45446f,0.45601f
			,0.45757f,0.45912f,0.46067f,0.46222f,0.46377f,0.46532f
			,0.46686f,0.46841f,0.46995f,0.47149f,0.47303f,0.47457f
			,0.47611f,0.47764f,0.47918f,0.48071f,0.48224f,0.48377f
			,0.4853f,0.48683f,0.48835f,0.48988f,0.4914f,0.49292f
			,0.49444f,0.49596f,0.49748f,0.49899f,0.5005f,0.50202f
			,0.50353f,0.50504f,0.50654f,0.50805f,0.50955f,0.51106f
			,0.51256f,0.51406f,0.51555f,0.51705f,0.51855f,0.52004f
			,0.52153f,0.52302f,0.52451f,0.526f,0.52748f,0.52896f
			,0.53045f,0.53193f,0.53341f,0.53488f,0.53636f,0.53783f
			,0.5393f,0.54077f,0.54224f,0.54371f,0.54518f,0.54664f
			,0.5481f,0.54956f,0.55102f,0.55248f,0.55393f,0.55539f
			,0.55684f,0.55829f,0.55974f,0.56119f,0.56263f,0.56408f
			,0.56552f,0.56696f,0.5684f,0.56983f,0.57127f,0.5727f
			,0.57413f,0.57556f,0.57699f,0.57842f,0.57984f,0.58126f
			,0.58268f,0.5841f,0.58552f,0.58694f,0.58835f,0.58976f
			,0.59117f,0.59258f,0.59399f,0.59539f,0.5968f,0.5982f
			,0.5996f,0.60099f,0.60239f,0.60378f,0.60517f,0.60656f
			,0.60795f,0.60934f,0.61072f,0.61211f,0.61349f,0.61487f
			,0.61624f,0.61762f,0.61899f,0.62036f,0.62173f,0.6231f
			,0.62447f,0.62583f,0.62719f,0.62855f,0.62991f,0.63126f
			,0.63262f,0.63397f,0.63532f,0.63667f,0.63802f,0.63936f
			,0.6407f,0.64204f,0.64338f,0.64472f,0.64605f,0.64739f
			,0.64872f,0.65005f,0.65137f,0.6527f,0.65402f,0.65534f
			,0.65666f,0.65798f,0.65929f,0.6606f,0.66191f,0.66322f
			,0.66453f,0.66583f,0.66714f,0.66844f,0.66974f,0.67103f
			,0.67233f,0.67362f,0.67491f,0.6762f,0.67748f,0.67877f
			,0.68005f,0.68133f,0.68261f,0.68388f,0.68516f,0.68643f
			,0.6877f,0.68897f,0.69023f,0.6915f,0.69276f,0.69402f
			,0.69527f,0.69653f,0.69778f,0.69903f,0.70028f,0.70153f
			,0.70277f,0.70401f,0.70525f,0.70649f,0.70772f,0.70896f
			,0.71019f,0.71142f,0.71264f,0.71387f,0.71509f,0.71631f
			,0.71753f,0.71875f,0.71996f,0.72117f,0.72238f,0.72359f
			,0.72479f,0.726f,0.7272f,0.72839f,0.72959f,0.73078f
			,0.73198f,0.73317f,0.73435f,0.73554f,0.73672f,0.7379f
			,0.73908f,0.74025f,0.74143f,0.7426f,0.74377f,0.74493f
			,0.7461f,0.74726f,0.74842f,0.74958f,0.75073f,0.75189f
			,0.75304f,0.75419f,0.75533f,0.75648f,0.75762f,0.75876f
			,0.7599f,0.76103f,0.76216f,0.76329f,0.76442f,0.76555f
			,0.76667f,0.76779f,0.76891f,0.77002f,0.77114f,0.77225f
			,0.77336f,0.77446f,0.77557f,0.77667f,0.77777f,0.77887f
			,0.77996f,0.78105f,0.78214f,0.78323f,0.78432f,0.7854f
			,0.78648f,0.78756f,0.78863f,0.78971f,0.79078f,0.79184f
			,0.79291f,0.79397f,0.79503f,0.79609f,0.79715f,0.7982f
			,0.79925f,0.8003f,0.80135f,0.80239f,0.80344f,0.80447f
			,0.80551f,0.80655f,0.80758f,0.80861f,0.80963f,0.81066f
			,0.81168f,0.8127f,0.81372f,0.81473f,0.81574f,0.81675f
			,0.81776f,0.81876f,0.81976f,0.82076f,0.82176f,0.82275f
			,0.82375f,0.82474f,0.82572f,0.82671f,0.82769f,0.82867f
			,0.82965f,0.83062f,0.83159f,0.83256f,0.83353f,0.83449f
			,0.83545f,0.83641f,0.83737f,0.83832f,0.83927f,0.84022f
			,0.84117f,0.84211f,0.84305f,0.84399f,0.84493f,0.84586f
			,0.84679f,0.84772f,0.84864f,0.84957f,0.85049f,0.85141f
			,0.85232f,0.85323f,0.85414f,0.85505f,0.85596f,0.85686f
			,0.85776f,0.85865f,0.85955f,0.86044f,0.86133f,0.86221f
			,0.8631f,0.86398f,0.86486f,0.86573f,0.86661f,0.86748f
			,0.86835f,0.86921f,0.87007f,0.87093f,0.87179f,0.87265f
			,0.8735f,0.87435f,0.87519f,0.87604f,0.87688f,0.87772f
			,0.87855f,0.87939f,0.88022f,0.88104f,0.88187f,0.88269f
			,0.88351f,0.88433f,0.88514f,0.88596f,0.88676f,0.88757f
			,0.88837f,0.88917f,0.88997f,0.89077f,0.89156f,0.89235f
			,0.89314f,0.89392f,0.8947f,0.89548f,0.89626f,0.89703f
			,0.8978f,0.89857f,0.89934f,0.9001f,0.90086f,0.90162f
			,0.90237f,0.90312f,0.90387f,0.90462f,0.90536f,0.9061f
			,0.90684f,0.90758f,0.90831f,0.90904f,0.90976f,0.91049f
			,0.91121f,0.91193f,0.91264f,0.91336f,0.91407f,0.91477f
			,0.91548f,0.91618f,0.91688f,0.91757f,0.91827f,0.91896f
			,0.91965f,0.92033f,0.92101f,0.92169f,0.92237f,0.92304f
			,0.92371f,0.92438f,0.92505f,0.92571f,0.92637f,0.92702f
			,0.92768f,0.92833f,0.92898f,0.92962f,0.93026f,0.9309f
			,0.93154f,0.93217f,0.93281f,0.93343f,0.93406f,0.93468f
			,0.9353f,0.93592f,0.93653f,0.93714f,0.93775f,0.93836f
			,0.93896f,0.93956f,0.94016f,0.94075f,0.94134f,0.94193f
			,0.94252f,0.9431f,0.94368f,0.94425f,0.94483f,0.9454f
			,0.94597f,0.94653f,0.94709f,0.94765f,0.94821f,0.94876f
			,0.94931f,0.94986f,0.95041f,0.95095f,0.95149f,0.95202f
			,0.95256f,0.95309f,0.95361f,0.95414f,0.95466f,0.95518f
			,0.9557f,0.95621f,0.95672f,0.95723f,0.95773f,0.95823f
			,0.95873f,0.95922f,0.95972f,0.96021f,0.96069f,0.96118f
			,0.96166f,0.96213f,0.96261f,0.96308f,0.96355f,0.96402f
			,0.96448f,0.96494f,0.9654f,0.96585f,0.9663f,0.96675f
			,0.9672f,0.96764f,0.96808f,0.96851f,0.96895f,0.96938f
			,0.96981f,0.97023f,0.97065f,0.97107f,0.97149f,0.9719f
			,0.97231f,0.97272f,0.97312f,0.97352f,0.97392f,0.97431f
			,0.97471f,0.97509f,0.97548f,0.97586f,0.97624f,0.97662f
			,0.97699f,0.97737f,0.97773f,0.9781f,0.97846f,0.97882f
			,0.97918f,0.97953f,0.97988f,0.98023f,0.98057f,0.98091f
			,0.98125f,0.98159f,0.98192f,0.98225f,0.98257f,0.9829f
			,0.98322f,0.98354f,0.98385f,0.98416f,0.98447f,0.98477f
			,0.98508f,0.98538f,0.98567f,0.98596f,0.98626f,0.98654f
			,0.98683f,0.98711f,0.98739f,0.98766f,0.98793f,0.9882f
			,0.98847f,0.98873f,0.98899f,0.98925f,0.9895f,0.98975f
			,0.99f,0.99025f,0.99049f,0.99073f,0.99096f,0.9912f
			,0.99143f,0.99165f,0.99188f,0.9921f,0.99231f,0.99253f
			,0.99274f,0.99295f,0.99316f,0.99336f,0.99356f,0.99375f
			,0.99395f,0.99414f,0.99433f,0.99451f,0.99469f,0.99487f
			,0.99504f,0.99522f,0.99539f,0.99555f,0.99572f,0.99588f
			,0.99603f,0.99619f,0.99634f,0.99649f,0.99663f,0.99677f
			,0.99691f,0.99705f,0.99718f,0.99731f,0.99744f,0.99756f
			,0.99768f,0.9978f,0.99791f,0.99802f,0.99813f,0.99824f
			,0.99834f,0.99844f,0.99853f,0.99863f,0.99872f,0.9988f
			,0.99889f,0.99897f,0.99905f,0.99912f,0.99919f,0.99926f
			,0.99933f,0.99939f,0.99945f,0.99951f,0.99956f,0.99961f
			,0.99966f,0.9997f,0.99974f,0.99978f,0.99982f,0.99985f
			,0.99988f,0.9999f,0.99993f,0.99995f,0.99996f,0.99998f
			,0.99999f,0.99999f,1.0f,1.0f
	};
	if (index < TABLE_SIZE)
	{
		value = lookup[index];
	}
	else
	{
		value = 0.0;
	}
	return value;
}

void FSM_PWM_ControlEvaluate(Event_PWM_Cntrl event)
{
	actualState = FSM_PWMCntrl[actualState].nextState[event];
	FSM_PWMCntrl[actualState].action();
}

void FSM_PWM_ControlSetCarrierFrequency(frequency carrierFrequency)
{
	frequencyConfiguration[FREQUENCY_CARRIER]=carrierFrequency;
}


frequency FSM_PWM_ControlGetCarrierFrequency()
{
	return frequencyConfiguration[FREQUENCY_CARRIER];
}


void FSM_PWM_ControlSetOutputFrequency(frequency outputFrequency)
{
	frequencyConfiguration[FREQUENCY_OUTPUT]=outputFrequency;
}


frequency FSM_PWM_ControlGetOutputFrequency()
{
	return frequencyConfiguration[FREQUENCY_OUTPUT];
}
void FSM_PWM_Control_InitConfiguration()
{
	FTM_ClockGatingEnable(FTM_0);
	SIM_SCGC5 =SIM_SCGC5|0x3E00; //enable port A/B/C/D/E clock
	PinConfig_PWMOutputPinInitialization();
	float initialFrequencyOutput = 60.0;//60
	float initialFrequencyCarrier = 2400.0;
	FSM_PWM_ControlSetOutputFrequency(initialFrequencyOutput);// Sets the initial output frequency.
	FSM_PWM_ControlSetCarrierFrequency(initialFrequencyCarrier);//Sets the frequency of the sampling signal. The higher the better the better resolution.
	FlexTimer_InitPWMcenterAlligned();// Configure the PWM as center alligned and two opposite pwm signals on the FTM0 channel. It doesn't start the PWM.
	FSM_PWM_Control_ConfigureParameters();//Generate the array of the duty cycles that are going to be used to generate the SPWM.
	actualState = FSM_PWM_CNTRL_STOP;//Initialization of the actual state

}


