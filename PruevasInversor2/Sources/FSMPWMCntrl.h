/*
 * FSMPWMCntrl.h
	\file
	\brief
		This is the header file of the FSMPWMCntrl driver. It contains the different functions needed
		to initialize and configure the pwm control state machine. It also has some definitions that
		are necessary to interact with the finite state machine.
	\author Erick Ortega Prudencio and Damian Garcia Serrano.
	\date	11/2/2016
 */

#ifndef SOURCES_FSMPWMCNTRL_H_
#define SOURCES_FSMPWMCNTRL_H_


/*
 * FSM_PWM_Control.h
 *
 *  Created on: Nov 16, 2016
 *      Author: operi
 */

#include "Eventos.h"
#include "DataTypeDefinitions.h"


/*
 *Internal PWM configuration  -----------------------------------------------------------------------------------------------------
 */

#define MAX_RES 300
/*
 * Defines used on the configuration of the PWM module.
 */
/*!
 	 \def FSM_PWM_A_PORT_OUT
 	 \brief Constant that indicate the port of the PWM A output.
 */
#define FSM_PWM_A_PORT_OUT GPIOC

/*!
 	 \def FSM_PWM_A_PIN_OUT
 	 \brief Constant that indicate the pin of the PWM A output.
 */
#define FSM_PWM_A_PIN_OUT 1

/*!
 	 \def FSM_PWM_B_PORT_OUT
 	 \brief Constant that indicate the port of the PWM B output.
 */
#define FSM_PWM_B_PORT_OUT GPIOC

/*!
 	 \def FSM_PWM_B_PIN_OUT
 	 \brief Constant that indicate the pin of the PWM B output.
 */
#define FSM_PWM_B_PIN_OUT 2

/*!
 * \brief Definition of the frequency variable type.
 */
typedef float frequency;
/*!
 * \brief Definition of a pointer to a function variable.
 */
typedef void(*fptrAction)();

/*!
 * \brief Definition of the enumerators that define the states of the actual machine.
 */
typedef enum
{
	FSM_PWM_CNTRL_RUN,
	FSM_PWM_CNTRL_STOP,
	FSM_PWM_CNTRL_CONFIG,
	FSM_PWM_CNTRL_MAX_ENUM
}FSM_PWMCntrlState;

/*!
 * \brief Structure of a state of the PWM control machine.
 */
typedef struct
{
	fptrAction action; //Receive the argument that is going to modify
	FSM_PWMCntrlState currentState;
	FSM_PWMCntrlState nextState[EVENT_PWM_CTRL_MAX_ENUM];
}FsmPWMCntrlType;

/*!
 	 \brief	 This function configures the pwm output pin as a pwm pin.
 	 \return void
 */
void PinConfig_PWMOutputPinSInitialization();



/*
 *Finite state machines functions -----------------------------------------------------------------------------------------------------
 */
/*!
 	 \brief	 This function configures the flex timmer module as a center alligned PWM.
 	 \return void
 */
void FSM_PWM_Control_InitConfiguration();
/*!
 	 \brief	 This function evaluates the actual state of the state machine.
 	 \return void
 */
void FSM_PWM_ControlEvaluate(Event_PWM_Cntrl event);
/*!
 	 \brief	 This function sets the current carrier frequency value only if it is an acceptable value.
 	 \return void
 */
void FSM_PWM_ControlSetCarrierFrequency(frequency carrierFrequency);
/*!
 	 \brief	 This function returns the current carrier frequency value.
 	 \return void
 */
frequency FSM_PWM_ControlGetCarrierFrequency();
/*!
 	 \brief	 This function sets the current output frequency only if it is an acceptable value.
 	 \return void
 */
void FSM_PWM_ControlSetOutputFrequency(frequency outputFrequency);
/*!
 	 \brief	 This function returns the current output frequency value
 	 \return void
 */
frequency FSM_PWM_ControlGetOutputFrequency();

//void FlexTimerFastConfig_updateSampleFrequency();
//void FlexTimerFastConfig_updateDutyCycle();

#endif /* SOURCES_FSMPWMCNTRL_H_ */
