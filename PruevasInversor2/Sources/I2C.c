/**
	\file
	\brief
		This is the source file for the I2C device driver.
		i.e., this is the application programming interface (API) for the GPIO peripheral.
	\author Erick Ortega and Damian Garcia Serrano.
	\date	7/09/2014
 */
#include "I2C.h"

static BooleanType errorFlag = FALSE;
float sysClock = 21000000; // Cabe destacar que por ahora se dejara en esta frecuencia.
float period = 10;// Solo utilizar para el debuggeo: 10;

void tms(unsigned int ms)
{
	volatile unsigned char i, k;
	volatile unsigned int ms2;
	ms2 = ms;
	while (ms2)
	{
		for (i = 0; i <= 140; i++)
		{
			k++;
		}
		ms2--;
		k++;
	}
}

 uint8 I2C_busy(I2C_ChannelType currentChannel)
 {
	switch(currentChannel)
	{
	case I2C_0:
		return I2C0_S & I2C_S_BUSY_MASK;//Escribimos el registro
		break;
	case I2C_1:
		return I2C1_S & I2C_S_BUSY_MASK;//Escribimos el registro
		break;
	case I2C_2:
		return I2C2_S & I2C_S_BUSY_MASK;//Escribimos el registro
		break;
	default:
		break;
	}
 }

 void I2C_MST_OrSLV_Mode(I2C_ChannelType currentChannel, I2C_MODE mode)//Metodo que se realiza al final (envia una senal de start en mst)
 {
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 &= ~I2C_C1_MST_MASK;// Limpiamos el registro
		I2C0_C1 |= I2C_C1_MST(mode) ;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_C1 &= ~I2C_C1_MST_MASK;// Limpiamos el registro
		I2C1_C1 |= I2C_C1_MST(mode) ;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_C1 &= ~I2C_C1_MST_MASK;// Limpiamos el registro
		I2C2_C1 |= I2C_C1_MST(mode) ;//Escribimos el registro
		break;
	default:
		break;
	}
 }

 void I2C_TX_RX_Mode(I2C_ChannelType currentChannel, I2C_COM_MODE comMode)
 {
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 &= ~I2C_C1_TX_MASK;// Limpiamos el registro
		I2C0_C1 |= I2C_C1_TX(comMode) ;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_C1 &= ~I2C_C1_TX_MASK;// Limpiamos el registro
		I2C1_C1 |= I2C_C1_TX(comMode) ;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_C1 &= ~I2C_C1_TX_MASK;// Limpiamos el registro
		I2C2_C1 |= I2C_C1_TX(comMode) ;//Escribimos el registro
		break;
	default:
		break;
	}
 }

 void I2C_NACK(I2C_ChannelType currentChannel)
 {
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 |= I2C_C1_TXAK_MASK;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_C1 |= I2C_C1_TXAK_MASK;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_C1 |= I2C_C1_TXAK_MASK;//Escribimos el registro
		break;
	default:
		break;
	}
 }

 void I2C_repeted_Start(I2C_ChannelType currentChannel)
 {
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 |= I2C_C1_RSTA_MASK;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_C1 |= I2C_C1_RSTA_MASK;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_C1 |= I2C_C1_RSTA_MASK;//Escribimos el registro
		break;
	default:
		break;
	}
 }

void I2C_write_Byte(I2C_ChannelType currentChannel, uint8 byte)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_D = I2C_D_DATA(byte)  ;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_D = I2C_D_DATA(byte)  ;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_D = I2C_D_DATA(byte)  ;//Escribimos el registro
		break;
	default:
		break;
	}
}

uint8  I2C_read_Byte(I2C_ChannelType currentChannel)
{
	switch(currentChannel)
	{
	case I2C_0:
		return I2C0_D ;
		break;
	case I2C_1:
		return I2C1_D ;
		break;
	case I2C_2:
		return I2C2_D ;
		break;
	default:
		return 0;
		break;
	}
}
void I2C_wait(I2C_ChannelType currentChannel)// El trasnfer bit no sirve por lo que utilizamos este de interrupcion
{
	switch(currentChannel)
	{
	case I2C_0:
		while(FALSE == (I2C0_S & I2C_S_IICIF_MASK));
		I2C0_S |= I2C_S_IICIF(1) ;
		I2C0_S |= I2C_S_IICIF_MASK;
		break;
	case I2C_1:
		while(FALSE == (I2C1_S & I2C_S_IICIF_MASK));
		I2C1_S |= I2C_S_IICIF(1) ;
		I2C1_S |= I2C_S_IICIF_MASK;
		break;
	case I2C_2:
		while(FALSE == (I2C2_S & I2C_S_IICIF_MASK));
		I2C2_S |= I2C_S_IICIF(1) ;
		I2C2_S |= I2C_S_IICIF_MASK;
		break;
	default:
		break;
	}
	/*
	 * Se habilita el pit delay (para comenzar la cuenta regresiva de esperar el acknowledge.
	 */
	PIT_delay(I2CPIT, sysClock, period);
}

BooleanType I2C_get_ACK(I2C_ChannelType currentChannel)//Retorna si recibio acknowladge o no
{
	BooleanType tempVal = 0;
	BooleanType tempFlagPit = (PIT_getInterruptionFlag(I2CPIT));
	switch(currentChannel)
	{
	case I2C_0:
		 return ((I2C0_S & I2C_S_RXAK_MASK) && (!tempFlagPit) );
		 break;
	case I2C_1:
		 return ((I2C1_S & I2C_S_RXAK_MASK) && (!tempFlagPit) );
		break;
	case I2C_2:
		 return ((I2C2_S & I2C_S_RXAK_MASK) && (!tempFlagPit) );
		break;
	default:
		break;
	}
}

void I2C_start(I2C_ChannelType currentChannel)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 |= I2C_C1_TX(1) ;//Escribimos el registro
		I2C0_C1 |= I2C_C1_MST_MASK;// Limpiamos el registro
		break;
	case I2C_1:
		I2C1_C1 |= I2C_C1_TX(1) ;//Escribimos el registro
		I2C1_C1 |= I2C_C1_MST_MASK;// Limpiamos el registro
		break;
	case I2C_2:
		I2C2_C1 |= I2C_C1_TX(1) ;//Escribimos el registro
		I2C2_C1 |= I2C_C1_MST_MASK;// Limpiamos el registro
		break;
	default:
		break;
	}
}

void I2C_stop(I2C_ChannelType currentChannel)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 &= ~I2C_C1_MST_MASK;// Limpiamos el registro
		break;
	case I2C_1:
		I2C1_C1 &= ~I2C_C1_MST_MASK;// Limpiamos el registro
		break;
	case I2C_2:
		I2C2_C1 &= ~I2C_C1_MST_MASK;// Limpiamos el registro
		break;
	default:
		break;
	}
}
void I2C_setMultiplierFactor(I2C_ChannelType currentChannel, uint8 multiplierFactor)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_F |= I2C_F_MULT(multiplierFactor) ;
		break;
	case I2C_1:
		I2C1_F |= I2C_F_MULT(multiplierFactor) ;
		break;
	case I2C_2:
		I2C2_F |= I2C_F_MULT(multiplierFactor) ;
		break;
	default:
		break;
	}
}
void I2C_setClockRate(I2C_ChannelType currentChannel, uint8 clockRatePrescaler)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_F |= I2C_F_ICR(clockRatePrescaler) ;
		break;
	case I2C_1:
		I2C1_F |= I2C_F_ICR(clockRatePrescaler) ;
		break;
	case I2C_2:
		I2C2_F |= I2C_F_ICR(clockRatePrescaler) ;
		break;
	default:
		break;
	}
}

//Este metodo solamente habilitara. Por el momento no deshabilitara.
void I2C_setEnableI2C(I2C_ChannelType currentChannel, BooleanType enable)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 &= ~I2C_C1_IICEN_MASK;// Limpiamos el registro
		I2C0_C1 |= I2C_C1_IICEN(enable) ;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_C1 &= ~I2C_C1_IICEN_MASK;// Limpiamos el registro
		I2C1_C1 |= I2C_C1_IICEN(enable) ;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_C1 &= ~I2C_C1_IICEN_MASK;// Limpiamos el registro
		I2C2_C1 |= I2C_C1_IICEN(enable) ;//Escribimos el registro
		break;
	default:
		break;
	}
}
void I2C_setInterruptEnable(I2C_ChannelType currentChannel, BooleanType interruptEnable)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_C1 &= ~I2C_C1_IICIE_MASK;// Limpiamos el registro
		I2C0_C1 |= I2C_C1_IICIE(interruptEnable) ;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_C1 &= ~I2C_C1_IICIE_MASK;// Limpiamos el registro
		I2C1_C1 |= I2C_C1_IICIE(interruptEnable) ;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_C1 &= ~I2C_C1_IICIE_MASK;// Limpiamos el registro
		I2C2_C1 |= I2C_C1_IICIE(interruptEnable) ;//Escribimos el registro
		break;
	default:
		break;
	}
}

void I2C_clockGating(I2C_ChannelType currentChannel)
{
	switch(currentChannel)
	{
	case I2C_0:
		SIM_SCGC4 |= SIM_SCGC4_I2C0_MASK ;
		break;
	case I2C_1:
		SIM_SCGC4 |= SIM_SCGC4_I2C1_MASK ;
		break;
	case I2C_2:
		SIM_SCGC1 |= SIM_SCGC1_I2C2_MASK ;
		break;
	default:
		break;
	}
}

void I2C_setFack(I2C_ChannelType currentChannel)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_SMB |= I2C_SMB_FACK_MASK;//Escribimos el registro
		break;
	case I2C_1:
		I2C1_SMB |= I2C_SMB_FACK_MASK;//Escribimos el registro
		break;
	case I2C_2:
		I2C2_SMB |= I2C_SMB_FACK_MASK;//Escribimos el registro
		break;
	default:
		break;
	}
}

void I2C_clearSRW(I2C_ChannelType currentChannel)
{
	switch(currentChannel)
	{
	case I2C_0:
		I2C0_S &= ~I2C_S_SRW_MASK;
		break;
	case I2C_1:
		I2C1_S &= ~I2C_S_SRW_MASK;
		break;
	case I2C_2:
		I2C2_S &= ~I2C_S_SRW_MASK;
		break;
	default:
		break;
	}
}


BooleanType getErrorFlag()
{
	if(errorFlag == TRUE)
	{
		errorFlag = FALSE;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void I2C_initAsMaster(I2CStructure* i2CStruct)
{
	I2C_clockGating(i2CStruct->channel);
	//I2C_setFack();
	I2C_TX_RX_Mode(i2CStruct->channel, i2CStruct->comMode);
	//I2C_setMultiplierFactor(i2CStruct->multiplierFactor);
	//I2C_setClockRate(i2CStruct->clockratePrescaler);
	I2C0_F = 0x56;//0x56;//0x8C;//0x56;
	I2C_setInterruptEnable(i2CStruct->channel, i2CStruct->interruptEnable);
	I2C_MST_OrSLV_Mode(i2CStruct->channel, i2CStruct->mode);
	I2C_setEnableI2C(i2CStruct->channel, TRUE);
}
