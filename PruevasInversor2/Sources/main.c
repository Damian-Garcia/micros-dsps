/**	 \file main.c
	 \brief
	 	 This is the main file for the project
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 */

#include "MK64F12.h"
#include "FSMPWMCntrl.h"
#include "ModuleConfigurations.h"
#include "MK64F12.h"
#include "DataTypeDefinitions.h"
#include "GPIO.h"
#include "UARTDriver.h"
#include "NVIC.h"
#include "MCG.h"
#include "FSM_Control.h"
#include "Eventos.h"
#include "CMP.h"


static uint32 i = 0;
static uint32 p = 0;


///Uart module for bluetooth communication
uartStruct uartBluetoothConfigStruct =
{
		UART_4,
		60000000,//21000000,
		BD_9600
};

/**
 * Selects the desired mode for the comparator.
 * It also sets an initial minimum voltage value.
 */
void Comparator_init(){
	CMP_clockGating();
	PORTC_PCR7 = PORT_PCR_MUX(0);
	CMP_DAC_setReference(CMP_0,CMP_DAC_VDD);
	CMP_DAC_setValue(CMP_0,10*1/11.0);
	CMP_DAC_enable(CMP_0,TRUE);
	CMP_MUX_selectInput(CMP_0,CMP_MUX_IN1, CMP_MUX_IN7);
	CMP_MUX_enable(CMP_0,TRUE);
	CMP_sampledFilteredModeB(CMP_0,2,60);

	CMP_clearFallingFlag(CMP_0);
	CMP_clearRisingFlag(CMP_0);
	CMP_InterruptEnable(CMP_0,FALSE, FALSE);
	CMP_clearFallingFlag(CMP_0);
	CMP_clearRisingFlag(CMP_0);
}



int main(void)
{

	/// Sets the system to a high frequency
	initializeMCGHighFrequence();

	GPIO_clockGating(GPIOC);
	GPIO_pinControlRegisterType pcrBluetooth = GPIO_MUX3;
	GPIO_pinControlRegister(GPIOC, 15, &pcrBluetooth);
	GPIO_pinControlRegister(GPIOC, 14, &pcrBluetooth);

	///Inicializacion de los modulos de UART

	UART_init(&uartBluetoothConfigStruct);
	UART_interruptEnable(UART_4);

	/// calls the comparator init function
	Comparator_init();

	/**Enables the UART 0 interrupt in the NVIC*/
	NVIC_enableInterruptAndPriotity(CMP0_IRQ, PRIORITY_10);
	NVIC_enableInterruptAndPriotity(UART4_IRQ, PRIORITY_10);
	EnableInterrupts;

	///Configures the initial values for the control FSM.
	FSM_Control_init();
	///Configures the initial values for the PWM generation FSM.
	FSM_PWM_Control_InitConfiguration();

	for (;;) {
		///pops the last user input event
		FSM_Control_Event current = getEvent();
		///calls the evaluation of the current state with the new event
		FSM_Control_eval(current);
		///pops the last control event
		Event_PWM_Cntrl evento = getEventPWMCntrl();
		///calls the evaluation of the current PWM generation state with the new control event
		FSM_PWM_ControlEvaluate(evento);
	}

    return 0;
}
///////////////////////////////////////,/////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
