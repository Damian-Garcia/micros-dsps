/**
	\file  ModuleConfigurations.h
	\brief
		Este es el archivo cabecera de la configuracion de los modulos utilizados.
		Dentro de este archivo se encuentra la configuracion e inicializacion de los distintos
		modulos, ademas de los modulos de GPIO que son utilizados por estos mismos modulos.
		Tambien contiene la especificacion de los pines que son utilizados por cada uno de los
		modulos.
		Por ultimo, este archivo contiene la funcion de inicializacion del MCG que permite
		elevar la frecuencia de operacion de la kinetis a un valor de 60 Mhz.

	\author Erick Ortega Prudencio y Damian Garcia Serrano.
	\date	10/03/2016
 */


#ifndef SOURCES_MODULECONFIGURATIONS_H_
#define SOURCES_MODULECONFIGURATIONS_H_

#include "MK64F12.h"
#include "NVIC.h"
#include "DataTypeDefinitions.h"
#include "GPIO.h"
#include "UARTDriver.h"

/*
 * Puertos y pines de los botones de seleccion ----------------------------------------------------------
 * Se definen las macros en donde se especifica cuales son los pines y los puertos que se estan
 * utilizando para la comunicacion serial por el puerto bluetooth, la comunicacion por el
 * puerto serial que se encuentra en la conexion USB que se encuentra embebida en el JTAG y
 * para la comunicacion con el puerto I2C.
 */
#define UART_SCREEN_CHANNEL UART_0
#define UART_BLUETOOTH_CHANNEL UART_4
#define I2C_MEM_SDA_PORT GPIOE
#define i2C_MEM_SDA_PIN 25

#define I2C_MEM_SCL_PORT GPIOE
#define i2C_MEM_SCL_PIN 24

#define UART_SCREEN_RX_PORT GPIOB
#define UART_SCREEN_RX_PIN 16

#define UART_SCREEN_TX_PORT GPIOB
#define UART_SCREEN_TX_PIN 17

#define UART_BLUETOOTH_TX_PORT GPIOC
#define UART_BLUETOOTH_TX_PIN 15

#define UART_BLUETOOTH_RX_PORT GPIOC
#define UART_BLUETOOTH_RX_PIN 14
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

/*!
 	 \def FSM_PORT_BTN0
 	 \brief Constant that indicate the port of the button 0
 */
#define FSM_PORT_BTN0 GPIOC
/*!
 	 \def FSM_PIN_BTN0
 	 \brief Constant that indicate the pin number of the button 0
 */
#define FSM_PIN_BTN0 0
/*!
 	 \def FSM_PORT_BTN1
 	 \brief Constant that indicate the port of the button 1
 */
#define FSM_PORT_BTN1 GPIOC
/*!
 	 \def FSM_PIN_BTN1
 	 \brief Constant that indicate the pin number of the button 1
 */
#define FSM_PIN_BTN1 1
/*!
 	 \def FSM_PORT_BTN2
 	 \brief Constant that indicate the port of the button 2
 */
#define FSM_PORT_BTN2 GPIOC
/*!
 	 \def FSM_PIN_BTN2
 	 \brief Constant that indicate the pin number of the button 2
 */
#define FSM_PIN_BTN2 2
/*!
 	 \def FSM_PORT_BTN3
 	 \brief Constant that indicate the port of the button 3
 */
#define FSM_PORT_BTN3 GPIOC
/*!
 	 \def FSM_PIN_BTN3
 	 \brief Constant that indicate the pin number of the button 3
 */
#define FSM_PIN_BTN3 3
/*!
 	 \def FSM_PORT_BTN4
 	 \brief Constant that indicate the port of the button 4
 */
#define FSM_PORT_BTN4 GPIOC
/*!
 	 \def FSM_PIN_BTN4
 	 \brief Constant that indicate the pin number of the button 4
 */
#define FSM_PIN_BTN4 4
/*!
 	 \def FSM_PORT_BTN5
 	 \brief Constant that indicate the port of the button 5
 */
#define FSM_PORT_BTN5 GPIOC
/*!
 	 \def FSM_PIN_BTN5
 	 \brief Constant that indicate the pin number of the button 5
 */
#define FSM_PIN_BTN5 5
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
/*
 * Puertos y pines de la pantalla -------------------------------------------------
 */
/*!
 	 \def SCREEN_PORT_RST
 	 \brief Constant that indicate the output port of the screen reset.
 */
#define SCREEN_PORT_RST GPIOD
/*!
 	 \def SCREEN_PIN_RST
 	 \brief Constant that indicate the output pin of the screen reset.
 */
#define SCREEN_PIN_RST 0
/*!
 	 \def SCREEN_PORT_DC
 	 \brief Constant that indicate the output port of the screen dc.
 */

#define SCREEN_PORT_DC GPIOD
/*!
 	 \def SCREEN_PIN_DC
 	 \brief Constant that indicate the output pin of the screen dc.
 */
#define SCREEN_PIN_DC 3

/*!
 	 \def SCREEN_PORT_DIN
 	 \brief Constant that indicate the output port of the screen din.
 */
#define SCREEN_PORT_DIN GPIOD
/*!
 	 \def SCREEN_PIN_DIN
 	 \brief Constant that indicate the output pin of the screen din.
 */
#define SCREEN_PIN_DIN 2
/*!
 	 \def SCREEN_PORT_CLK
 	 \brief Constant that indicate the output port of the screen clk.
 */
#define SCREEN_PORT_CLK GPIOD
/*!
 	 \def SCREEN_PIN_CLK
 	 \brief Constant that indicate the output pin of the screen clk.
 */
#define SCREEN_PIN_CLK 1
/*!
 	 \def BTNPIT
 	 \brief Constant THAT INDICATES THE PIT THAT THE BTNs are using.
 */
#define BTNPIT PIT_2
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
/*!
 	 \brief this function initializes the necessary registers to enable the I2C module to start
 	 working as soon as possible.
*/
void PinConfiguration_initializeI2CModules();
/*!
 	 \brief this function configures the UART registers used in this practice so that
 	 the kinetis can use the different UART modules.
*/
void PinConfiguration_initializeUartModules();
/*!
 	 \brief This function runs different MCG functions to enable the 60 Mhz frequence of operation
 	 of the processor.
*/
void initializeMCGHighFrequence();

/*!
 	 \brief	 This function configures the pins of the push buttons connected (6 push buttons.).
 	 \return void
 */
void PinConfig_BtnInputPinsInitialization();

/*!
 	 \brief	 This function configures the interruption of the port where the push buttons are connected.
 	 Tambien realiza la inicializacion del pit timmer 2.
 	 \return void
 */
void PinConfig_BtnInterruptPortInitialization();
/*
 * Realiza la inicializacion de la pantalla LCD.
 */
void PinConfig_ScreenInitialization();

#endif /* SOURCES_MODULECONFIGURATIONS_H_ */
