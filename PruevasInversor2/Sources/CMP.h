/**	 \file CMP.h
	 \brief
	 	 This is the header file for the Internal Comparator Driver
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 */
#ifndef SOURCES_CMP_H_
#define SOURCES_CMP_H_

#include "DataTypeDefinitions.h"

/**
 * \brief enum for the possible comparator module configurations
 */
typedef enum{
	CMP_MODE_DISABLED,          //!< CMP_MODE_DISABLED
	CMP_MODE_CONTINUOU0S_A, 	///not recommended
	CMP_MODE_CONTINUOU0S_B, 	///not recommended
	CMP_MODE_SAMPLED_NOFILTER_A,//!< CMP_MODE_SAMPLED_NOFILTER_A
	CMP_MODE_SAMPLED_NOFILTER_B,//!< CMP_MODE_SAMPLED_NOFILTER_B
	CMP_MODE_SAMPLED_FILTER_A,  //!< CMP_MODE_SAMPLED_FILTER_A
	CMP_MODE_SAMPLED_FILTER_B,  //!< CMP_MODE_SAMPLED_FILTER_B
	CMP_MODE_WINDOWED_A,        //!< CMP_MODE_WINDOWED_A
	CMP_MODE_WINDOWED_B,        //!< CMP_MODE_WINDOWED_B
	CMP_MODE_WINDOWED_RESAMPLED,//!< CMP_MODE_WINDOWED_RESAMPLED
	CMP_MODE_WINDOWED_FILTERED, //!< CMP_MODE_WINDOWED_FILTERED
}CMP_Mode;

/**
 * \brief enum type for the comparator mux inputs
 */
typedef enum{
	CMP_MUX_IN0,//!< CMP_MUX_IN0
	CMP_MUX_IN1,//!< CMP_MUX_IN1
	CMP_MUX_IN2,//!< CMP_MUX_IN2
	CMP_MUX_IN3,//!< CMP_MUX_IN3
	CMP_MUX_IN4,//!< CMP_MUX_IN4
	CMP_MUX_IN5,//!< CMP_MUX_IN5
	CMP_MUX_IN6,//!< CMP_MUX_IN6
	CMP_MUX_IN7,//!< CMP_MUX_IN7
}CMP_Mux_Input;


/**
 * enum type for the 6 bit DAC within the comparator
 */
typedef enum{
	CMP_DAC_VREFOUT,//!< CMP_DAC_VREFOUT
	CMP_DAC_VDD     //!< CMP_DAC_VDD
}CMP_DAC_Reference;

/**
 * enum for the internal comparator in K64.
 */
typedef enum{
	CMP_0,//!< CMP_0
	CMP_1 //!< CMP_1
}CMP_NameType;

void CMP_clockGating();

/**
 * \brief enables/disables cmp module interrupts
 * \param CMP_NameType
 * \param raising
 * \param falling
 * \return
 */
BooleanType CMP_InterruptEnable(CMP_NameType, BooleanType raising, BooleanType falling);

/**
 * \brief enables the internal MUX references
 * \param CMP_NameType
 * \param BooleanType
 * \return BooleanType
 */
BooleanType CMP_MUX_enable(CMP_NameType, BooleanType);
/**
 * \brief selects the inputs for a given comparator
 * \param CMP_NameType
 * \param plusInput
 * \param minusInput
 * \return BooleanType
 */
BooleanType CMP_MUX_selectInput(CMP_NameType, CMP_Mux_Input plusInput, CMP_Mux_Input minusInput);

/**
 * \brief enables the 6 bit dac
 * \param CMP_NameType
 * \param BooleanType
 * \return BooleanType
 */
BooleanType CMP_DAC_enable(CMP_NameType, BooleanType);
/**
 * \brief sets the internal DAC reference voltage
 * \param CMP_NameType
 * \param  CMP_DAC_Reference
 * \return BooleanType
 */
BooleanType CMP_DAC_setReference(CMP_NameType, CMP_DAC_Reference);
/**
 * \brief sets the DAC output value
 * \param CMP_NameType
 * \param voltage
 * \return BooleanType
 */
BooleanType CMP_DAC_setValue(CMP_NameType, float voltage);


///devuelve la salida del comparador
BooleanType CMP_comparatorOutput(CMP_NameType);
///Devuelve 1 si ha ocurrido un flanco de subida a la salida del comp.
BooleanType CMP_risignFlag(CMP_NameType);
///Clears the rising edge interrupt flag
BooleanType CMP_clearRisingFlag(CMP_NameType);
///Devuelve 1 si ha ocurrido un flanco de bajada a la salida del comp.
BooleanType CMP_fallingFlag(CMP_NameType);
///Clears the falling edge interrupt flag
BooleanType CMP_clearFallingFlag(CMP_NameType);
///Sets the disable mode for a comparator
BooleanType CMP_disable(CMP_NameType);
///Sets the continuous mode for a comparator. Not recommended mode.
BooleanType CMP_continuousModeA(CMP_NameType);
///Sets the continuous mode for a comparator. Not recommended mode.
BooleanType CMP_continuousModeB(CMP_NameType);

///Sets the sampled Mode
BooleanType CMP_sampledNonFilteredModeA(CMP_NameType);
///Sets the sampled Mode. Specifies a filter period
BooleanType CMP_sampledNonFilteredModeB(CMP_NameType, uint8 filterPeriod);
///Sets the sampled filtered Mode. Specifies a filter count.
BooleanType CMP_sampledFilteredModeA(CMP_NameType, uint8 filterCount);
///Sets the sampled filtered Mode. Specifies a filter count and a filter period.
BooleanType CMP_sampledFilteredModeB(CMP_NameType, uint8 filterCount, uint8 filterPeriod);
///Sets the windowed mode.
BooleanType CMP_windowedModeA(CMP_NameType);
///Sets the windowed mode.
BooleanType CMP_windowedModeB(CMP_NameType);
///Sets tje windowed resampled mode.
BooleanType CMP_windowedResampledMode(CMP_NameType, uint8 filterPeriod);
/// Sets the windowed filtered mode.
BooleanType CMP_windowedFilteredMode(CMP_NameType, uint8 filterCount, uint8 filterPeriod);

#endif /* SOURCES_CMP_H_ */
