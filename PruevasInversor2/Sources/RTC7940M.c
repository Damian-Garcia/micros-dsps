/**	 \file RTC7940M.c
	 \brief
	 	 Este es el archivo fuente del driver del RTC que se comunica con la tarjeta kinetis
	 	 por medio del protocolo de I2C. Este archivo contiene la definicion de las funciones
	 	 que permiten la escritura de la fecha, lectura de la fecha, escritura de la hora y
	 	 lectura de la fecha.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	11/2/2016
 */
#include "RTC7940M.h"
#include "Formatter.h"

#define SYSTEMCLK 21000000
#define CONTROLCODE 1010
#define MEMADDRESS 0001
#define ADDRSRTCCONTROL 0b11011110

static uint8 amPmFormatEnabled = 0;
static uint8 pmEnabled;
static uint8 secondsDigitsArray[2];
static uint8 minutesDigitsArray[2];
static uint8 hoursDigitsArray[2];
static uint8 dayDigitsArray[2];
static uint8 monthsDigitsArray[2];
static uint8 yearsDigitsArray[2];
static uint8 centuryYear[2];//Dos digitos del century year;
static uint8 lastYear; //Cada vez que este sea diferente al anio actual entonces se incrementara century year
static uint8 centuryYearData;
static BooleanType errorRtcFlag = FALSE;

static I2CStructure rtcStruct=
{
		I2C_0,
		I2C_MASTER,
		I2C_TX_MODE,
		TRUE,
		SYSTEMCLK,
		100961,
		0x15,
		0x2
};

BooleanType getErrorRtcFlag()
{
	return errorRtcFlag;
}

void RTC_configureMemoryConfiguration()
{
	I2C_initAsMaster(&rtcStruct);
}

void RTC_writeByteToMemoryAddress(uint8 byteData, uint8 memoryAddress)
{
	errorRtcFlag = FALSE;
	tms(100);
	uint8 addressByte = ADDRSRTCCONTROL;
	/*
	 * Escritura del control de escritura de datos en el RTC ------------------------------------------
	 */
	I2C_start(rtcStruct.channel);
	I2C_write_Byte(rtcStruct.channel, addressByte);
	I2C_wait(rtcStruct.channel);
	while(I2C_get_ACK(rtcStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorRtcFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(rtcStruct.channel);
		return ;
	}	//write of the I2C  memory address
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la direccion  ---------------------------------------------------------------------
	 */

	I2C_write_Byte(rtcStruct.channel, memoryAddress);
	I2C_wait(rtcStruct.channel);
	while(I2C_get_ACK(rtcStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorRtcFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(rtcStruct.channel);
		return ;
	}	//write of the I2C  memory byte
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de los datos----------------------------------------------------------------------
	 */
	I2C_write_Byte(rtcStruct.channel, byteData);
	I2C_wait(rtcStruct.channel);
	while(I2C_get_ACK(rtcStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorRtcFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(rtcStruct.channel);
		return ;
	}	//Detenemos la transmision de datos.
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	I2C_stop(rtcStruct.channel);
}

uint8 RTC_readByteFromMemoryAddress( uint8 memoryAddress)
{
	errorRtcFlag = FALSE;
	tms(100);
	I2C_TX_RX_Mode(rtcStruct.channel, I2C_TX_MODE);
	uint8 datosleidos = 0;
	uint8 addressByte1 = ADDRSRTCCONTROL;
	uint8 addressByte2 = addressByte1|1;
	/*
	 * Escritura del control de escritura de datos en el RTC ------------------------------------------
	 */

	I2C_start(rtcStruct.channel);
	I2C_write_Byte(rtcStruct.channel, addressByte1);
	I2C_wait(rtcStruct.channel);
	while(I2C_get_ACK(rtcStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorRtcFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(rtcStruct.channel);
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la direccion  ---------------------------------------------------------------------
	 */

	I2C_write_Byte(rtcStruct.channel, memoryAddress);
	I2C_wait(rtcStruct.channel);
	while(I2C_get_ACK(rtcStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorRtcFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(rtcStruct.channel);
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de control para lectura  ---------------------------------------------------------------------
	 */
	I2C_repeted_Start(rtcStruct.channel);
	I2C_write_Byte(rtcStruct.channel, addressByte2);
	I2C_wait(rtcStruct.channel);
	while(I2C_get_ACK(rtcStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorRtcFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(rtcStruct.channel);
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	I2C_TX_RX_Mode(rtcStruct.channel, I2C_RX_MODE);
	/*
	 * Lectura de datos ------------------------------------------------------------------------
	 */
	datosleidos= I2C_read_Byte(rtcStruct.channel);
	I2C_NACK(rtcStruct.channel);
	I2C_wait(rtcStruct.channel);
	I2C_stop(rtcStruct.channel);
	datosleidos= I2C_read_Byte(rtcStruct.channel);
	return datosleidos;
}

void RTC_updateSecondsArray()
{
	uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_SECONDS);
	uint8 seconds =(dataReceived & 0b01111111);
	formatBCDint(seconds, secondsDigitsArray, 2);
}

void RTC_updateMinutesArray()
{
	uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_MINUTES);
	uint8 minutes =(dataReceived & 0b01111111);
	formatBCDint(minutes, minutesDigitsArray, 2);
}

void RTC_updateHoursArray()
{
	if(amPmFormatEnabled)//Si el formato de am pm esta habilitado
	{
		uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_HOURS);
		pmEnabled = (dataReceived & 0b0100000)>>5;
		uint8 hours =(dataReceived & 0b011111);
		formatBCDint(hours, hoursDigitsArray, 2);
	}
	else//Si el formato de 24 horas esta habilitado.
	{
		uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_HOURS);
		uint8 hours =(dataReceived & 0b0111111);
		formatBCDint(hours, hoursDigitsArray, 2);
	}
}

void RTC_updateDaysArray()
{
	uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_DAY);
	uint8 days =(dataReceived); //Los bits no implementados se leen como cero
	formatBCDint(days, dayDigitsArray, 2);
}

void RTC_updateMonthsArray()
{
	uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_MONTH);
	uint8 months =(dataReceived & 0b011111); //Los bits no implementados se leen como cero
	formatBCDint(months, monthsDigitsArray, 2);
}

void RTC_updateYearsArray()
{
	uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_YEAR);
	uint8 year =(dataReceived); //Los bits no implementados se leen como cero
	formatBCDint(year, yearsDigitsArray, 2);
	if(lastYear != year)
	{
		centuryYearData++;
	}
	formatBCDint(centuryYearData, centuryYear, 2);

}

void RTC_initializeDate(dateStruct* dateStruct)
{
	RTC_writeByteToMemoryAddress((0b01111111 & dateStruct->initialMinutes), ADDRS_MINUTES);
	if(TRUE == dateStruct->initialAmpmFormatEnable)
	{
		amPmFormatEnabled = 1;
		pmEnabled = 1;
		RTC_writeByteToMemoryAddress((0b1000000 | ((dateStruct->initialAmDisable << 5) | dateStruct->initialHours)), ADDRS_HOURS);
	}
	else
	{
		amPmFormatEnabled = 0;
		pmEnabled = 0;
		RTC_writeByteToMemoryAddress((0b0111111 & dateStruct->initialHours), ADDRS_HOURS);
	}
	RTC_writeByteToMemoryAddress(dateStruct->initialDay, ADDRS_DAY);
	RTC_writeByteToMemoryAddress(dateStruct->initialMonth, ADDRS_MONTH);
	RTC_writeByteToMemoryAddress(dateStruct->initialYear, ADDRS_YEAR);
	centuryYearData = dateStruct->initialCenturyYear;
	lastYear = dateStruct->initialYear;

	RTC_writeByteToMemoryAddress((dateStruct->initialSeconds | 0x80), ADDRS_SECONDS);
}

void RTC_writeSeconds(uint8 bcdDigit)
{
	RTC_writeByteToMemoryAddress((0x80 | bcdDigit), ADDRS_SECONDS);
}

void RTC_writeMinutes(uint8 bcdDigit)
{
	if(bcdDigit > 0)
	{
		// Deshabilitamos temporalmente los segundos
		uint8 actualSeconds = RTC_readByteFromMemoryAddress(ADDRS_SECONDS);
		RTC_writeByteToMemoryAddress(0x00, ADDRS_SECONDS);

		//Escribimos el dato que esta siendo modificado
		RTC_writeByteToMemoryAddress((0b01111111 & bcdDigit), ADDRS_MINUTES);

		//Volvemos a habilitar el reloj externo y ponemos los segundos denuevo.
		RTC_writeSeconds(actualSeconds);
	}

}

void RTC_writeHours(uint8 bcdDigit)
{
	// Deshabilitamos temporalmente los segundos
	uint8 actualSeconds = RTC_readByteFromMemoryAddress(ADDRS_SECONDS);
	RTC_writeByteToMemoryAddress(0x00, ADDRS_SECONDS);
	// Actualizamos el pm enabled para saber si si estamos en am o pm  y despues lo escribimos devuelta como llego
	if(amPmFormatEnabled)//Si el formato de am pm esta habilitado
	{
		uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_HOURS);
		pmEnabled = (dataReceived & 0b0100000)>>5;
	}
	else//Si el formato de 24 horas esta habilitado.
	{
		//uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_HOURS);
	}

	if(amPmFormatEnabled)//Si nos encontramos en formato am o pm lo que hacemos es habilitar el bit de formato
	{
		RTC_writeByteToMemoryAddress((0b1000000 | ((pmEnabled << 5) | (0b011111 &bcdDigit))), ADDRS_HOURS);
	}
	else//Si nos encontramos en formato de 24 horas entonces lo escribimos tal cual
	{
		RTC_writeByteToMemoryAddress((0b0111111 & bcdDigit), ADDRS_HOURS);
	}
	//Volvemos a habilitar el reloj externo y ponemos los segundos denuevo.
	RTC_writeSeconds(actualSeconds);
}

void RTC_writeDay(uint8 bcdDigit)
{
	// Deshabilitamos temporalmente los segundos
	uint8 actualSeconds = RTC_readByteFromMemoryAddress(ADDRS_SECONDS);
	RTC_writeByteToMemoryAddress(0x00, ADDRS_SECONDS);

	RTC_writeByteToMemoryAddress(bcdDigit, ADDRS_DAY);

	//Volvemos a habilitar el reloj externo y ponemos los segundos denuevo.
	RTC_writeSeconds(actualSeconds);
}

void RTC_writeMonth(uint8 bcdDigit)
{
	// Deshabilitamos temporalmente los segundos
	uint8 actualSeconds = RTC_readByteFromMemoryAddress(ADDRS_SECONDS);
	RTC_writeByteToMemoryAddress(0x00, ADDRS_SECONDS);

	RTC_writeByteToMemoryAddress(bcdDigit, ADDRS_MONTH);

	//Volvemos a habilitar el reloj externo y ponemos los segundos denuevo.
	RTC_writeSeconds(actualSeconds);

}

void RTC_writeYear(uint16 bcdDigit)//Recibe cuatro digitos, de los cuales solamente escribira 2 en RTC y 2 en software
{
	lastYear = 0x00FF & bcdDigit;
	// Deshabilitamos temporalmente los segundos
	uint8 actualSeconds = RTC_readByteFromMemoryAddress(ADDRS_SECONDS);
	RTC_writeByteToMemoryAddress(0x00, ADDRS_SECONDS);
	RTC_writeByteToMemoryAddress((uint8)(0x00FF & bcdDigit), ADDRS_YEAR);
	centuryYear[0] = (0x0F &(bcdDigit>>8));
	centuryYear[1] = (0x0F &(bcdDigit>>12));
	//Volvemos a habilitar el reloj externo y ponemos los segundos denuevo.
	RTC_writeSeconds(actualSeconds);
}

void RTC_ampmEnableFormat(BooleanType value)//Falta todavia de realizar completa y correctamente
{

	if((TRUE == value) && (amPmFormatEnabled == TRUE))
	{
		return;
	}
	else if ((TRUE == value) && (amPmFormatEnabled == FALSE))//Convertimos las horas a formato de am pm
	{
		uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_HOURS);
		//pmEnabled = (dataReceived & 0b0100000);//Esta en alto cuando es pm.
		uint8 hours =convertBCDtoBinary(dataReceived & 0b011111);//Solamente el BCD de las horas
		hours = convert_format_24_12(hours);
		hours = convertBinaryToBCD(hours);
		//RTC_writeHours(pmEnabled(hours));		//RTC_writeByteToMemoryAddress((0b1000000 | ((dateStruct->initialAmDisable << 5) | dateStruct->initialHours)), ADDRS_HOURS);
		RTC_writeByteToMemoryAddress((0b1000000 | ((pmEnabled << 5) | (0b011111 & hours))), ADDRS_HOURS);

	}
	else if((FALSE == value) && (amPmFormatEnabled == FALSE))
	{
		return;
	}
	else if((FALSE == value) && (amPmFormatEnabled == TRUE))//Convertimos a formato de 24 hrs
	{
		uint8 dataReceived = RTC_readByteFromMemoryAddress(ADDRS_HOURS);
		pmEnabled = (dataReceived & 0b0100000)>>5;//Esta en alto cuando es pm.
		uint8 hours =convertBCDtoBinary(dataReceived & 0b011111);//obtenemos horas
		hours = convert_format_12_24(hours);
		hours = convertBinaryToBCD(hours);
		RTC_writeByteToMemoryAddress((0b0111111 & hours), ADDRS_HOURS);

	}
}

uint8 RTC_getSecondsOnesAscii()
{
	RTC_updateSecondsArray();
	return secondsDigitsArray[1];
}

uint8 RTC_getSecondsTensAscii()
{
	RTC_updateSecondsArray();
	return secondsDigitsArray[0];
}

uint8 RTC_getMinutesOnesAscii()
{
	RTC_updateMinutesArray();
	return minutesDigitsArray[1];
}

uint8 RTC_getMinutesTensAscii()
{
	RTC_updateMinutesArray();
	return minutesDigitsArray[0];
}

uint8 RTC_getHoursOnesAscii()
{
	RTC_updateHoursArray();
	return hoursDigitsArray[1];
}

uint8 RTC_getHoursTensAscii()
{
	RTC_updateHoursArray();
	return hoursDigitsArray[0];
}

uint8 RTC_getDayOnesAscii()
{
	RTC_updateDaysArray();
	return dayDigitsArray[1];
}

uint8 RTC_getDayTensAscii()
{
	RTC_updateDaysArray();
	return dayDigitsArray[0];
}

uint8 RTC_getMonthOnesAscii()
{
	RTC_updateMonthsArray();
	return monthsDigitsArray[1];
}

uint8 RTC_getMonthTensAscii()
{
	RTC_updateMonthsArray();
	return monthsDigitsArray[0];
}

uint8 RTC_getYearOnesAscii()
{
	RTC_updateYearsArray();
	return yearsDigitsArray[1];
}

uint8 RTC_getYearTensAscii()
{
	RTC_updateYearsArray();
	return yearsDigitsArray[0];
}

uint8 RTC_getYearCentsOnesAscii()
{
	RTC_updateYearsArray();
	return centuryYear[1];

}

uint8 RTC_getYearCentsTensAscii()
{
	RTC_updateYearsArray();
	return centuryYear[0];
}

uint8 convertBinaryToBCD(uint8 data)
{
	uint8 result = 0;
	int i= 0;
	while(data>0)
	{
		i++;
		result<<=4;
		result |= data %10;
		data /= 10;
	}
	if(i==2)
	{
		uint8 temp = (0xF0 & result)>>4;
		result= ((0x0F & result)<<4)|temp;

	}
	return result;
}

uint8 convertBCDtoBinary(uint8 data)
{
	uint8 dec=0;
	uint8 mult=1;
	int i = 0;
	for(mult = 1; data; data = data>>4, mult*=10)
	{
		dec += (data & 0x0f) *mult;
	}
	return dec;
}

uint8 convert_format_12_24(uint8 hours)
{
	if(pmEnabled)
	{
		return hours+12;
	}
	else
	{
		return hours;
	}
}

uint8 convert_format_24_12(uint8 hours)
{
	amPmFormatEnabled = TRUE;
	if(hours>=13)
	{
		pmEnabled = TRUE;
		return hours-12;
	}
	else
	{
		pmEnabled = FALSE;

		if(hours<1)
		{
			return hours+12;
		}
		else
		{
			return hours;
		}
	}
}

BooleanType RTC_isAMPMenabled()
{
	return amPmFormatEnabled;
}

BooleanType RTC_isPMactive()
{
	return pmEnabled;
}

//La parte mas significativa es la que se encuentra en la posicion mas baja del buffer
void RTC_setFullHour(uint8* buffer)//Recibe en formato en horas, minutos y segundos
{
	RTC_writeHours(buffer[0]);
	RTC_writeMinutes(buffer[1]);
	RTC_writeSeconds(buffer[2]);
}

//La parte mas significativa (dias)es la que se encuentra en la parte mas baja del buffer
void RTC_setFullDate(uint16* buffer)//Recibe el formato en dias, meses, anios
{
	RTC_writeDay(buffer[0]);
	RTC_writeMonth(buffer[1]);
	RTC_writeYear(buffer[2]);
}
