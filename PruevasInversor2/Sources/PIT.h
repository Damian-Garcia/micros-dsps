/**
 \file PIT.h
 \brief
 This is the header file for the PIT driver.
 It contains public functions to interface with the PIT Module.
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	9/28/2016
 */
#ifndef PIT_H_
#define PIT_H_
#include "DataTypeDefinitions.h"
#include "MK64F12.h"


//! \def The Base frequency of the system
#define SYSTEM_CLOCK 21000000

/*! \typedef PIT_TimerType
 *	\brief This enumerated type is used to select the PIT to be used
 */
typedef enum {PIT_0,PIT_1,PIT_2,PIT_3} PIT_TimerType;

/*! \typedef PIT_TimerType
 *	\brief This enumerated type is used to select the state of the interrupt and enable flags
 */
typedef enum {PIT_DISABLE, PIT_ENABLE} PIT_StatusType;

/*!	\typedef PIT_MCRConfig
 */
typedef struct PIT_MCR_Config {
	PIT_StatusType mdis;	///< Module disable bit
	PIT_StatusType freeze;	///< Freeze timer bit
} PIT_MCRConfig;

/*!	\typedef PIT_ChainMode
 * \brief type for the PIT chained mode
 */
typedef enum {NOTCHAINED, CHAINED} PIT_ChainMode;

/*! \struct PIT_TCRTRLn_Config
 * 	\typedef PIT_TCRConfig
 * 	\brief Type structure containing a generic configuration register for a PIT timer
 */
typedef struct PIT_TCRTRLn_Config {
	PIT_TimerType pitTimer;					///< PIT timer to be configured
	PIT_ChainMode chainMode;				///< Chain mode bit
	PIT_StatusType timerInterruptEnable;	///< Bit for enabling PIT interrupts
	PIT_StatusType timerEnable;				///< Bit for enabling a PIT timer
} PIT_TCRConfig;

/*!
  	\fn void PIT_configureMCRRegister(PIT_MCRConfig pitMCR);
 	\brief	This function returns the state of an interrupt flag of a PIT timer.
 	It does not return the TIF flag of a PIT TCR, but a local variable instead.
 	\param[in]  pitMCR of interest.
 	\return PITn_Flag
 */
void PIT_configureMCRRegister(PIT_MCRConfig pitMCR);

/*!
  	\fn void PIT_ClockGatingEnable();
 	\brief	This function configures the clock gating for the PIT module.
	\return void
 */
void PIT_ClockGatingEnable();

/*!
 * 	\fn void PIT_ClockGatingDisable();
 * 	\brief This function turns off the clock gating for the PIT module.
 * 	\return void.
 */
void PIT_ClockGatingDisable();

/*!
 * \fn uint32_t PIT_currentTimmerValueRegister(PIT_TimerType pitTimer);
 * \brief Returns the actual count value of a PIT Timer
 * @param pitTimer PIT timer count value to be read.
 * @return the actual count of the selected PIT channel
 */
uint32_t PIT_currentTimmerValueRegister(PIT_TimerType pitTimer);

/*!
 * \fn void PIT_setLDVALn(PIT_TimerType pitTimer, uint32_t value);
 * \brief This functions sets the LDVAL register of the selected PIT timer.
 * The LDVAL selects the number of counts before an interrupt.
 * @param pitTimer Selects the LDVAL of the corresponding pitTimer.
 * @param value	The number of counts before an Interrupt.
 */
void PIT_setLDVALn(PIT_TimerType pitTimer, uint32_t value);

/*!
 * \fn void PIT_setTimeDelay(PIT_TimerType pitTimer, float systemClock, float period);
 * \brief This functions calculates the value of the LDVAL to generate a specific delay.
 * @param pitTimer The pitTimer to be used
 * @param systemClock The clock rate of the Board
 * @param period	The desired interrupt period
 */
void PIT_setTimeDelay(PIT_TimerType pitTimer, float systemClock, float period);

/*!
 * \fn void PIT_configureTimmerControlRegister(PIT_TCRConfig pitTCRConfig);
 * \brief This function configures the TCTRL register of a timer using the defined structure
 * @param pitTCRConfig
 */
void PIT_configureTimmerControlRegister(PIT_TCRConfig pitTCRConfig);

/*!
 * \fn uint32_t PIT_isEnabled(PIT_TimerType pitTimer);
 *
 * @param pitTimer PIT Timer of interest
 * @return 0 if either the interrupt enable or timer run are cleared, different from 0 otherwise.
 */
uint32_t PIT_isEnabled(PIT_TimerType pitTimer);

/*!
 * \fn void PIT_setEnable(PIT_TimerType pitTimer);
 * \brief This function sets the Timer Enable flag in the TCTRL register of a PIT timer
 * @param pitTimer PIT timer to be enabled.
 */
void PIT_setEnable(PIT_TimerType pitTimer);

/*!
 * \fn void PIT_clearEnable(PIT_TimerType pitTimer);
 * \brief This function clears the Timer Enable flag in the TCTRL register of a PIT timer
 * @param pitTimer PIT timer to be disabled.
 */
void PIT_clearEnable(PIT_TimerType pitTimer);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function configure the PIT to generate a delay base on the system clock.
 	 Internally it configures the clock gating and enables the PIT module.
 	 It is important to note that this strictly is not device driver since everything is
 	 contained in a single function, but in general you have to avoid this practices, this only
 	 for the propose of the homework.
 	 Deprecated

 	 \param[in]  pitTimer pit to be configured
 	 \param[in]  systemClock to be taken.
 	 \param[in]  period Period to be configured
 	 \return void
 */
void PIT_delay(PIT_TimerType pitTimer,float systemClock ,float period);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \fn void PIT_CLOCKGATING();
 	 \deprecated This is a deprecated method
 	 \brief	 This function configures the clock gating for the PIT module.
 	 \return void
 */
void PIT_CLOCKGATING();

/*!
  	\fn uint8_t PIT_getInterruptionFlag(PIT_TimerType pitTimer);
 	\brief	This function returns the state of an interrupt flag of a PIT timer.
 	It does not return the TIF flag of a PIT, but a local variable instead
 	\param[in]  pitTimer of interest.
 	\return PITn_Flag
 */
uint8_t PIT_getInterruptionFlag(PIT_TimerType pitTimer);


#endif /* PIT_H_ */
