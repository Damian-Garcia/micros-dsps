/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK64F12.h"
#include "GPIO.h"
#include "DataTypeDefinitions.h"

#define SYSTEM_CLOCK 21000000

void delay_mS(uint16 time, uint32 systemClock);
void delay_cycles(uint32 cycles);

int main(void)
{

	GPIO_clockGating(GPIOC);
	GPIO_pinControlRegisterType sal = GPIO_MUX1;
	GPIO_pinControlRegister(GPIOC, 3, &sal);
	GPIO_dataDirectionPIN(GPIOC,GPIO_OUTPUT, 3);
	for (;;) {
		GPIO_tooglePIN(GPIOC,3);
		delay_mS(500, SYSTEM_CLOCK);
		GPIO_tooglePIN(GPIOC,3);
		delay_mS(400, SYSTEM_CLOCK);
	}
    /* Never leave main */
    return 0;
}

void delay_mS(uint16 time, uint32 systemClock){
	uint32 ciclos;
	ciclos = (time)*(systemClock/(2000 * 2));
	delay_cycles(ciclos);
}

//void delay_cycles(uint32 cycles){
//	volatile int i;
//	for(i = 0; i<cycles; i++){
//	}
//}

register int rb __asm ("r5");
//extern void _add();
//extern void asm_test();

//void func(register unsigned int result1, register unsigned int result2, register unsigned int result3);

void delay_cycles(uint32_t cycles)
{
	__asm ("mov %0, %1" : "=r" (rb) : "r" (cycles));				//test = rb = 6


	//"mov %0, %1" : "r" (rb) : "r" (cycles)

	__asm("DECREMENTO:");
	__asm("SUB r5, r5, #0x1\n\t");
	__asm("CBZ r5, END");
	__asm("B DECREMENTO");
	__asm("END:");


}
