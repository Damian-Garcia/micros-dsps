/*
 * DAC.h
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_DAC_H_
#define SOURCES_DAC_H_
#include "MK64F12.h"
#include "DataTypeDefinitions.h"

typedef enum
{
	DAC_0, /*!< Definition to select DAC 0*/
	DAC_1, /*!< Definition to select DAC 1 */
} DAC_name;
typedef enum
{
	DAC_REF1, /*!< Definition to select DAC 0*/
	DAC_REF2, /*!< Definition to select DAC 1 */
}DAC_referenceType;
typedef enum
{
	DAC_HARDWARETRIGGER, /*!< Definition to select DAC 0*/
	DAC_SOFTWARETRIGGER, /*!< Definition to select DAC 1 */
}DAC_triggerType;
typedef enum
{
	DAC_HIPWR, /*!< Definition to select DAC 0*/
	DAC_LWPWR, /*!< Definition to select DAC 1 */
}DAC_PowerType;
typedef enum
{
	DAC_DISABLE, /*!< Definition to select DAC 0*/
	DAC_ENABLE, /*!< Definition to select DAC 1 */
}DAC_status;
typedef enum
{
  DAC_INVALIDTRG,
  DAC_VALIDTRG,
}DAC_softwareTrigger;

typedef struct DAC_Configuration_C0 {
	DAC_name dacName;
	DAC_referenceType dacReference;
	DAC_triggerType dacTrigger;
	DAC_PowerType dacPower;
	DAC_status topPtrInterrupt;
	DAC_status bottomPtrInterrupt;
} DAC_ConfigurationC0;
typedef enum
{
  DAC_NORMAL,
  DAC_ONETIMESCAN,
}DAC_BufferMode;

typedef struct DAC_Configuration_C1 {
	DAC_name dacName;
	DAC_BufferMode dacBufferMode;
	DAC_status dacDmaStatus;
	DAC_status dacBufferEnable;
} DAC_ConfigurationC1;

BooleanType DAC_ClockGatingDisable(DAC_name dacName);
BooleanType DAC_clockGatingEnable(DAC_name dacName);
void DAC_setDACx_DATnL(DAC_name dacName, uint8_t index, uint8_t value);
void DAC_setDACx_DATnH(DAC_name dacName, uint8_t index, uint8_t value);
void DAC_setDACx_DATnValue(DAC_name dacName, uint8_t index, uint16_t value);
BooleanType DACx_SRreadTopPositionFlag(DAC_name dacName);//returns DACBFRPTF
BooleanType DACx_SRreadBottomPositionFlag(DAC_name dacName);//returns DACBFRPBF
void DACx_SRclearTopPositionFlag(DAC_name dacName);
void DACx_SRclearBottomPositionFlag(DAC_name dacName);
void DAC_ControlRegisterZeroConfig(DAC_ConfigurationC0 dacConfigC0);
void DAC_EnableDACx(DAC_name dacName);
void DAC_DisableDACx(DAC_name dacName);
void DAC_EnableRegister(DAC_name dacName, DAC_status dacStatus);
void DAC_SoftTriggerValidDACx(DAC_name dacName);
void DAC_SoftTriggerInvalidDACx(DAC_name dacName);
void DAC_SoftwareTriggerRegister(DAC_name dacName, DAC_status dacStatus);
void DAC_ControlRegisterOneConfig(DAC_ConfigurationC1 dacConfigC1);
uint8_t DAC_getDACBufferPointer(DAC_name dacName);
void DAC_setDACBufferPointer(DAC_name dacName, uint8_t bfrPtr);
void DAC_setBufferUpperLimit(DAC_name dacName, uint8_t bfrPtrLimit);

#endif /* SOURCES_DAC_H_ */
