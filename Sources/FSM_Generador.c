/*
 * FSM_Generador.c
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia y Erick Ortega Prudencio
 */
#include "FSM_Generador.h"

static uint8_t genCounter = 0;
static uint16_t squareValue[]={};
static uint16_t sineValue[]={};
static uint16_t triangularValue[]={};
/*
typedef struct
{
	uint8_t actionArgument[]; //Indicates which is going to be the paramter of the action
	void(*fptrAction)(uint32_t *);//Receive the argument that is going to modify
	uint8 nextEstate[8];
}FsmGenStateType;
*/

const FsmGenStateType FSMGenMoore[8]=
{
{squareValue,FSM_GeneratorNoAction,{FSM_GN_IDLE, FSM_GN_IDLE_ACTIVE, FSM_GN_IDLE, FSM_GN_IDLE}},//FSM_GN_IDLE
{squareValue,FSM_GeneratorNoAction,{FSM_GN_IDLE_ACTIVE, FSM_GN_IDLE_ACTIVE, FSM_GN_SQUARE, FSM_GN_IDLE_ACTIVE}},//FSM_GN_IDLE_ACTIVE
{squareValue,FSM_GeneratorVoltageOutput,{FSM_GN_SQUARE, FSM_GN_IDLE,FSM_GN_SINE,FSM_GN_SQUARE_NEXT}},//FSM_GN_SQUARE
{ &genCounter, FSM_GeneratorIncrementCounter,{ FSM_GN_SQUARE, FSM_GN_IDLE, FSM_GN_SINE, FSM_GN_SQUARE }},//FSM_GN_SQUARE_NEXT
{sineValue,FSM_GeneratorVoltageOutput,{FSM_GN_SINE, FSM_GN_IDLE, FSM_GN_TRIANGLE,FSM_GN_SINE_NEXT}},//FSM_GN_SINE
{&genCounter,FSM_GeneratorIncrementCounter,{ FSM_GN_SINE, FSM_GN_IDLE, FSM_GN_TRIANGLE, FSM_GN_SINE }},//FSM_GN_SINE_NEXT
{triangularValue,FSM_GeneratorVoltageOutput,{ FSM_GN_TRIANGLE, FSM_GN_IDLE, FSM_GN_IDLE, FSM_GN_TRIANGLE_NEXT}},//FSM_GN_TRIANGLE
{&genCounter,FSM_GeneratorIncrementCounter,{ FSM_GN_TRIANGLE, FSM_GN_IDLE, FSM_GN_IDLE, FSM_GN_TRIANGLE }}//FSM_GN_TRIANGLE_NEXT
};
void FSM_GeneratorInitializeProcess(uint16_t* nothing)
{
	/*
	 * Initialization of the PIT clock, GPIO pin for the output of the voltage.
	 */


}

void FSM_GeneratorVoltageOutput(uint16_t* array)
{

}
void FSM_GeneratorNoAction(uint16_t * nothing)
{

}
void FSM_GeneratorIncrementCounter(uint16_t* counter)
{

}


