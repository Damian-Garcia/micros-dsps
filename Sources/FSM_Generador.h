/*
 * FSM_Generador.h
 *
 *  Created on: Sep 22, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_FSM_GENERADOR_H_
#define SOURCES_FSM_GENERADOR_H_
#include "DataTypeDefinitions.h"
#include "MK64F12.h"

/*
 * Structure of a state on the machine. Each state knows the next state (depending on the input) and also
 * has an action to perform in the current state.
 */
typedef enum
{
	FSM_GN_IDLE,
	FSM_GN_IDLE_ACTIVE,
	FSM_GN_SQUARE,
	FSM_GN_SQUARE_NEXT,
	FSM_GN_SINE,
	FSM_GN_SINE_NEXT,
	FSM_GN_TRIANGLE,
	FSM_GN_TRIANGLE_NEXT,
}FSM_GeneratorState;

typedef struct
{
	uint8_t* actionArgument; //Indicates which is going to be the paramter of the action
	void(*fptrAction)(uint16_t *);//Receive the argument that is going to modify
	uint8 nextEstate[4];
}FsmGenStateType;

void FSM_GeneratorInitializeProcess(uint16_t* nothing);
void FSM_GeneratorVoltageOutput(uint16_t* array);
void FSM_GeneratorNoAction(uint16_t * nothing);
void FSM_GeneratorIncrementCounter(uint16_t* counter);


#endif /* SOURCES_FSM_GENERADOR_H_ */
