/*
 * PIT.h
 * Periodic interrupt timmer
 *  Created on: Oct, 23, 2016
 *      Author: Erick Ortega
 */
#ifndef PIT_H_
#define PIT_H_
#include "DataTypeDefinitions.h"
#include "MK64F12.h"

/*! This enumerated constant are used to select the PIT to be used*/
typedef enum {PIT_0,PIT_1,PIT_2,PIT_3} PIT_TimerType;
typedef enum {PIT_DISABLE, PIT_ENABLE} PIT_StatusType;
typedef struct PIT_MCR_Config {
	PIT_StatusType mdis;
	PIT_StatusType freeze;
} PIT_MCRConfig;
typedef enum {NOTCHAINED, CHAINED} PIT_ChainMode;
typedef struct PIT_TCRTRLn_Config {
	PIT_TimerType pitTimer;
	PIT_ChainMode chainMode;
	PIT_StatusType timerInterruptEnable;
	PIT_StatusType timerEnable;
} PIT_TCRConfig;

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function configure the PIT to generate a delay base on the system clock.
 	 Internally it configures the clock gating and enables the PIT module.
 	 It is important to note that this strictly is not device driver since everything is 
 	 contained in a single function, but in general you have to avoid this practices, this only
 	 for the propose of the homework
 	 	  	 	 
 	 \param[in]  portName Port to be configured.
 	 \return void
 */

uint8_t PIT_getInterruptionFlag(PIT_TimerType pitTimer);
void PIT_configureMCRRegister(PIT_MCRConfig pitMCR);
void PIT_ClockGatingEnable();
void PIT_ClockGatingDisable();
uint32_t PIT_currentTimmerValueRegister(PIT_TimerType pitTimer);
void PIT_setLDVALn(PIT_TimerType pitTimer, uint32_t value);
void PIT_setTimeDelay(PIT_TimerType pitTimer, float systemClock, float period);
void PIT_configureTimmerControlRegister(PIT_TCRConfig pitTCRConfig);


/*
 * Deprecated functions of the PIT module.
 */
void PIT_delay(PIT_TimerType pitTimer,float systemClock ,float period);
void PIT_CLOCKGATING();
#endif /* PIT_H_ */
