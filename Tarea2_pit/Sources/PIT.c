/*
 * PITC.c
 *
 *  Created on: Sep 8, 2016
 *      Author: Damian Garcia
 */

#include "MK64F12.h"
#include "PIT.h"

uint8 PIT0_Flag;
void PIT0_IRQHandler()
{
	PIT0_Flag = TRUE;
	PIT_TFLG0 |= PIT_TFLG_TIF_MASK; // clears timer 0 flag
	PIT_TCTRL0; //read control register for clear PIT flag, this is silicon bug
	PIT_TCTRL0 &= ~(PIT_TCTRL_TIE_MASK);//disables PIT timer interrupt
	PIT_TCTRL0 &= ~(PIT_TCTRL_TEN_MASK);//disables timer0

}
void PIT_delay(PIT_TimerType pitTimer, float systemClock, float period) {
	uint32 cuentas = systemClock*period/2 - 1; //
	PIT_MCR = 0; // Clears Freeze , Activates module's clock
	switch (pitTimer) {
	case PIT_0:					//selects right PIT timer
		PIT_LDVAL0 = cuentas;				//sets the timer start value
		PIT_TCTRL0 = PIT_TCTRL_TIE_MASK;	//sets the timer interrupt enable
		PIT_TCTRL0 |= PIT_TCTRL_TEN_MASK;	//sets the timer run enable
		break;
	case PIT_1:
		PIT_LDVAL1 = cuentas;
		PIT_TCTRL1 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL1 |= PIT_TCTRL_TEN_MASK;
		break;
	case PIT_2:
		PIT_LDVAL2 = cuentas;
		PIT_TCTRL2 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL2 |= PIT_TCTRL_TEN_MASK;
		break;
	case PIT_3:
		PIT_LDVAL3 = cuentas;
		PIT_TCTRL3 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL3 |= PIT_TCTRL_TEN_MASK;
		break;
	default:
		break;
	}

}
