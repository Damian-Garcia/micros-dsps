/**
	\file 
	\brief 
		This is a starter file to implement a function able to produce an accurate delay
		using the PIT module. 

	\author J. Luis Pizano Escalante, luispizano@iteso.mx
	\date	7/09/2014
 */

#include "MK64F12.h"
#include "PIT.h"
#include "NVIC.h"
#include "GPIO.h"
#define SYSTEM_CLOCK 21000000
#define DELAY 0.00050 //0.01785//0.25

extern uint8 PIT0_Flag;

int main(void)
{
	GPIO_pinControlRegisterType pinControlRegisterPORTD = GPIO_MUX1;
	SIM_SCGC6|= 0x800000;
	GPIO_clockGating(GPIOD);
	GPIO_pinControlRegister(GPIOD,BIT0,&pinControlRegisterPORTD);
	GPIO_dataDirectionPIN(GPIOD,GPIO_OUTPUT,BIT0);
	GPIO_setPIN(GPIOD,BIT0);
    GPIO_tooglePIN(GPIOD,BIT0);
   	GPIO_tooglePIN(GPIOD,BIT0);
	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_CH0_IRQ, PRIORITY_5);
	EnableInterrupts;
	
	for(;;) {	   

	   		GPIO_tooglePIN(GPIOD,BIT0);
	   	 	PIT0_Flag = FALSE;
	   	 	PIT_delay(PIT_0,SYSTEM_CLOCK,DELAY);
	   	 	while(FALSE == PIT0_Flag);
	   		GPIO_tooglePIN(GPIOD,BIT0);
	   		PIT0_Flag = FALSE;
	   		PIT_delay(PIT_0,SYSTEM_CLOCK,DELAY);
	   		while(FALSE == PIT0_Flag);
	}
	
	return 0;
}
