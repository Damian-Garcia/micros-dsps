/**
	\file
	\brief
		This is the source file for the GPIO device driver for Kinetis K64.
		It contains all the implementation for configuration functions and runtime functions.
		i.e., this is the application programming interface (API) for the GPIO peripheral.
	\author J. Luis Pizano Escalante, luispizano@iteso.mx
	\date	7/09/2014
	\todo
	    Interrupts are not implemented in this API implementation.
 */
#include "MK64F12.h"
#include "GPIO.h"



uint8 GPIO_clockGating(GPIO_portNameType portName)
{
	/*!
	 * \dot
	digraph G {
	main -> parse -> execute;
	main -> init;
	main -> cleanup;
	execute -> make_string;
	execute -> printf
	init -> make_string;
	main -> printf;
	execute -> compare;
	 }
	 \enddot
	*/
	switch(portName)/** Selecting the GPIO for clock enabling*/
			{
				case GPIOA: /** GPIO A is selected*/
					SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTA; /** Bit 9 of SIM_SCGC5 is  set*/
					break;
				case GPIOB: /** GPIO B is selected*/
					SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTB; /** Bit 10 of SIM_SCGC5 is set*/
					break;
				case GPIOC: /** GPIO C is selected*/
					SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTC; /** Bit 11 of SIM_SCGC5 is set*/
					break;
				case GPIOD: /** GPIO D is selected*/
					SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTD; /** Bit 12 of SIM_SCGC5 is set*/
					break;
				case GPIOE: /** GPIO E is selected*/
					SIM_SCGC5 |= GPIO_CLOCK_GATING_PORTE; /** Bit 13 of SIM_SCGC5 is set*/
					break;
				default: /**If doesn't exist the option*/
					return(FALSE);
			}// end switch
	/**Successful configuration*/
	return(TRUE);
}// end function

uint8 GPIO_pinControlRegister(GPIO_portNameType portName,uint8 pin,const GPIO_pinControlRegisterType*  pinControlRegister)
{

	switch(portName)
		{
		case GPIOA:/** GPIO A is selected*/
			PORTA_PCR(pin)= *pinControlRegister;
			break;
		case GPIOB:/** GPIO B is selected*/
			PORTB_PCR(pin)= *pinControlRegister;
			break;
		case GPIOC:/** GPIO C is selected*/
			PORTC_PCR(pin)= *pinControlRegister;
			break;
		case GPIOD:/** GPIO D is selected*/
			PORTD_PCR(pin)= *pinControlRegister;
			break;
		case GPIOE: /** GPIO E is selected*/
			PORTE_PCR(pin)= *pinControlRegister;
		default:/**If doesn't exist the option*/
			return(FALSE);
		break;
		}
	/**Successful configuration*/
	return(TRUE);
}

void GPIO_writePORT(GPIO_portNameType portName, uint32 Data );
uint32 GPIO_readPORT(GPIO_portNameType portName);
uint8 GPIO_readPIN(GPIO_portNameType portName, uint8 pin);
void GPIO_setPIN(GPIO_portNameType portName, uint8 pin){
	uint32 valor = (1<<pin);
	switch(portName){
	case GPIOA:
		GPIOA_PSOR |= valor;
		break;
	case GPIOB:
		GPIOB_PSOR |= valor;
		break;
	case GPIOC:
		GPIOC_PSOR |= valor;
		break;
	case GPIOD:
		GPIOD_PSOR |= valor;
		break;
	case GPIOE:
		GPIOE_PSOR |= valor;
		break;
	}
}
void GPIO_clearPIN(GPIO_portNameType portName, uint8 pin);
void GPIO_tooglePIN(GPIO_portNameType portName, uint8 pin){
	uint32 valor;
	valor = (1<<pin);
	switch(portName){
	case GPIOA:
		GPIOA_PTOR |= valor;
		break;
	case GPIOB:
			GPIOB_PTOR |= valor;
			break;
	case GPIOC:
			GPIOC_PTOR |= valor;
			break;
	case GPIOD:
			GPIOD_PTOR |= valor;
			break;
	case GPIOE:
			GPIOE_PTOR |= valor;
			break;
	default:
			break;
	}
}
void GPIO_dataDirectionPORT(GPIO_portNameType portName ,uint32 direction);

void GPIO_dataDirectionPIN(GPIO_portNameType portName, uint8 State, uint8 pin){
	uint32 valor = (1<<pin);
	switch(portName){
	case GPIOA:
		GPIOA_PDDR |= valor;
		break;
	case GPIOB:
			GPIOB_PDDR |= valor;
			break;
	case GPIOC:
			GPIOC_PDDR |= valor;
			break;
	case GPIOD:
			GPIOD_PDDR |= valor;
			break;
	case GPIOE:
			GPIOE_PDDR |= valor;
			break;
	default:
			break;
	}
}




