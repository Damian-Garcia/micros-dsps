/*
 * Eventos.h
 *
 *  Created on: Sep 23, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_EVENTOS_H_
#define SOURCES_EVENTOS_H_

#include "DataTypeDefinitions.h"

typedef enum{
	EVENT_PWM_CTRL_NOP,
	EVENT_PWM_CTRL_RUN,
	EVENT_PWM_CTRL_STOP,
	EVENT_PWM_CTRL_CONFIG,
	EVENT_PWM_CTRL_MAX_ENUM
}Event_PWM_Cntrl;


void eventos_init(void);
Event_PWM_Cntrl getEventPWMCntrl();
void addEventPWMCntrl(Event_PWM_Cntrl e);

#endif /* SOURCES_EVENTOS_H_ */
