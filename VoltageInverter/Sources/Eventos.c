/*
 * Eventos.c
 *
 *  Created on: Sep 23, 2016
 *      Author: Damian Garcia
 */

#include "Eventos.h"
struct {
	uint8 run : 1;
	uint8 stop: 1;
	uint8 config:1;
} Event_Struct_PwmCntrl;


void eventos_init(void)
{
	Event_Struct_PwmCntrl.config = 0;
	Event_Struct_PwmCntrl.run = 0;
	Event_Struct_PwmCntrl.stop = 0;
}

Event_PWM_Cntrl getEventPWMCntrl()
{
	if(Event_Struct_PwmCntrl.stop)
	{
		return EVENT_PWM_CTRL_STOP;
	}
	else if(Event_Struct_PwmCntrl.run)
	{
		return EVENT_PWM_CTRL_RUN;
	}
	else if(Event_Struct_PwmCntrl.config)
	{
		return EVENT_PWM_CTRL_CONFIG;
	}
	return EVENT_PWM_CTRL_NOP;
}



void addEventPWMCntrl(Event_PWM_Cntrl e)
{
	if(EVENT_PWM_CTRL_RUN == e)
	{
		Event_Struct_PwmCntrl.run = 1;
	}
	else if(EVENT_PWM_CTRL_STOP == e)
	{
		Event_Struct_PwmCntrl.stop = 1;
	}
	else if(EVENT_PWM_CTRL_CONFIG == e)
	{
		Event_Struct_PwmCntrl.config = 1;
	}
}
