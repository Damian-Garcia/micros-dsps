/*
 * UARTDriver.c
	\file
	\brief
		This is the source file of the UART driver. It contains the different functions needed
		to initialize and configure a Uart channel of the Kinetis. It also contains the structures
		that facilitate the reading of the data that is received by the UART.
	\author Erick Ortega and Damian Garcia Serrano.
	\date	11/2/2016
 */
#include "UARTDriver.h"

UART_MailBoxType UART0_MailBox =
{
		0,0
};
UART_MailBoxType UART1_MailBox =
{
		0,0
};
UART_MailBoxType UART2_MailBox =
{
		0,0
};
UART_MailBoxType UART3_MailBox =
{
		0,0
};
UART_MailBoxType UART4_MailBox =
{
		0,0
};
UART_MailBoxType UART5_MailBox =
{
		0,0
};

void UART0_RX_TX_IRQHandler(void)
{
	 while (!(UART0_S1 & UART_S1_RDRF_MASK));
	 UART0_MailBox.mailBox = UART0_D;
	 UART0_MailBox.flag = 1;
}
void UART1_RX_TX_IRQHandler(void)
{
	 while (!(UART1_S1 & UART_S1_RDRF_MASK));
	 UART1_MailBox.mailBox = UART1_D;
	 UART1_MailBox.flag = 1;
}
void UART2_RX_TX_IRQHandler(void)
{
	 while (!(UART2_S1 & UART_S1_RDRF_MASK));
	 UART2_MailBox.mailBox = UART2_D;
	 UART2_MailBox.flag = 1;
}
void UART3_RX_TX_IRQHandler(void)
{
	 while (!(UART3_S1 & UART_S1_RDRF_MASK));
	 UART3_MailBox.mailBox = UART3_D;
	 UART3_MailBox.flag = 1;
}
void UART4_RX_TX_IRQHandler(void)
{
	 while (!(UART4_S1 & UART_S1_RDRF_MASK));
	 UART4_MailBox.mailBox = UART4_D;
	 UART4_MailBox.flag = 1;
}
void UART5_RX_TX_IRQHandler(void)
{
	 while (!(UART5_S1 & UART_S1_RDRF_MASK));
	 UART5_MailBox.mailBox = UART5_D;
	 UART5_MailBox.flag = 1;
}

//
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It configures the UART to be used
 	 \param[in]  uartChannel indicates which UART will be used.
 	 \param[in]  systemClk indicates the MCU frequency.
 	 \param[in]  baudRate sets the baud rate to transmit.
 	 \return void
 */
void UART_init(uartStruct* uartConfigStruct)
{
	int sbr = (uartConfigStruct->systemClk)/(16*uartConfigStruct->baudRate);
	double brfd = (uartConfigStruct->systemClk)/(16*uartConfigStruct->baudRate)-sbr;
	int brfa = (brfd*32.0);
	UART_ClockGating(uartConfigStruct->uartChannel);
	switch(uartConfigStruct->uartChannel)
	{
	case UART_0:
		UART0_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
		UART0_BDH = UART_BDH_SBR((sbr>>8));
		UART0_BDL = UART_BDL_SBR((0xFF&sbr));
		UART0_C4 |= UART_C4_BRFA(brfa);
		UART0_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
		break;
	case UART_1:
		UART1_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
		UART1_BDH = UART_BDH_SBR((sbr>>8));
		UART1_BDL = UART_BDL_SBR((0xFF&sbr));
		UART1_C4 |= UART_C4_BRFA(brfa);
		UART1_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
		break;
	case UART_2:
		UART2_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
		UART2_BDH = UART_BDH_SBR((sbr>>8));
		UART2_BDL = UART_BDL_SBR((0xFF&sbr));
		UART2_C4 |= UART_C4_BRFA(brfa);
		UART2_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
		break;
	case UART_3:
		UART3_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
		UART3_BDH = UART_BDH_SBR((sbr>>8));
		UART3_BDL = UART_BDL_SBR((0xFF&sbr));
		UART3_C4 |= UART_C4_BRFA(brfa);
		UART3_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
		break;
	case UART_4:
		UART4_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
		UART4_BDH = UART_BDH_SBR((sbr>>8));
		UART4_BDL = UART_BDL_SBR((0xFF&sbr));
		UART4_C4 |= UART_C4_BRFA(brfa);
		UART4_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
		break;
	case UART_5:
		UART5_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
		UART5_BDH = UART_BDH_SBR((sbr>>8));
		UART5_BDL = UART_BDL_SBR((0xFF&sbr));
		UART5_C4 |= UART_C4_BRFA(brfa);
		UART5_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
		break;
	}

}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 enables the RX UART interrupt). This function should include the next sentence:
 	 while (!(UART0_S1 & UART_S1_RDRF_MASK)). It is to guaranty that the incoming data is complete
 	 when reception register is read. For more details see chapter 52 in the kinetis reference manual.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \return void
 */
void UART_interruptEnable(UART_ChannelType uartChannel)
{
	switch(uartChannel)
	{
	case UART_0:
		UART0_C2 |= UART_C2_RIE_MASK;
		break;
	case UART_1:
		UART1_C2 |= UART_C2_RIE_MASK;
		break;
	case UART_2:
		UART2_C2 |= UART_C2_RIE_MASK;
		break;
	case UART_3:
		UART3_C2 |= UART_C2_RIE_MASK;
		break;
	case UART_4:
		UART4_C2 |= UART_C2_RIE_MASK;
		break;
	case UART_5:
		UART5_C2 |= UART_C2_RIE_MASK;
		break;
	}
}

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It sends one character through the serial port. This function should include the next sentence:
 	 while(!(UART0_S1 & UART_S1_TC_MASK)). It is to guaranty that before to try to transmit a byte, the previous
 	 one was transmitted. In other word, to avoid to transmit data while the UART is busy transmitting information.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \param[in]  character to be transmitted.
 	 \return void
 */

void UART_putChar (UART_ChannelType uartChannel, uint8 character)
{
	switch(uartChannel)
	{
	case UART_0:
		while(!(UART0_S1 & UART_S1_TC_MASK));
		UART0_D  = character;
		break;
	case UART_1:
		while(!(UART1_S1 & UART_S1_TC_MASK));
		UART1_D  = character;
		break;
	case UART_2:
		while(!(UART2_S1 & UART_S1_TC_MASK));
		UART2_D  = character;
		break;
	case UART_3:
		while(!(UART3_S1 & UART_S1_TC_MASK));
		UART3_D  = character;
		break;
	case UART_4:
		while(!(UART4_S1 & UART_S1_TC_MASK));
		UART4_D  = character;
		break;
	case UART_5:
		while(!(UART5_S1 & UART_S1_TC_MASK));
		UART5_D  = character;
		break;
	}
}
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It sends a string character through the serial port.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \param[in]  string pointer to the string to be transmitted.
 	 \return void
 */
void UART_putString(UART_ChannelType uartChannel, sint8* string)
{
	sint8 * puntero = string;
	while(*puntero)
	{
		UART_putChar(uartChannel, *puntero);
		puntero++;
	}
}
void UART_ClockGating(UART_ChannelType uartChannel)
{
	switch(uartChannel)
	{
	case UART_0:
		SIM_SCGC4 |= SIM_SCGC4_UART0_MASK;
		break;
	case UART_1:
		SIM_SCGC4 |= SIM_SCGC4_UART1_MASK;
		break;
	case UART_2:
		SIM_SCGC4 |= SIM_SCGC4_UART2_MASK;
		break;
	case UART_3:
		SIM_SCGC4 |= SIM_SCGC4_UART3_MASK;
		break;
	case UART_4:
		SIM_SCGC1 |= SIM_SCGC1_UART4_MASK;
		break;
	case UART_5:
		SIM_SCGC1 |= SIM_SCGC1_UART5_MASK;
		break;
	}
}
BooleanType UART_getMailboxFlag(UART_ChannelType uartChannel)
{
	switch(uartChannel)
	{
	case UART_0:
		if(UART0_MailBox.flag == TRUE)
		{
			UART0_MailBox.flag =0;
			return TRUE;
		}
		else
		{
			return FALSE;
		}

		break;
	case UART_1:
		if(UART1_MailBox.flag == TRUE)
		{
			UART1_MailBox.flag =0;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		break;
	case UART_2:
		if(UART2_MailBox.flag == TRUE)
		{
			UART2_MailBox.flag =0;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		break;
	case UART_3:
		if(UART3_MailBox.flag == TRUE)
		{
			UART3_MailBox.flag =0;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		break;
	case UART_4:
		if(UART4_MailBox.flag == TRUE)
		{
			UART4_MailBox.flag =0;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		break;
	case UART_5:
		if(UART5_MailBox.flag == TRUE)
		{
			UART5_MailBox.flag =0;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		break;
	}
}
uint8 UART_getMailboxContent(UART_ChannelType uartChannel)
{
	switch(uartChannel)
	{
	case UART_0:
		return UART0_MailBox.mailBox;
		break;
	case UART_1:
		return UART1_MailBox.mailBox;
		break;
	case UART_2:
		return UART2_MailBox.mailBox;
		break;
	case UART_3:
		return UART3_MailBox.mailBox;
		break;
	case UART_4:
		return UART4_MailBox.mailBox;
		break;
	case UART_5:
		return UART5_MailBox.mailBox;
		break;
	}
}


