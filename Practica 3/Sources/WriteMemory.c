/*
 * WriteMemory.c
 *
 *  Created on: Oct 31, 2016
 *      Author: Damian Garcia
 */
#include "WriteMemory.h"
#include "EEPROM24LC256.h"
#include "Formatter.h"
typedef void(*action)(void);

struct FSM_WriteMemory_StateType {
	action a;
	const FSM_WriteMemory_StateEnumType *nextList;
};

static FSM_WriteMemory_InstanceType fsm;
static BooleanType busyFlag;
static uint8 lengthDireccion;
static uint8 lastChar;
static uint8 bufferDireccion[10];
static uint8 bufferMensaje[100];
static uint8 lengthMensaje;

struct FSM_ReadTime_StateType {
	action a;
	const FSM_WriteMemory_StateEnumType *nextList;
};

static void Idle(){

}

static void waitAddress(){

}

static void caracterDireccion(){
	bufferDireccion[lengthDireccion++] = lastChar;
	bufferDireccion[lengthDireccion]  = 0;
	fsm.current = FSM_WRITEMEMORY_WAIT_ADDRESS;
	UART_putChar(fsm.uart,lastChar);
}

static void mensajeMensaje(){
	UART_putString(fsm.uart,"Texto a guardar");
	fsm.current = FSM_WRITEMEMORY_WAIT_MENSAJE;
}

static void waitMensaje(){

}


static void caracterMensaje(){
	bufferMensaje[lengthMensaje++] = lastChar;
	bufferMensaje[lengthMensaje] = 0;
	fsm.current = FSM_WRITEMEMORY_WAIT_MENSAJE;
	UART_putChar(fsm.uart,lastChar);
}

static void escribirMemoria(){
	UART_putString(fsm.uart,"c");
	uint16 address = convertAsciiAddressToHexadecimalBCDWithNumberOfDigits(bufferDireccion, lengthDireccion);
	if(address > 0x7FFF)
	{
		UART_putString(fsm.uart, " Direccion invalida");
	}
	else
	{
	writeStreamofBytesToMemoryAddress(bufferMensaje, address, lengthMensaje);

	if( FALSE == getErrorMemoryFlag())
	{
		UART_putString(fsm.uart, "Su texto ha sido cambiado");
		fsm.current = FSM_WRITEMEMORY_WAIT_ESC;
	}else{
		UART_putString(fsm.uart, "Su texto ha sido fracasado en ser escrito, como usted :( ");
		fsm.current = FSM_WRITEMEMORY_WAIT_ESC;
	}
	fsm.current = FSM_WRITEMEMORY_WAIT_ESC;
	}
}


static void error(){

}

static void waitEsc(){

}

static void end(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
}

static const FSM_WriteMemory_StateEnumType IdleNextList[FSM_WRITEMEMORY_ENUM_MAX] = {
		FSM_WRITEMEMORY_WAIT_ADDRESS, //char
		FSM_WRITEMEMORY_WAIT_ADDRESS, //esc
		FSM_WRITEMEMORY_WAIT_ADDRESS, //enter
		FSM_WRITEMEMORY_WAIT_ADDRESS, //desconexion
		FSM_WRITEMEMORY_WAIT_ADDRESS, //nop
};

static const FSM_WriteMemory_StateEnumType WaitAddressNextList[FSM_WRITEMEMORY_ENUM_MAX] = {
		FSM_WRITEMEMORY_ADDRESS, 		//
		FSM_WRITEMEMORY_END,			//
		FSM_WRITEMEMORY_MENSAJE_MENSAJE, //
		FSM_WRITEMEMORY_ERROR,			//
		FSM_WRITEMEMORY_WAIT_ADDRESS,	//
};

static const FSM_WriteMemory_StateEnumType addressNextList[FSM_WRITEMEMORY_ENUM_MAX] = {

};

static const FSM_WriteMemory_StateEnumType mensajeMensajeNextList[FSM_WRITEMEMORY_ENUM_MAX] = {

};

static const FSM_WriteMemory_StateEnumType WaitMensajeNextList[FSM_WRITEMEMORY_ENUM_MAX] = {
		FSM_WRITEMEMORY_MENSAJE,
		FSM_WRITEMEMORY_END,
		FSM_WRITEMEMORY_ESCRIBIR,
		FSM_WRITEMEMORY_ERROR,
		FSM_WRITEMEMORY_WAIT_MENSAJE,
};

static const FSM_WriteMemory_StateEnumType mensajeNextList[FSM_WRITEMEMORY_ENUM_MAX] = {
};

static const FSM_WriteMemory_StateEnumType escribirNextList[FSM_WRITEMEMORY_ENUM_MAX] = {
		FSM_WRITEMEMORY_WAIT_ESC,		//
		FSM_WRITEMEMORY_WAIT_ESC,		//
		FSM_WRITEMEMORY_WAIT_ESC,		//
		FSM_WRITEMEMORY_WAIT_ESC,		//
		FSM_WRITEMEMORY_WAIT_ESC,		//
};

static const FSM_WriteMemory_StateEnumType endNextList[FSM_WRITEMEMORY_ENUM_MAX] = {
	FSM_WRITEMEMORY_IDLE,	//
	FSM_WRITEMEMORY_IDLE,	//
	FSM_WRITEMEMORY_IDLE,	//
	FSM_WRITEMEMORY_IDLE,	//
	FSM_WRITEMEMORY_IDLE,	//
};

static const FSM_WriteMemory_StateEnumType errorNextList[FSM_WRITEMEMORY_ENUM_MAX]  = {
		FSM_WRITEMEMORY_WAIT_ESC,			//
		FSM_WRITEMEMORY_WAIT_ESC,			//
		FSM_WRITEMEMORY_WAIT_ESC,			//
		FSM_WRITEMEMORY_WAIT_ESC,			//
		FSM_WRITEMEMORY_WAIT_ESC,			//
};

static const FSM_WriteMemory_StateEnumType waitEscNextList[FSM_WRITEMEMORY_ENUM_MAX]  = {
		FSM_WRITEMEMORY_WAIT_ESC,	//
		FSM_WRITEMEMORY_END,			//
		FSM_WRITEMEMORY_WAIT_ESC,	//
		FSM_WRITEMEMORY_WAIT_ESC,	//
		FSM_WRITEMEMORY_WAIT_ESC,	//
};

static const struct FSM_WriteMemory_StateType Estados[FSM_WRITEMEMORY_ENUM_MAX] = {
	{Idle, IdleNextList},
	{waitAddress, WaitAddressNextList},
	{caracterDireccion, addressNextList},
	{mensajeMensaje, mensajeMensajeNextList},
	{waitMensaje, WaitMensajeNextList},
	{caracterMensaje, mensajeNextList},
	{escribirMemoria, escribirNextList},
	{error, errorNextList},
	{waitEsc, waitEscNextList},
	{end, endNextList}
};


void WriteMemory_init(UART_ChannelType uart){
	busyFlag = TRUE;
	lengthDireccion = 0;
	lengthMensaje = 0;
	fsm.uart = uart;
	fsm.current = FSM_WRITEMEMORY_IDLE;
	fsm.enable = TRUE;
}


void WriteMemory_eval(FSM_Escribir_Memoria_EVT_Type evt){
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}


BooleanType WriteMemory_isBusy(){
	return busyFlag;
}

BooleanType WriteMemory_isEnabled(){
	return fsm.enable;
}


void WriteMemory_addChar(uint8 c){
	lastChar = c;
}

void WriteMemory_clearBusy(){
	busyFlag = FALSE;
}
