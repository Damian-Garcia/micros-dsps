/*
 * FIFO.c
 *
 *  Created on: Nov 3, 2016
 *      Author: Damian Garcia
 */
#include "FIFO.h"

void FIFO_init(FIFO_type* f) {
	f->count = 0;
	f->emptyFlag = TRUE;
}

void FIFO_push(FIFO_type* f, uint8 c) {
	if(f->count > FIFO_SIZE)
		return;
	f->arreglo[f->count] = c;
	(f->count)++;
	f->emptyFlag = FALSE;
}

uint8 FIFO_pop(FIFO_type* f) {
	uint8 i;
	if (!f->emptyFlag && FIFO_SIZE > i) {
		f->count--;
		f->emptyFlag = (f->count == 0)? TRUE: FALSE;
		uint8 t =f->arreglo[0];
		for(i = 0 ; i <= f->count; i++){
			f->arreglo[i] = f->arreglo[i+1];
		}
		return t;
	}
}

BooleanType FIFO_isEmpty(FIFO_type* f) {
	return f->emptyFlag;
}

void FIFO_flush(FIFO_type* f) {
	f->count = 0;
	f->emptyFlag = TRUE;
}
