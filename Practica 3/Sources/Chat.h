/*
 * Chat.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_CHAT_H_
#define SOURCES_CHAT_H_

#include "DataTypeDefinitions.h"
#include "UARTDriver.h"
#include "Eventos.h"

typedef enum{
	CHAT_IDLE,
	CHAT_ESTABLECER,
	CHAT_ESPERAR,
	CHAT_ALMACENAR,
	CHAT_ENVIAR,
	CHAT_SALIR,
}Chat_Estado_EnumType;

void Chat_addCliente(void *);
void Chat_addChar(UART_ChannelType, uint8);
void Chat_addEvt(UART_ChannelType, Chat_EVT_EnumType);
void Chat_init();
void Chat_eval();

#endif /* SOURCES_CHAT_H_ */
