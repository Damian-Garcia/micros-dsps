/**	 \file RTC7940M.h
	 \brief
	 	 Este es el archivo cabecerea del driver del RTC que se comunica con la tarjeta kinetis
	 	 por medio del protocolo de I2C. Este archivo contiene la declaracion de las funciones
	 	 que permiten la escritura de la fecha, lectura de la fecha, escritura de la hora y
	 	 lectura de la fecha.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	11/2/2016
 */
#ifndef SOURCES_RTC7940M_H_
#define SOURCES_RTC7940M_H_

/*
 * Macros de definicion que contienen la direccion donde se encuentran contenidos las
 * horas, minutos, segundos, dias, meses y anios dentro del RTC.
 */
#define ADDRS_SECONDS 0
#define ADDRS_MINUTES 1
#define ADDRS_HOURS 2
#define ADDRS_DAY 4
#define ADDRS_MONTH 5
#define ADDRS_YEAR 6
#define ADDRS_CENTURYYEAR 0x20

#include "I2C.h"

/*! \typedef ADC_Config
 *	\brief Structure used to configure the date and time of the Real time clock.
*/
typedef struct DateStruct
{
	uint8 initialSeconds;
	uint8 initialMinutes;
	uint8 initialHours;
	uint8 initialDay;
	uint8 initialMonth;
	uint8 initialYear;
	uint8 initialCenturyYear;//Century year se recibe en decimal. El resto en hexadecimal.
	uint8 initialAmpmFormatEnable;
	uint8 initialAmDisable;
}dateStruct;

/*!
 	 \brief Esta funcion retorna el valor actual de la bandera de reloj del modulo
 	 del real time clock. Esta bandera es verdadera si hubo un error en la lectura
 	 o en la escritura del RTC.
*/
BooleanType getErrorRtcFlag();
/*!
 	 \brief Esta funcion escribe un byte especificado en la memoria especificada en el
 	 Real time clock.
*/
void RTC_writeByteToMemoryAddress(uint8 byteData, uint8 memoryAddress);
/*!
 	 \brief Esta funcion retorna el valor contenido en la direccion especificada del real time
 	 clock. Este valor puede es un byte.
*/
uint8 RTC_readByteFromMemoryAddress( uint8 memoryAddress);

/*!
 	 \brief Esta funcion Realiza la configuracion del modulo I2C para la transmision de datos.
 	 Ademas tambien configura al Modulo como maestro para que se encargue de la realizacion
 	 del albitraje de la comunicacion.
*/
void RTC_configureMemoryConfiguration();

/*!
 	 \brief Esta funcion toma una estructura que contiene tanto la fecha de inicio del
 	 reloj que se asignara al comienzo de la ejecucion del programa como la hora de inicio del reloj
 	 del sistema.
*/
void RTC_initializeDate(dateStruct* dayStruct);
/*
 * Cada vez que se vaya a agregar algun digito al reloj lo que sucedera sera lo siguiente:
 * Se recibira cada uno de los digitos separados en formato ascii
 * Se convertira cada uno de los digitos a digitos de verdad (no ascii)Se creara el numero uint8 con cada uno de los dos numeros en formato BCD.
 * Se enviara el numero en BCD a la funcion requerida.
 */

/*!
 	 \brief Esta funcion realiza la escritura de los segundos. Toma en cuenta que se debe
 	 de mantener siempre encendido el bit mas significativo del registro de segundos del RTC.
*/
void RTC_writeSeconds(uint8 bcdDigit);
/*!
 	 \brief Esta funcion permite realizar la escritura de los minutos del RTC en formato BCD.
*/
void RTC_writeMinutes(uint8 bcdDigit);
/*!
 	 \brief Esta funcion permite realizar la escritura de horas dentro del RTC.
*/
void RTC_writeHours(uint8 bcdDigit);
/*!
 	 \brief Esta funcion permite realizar la escritura de los dias que se encuentran en
 	 el RTC. Se ignoro la comprobacion de la bandera de anio bisisesto.
*/
void RTC_writeDay(uint8 bcdDigit);
/*!
 	 \brief Esta funcion realiza la escritura de los meses del RTC.
*/
void RTC_writeMonth(uint8 bcdDigit);
/*!
 	 \brief Esta funcion realiza la escritura del primer byte de los anios contenido dentro
 	 del RTC. La otra parte de los anios es la que se encuentra en la memoria de la tarjeta Kinetis
*/
void RTC_writeYear(uint16 bcdDigit);//Recibe cuatro digitos, de los cuales solamente escribira 2 en RTC y 2 en software
/*!
 	 \brief Esta funcion permite realizar la seleccion del formato en el que se muestran las horas.
 	 Cuando la hora es de am y se realiza una conversion a formato PM lo que sucede es que se
 	 realiza la lectura de los datos actuales de las horas y se calcula el equivalente en el
 	 otro formato de tiempo. Todos estos calculos se realizan por medio de funciones del Formatter.h
*/
void RTC_ampmEnableFormat(BooleanType value);//Recibe true si se quiere habilitar am o pm . Recibe false si se quiere deshabilitar

/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los segundos.
*/
uint8 RTC_getSecondsOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las decenas de los segundos.
*/
uint8 RTC_getSecondsTensAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los minutos.
*/
uint8 RTC_getMinutesOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las decenas de los minutos.
*/
uint8 RTC_getMinutesTensAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los horas.
*/
uint8 RTC_getHoursOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las decenas de los horas.
*/
uint8 RTC_getHoursTensAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los dias.
*/
uint8 RTC_getDayOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los dias.
*/
uint8 RTC_getDayTensAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los meses.
*/
uint8 RTC_getMonthOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las decenas de los meses.
*/
uint8 RTC_getMonthTensAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las unidades de los anios.
*/
uint8 RTC_getYearOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las decenas de los anios.
*/
uint8 RTC_getYearTensAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las centenas de los anios.
*/
uint8 RTC_getYearCentsOnesAscii();
/*!
 	 \brief Esta funcion retorna en codigo ascii las Milenas de los anios.
*/
uint8 RTC_getYearCentsTensAscii();
/*!
 	 \brief Esta funcion retorna si el modo de 12 horas se encuentra activado
*/
BooleanType RTC_isAMPMenabled();
/*
 *  \brief Esta funcion retorna verdadero si se encuentra en PM la hora actual (En modo de 12 horas).
 */
BooleanType RTC_isPMactive();
/*!
 	 \brief Esta funcion convierte un numero de formato binario normal a formato binario codificado en decimal.
 	 Retorna el entero equvialente al BCD.
*/
uint16 convertBinaryToBCD(uint16 data);
/*!
 	 \brief Esta funcion conveirte un numero de Binario codificado decimal a binario normal.
 	 Retorna el byte equivalente sin signo.
*/
uint8 convertBCDtoBinary(uint8 data);
/*!
 	 \brief Esta funcion convierte el formato de 12 horas a formato de 24 horas. Recibe las horas
 	 actuales en formato de entero de 8 bits.
*/
uint8 convert_format_12_24(uint8 hours);
/*!
 	 \brief Esta funcion convierte el formato de 24 horas a formato de 12 horas. Recibe las horas
 	 actuales en formato de entero de 8 bits.
*/
uint8 convert_format_24_12(uint8 hours);

/*!
 	 \brief Esta funcion establece una hora completa. El tamanod el buffer que recibe
 	 contiene los caracteres de la hora en ascii comenzando con las horas en la posicion mas baja
 	 y terminando con los segundos en la posicion mas alta del buffer que se recibio.
 	 En total el buffer recibido solamente contiene 6 elementos (si contiene mas no se
 	 tomaran en cuenta).
*/
void RTC_setFullHour(uint8* buffer);//Recibe en formato en horas, minutos y segundos
/*!
 	 \brief Esta funcion establece una fecha completa. El tamanod el buffer que recibe
 	 contiene los caracteres de la fecha en ascii comenzando con los dias en la posicion mas baja
 	 y terminando con los anios en la posicion mas alta del buffer que se recibio.
 	 En total el buffer recibido solamente contiene 8 elementos (si contiene mas no se
 	 tomaran en cuenta).
*/
void RTC_setFullDate(uint16* buffer);//Recibe el formato en dias, meses, anios

void RTC_writeYearAndCent(uint16 bcdDigit, uint8 cent);//Recibe cuatro digitos, de los cuales solamente escribira 2 en RTC y 2 en software
int HexToInteger(int number, int digits);
int IntegerToHex(int number, int digits);
#endif /* SOURCES_RTC7940M_H_ */
