/*
 * Eventos.c
 *
 *  Created on: Oct 29, 2016
 *      Author: Damian Garcia
 */
#include "Eventos.h"
#include "FSM_Selector.h"

FSM_SEL_EVT_Type decodeMainMenuEvent(uint8 evt) {
	switch (evt) {
	case '1':
		return FSM_SEL_EVT_1;
		break;
	case '2':
		return FSM_SEL_EVT_2;
		break;
	case '3':
		return FSM_SEL_EVT_3;
		break;
	case '4':
		return FSM_SEL_EVT_4;
		break;
	case '5':
		return FSM_SEL_EVT_5;
		break;
	case '6':
		return FSM_SEL_EVT_6;
		break;
	case '7':
		return FSM_SEL_EVT_7;
			break;
	case '8':
		return FSM_SEL_EVT_8;
		break;
	case '9':
		return FSM_SEL_EVT_9;
		break;
	case 27:
		return FSM_SEL_EVT_ESC;
		break;
	case 13:
		return FSM_SEL_EVT_ENTER;
		break;
	default:
		return FSM_SEL_EVT_NOP;
	}
}

FSM_Escribir_Memoria_EVT_Type decodeLeerMemoriaMenuEvent(uint8 evt) {
	if('0' <= evt && '9' >= evt){
		return 	FSM_LEER_MEMORIA_CHAR;
	}
	if('A' <= evt && 'Z' >= evt){
		return 	FSM_ESCRIBIR_MEMORIA_CHAR;
	}
	if('a' <= evt && 'z' >= evt){
			return 	FSM_ESCRIBIR_MEMORIA_CHAR;
	}
	switch (evt) {
	case 27:
		return FSM_LEER_MEMORIA_ESC;
		break;
	case 13:
		return FSM_LEER_MEMORIA_ENTER;
		break;
	default:
		return FSM_LEER_MEMORIA_NOP;
	}
}

FSM_Leer_Memoria_EVT_Type decodeEscribirMemoriaMenuEvent(uint8 evt) {
	if('0' <= evt && '9' >= evt){
		return 	FSM_ESCRIBIR_MEMORIA_CHAR;
	}
	if('A' <= evt && 'Z' >= evt){
		return 	FSM_ESCRIBIR_MEMORIA_CHAR;
	}
	if('a' <= evt && 'z' >= evt){
			return 	FSM_ESCRIBIR_MEMORIA_CHAR;
	}
	switch (evt) {
	case 27:
		return FSM_ESCRIBIR_MEMORIA_ESC;
		break;
	case 13:
		return FSM_ESCRIBIR_MEMORIA_ENTER;
		break;
	default:
		return FSM_ESCRIBIR_MEMORIA_NOP;
	}
}

FSM_Establecer_Hora_EVT_Type decodeEstablecerHoraEvent(uint8 evt) {
	if('0' <= evt && '9' > evt){
		return 	FSM_ESTABLECER_HORA_NUM;
	}
	switch (evt) {
	case '/':
		return FSM_ESTABLECER_HORA_NUM;
	case ':':
		return FSM_ESTABLECER_HORA_NUM;
	case 27:
		return FSM_ESTABLECER_HORA_ESC;
		break;
	case 13:
		return FSM_ESTABLECER_HORA_ENTER;
		break;
	default:
		return FSM_ESTABLECER_HORA_NOP;
	}
}

FSM_Establecer_Fecha_EVT_Type decodeEstabelcerFechaEvent(uint8 evt) {
	if('0' <= evt && '9' > evt){
		return 	FSM_ESTABLECER_FECHA_EVT_NUM;
	}
	switch (evt) {
	case '/':
		return FSM_ESTABLECER_FECHA_EVT_NUM;
	case 27:
		return FSM_ESTABLECER_FECHA_EVT_ESC;
		break;
	case 13:
		return FSM_ESTABLECER_FECHA_EVT_ENTER;
		break;
	default:
		return FSM_ESTABLECER_FECHA_EVT_NOP;
	}
}

FSM_Formato_Hora_EVT_Type decodeFormatoHoraEvent(uint8 evt) {
	if('A' <= evt && 'Z' > evt){
		return 	FSM_FORMATO_HORA_CHAR;
	}
	if('a' <= evt && 'z' > evt){
			return 	FSM_FORMATO_HORA_CHAR;
	}
	switch (evt) {
	case '/':
		return FSM_ESTABLECER_FECHA_EVT_NUM;
	case 27:
		return FSM_ESTABLECER_FECHA_EVT_ESC;
		break;
	case 13:
		return FSM_ESTABLECER_FECHA_EVT_ENTER;
		break;
	default:
		return FSM_ESTABLECER_FECHA_EVT_NOP;
	}
}

FSM_Leer_Hora_EVT_Type decodeLeerHoraEvent(uint8 evt) {
	switch (evt) {
	case 27:
		return FSM_LEER_HORA_ESC;
		break;
	case 13:
		return FSM_LEER_HORA_ENTER;
		break;
	default:
		return FSM_LEER_HORA_NOP;
	}
}

FSM_Leer_Fecha_EVT_Type decodeLeerFechaEvent(uint8 evt) {
	switch (evt) {
	case 27:
		return FSM_LEER_FECHA_ESC;
		break;
	case 13:
		return FSM_LEER_FECHA_ENTER;
		break;
	default:
		return FSM_LEER_FECHA_NOP;
	}
}

Chat_EVT_EnumType decodeChatEvent(uint8 evt){
	if('a' <= evt && 'z' >= evt ){
		return CHAT_CHAR_EVT;
	}
	if('A'  <= evt && 'Z' >= evt ){
		return CHAT_CHAR_EVT;
	}
	if('0' <= evt && '9' >= evt ){
		return CHAT_CHAR_EVT;
	}
	switch (evt) {
	case 27:
		return CHAT_ESC_EVT;
		break;
	case 13:
		return CHAT_ENTER_EVT;
		break;
	case 130:
		return CHAT_INIT_EVT;
	default:
		return CHAT_NOP;
	}

}

FSM_Eco_LCD_EVT_Type decodeEcoLCDEvent(uint8 evt) {
	switch (evt) {
	case 27:
		return FSM_ECO_LCD_ESC;
		break;
	case 13:
		return FSM_ECO_LCD_ENTER;
		break;
	default:
		return FSM_ECO_LCD_CHAR;
	}
}
