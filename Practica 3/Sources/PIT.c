#include "PIT.h"
#include "GPIO.h"
#include "ControlPantalla.h"
static uint8 PIT0_Flag;
static uint8 PIT1_Flag;
static uint8 PIT2_Flag;
static uint8 PIT3_Flag;

uint8_t PIT_getInterruptionFlag(PIT_TimerType pitTimer) {
	switch (pitTimer) {
	case PIT_0:
		return PIT0_Flag;
		break;
	case PIT_1:
		return PIT1_Flag;
		break;
	case PIT_2:
		return PIT2_Flag;
		break;
	case PIT_3:
		return PIT3_Flag;
		break;
	default:
		return 0;
		break;
	}
}
void PIT_clearInterruptionFlag(PIT_TimerType pitTimer) {
	switch (pitTimer) {
	case PIT_0:
		PIT0_Flag = FALSE;
		break;
	case PIT_1:
		PIT1_Flag = FALSE;
		break;
	case PIT_2:
		PIT2_Flag = FALSE;
		break;
	case PIT_3:
		PIT3_Flag = FALSE;
		break;
	default:
		break;
	}
}

void PIT0_IRQHandler() //Utilizado por el generador
{

	PIT0_Flag = TRUE;
	PIT_TFLG0 |= PIT_TFLG_TIF_MASK;
	PIT_TCTRL0; //read control register for clear PIT flag, this is silicon bug
	PIT_TCTRL0 &= ~(PIT_TCTRL_TIE_MASK); //enables PIT timer interrupt
	PIT_TCTRL0 &= ~(PIT_TCTRL_TEN_MASK); //enables timer0
//
//	PIT0_Flag = TRUE;
//	PIT_TFLG0 |= PIT_TFLG_TIF_MASK;
//	PIT_TCTRL0; //read control register for clear PIT flag, this is silicon bug
//	//PIT_TCTRL0 &= ~(PIT_TCTRL_TIE_MASK); //enables PIT timer interrupt
//	//ControlPantalla_enablePrint();
}
void PIT1_IRQHandler() //Utilizado por el generador
{

	PIT1_Flag = TRUE;
	PIT_TFLG1 |= PIT_TFLG_TIF_MASK;
	PIT_TCTRL1; //read control register for clear PIT flag, this is silicon bug
	ControlPantalla_setReady();
//
//	PIT0_Flag = TRUE;
//	PIT_TFLG0 |= PIT_TFLG_TIF_MASK;
//	PIT_TCTRL0; //read control register for clear PIT flag, this is silicon bug
//	//PIT_TCTRL0 &= ~(PIT_TCTRL_TIE_MASK); //enables PIT timer interrupt
//	//ControlPantalla_enablePrint();
}

void PIT2_IRQHandler()	//utilizado por el teclado matricial para el barrido
{
	//PIT_clearEnable(PIT_2);
	PIT2_Flag = TRUE;
	PIT_TFLG2 |= PIT_TFLG_TIF_MASK;
	PIT_TCTRL2; //read control register for clear PIT flag, this is silicon bug

//	uint32 data = GPIO_readPORT(GPIOC);
//	NVIC_EnableIRQ(PORTC_IRQn);
//	data = (~data)&0x3F;
//	if(data == 1){
//		addEventoBoton(EVT_BTN_0);
//	}else if(data == 2){
//		addEventoBoton(EVT_BTN_1);
//	}else if(data == 4){
//		addEventoBoton(EVT_BTN_2);
//	}else if(data == 8){
//		addEventoBoton(EVT_BTN_3);
//	}else if(data == 16){
//		addEventoBoton(EVT_BTN_4);
//	}else if(data == 32){
//		addEventoBoton(EVT_BTN_5);
//	}else{
//		return;
//	}

}

void PIT_configureMCRRegister(PIT_MCRConfig pitMCR) {
	PIT_MCR = PIT_MCR_MDIS(pitMCR.mdis) | PIT_MCR_FRZ(pitMCR.freeze);
}

void PIT_ClockGatingEnable() {
	SIM_SCGC6 |= SIM_SCGC6_PIT(PIT_ENABLE);
}

void PIT_ClockGatingDisable() {
	SIM_SCGC6 &= ~(SIM_SCGC6_PIT(PIT_ENABLE));
}

uint32_t PIT_currentTimmerValueRegister(PIT_TimerType pitTimer) {
	switch (pitTimer) {
	case PIT_0:
		return PIT_CVAL0;
		break;
	case PIT_1:
		return PIT_CVAL1;
		break;
	case PIT_2:
		return PIT_CVAL2;
		break;
	case PIT_3:
		return PIT_CVAL3;
		break;
	default:
		return 0;
		break;
	}
}

void PIT_configureTimmerControlRegister(PIT_TCRConfig pitTCRConfig) {
	switch (pitTCRConfig.pitTimer) {
	case PIT_0:
		PIT_TCTRL0; //read control register for clear PIT flag, this is silicon bug
		PIT_TCTRL0 =
				PIT_TCTRL_CHN(
						pitTCRConfig.chainMode) | PIT_TCTRL_TIE(pitTCRConfig.timerInterruptEnable)| PIT_TCTRL_TEN(pitTCRConfig.timerEnable);
		break;
	case PIT_1:
		PIT_TCTRL1; //read control register for clear PIT flag, this is silicon bug
		PIT_TCTRL1 =
				PIT_TCTRL_CHN(
						pitTCRConfig.chainMode) | PIT_TCTRL_TIE(pitTCRConfig.timerInterruptEnable)
						| PIT_TCTRL_TEN(pitTCRConfig.timerEnable);
		break;
	case PIT_2:
		PIT_TCTRL2; //read control register for clear PIT flag, this is silicon bug
		PIT_TCTRL2 =
				PIT_TCTRL_CHN(
						pitTCRConfig.chainMode) | PIT_TCTRL_TIE(pitTCRConfig.timerInterruptEnable)
						| PIT_TCTRL_TEN(pitTCRConfig.timerEnable);
		break;
	case PIT_3:
		PIT_TCTRL3; //read control register for clear PIT flag, this is silicon bug
		PIT_TCTRL3 =
				PIT_TCTRL_CHN(
						pitTCRConfig.chainMode) | PIT_TCTRL_TIE(pitTCRConfig.timerInterruptEnable)
						| PIT_TCTRL_TEN(pitTCRConfig.timerEnable);
		break;
	default:
		break;
	}
}

void PIT_CLOCKGATING() {
	SIM_SCGC6 |= 0x800000;
}

void PIT_setLDVALn(PIT_TimerType pitTimer, uint32_t value) {
	switch (pitTimer) {
	case PIT_0:
		PIT_LDVAL0 = value;
		break;
	case PIT_1:
		PIT_LDVAL1 = value;
		break;
	case PIT_2:
		PIT_LDVAL2 = value;
		break;
	case PIT_3:
		PIT_LDVAL3 = value;
		break;
	default:
		break;
	}
}

void PIT_setTimeDelay(PIT_TimerType pitTimer, float systemClock, float period) {
	uint32 value = systemClock * period - 1;
	PIT_setLDVALn(pitTimer, value);

}

void PIT_setEnable(PIT_TimerType pitTimer){
	switch (pitTimer) {
	case PIT_0:
		PIT_TCTRL0 |= PIT_TCTRL_TIE_MASK | PIT_TCTRL_TEN_MASK;
		break;
	case PIT_1:
		PIT_TCTRL1 |= PIT_TCTRL_TIE_MASK | PIT_TCTRL_TEN_MASK;
		break;
	case PIT_2:
		PIT_TCTRL2 |= PIT_TCTRL_TIE_MASK | PIT_TCTRL_TEN_MASK;
		break;
	case PIT_3:
		PIT_TCTRL3 |= PIT_TCTRL_TIE_MASK | PIT_TCTRL_TEN_MASK;
		break;
	default:
		return;
		break;
	}
}

void PIT_clearEnable(PIT_TimerType pitTimer) {
	switch (pitTimer) {
	case PIT_0:
		PIT_TCTRL0 &= ~PIT_TCTRL_TEN_MASK;
		break;
	case PIT_1:
		PIT_TCTRL1 &= ~PIT_TCTRL_TEN_MASK;
		break;
	case PIT_2:
		PIT_TCTRL2 &= ~PIT_TCTRL_TEN_MASK;
		break;
	case PIT_3:
		PIT_TCTRL3 &= ~PIT_TCTRL_TEN_MASK;
		break;
	default:
		return;
		break;
	}
}

uint32_t PIT_isEnabled(PIT_TimerType pitTimer) {
	switch (pitTimer) {
	case PIT_0:
		return (PIT_TCTRL0 & PIT_TCTRL_TIE_MASK)
				&& (PIT_TCTRL0 & PIT_TCTRL_TEN_MASK);
		break;
	case PIT_1:
		return (PIT_TCTRL1 & PIT_TCTRL_TIE_MASK)
				&& (PIT_TCTRL1 & PIT_TCTRL_TEN_MASK);
		break;
	case PIT_2:
		return (PIT_TCTRL2 & PIT_TCTRL_TIE_MASK)
				&& (PIT_TCTRL2 & PIT_TCTRL_TEN_MASK);
		break;
	case PIT_3:
		return (PIT_TCTRL3 & PIT_TCTRL_TIE_MASK)
				&& (PIT_TCTRL3 & PIT_TCTRL_TEN_MASK);
		break;
	default:
		return 0;
		break;
	}
}

void PIT_delay(PIT_TimerType pitTimer, float systemClock, float period) {
	PIT_clearInterruptionFlag(pitTimer);
	uint32 cuentas = systemClock * period - 1;
	PIT_MCR = 0;
	switch (pitTimer) {
	case PIT_0:
		PIT0_Flag = FALSE;
		PIT_LDVAL0 = cuentas;
		PIT_TCTRL0 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL0 |= PIT_TCTRL_TEN_MASK;
		break;
	case PIT_1:
		PIT_LDVAL1 = cuentas;
		PIT_TCTRL1 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL1 |= PIT_TCTRL_TEN_MASK;
		break;
	case PIT_2:
		PIT_LDVAL2 = cuentas;
		PIT_TCTRL2 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL2 |= PIT_TCTRL_TEN_MASK;
		break;
	case PIT_3:
		PIT_LDVAL3 = cuentas;
		PIT_TCTRL3 = PIT_TCTRL_TIE_MASK;
		PIT_TCTRL3 |= PIT_TCTRL_TEN_MASK;
		break;
	default:
		break;
	}
}
