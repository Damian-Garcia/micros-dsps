/*
 * WriteDate.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_WRITEDATE_H_
#define SOURCES_WRITEDATE_H_

#include "DataTypeDefinitions.h"
#include "Eventos.h"
#include "UARTDriver.h"

typedef enum{
	FSM_WRITEDATE_IDLE,
	FSM_WRITEDATE_WAIT_NUMBER,
	FSM_WRITEDATE_STORE_NUMBER,
	FSM_WRITEDATE_WRITE_RTC,
	FSM_WRITEDATE_ERROR,
	FSM_WRITEDATE_WAIT_ESC,
	FSM_WRITEDATE_END,
	FSM_WRITEDATE_ENUM_MAX,
}FSM_WriteDate_StateEnumType;

typedef struct  {
	FSM_WriteDate_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_WriteDate_InstanceType;

FSM_WriteDate_InstanceType* WriteDate_getInstance();
void WriteDate_init(UART_ChannelType);
BooleanType WriteDate_isBusy();
BooleanType WriteDate_isEnabled();
void WriteDate_eval(FSM_Establecer_Fecha_EVT_Type);
void WriteDate_addChar(uint8);

void WriteDate_clearBusy();

#endif /* SOURCES_WRITEDATE_H_ */
