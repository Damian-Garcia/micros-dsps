/*
 * FIFO.h
 *
 *  Created on: Nov 3, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_FIFO_H_
#define SOURCES_FIFO_H_

#include "DataTypeDefinitions.h"

#define FIFO_SIZE 1024

typedef struct{
	uint8 arreglo[FIFO_SIZE];
	BooleanType emptyFlag;
	uint8 count;
}FIFO_type;

void FIFO_init(FIFO_type*);
void FIFO_push(FIFO_type*,uint8);
uint8 FIFO_pop(FIFO_type*);
BooleanType FIFO_isEmpty(FIFO_type*);
void FIFO_flush(FIFO_type*);

#endif /* SOURCES_FIFO_H_ */
