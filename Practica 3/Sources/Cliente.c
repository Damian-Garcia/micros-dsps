/*
 * Cliente.c
 *
 *  Created on: Oct 29, 2016
 *      Author: Damian Garcia
 */
#include "Cliente.h"
#include "FSM_Selector.h"
#include "ReadMemory.h"
#include "ReadDate.h"
#include "ReadTime.h"
#include "WriteMemory.h"
#include "WriteDate.h"
#include "WriteTime.h"
#include "WriteFormat.h"
#include "ControlPantalla.h"
#include "Chat.h"

void addClientEvent(Cliente *c, uint8 evt) {
	Process p = c->currentProcess;
	c->eventReady = TRUE;
	switch (p) {
	case MAIN_MENU:
		c->currentEvent = decodeMainMenuEvent(evt);
		break;
	case LEER_MEMORIA:
		ReadMemory_addChar(evt);
		c->currentEvent = decodeLeerMemoriaMenuEvent(evt);
		break;
	case ESCRIBIR_MEMORIA:
		WriteMemory_addChar(evt);
		c->currentEvent = decodeEscribirMemoriaMenuEvent(evt);
		break;
	case ESTABLECER_HORA:
		WriteTime_addChar(evt);
		c->currentEvent = decodeEstablecerHoraEvent(evt);
		break;
	case ESTABLECER_FECHA:
		WriteDate_addChar(evt);
		c->currentEvent = decodeEstabelcerFechaEvent(evt);
		break;
	case FORMATO_HORA:
		WriteFormat_addChar(evt);
		c->currentEvent = decodeFormatoHoraEvent(evt);
		break;
	case LEER_HORA:
		c->currentEvent = decodeLeerHoraEvent(evt);
		break;
	case LEER_FECHA:
		c->currentEvent = decodeLeerFechaEvent(evt);
		break;
	case COMUNICACION_TERMINAL:
		Chat_addEvt(c->uart,decodeChatEvent(evt));

		Chat_addChar(c->uart, evt);

//		c->currentEvent = decodeChatEvent(evt);
		break;
	case ECO_LCD:
		ControlPantalla_addEcoChar(evt);
		c->currentEvent = decodeEcoLCDEvent(evt);
		break;
	default:
		break;
	}
}

uint8 Cliente_getEvento(Cliente *c){
	return c->currentEvent;
}

void Cliente_init(Cliente *c, UART_ChannelType UART){
	c->currentProcess = MAIN_MENU;
	c->currentFSM = &(c->Selector);
	c->currentEvent = FSM_SEL_EVT_NOP;
	c->uart = UART;
	c->eventReady = TRUE;
	FSM_Selector_init(c->currentFSM, c->uart, c);
}

void Cliente_evaluate(Cliente* c) {
	FSM_ReadTime_InstanceType *fsm;
	if(c->eventReady){
		c->eventReady = FALSE;
	switch (c->currentProcess) {
		case MAIN_MENU:
			FSM_Selector_eval(&c->Selector, c->currentEvent);
			break;
		case LEER_MEMORIA:
			ReadMemory_eval(c->currentEvent);
			if(!ReadMemory_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		case ESCRIBIR_MEMORIA:
			WriteMemory_eval(c->currentEvent);
			if(!WriteMemory_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		case ESTABLECER_HORA:
			WriteTime_eval(c->currentEvent);
			if(!WriteTime_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		case ESTABLECER_FECHA:
			WriteDate_eval(c->currentEvent);
			if(!WriteDate_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		case FORMATO_HORA:
			WriteFormat_eval(c->currentEvent);
			if(!WriteFormat_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		case LEER_HORA:
			ReadTime_eval(c->currentEvent);
			if(!ReadTime_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		case LEER_FECHA:
			//fsm = ReadDate_getInstance();
			ReadDate_eval(c->currentEvent);
			if(!ReadDate_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.enable = TRUE;
				c->Selector.current = FSM_SEL_IDLE;
			}
			break;
		case COMUNICACION_TERMINAL:
			Chat_eval();

			break;
		case ECO_LCD:
			ControlPantalla_evalEco(c->currentEvent);
			if(!ControlPantalla_isEnabled()){
				c->currentProcess = MAIN_MENU;
				c->eventReady = TRUE;
				c->currentEvent = FSM_SEL_EVT_NOP;
				c->Selector.current = FSM_SEL_IDLE;
				c->Selector.enable = TRUE;
			}
			break;
		}
	}
}
