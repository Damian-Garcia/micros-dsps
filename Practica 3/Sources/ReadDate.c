/*
 * ReadDate.c
 *
 *  Created on: Oct 30, 2016
 *      Author: Damian Garcia
 */


#include  "ReadDate.h"
#include "RTC7940M.h"
typedef void (*action) (void);

FSM_ReadDate_InstanceType fsm;
BooleanType busyFlag;

struct FSM_ReadDate_StateType {
	action a;
	const FSM_ReadDate_StateEnumType *nextList;
};

static void Idle() {

}

static void Read() {
	//static uint8 arreglo[] = "1234\r\n";
	//UART_putString(fsm.uart, arreglo);
	fsm.current = FSM_READDATE_WAITING;


	UART_putChar(fsm.uart, ' ');
	static uint8 arrayOfTime[12] = {0};
	arrayOfTime[0] = RTC_getDayTensAscii();
	arrayOfTime[1] = RTC_getDayOnesAscii();
	arrayOfTime[2] = '/';
	arrayOfTime[3] = RTC_getMonthTensAscii();
	arrayOfTime[4] = RTC_getMonthOnesAscii();
	arrayOfTime[5] = '/';
	arrayOfTime[6] = RTC_getYearCentsTensAscii();
	arrayOfTime[7] = RTC_getYearCentsOnesAscii();
	arrayOfTime[8] = RTC_getYearTensAscii();
	arrayOfTime[9] = RTC_getYearOnesAscii();
	arrayOfTime[10] = '\r';
	arrayOfTime[11] = '\n';
	if(FALSE == getErrorRtcFlag()){
		UART_putString(fsm.uart, arrayOfTime);
	}else{
		UART_putString(fsm.uart, "Error de lectura de fecha");
	}

}

static void Error() {
	static uint8 arreglo[] = "mensaje de error en lectura\r\n";
}

static void Waiting() {

}

static void End() {
	fsm.enable = FALSE;
	busyFlag = FALSE;
}

static FSM_ReadDate_StateEnumType IdleNext[FSM_READDATE_ENUM_MAX] = {
	FSM_READDATE_READ,
	FSM_READDATE_READ,
	FSM_READDATE_READ,
	FSM_READDATE_READ,
	FSM_READDATE_READ,
	FSM_READDATE_READ,
};

static FSM_ReadDate_StateEnumType ReadNext[FSM_READDATE_ENUM_MAX] = {
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
};

static FSM_ReadDate_StateEnumType ErrorNext[FSM_READDATE_ENUM_MAX] = {
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
};

static FSM_ReadDate_StateEnumType WaitingNext[FSM_READDATE_ENUM_MAX] = {
	FSM_READDATE_END,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
	FSM_READDATE_WAITING,
};

static FSM_ReadDate_StateEnumType EndNext[FSM_READDATE_ENUM_MAX] = {
	FSM_READDATE_IDLE,
	FSM_READDATE_IDLE,
	FSM_READDATE_IDLE,
	FSM_READDATE_IDLE,
	FSM_READDATE_IDLE,
	FSM_READDATE_IDLE,
};


static const struct FSM_ReadDate_StateType Estados[] = {
	{ Idle, IdleNext },
	{ Read, ReadNext },
	{ Error, ErrorNext },
	{ Waiting, WaitingNext },
	{ End, EndNext },
};

FSM_ReadDate_InstanceType* ReadDate_getInstance() {
	return &fsm;
}

void ReadDate_init(UART_ChannelType uart) {
	fsm.current = FSM_READDATE_IDLE;
	fsm.uart = uart;
	fsm.enable = TRUE;
	busyFlag = TRUE;
}

BooleanType ReadDate_isBusy() {
	return busyFlag;
}

BooleanType ReadDate_isEnabled() {
	return fsm.enable;
}

void ReadDate_eval(FSM_Leer_Fecha_EVT_Type evt) {
	//fsm.current = fsm.current
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}

void ReadDate_clearBusyFlag(){
	busyFlag = FALSE;
}
