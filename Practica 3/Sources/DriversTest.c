/*
 * DriversTest.c
 *
 *  Created on: Nov 1, 2016
 *      Author: operi
 */
//#include "24LC256.h"
#include "RTC7940M.h"
#include "ModuleConfigurations.h"
#include "Formatter.h"
#include "UARTDriver.h"
#include "NVIC.h"
#include "EEPROM24LC256.h"


void DriversTest_I2C_Modules()
{
    initializeMCGHighFrequence();
	uint8 dato1 = 0;
	uint8 dato2 = 0;
	uint8 dato3 = 0;
	PinConfiguration_initializeI2CModules();
	BooleanType tipo = getErrorMemoryFlag();
	writeByteToMemoryAddress(0x95, 0x4);
	tipo = getErrorMemoryFlag();
	//PinConfiguration_initializeI2CModules();
	writeByteToMemoryAddress(0x32, 0x9);
	tipo = getErrorMemoryFlag();
	dato1=readByteFromMemoryAddress(0x4);
	dato2=readByteFromMemoryAddress(0x9);

	uint8 secondsAsciiOne = 0;
	uint8 secondsAsciiTen = 0;
	uint8 minutesAsciiOne = 0;
	uint8 minutesAsciiTen = 0;
	uint8 hoursAsciiOne = 0;
	uint8 hoursAsciiTen = 0;
	uint8 daysAsciiOne = 0;
	uint8 daysAsciiTen = 0;
	uint8 monthsAsciiOne = 0;
	uint8 monthsAsciiTen = 0;
	uint8 yearsAsciiOne = 0;
	uint8 yearsAsciiTen = 0;
	uint8 yearsAsciiCentsOne = 0;
	uint8 yearsAsciiCentsTens = 0;

	//RTC_writeByteToMemoryAddress(0x80, 0);
	//dato3=RTC_readByteFromMemoryAddress(0);
	uint8 bufferAddress[4] = {0x32, 0x31, 0x46, 0x46};
	uint8 bufferReadNumber[3] = {0x30,0x35,0x32};


	// Para saber la direccion se necesita conocer cuantos digitos fueron recibidos.
	uint16 address = convertAsciiAddressToHexadecimalBCDWithNumberOfDigits(bufferAddress, 2);
	uint16 numberOfReads= convertAsciiBufferToOneUnsignedInt(bufferReadNumber, 2);
	uint8 bufferWriteArray[10] = {0x72,0x82,0x12,0xFF,0x24,0x0,0x23,0x3,0x90,0x89};
	uint8 bufferReadArray[10] = {0};
	writeStreamofBytesToMemoryAddress(bufferWriteArray, address, 10);
	readStreamOfBytesFromMemoryToArray(bufferReadArray, address, numberOfReads);


	// Prueba de escritura de horas (reciben solamente en formato bcd)
	uint16 date[3] = {0x01,0x01,0x2015};
	RTC_setFullDate(date);//Recibe el formato en dias, meses, anios
	uint8 hoursArray[3] = {0x11,0x30,0x1};
	RTC_setFullHour(hoursArray);
    int val = 0;
    uint8 y = 0;
	for (;;) {
		//uint8 reading = readByteFromMemoryAddress( 0x50+y);
    	secondsAsciiOne = RTC_getSecondsOnesAscii()-0x30;
    	secondsAsciiTen =RTC_getSecondsTensAscii()-0x30;
    	minutesAsciiOne = RTC_getMinutesOnesAscii()-0x30;
    	minutesAsciiTen = RTC_getMinutesTensAscii()-0x30;
    	hoursAsciiOne = RTC_getHoursOnesAscii()-0x30;
    	hoursAsciiTen = RTC_getHoursTensAscii()-0x30;
    	daysAsciiOne = RTC_getDayOnesAscii()-0x30;
    	daysAsciiTen = RTC_getDayTensAscii()-0x30;
    	monthsAsciiOne = RTC_getMonthOnesAscii()-0x30;
    	monthsAsciiTen = RTC_getMonthTensAscii()-0x30;
    	yearsAsciiOne = RTC_getYearOnesAscii()-0x30;
    	yearsAsciiTen = RTC_getYearTensAscii()-0x30;
    	yearsAsciiCentsOne = RTC_getYearCentsOnesAscii()-0x30;
    	yearsAsciiCentsTens = RTC_getYearCentsTensAscii()-0x30;
    	val++;
    	if(val == 3)
    	{
    		RTC_ampmEnableFormat(TRUE);
    	}
    	if(val == 6)
    	{
    		RTC_ampmEnableFormat(FALSE);
    	}

		y++;

    	//RTC_writeDay(6);
    	//RTC_writeMinutes(0x33);
    	//RTC_writeMonth(0x01);
    }
}
///*
// * Codigo de prueba de reloj y de memoria
// * 	uint8 dato1 = 0;
//	uint8 dato2 = 0;
//	uint8 dato3 = 0;
//	PinConfiguration_initializeMemoryI2C();
//	configureMemoryConfiguration();
//	writeByteToMemoryAddress(0x2, 0x4);
//	writeByteToMemoryAddress(0x87, 0x9);
//	dato1=readByteFromMemoryAddress(0x4);
//	dato2=readByteFromMemoryAddress(0x9);
//
//	RTC_initializeDate(&date);
//	uint8 secondsAsciiOne = 0;
//	uint8 secondsAsciiTen = 0;
//	uint8 minutesAsciiOne = 0;
//	uint8 minutesAsciiTen = 0;
//	uint8 hoursAsciiOne = 0;
//	uint8 hoursAsciiTen = 0;
//	uint8 daysAsciiOne = 0;
//	uint8 daysAsciiTen = 0;
//	uint8 monthsAsciiOne = 0;
//	uint8 monthsAsciiTen = 0;
//	uint8 yearsAsciiOne = 0;
//	uint8 yearsAsciiTen = 0;
//	uint8 yearsAsciiCentsOne = 0;
//	uint8 yearsAsciiCentsTens = 0;
//
//	//RTC_writeByteToMemoryAddress(0x80, 0);
//	//dato3=RTC_readByteFromMemoryAddress(0);
//	uint8 bufferAddress[4] = {0x31, 0x30, 0x46, 0x46};
//	uint16 address = convertAsciiAddressToHexadecimal(bufferAddress);
//	uint8 bufferWriteArray[10] = {0x20,0x18,0xFF,0x76,0x21,0x0,0x23,0x3,0x90,0x89};
//	uint8 bufferReadArray[10] = {0};
//	writeStreamofBytesToMemoryAddress(bufferWriteArray, address, 10);
//	readStreamOfBytesFromMemoryToArray(bufferReadArray, address, 10);
//    int val = 0;
//    uint8 y = 0;
//	for (;;) {
//		//uint8 reading = readByteFromMemoryAddress( 0x50+y);
//    	secondsAsciiOne = RTC_getSecondsOnesAscii()-0x30;
//    	secondsAsciiTen =RTC_getSecondsTensAscii()-0x30;
//    	minutesAsciiOne = RTC_getMinutesOnesAscii()-0x30;
//    	minutesAsciiTen = RTC_getMinutesTensAscii()-0x30;
//    	hoursAsciiOne = RTC_getHoursOnesAscii()-0x30;
//    	hoursAsciiTen = RTC_getHoursTensAscii()-0x30;
//    	daysAsciiOne = RTC_getDayOnesAscii()-0x30;
//    	daysAsciiTen = RTC_getDayTensAscii()-0x30;
//    	monthsAsciiOne = RTC_getMonthOnesAscii()-0x30;
//    	monthsAsciiTen = RTC_getMonthTensAscii()-0x30;
//    	yearsAsciiOne = RTC_getYearOnesAscii()-0x30;
//    	yearsAsciiTen = RTC_getYearTensAscii()-0x30;
//    	yearsAsciiCentsOne = RTC_getYearCentsOnesAscii()-0x30;
//    	yearsAsciiCentsTens = RTC_getYearCentsTensAscii()-0x30;
//    	val++;
//    	if(val == 3)
//    	{
//    		RTC_ampmEnableFormat(TRUE);
//    	}
//    	if(val == 6)
//    	{
//    		RTC_ampmEnableFormat(FALSE);
//    	}
//		y++;
//
//    	//RTC_writeDay(6);
//    	//RTC_writeMinutes(0x33);
//    	//RTC_writeMonth(0x01);
//    }
//}
void DriversTest_Uart_Modules()
{
    initializeMCGHighFrequence();
	PinConfiguration_initializeUartModules();
	/**Enables the UART 0 interrupt*/
	UART_interruptEnable(UART_0);
	/**Enables the UART 0 interrupt in the NVIC*/
	NVIC_enableInterruptAndPriotity(UART0_IRQ, PRIORITY_10);

	/**The following sentences send strings to PC using the UART_putString function. Also, the string
	 * is coded with terminal code*/
	/** VT100 command for text in red and background in cyan*/
	UART_putString(UART_0,"\033[0;32;46m");
	/*VT100 command for clearing the screen*/
	UART_putString(UART_0,"\033[2J");
	/** VT100 command for text in red and background in green*/
	UART_putString(UART_0,"\033[0;32;41m");
	/** VT100 command for positioning the cursor in x and y position*/
	UART_putString(UART_0,"\033[10;10H");
	UART_putString(UART_0, "Micros y DSPs\r");
	/** VT100 command for positioning the cursor in x and y position*/
	UART_putString(UART_0,"\033[11;10H");
	UART_putString(UART_0, "    ITESO\r");
	/** VT100 command for positioning the cursor in x and y position*/
	UART_putString(UART_0,"\033[12;10H");

	/**Enables interrupts*/
	EnableInterrupts;

//	UART_interruptEnable(UART_0);
//	NVIC_enableInterruptAndPriotity(UART0_IRQ, PRIORITY_10);
//	/**Enables interrupts*/
//	EnableInterrupts;
//	/**The following sentences send strings to PC using the UART_putString function. Also, the string
//	 * is coded with terminal code*/
//	/** VT100 command for text in red and background in cyan*/
//	UART_putString(UART_0,"\033[0;32;46m");
//	/*VT100 command for clearing the screen*/
//	UART_putString(UART_0,"\033[2J");
//	/** VT100 command for text in red and background in green*/
//	UART_putString(UART_0,"\033[0;32;41m");
//	/** VT100 command for positioning the cursor in x and y position*/
//	UART_putString(UART_0,"\033[10;10H");
//	UART_putString(UART_0, "Micros y DSPs\r");
//	/** VT100 command for positioning the cursor in x and y position*/
//	UART_putString(UART_0,"\033[11;10H");
//	UART_putString(UART_0, "    ITESO\r");
//	/** VT100 command for positioning the cursor in x and y position*/
//	UART_putString(UART_0,"\033[12;10H");

	for(;;) {

		if(UART_getMailboxFlag(UART_0))
			{
				/**Sends to the PCA the received data in the mailbox*/
				UART_putChar (UART_0, UART_getMailboxContent(UART_0));
				/**clear the reception flag*/
			}
	}
}

