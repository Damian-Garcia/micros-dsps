/**	 \file Formatter.h
	 \brief
	 	 Este es el archivo cabecerea del formatter que contiene las distintas funciones
	 	 que se utilizan para darle formato a las variables que son mostradas en pantalla,
	 	 recibidas de la UART, enviadas a la UART, etc...
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	11/2/2016
 */

#ifndef SOURCES_FORMATTER_H_
#define SOURCES_FORMATTER_H_

#include "DataTypeDefinitions.h"
/*!
 * \brief This function stores an ASCII representation of an unsigned 32 bit integer.
 * \param number
 * \param buffer
 * \param digits
 */
void formatUnsignedInteger32(uint32 number, uint8 *buffer, sint8 digits);
/*!
 * \brief This function stores an ASCII representation of a 64 bit floating point number.
 * \param number
 * \param buffer
 * \param decimalDigits
 */
void formatDouble(double number, uint8 *buffer, uint8 decimalDigits);

/*!
 * \brief This function converts a Binary coded decimal number that is inside an array into a integer.
 * \param number
 * \param buffer
 * \param digits
 */
void formatBCDint(uint32 number, uint8 * buffer, sint8 digits);
/*!
 * \ brief
 * This function is deprecated. Please review the convertASciiAddresstohexadecimalBCDwithnumberofDigits
 * This function converts an ASCII address to an hexadecimal value.
 * It receives an array containing four elments in ascii and returns an integer cointaining the
 * address in a integer value that can be used by the read function of the memory.
 */
uint16 convertAsciiAddressToHexadecimal(uint8*buffer);

/*!
 * \brief This function converts an ascii value (the 0 character to the ninth character and the
 * A character to the F character to the hexadecimal equivalent value.
 */
uint8 convertAsciiDigitToBinary(uint8 value);

/*!
 * Important functions used to read and understand the user ascii entered values.
 */
/*!
 * \brief Esta funcion debe recibir la cantidad de digitos que envio el usuario y el buffer con cada uno de
 * los digitos en hexadecimal.
 * Se utiliza para saber cual es la direccion a la cual el usuario quiere acceder.
 */
uint16 convertAsciiAddressToHexadecimalBCDWithNumberOfDigits(uint8* buffer, uint8 digits);
/*!
 * \brief Esto recibe en digitos la cantidad de digitos que el usuario quiere leer y ademas los lee.
 * Se utiliza para poder comprender cuantos digitos quiere leer el usaurio.
 */
uint16 convertAsciiBufferToOneUnsignedInt(uint8* buffer, uint8 digits);//La posicion mas baja del buffer contiene el dato mas significativo

#endif /* SOURCES_FORMATTER_H_ */
