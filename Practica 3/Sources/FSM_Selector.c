/*
 * FSM_Selector.c
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#include "FSM_Selector.h"
#include "Cliente.h"
#include "UART.h"
#include "ReadDate.h"
#include "ReadTime.h"
#include "ReadMemory.h"
#include "WriteMemory.h"
#include "WriteDate.h"
#include "WriteTime.h"
#include "WriteFormat.h"
#include "ControlPantalla.h"
#include "Chat.h"

typedef void (*action)(FSM_Selector_InstanceType *FSM);

static uint8 mensajeMenu[] =
		"1) Leer Memoria 12C\r"
		"2) Escribir memoria I2C\r"
		"3) Establecer Hora\r"
		"4) Establecer Fecha\r"
		"5) Formato de Hora\r"
		"6) Leer Hora\r"
		"7) Leer fecha\r"
		"8) Comunicacion con terminal 2\r"
		"9) Eco en LCD\r";



struct FSM_Selector_StateType {
	action a;
	const FSM_Selector_StateEnumType *nextList;
};

static const FSM_Selector_StateEnumType IdleNextList[FSM_SEL_EVT_ENUM_MAX] =
{
		FSM_SEL_SEND_MAIN_MENU, // 1
		FSM_SEL_SEND_MAIN_MENU, // 2
		FSM_SEL_SEND_MAIN_MENU, // 3
		FSM_SEL_SEND_MAIN_MENU, // 4
		FSM_SEL_SEND_MAIN_MENU, // 5
		FSM_SEL_SEND_MAIN_MENU, // 6
		FSM_SEL_SEND_MAIN_MENU, // 7
		FSM_SEL_SEND_MAIN_MENU, // 8
		FSM_SEL_SEND_MAIN_MENU, // 9
		FSM_SEL_SEND_MAIN_MENU, // ESC
		FSM_SEL_SEND_MAIN_MENU, // ENTER
		FSM_SEL_SEND_MAIN_MENU, //
};


static const FSM_Selector_StateEnumType SendMainMenuNextList[FSM_SEL_EVT_ENUM_MAX] =
{
		FSM_SEL_IN_MAIN_MENU, // 1
		FSM_SEL_IN_MAIN_MENU, // 2
		FSM_SEL_IN_MAIN_MENU, // 3
		FSM_SEL_IN_MAIN_MENU, // 4
		FSM_SEL_IN_MAIN_MENU, // 5
		FSM_SEL_IN_MAIN_MENU, // 6
		FSM_SEL_IN_MAIN_MENU, // 7
		FSM_SEL_IN_MAIN_MENU, // 8
		FSM_SEL_IN_MAIN_MENU, // 9
		FSM_SEL_IN_MAIN_MENU, // ESC
		FSM_SEL_IN_MAIN_MENU, // ENTER
		FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType InMainMenuNextList[FSM_SEL_EVT_ENUM_MAX] = {
		FSM_SEL_SEND_MENU_1, //1
		FSM_SEL_SEND_MENU_2, //2
		FSM_SEL_SEND_MENU_3, //3
		FSM_SEL_SEND_MENU_4, //4
		FSM_SEL_SEND_MENU_5, //5
		FSM_SEL_SEND_MENU_6, //6
		FSM_SEL_SEND_MENU_7, //7
		FSM_SEL_SEND_MENU_8, //8
		FSM_SEL_SEND_MENU_9, //9
		FSM_SEL_IN_MAIN_MENU,//ESC
		FSM_SEL_IN_MAIN_MENU,//ENTER
		FSM_SEL_IN_MAIN_MENU //NOP
};

static const FSM_Selector_StateEnumType SendMenu1NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu2NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu3NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu4NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu5NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu6NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu7NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu8NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static const FSM_Selector_StateEnumType SendMenu9NextList[FSM_SEL_EVT_ENUM_MAX] = {
	FSM_SEL_IN_MAIN_MENU, // 1
	FSM_SEL_IN_MAIN_MENU, // 2
	FSM_SEL_IN_MAIN_MENU, // 3
	FSM_SEL_IN_MAIN_MENU, // 4
	FSM_SEL_IN_MAIN_MENU, // 5
	FSM_SEL_IN_MAIN_MENU, // 6
	FSM_SEL_IN_MAIN_MENU, // 7
	FSM_SEL_IN_MAIN_MENU, // 8
	FSM_SEL_IN_MAIN_MENU, // 9
	FSM_SEL_IN_MAIN_MENU, // ESC
	FSM_SEL_IN_MAIN_MENU, // ENTER
	FSM_SEL_IN_MAIN_MENU, // NOP
};

static void Idle(){

}

static void sendMainMenu(FSM_Selector_InstanceType *FSM){
	UART_putString(FSM->uart,mensajeMenu);
	FSM->current = FSM_SEL_IN_MAIN_MENU;
};

static void inMainMenu(FSM_Selector_InstanceType *FSM){

}

static void sendMenu1(FSM_Selector_InstanceType *FSM){
	if(ReadMemory_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "Direccion de lectura \r\n");
		FSM->enable = FALSE;
		ReadMemory_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = LEER_MEMORIA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_LEER_MEMORIA_NOP;
	}
}

static void sendMenu2(FSM_Selector_InstanceType *FSM){
	if(WriteMemory_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "Direccion de escritura \r\n");
		FSM->enable = FALSE;
		WriteMemory_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = ESCRIBIR_MEMORIA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_ESCRIBIR_MEMORIA_NOP;
	}
}

static void sendMenu3(FSM_Selector_InstanceType *FSM){
	if(WriteTime_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

		FSM->current = FSM_SEL_IDLE;
	}else{
		UART_putString(FSM->uart, "Escribir Hora: en HH/MM/SS: \r\n");
		FSM->enable = FALSE;
		WriteTime_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = ESTABLECER_HORA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_ESTABLECER_HORA_NOP;
	}
}

static void sendMenu4(FSM_Selector_InstanceType *FSM){
	if(WriteDate_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "Escribir fecha: en dd/mm/aa: \r\n");
		FSM->enable = FALSE;
		WriteDate_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = ESTABLECER_FECHA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_ESTABLECER_FECHA_EVT_NOP;
	}
}

static void sendMenu5(FSM_Selector_InstanceType *FSM){
	if(WriteFormat_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "Formato Actual: \r\n");
		FSM->enable = FALSE;
		WriteFormat_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = FORMATO_HORA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_FORMATO_HORA_NOP;
	}
}

static void sendMenu6(FSM_Selector_InstanceType *FSM){
	if(ReadTime_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "Mensaje leer Hora");
		FSM->enable = FALSE;
		ReadTime_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = LEER_HORA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_LEER_HORA_NOP;
	}
}

static void sendMenu7(FSM_Selector_InstanceType *FSM){
	if(ReadDate_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "Mensaje leer Fecha");
		FSM->enable = FALSE;
		ReadDate_init(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = LEER_FECHA;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_LEER_FECHA_NOP;
	}
}

static void sendMenu8(FSM_Selector_InstanceType *FSM){
	FSM->enable = FALSE;
	Chat_init();
	((Cliente *) FSM->Cliente)->currentProcess = COMUNICACION_TERMINAL;
	((Cliente *) FSM->Cliente)->eventReady = TRUE;
	((Cliente *) FSM->Cliente)->currentEvent = CHAT_INIT_EVT;
	Chat_addEvt(FSM->uart,  CHAT_INIT_EVT);
	Chat_eval();
}

static void sendMenu9(FSM_Selector_InstanceType *FSM){
	if(ControlPantalla_isBusy()){
		UART_putString(FSM->uart, "Este recurso se encuentra en uso, seleccione Otra Opcion:\r\n");
		FSM->current = FSM_SEL_IDLE;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;

	}else{
		UART_putString(FSM->uart, "ModoEco\r\n");
		FSM->enable = FALSE;
		ControlPantalla_setEcoMode(FSM->uart);
		((Cliente *) FSM->Cliente)->currentProcess = ECO_LCD;
		((Cliente *) FSM->Cliente)->eventReady = TRUE;
		((Cliente *) FSM->Cliente)->currentEvent = FSM_ECO_LCD_NOP;
	}
}

static const struct FSM_Selector_StateType Estados [FSM_SEL_ENUM_MAX] = {
	{ Idle, IdleNextList},
	{ sendMainMenu, SendMainMenuNextList }, // SendMainMenu
	{ inMainMenu, InMainMenuNextList },
	{ sendMenu1, SendMenu1NextList },
	{ sendMenu2, SendMenu2NextList },
	{ sendMenu3, SendMenu3NextList },
	{ sendMenu4, SendMenu4NextList },
	{ sendMenu5, SendMenu5NextList },
	{ sendMenu6, SendMenu6NextList },
	{ sendMenu7, SendMenu7NextList },
	{ sendMenu8, SendMenu8NextList },
	{ sendMenu9, SendMenu9NextList },
};

void FSM_Selector_init(FSM_Selector_InstanceType *FSM, UART_ChannelType UART, void *Cliente) {
	FSM->current = FSM_SEL_IDLE;
	FSM->enable = TRUE;
	FSM->uart = UART;
	FSM->Cliente = Cliente;
}

void FSM_Selector_eval(FSM_Selector_InstanceType *FSM, FSM_SEL_EVT_Type evt) {
//siguiente =
	if(FSM->enable){
		FSM->current = Estados[FSM->current].nextList[evt];
		Estados[FSM->current].a(FSM);
	}
}

void FSM_Selector_disable(FSM_Selector_InstanceType FSM){
	FSM.current = FSM_SEL_SEND_MAIN_MENU;
	FSM.enable = FALSE;
}
