/*
 * WriteMemory.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_WRITEMEMORY_H_
#define SOURCES_WRITEMEMORY_H_

#include "DataTypeDefinitions.h"
#include "UARTDriver.h"
#include "Eventos.h"

typedef enum{
	FSM_WRITEMEMORY_IDLE,
	FSM_WRITEMEMORY_WAIT_ADDRESS,
	FSM_WRITEMEMORY_ADDRESS,
	FSM_WRITEMEMORY_MENSAJE_MENSAJE,
	FSM_WRITEMEMORY_WAIT_MENSAJE,
	FSM_WRITEMEMORY_MENSAJE,
	FSM_WRITEMEMORY_ESCRIBIR,
	FSM_WRITEMEMORY_ERROR,
	FSM_WRITEMEMORY_WAIT_ESC,
	FSM_WRITEMEMORY_END,
	FSM_WRITEMEMORY_ENUM_MAX,
}FSM_WriteMemory_StateEnumType;


typedef struct  {
	FSM_WriteMemory_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_WriteMemory_InstanceType;

FSM_WriteMemory_InstanceType* WriteMemory_getInstance();
void WriteMemory_init(UART_ChannelType);
BooleanType WriteMemory_isBusy();
BooleanType WriteMemory_isEnabled();
void WriteMemory_eval(FSM_Escribir_Memoria_EVT_Type);
void WriteMemory_addChar(uint8);

void WriteMemory_clearBusy();

#endif /* SOURCES_WRITEMEMORY_H_ */
