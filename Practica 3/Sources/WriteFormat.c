/*
 * WriteFormat.c
 *
 *  Created on: Nov 1, 2016
 *      Author: Damian Garcia
 */

#include "WriteFormat.h"
#include "RTC7940M.h"
typedef void(*action)(void);

struct FSM_WriteFormat_StateType {
	action a;
	const FSM_WriteFormat_StateEnumType *nextList;
};

static FSM_WriteFormat_InstanceType fsm;
static BooleanType busyFlag;
static uint8 lengthFormat;
static uint8 lastChar;
static uint8 bufferFormato[10];


static void idle(){

}

static void current(){
	if(1){
		if(RTC_isAMPMenabled())
		{
			UART_putString(fsm.uart, "El formato actual es de 12 horas");
		}
		else
		{
			UART_putString(fsm.uart, "El formato actual es de 24 horas");
		}
		UART_putString(fsm.uart, "\r\nDeseas Cambiar el Formato?");
		fsm.current = FSM_WRITEFORMAT_WAIT_CHAR;
	}else{

	}
}

static void waitFormat(){

}

static void storeFormat(){
	UART_putChar(fsm.uart, lastChar);
	bufferFormato[lengthFormat++] = lastChar;
	fsm.current = FSM_WRITEFORMAT_WAIT_CHAR;
}

static void writeRTC(){
	if(1){
		if(bufferFormato[0] == 'S')
		{// Si si se quiere cambiar el formato entonces hacemos un toggle en el formato.
			if(TRUE == RTC_isAMPMenabled())
			{
				RTC_ampmEnableFormat(FALSE);// Desactivamos el ampm enabled
			}
			else
			{
				RTC_ampmEnableFormat(TRUE);// activamos el ampm
			}
			UART_putString(fsm.uart, "El formato ha cambiado\r\n");

		}
		fsm.current = FSM_WRITEFORMAT_WAIT_ESC;
	}else{
		fsm.current = FSM_WRITEFORMAT_ERROR;
	}
}

static void waitEsc(){

}

static void error(){

}

static void end(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
	lengthFormat = 0;
}

static FSM_WriteFormat_StateEnumType idleNextList[FSM_FORMATO_HORA_ENUM_MAX] = {
	FSM_WRITEFORMAT_CURRENT,
	FSM_WRITEFORMAT_CURRENT,
	FSM_WRITEFORMAT_CURRENT,
	FSM_WRITEFORMAT_CURRENT,
	FSM_WRITEFORMAT_CURRENT,
};

static FSM_WriteFormat_StateEnumType currentNextList[FSM_FORMATO_HORA_ENUM_MAX] = {
	FSM_WRITEFORMAT_WAIT_CHAR,
	FSM_WRITEFORMAT_WAIT_CHAR,
	FSM_WRITEFORMAT_WAIT_CHAR,
	FSM_WRITEFORMAT_WAIT_CHAR,
	FSM_WRITEFORMAT_WAIT_CHAR,
};

static const FSM_WriteFormat_StateEnumType waitFormatNextList[FSM_FORMATO_HORA_ENUM_MAX] = {
	FSM_WRITEFORMAT_STORE_CHAR,
	FSM_WRITEFORMAT_END,
	FSM_WRITEFORMAT_WRITE_RTC,
	FSM_WRITEFORMAT_ERROR,
	FSM_WRITEFORMAT_WAIT_CHAR,
};

static const FSM_WriteFormat_StateEnumType storeFormatNextList[FSM_FORMATO_HORA_ENUM_MAX] = {

};

static const FSM_WriteFormat_StateEnumType writeRTCNextList[FSM_FORMATO_HORA_ENUM_MAX] = {

};

static const FSM_WriteFormat_StateEnumType errorNextList[FSM_FORMATO_HORA_ENUM_MAX] = {

};

static const FSM_WriteFormat_StateEnumType waitEscNextList[FSM_FORMATO_HORA_ENUM_MAX] = {
	FSM_WRITEFORMAT_WAIT_ESC,
	FSM_WRITEFORMAT_END,
	FSM_WRITEFORMAT_WAIT_ESC,
	FSM_WRITEFORMAT_WAIT_ESC,
	FSM_WRITEFORMAT_WAIT_ESC,
};

static const FSM_WriteFormat_StateEnumType endNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {

};

static const struct FSM_WriteFormat_StateType Estados[FSM_WRITEFORMAT_ENUM_MAX] = {
	{idle, idleNextList},
	{current, currentNextList},
	{waitFormat, waitFormatNextList},
	{storeFormat, storeFormatNextList},
	{writeRTC, writeRTCNextList},
	{error, errorNextList},
	{waitEsc, waitEscNextList},
	{end, endNextList},
};

FSM_WriteFormat_InstanceType* WriteFormat_getInstance(){
	return &fsm;
}

void WriteFormat_init(UART_ChannelType uart){
	busyFlag = TRUE;
	lengthFormat = 0;
	lastChar = 0;
	fsm.uart = uart;
	fsm.current = FSM_WRITEFORMAT_IDLE;
	fsm.enable = TRUE;
}
BooleanType WriteFormat_isBusy(){
	return busyFlag;
}

BooleanType WriteFormat_isEnabled(){
	return fsm.enable;
}

void WriteFormat_eval(FSM_Formato_Hora_EVT_Type evt){
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}

void WriteFormat_addChar(uint8 c){
	lastChar = c;
}

void WriteFormat_clearBusy(){
	busyFlag = FALSE;
}
