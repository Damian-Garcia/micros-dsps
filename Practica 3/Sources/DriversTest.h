/**	 \file DriversTest.h
	 \brief
	 	 Este es el archivo de cabecera del Drivers test que contiene
	 	 funciones que implementan varios de los metodos implementados en el device driver
	 	 que permiten comprobar su correcto funcionamiento.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	11/2/2016
 */
#ifndef SOURCES_DRIVERSTEST_H_
#define SOURCES_DRIVERSTEST_H_


/*!
 * Se definieron los prototipos de los modulos que seran utilizados
 * para realizar pruebas de los modulos especificados enc ada una de las funciones.
 */


void DriversTest_I2C_Modules();
/*!
 *  * Este modulo configura la frecuencia del reloj del sistema a una frecuencia de 60 Mhz
 * Ademas condifigura tanto la uart bluetooth como la UART que se encuentra conectada
 * al puerto serial para poder ser uitilizada.
 */
void DriversTest_Uart_Modules();


#endif /* SOURCES_DRIVERSTEST_H_ */
