/*
 * ReadTime.h
 *
 *  Created on: Oct 30, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_READTIME_H_
#define SOURCES_READTIME_H_

#include  "DataTypeDefinitions.h"
#include "Eventos.h"
#include "UARTDriver.h"

typedef enum{
	FSM_READTIME_IDLE,
	FSM_READTIME_READ,
	FSM_READTIME_ERROR,
	FSM_READTIME_WAITING,
	FSM_READTIME_END,
	FSM_READTIME_ENUM_MAX,
}FSM_ReadTime_StateEnumType;

typedef struct  {
	FSM_ReadTime_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_ReadTime_InstanceType;

FSM_ReadTime_InstanceType* ReadTime_getInstance();
void ReadTime_init(UART_ChannelType);
BooleanType ReadTime_isBusy();
BooleanType ReadTime_isEnabled();
void ReadTime_addEvent();
void ReadTime_eval(FSM_Leer_Hora_EVT_Type);

void ReadTime_clearBusy();

#endif /* SOURCES_READTIME_H_ */
