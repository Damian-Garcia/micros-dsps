/*
 * Chat.c
 *
 *  Created on: Nov 3, 2016
 *      Author: Damian Garcia
 */

#include "Chat.h"
#include "Cliente.h"
#include "UART.h"
#include "FIFO.h"

#include "ReadDate.h"
#include "ReadMemory.h"
#include "ReadTime.h"
#include "WriteDate.h"
#include "WriteFormat.h"
#include "WriteMemory.h"
#include "WriteTime.h"
#include "ControlPantalla.h"

typedef void (*action ) (void);

typedef struct{
	action a;
	Chat_Estado_EnumType *nextList;
}FSM_Chat_StateType;

 static Cliente* clientes[2];

 static uint8 cuentaClientes = 0;

 static FIFO_type fifo1;
 static FIFO_type fifo2;

 static Chat_Estado_EnumType currentState;
 static UART_ChannelType lastUART;
 static Chat_EVT_EnumType lastEvt;

 static uint8 lastChar;

 static void Idle(){

}

 static Chat_Estado_EnumType idleNextList[] = {
	CHAT_ESTABLECER,
	CHAT_IDLE,
	CHAT_IDLE,
	CHAT_IDLE,
	CHAT_IDLE,
};

 static void Establecer(){

	 ReadDate_clearBusyFlag();
	 ReadMemory_clearBusyFlag();
	 ReadTime_clearBusy();
	 WriteDate_clearBusy();
	 WriteFormat_clearBusy();
	 WriteMemory_clearBusy();
	 WriteTime_clearBusy();
	 ControlPantalla_clearBusy();

	 UART_putString(UART_0, "Chat Iniciado:\r\n");
	 UART_putString(UART_4, "Chat Iniciado:\r\n");

	clientes[0]->currentProcess = COMUNICACION_TERMINAL;
	clientes[1]->currentProcess = COMUNICACION_TERMINAL;
	clientes[0]->eventReady = TRUE;
	clientes[1]->eventReady = TRUE;
	currentState = CHAT_ESPERAR;
}

 static  Chat_Estado_EnumType establecerNextList[] = {

};

 static void Esperar(){

}

 static  Chat_Estado_EnumType esperarNextList[] = {
		CHAT_ESPERAR,
		CHAT_ALMACENAR,
		CHAT_ENVIAR,
		CHAT_SALIR,
		CHAT_ESPERAR,
};

 static  void Almacenar(){
	switch(lastUART ){
	case UART_0:
		break;
	case UART_4:
		break;
	}
	currentState = CHAT_ESPERAR;
}

 Chat_Estado_EnumType almacenarNextList[] = {

};

 static  void Enviar(){
	switch(lastUART){
	case UART_0:
		if(COMUNICACION_TERMINAL != clientes[1]->currentProcess){
			break;
		}
		UART_putString(UART_4,"Terminal 1: ");

		while(!FIFO_isEmpty(&fifo1)){
			UART_putChar(UART_4,FIFO_pop(&fifo1));
		}
		break;
	case UART_4:
		if(COMUNICACION_TERMINAL != clientes[0]->currentProcess){
					break;
		}

		UART_putString(UART_0,"Terminal2 :");

		while(!FIFO_isEmpty(&fifo2)){
			UART_putChar(UART_0, FIFO_pop(&fifo2));
		}
		break;
	}
	currentState = CHAT_ESPERAR;
}

 static  Chat_Estado_EnumType enviarNextList[] = {

};

 static  void Salir(){
	switch(lastUART){
	case UART_0:
		if(COMUNICACION_TERMINAL == clientes[1]->currentProcess){
			UART_putString(clientes[1]->uart, "El cliente 0 se ha desconectado");
		}

		clientes[0]->currentProcess = MAIN_MENU;
		clientes[0]->eventReady = TRUE;
		clientes[0]->currentEvent = FSM_SEL_EVT_NOP;
		clientes[0]->Selector.current = FSM_SEL_IDLE;
		clientes[0]->Selector.enable = TRUE;
		cuentaClientes--;
		break;
	case UART_4:
		if(COMUNICACION_TERMINAL == clientes[0]->currentProcess){
			UART_putString(clientes[0]->uart ,"El cliente 1 se ha desconectado");
		}

		clientes[1]->currentProcess = MAIN_MENU;
		clientes[1]->eventReady = TRUE;
		clientes[1]->currentEvent = FSM_SEL_EVT_NOP;
		clientes[1]->Selector.current = FSM_SEL_IDLE;
		clientes[1]->Selector.enable = TRUE;
		cuentaClientes--;
		break;
	}
	if(cuentaClientes == 0){
		currentState = CHAT_IDLE;
	}else{
		currentState = CHAT_ESPERAR;
	}

}

 Chat_Estado_EnumType salirNextList[] = {

};

 static  FSM_Chat_StateType Estados[] = {
	{Idle, idleNextList},
	{Establecer, establecerNextList},
	{Esperar, esperarNextList},
	{Almacenar, almacenarNextList},
	{Enviar, enviarNextList},
	{Salir, salirNextList}
};

  void Chat_addCliente(void *c){
	if(cuentaClientes < 2){
		clientes[cuentaClientes] = (Cliente *) c;
		cuentaClientes++;
	}
}

  void Chat_addChar(UART_ChannelType uart, uint8 c){
	UART_putChar(uart, c);

	if(currentState !=  CHAT_ENVIAR){
		switch(uart){
		case UART_0:
			FIFO_push(&fifo1, c);
			break;
		case UART_4:
			FIFO_push(&fifo2, c);
			break;
		}
		lastChar = c;
	}
}

  void Chat_addEvt(UART_ChannelType uart, Chat_EVT_EnumType evt){
	  lastUART = uart;
	lastEvt = evt;
}

  void Chat_init(){
	FIFO_init(&fifo1);
	FIFO_init(&fifo2);
	cuentaClientes = 2;
}
  void Chat_eval(){
	currentState = Estados[currentState].nextList[lastEvt];
	Estados[currentState].a();
	lastEvt = CHAT_NOP;

}
