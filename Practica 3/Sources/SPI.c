/**
	\file
	\brief
		This is a source file to use the Nokia 5510 LCD.
		The LCD is connected as follows:
		reset-PDT0
		CE-GND
		DC-PTD3
		DIN-PTD2
		CLK-PTD1
	\author J. Luis Pizano Escalante, Modified by Damian Serrano and Erick Ortega.
	\date	10/03/2016
 */
#include "SPI.h"
#include "MK64F12.h"
#include "GPIO.h"

void SPI_enable(SPI_ChannelType spiChannelType)
{
	switch(spiChannelType)
		{
		case SPI_0:
			SPI0_MCR &= ~SPI_MCR_MDIS_MASK;
			break;
		case SPI_1:
			SPI1_MCR &= ~SPI_MCR_MDIS_MASK;
			break;
		case SPI_2:
			SPI2_MCR &= ~SPI_MCR_MDIS_MASK;
			break;
		default:
			break;
		}
}
/*It activate the clock gating*/
void SPI_clk(SPI_ChannelType spiChannelType)
{
	switch(spiChannelType)
		{
		case SPI_0:
			SIM_SCGC6 |= SIM_SCGC6_SPI0_MASK;
			break;
		case SPI_1:
			SIM_SCGC6 |= SIM_SCGC6_SPI1_MASK;
			break;
		case SPI_2:
			SIM_SCGC3 |= SIM_SCGC3_SPI2_MASK;
			break;
		default:
			break;
		}
}
/*It configure the SPI as a master or slave depending on the value of masterOrslave*/
void SPI_setMaster(SPI_ChannelType channel, SPI_MasterType masterOrSlave)
{
	switch(channel)
		{
		case SPI_0:
			SPI0_MCR &= ~SPI_MCR_MSTR_MASK;
			SPI0_MCR |= SPI_MCR_MSTR(masterOrSlave);
			break;
		case SPI_1:
			SPI1_MCR &= ~SPI_MCR_MSTR_MASK;
			SPI1_MCR |= SPI_MCR_MSTR(masterOrSlave);
			break;
		case SPI_2:
			SPI2_MCR &= ~SPI_MCR_MSTR_MASK;
			SPI2_MCR |= SPI_MCR_MSTR(masterOrSlave);
			break;
		default:
			break;
		}
}
/*It activates the TX and RX FIFOs of the SPI depending on the value of enableOrdisable*/
void SPI_fIFO(SPI_ChannelType channel, SPI_EnableFIFOType enableOrDisable)
{
	switch(channel)
		{
		case SPI_0:
			SPI0_MCR &= ~(SPI_MCR_DIS_RXF_MASK|SPI_MCR_DIS_TXF_MASK);
			SPI0_MCR |= SPI_MCR_DIS_RXF(enableOrDisable);
			SPI0_MCR |= SPI_MCR_DIS_TXF(enableOrDisable);
			break;
		case SPI_1:
			SPI1_MCR &= ~(SPI_MCR_DIS_RXF_MASK|SPI_MCR_CLR_TXF_MASK);
			SPI1_MCR |= SPI_MCR_DIS_RXF(enableOrDisable);
			SPI1_MCR |= SPI_MCR_DIS_TXF(enableOrDisable);
			break;
		case SPI_2:
			SPI2_MCR &= ~(SPI_MCR_DIS_RXF_MASK|SPI_MCR_CLR_TXF_MASK);
			SPI2_MCR |= SPI_MCR_DIS_RXF(enableOrDisable);
			SPI2_MCR |= SPI_MCR_DIS_TXF(enableOrDisable);
			break;
		default:
			break;
		}
}
/*It selects the clock polarity depending on the value of cpol*/
void SPI_clockPolarity(SPI_ChannelType channel, SPI_PolarityType cpol)
{
	switch(channel)
		{
		case SPI_0:
			SPI0_CTAR0 &= ~SPI_CTAR_CPOL_MASK;
			SPI0_CTAR0 |= SPI_CTAR_CPOL(cpol);
			break;
		case SPI_1:
			SPI1_CTAR0 &= ~SPI_CTAR_CPOL_MASK;
			SPI1_CTAR0 |= SPI_CTAR_CPOL(cpol);
			break;
		case SPI_2:
			SPI2_CTAR0 &= ~SPI_CTAR_CPOL_MASK;
			SPI2_CTAR0 |= SPI_CTAR_CPOL(cpol);
			break;
		default:
			break;
		}
}
/*It selects the frame size depending on the value of frameSize and the macros that are defined above*/
void SPI_frameSize(SPI_ChannelType channel, uint32 frameSize)
{
	switch(channel)
	{
	case SPI_0:
		SPI0_CTAR0 &= ~SPI_CTAR_FMSZ_MASK;
		SPI0_CTAR0 |= frameSize;
		break;
	case SPI_1:
		SPI1_CTAR0 &= ~SPI_CTAR_FMSZ_MASK;
		SPI1_CTAR0 |= frameSize;
		break;
	case SPI_2:
		SPI2_CTAR0 &= ~SPI_CTAR_FMSZ_MASK;
		SPI2_CTAR0 |= frameSize;
		break;
	default:
		break;
	}
}
/*It selects the clock phase depending on the value of cpha*/
void SPI_clockPhase(SPI_ChannelType channel, SPI_PhaseType cpha)
{
	if(SPI_LOW_PHASE == cpha)
	{
		switch(channel)
		{
		case SPI_0:
			SPI0_CTAR0 &= ~SPI_CTAR_CPHA_MASK ;
			break;
		case SPI_1:
			SPI1_CTAR0 &= ~SPI_CTAR_CPHA_MASK ;
			break;
		case SPI_2:
			SPI2_CTAR0 &= ~SPI_CTAR_CPHA_MASK ;
			break;
		default:
			break;
		}
	}
	else
	{
		switch(channel)
		{
		case SPI_0:
			SPI0_CTAR0 |= SPI_CTAR_CPHA_MASK ;
			break;
		case SPI_1:
			SPI1_CTAR0 |= SPI_CTAR_CPHA_MASK ;
			break;
		case SPI_2:
			SPI2_CTAR0 |= SPI_CTAR_CPHA_MASK ;
			break;
		default:
			break;
		}
	}
}
/*It selects the baud rate depending on the value of baudRate and the macros that are defined above*/
void SPI_baudRate(SPI_ChannelType channel, uint32 baudRate)
{
	switch(channel)
	{
	case SPI_0:
		SPI0_CTAR0 |= SPI_CTAR_BR(baudRate);
		break;
	case SPI_1:
		SPI1_CTAR0 |= SPI_CTAR_BR(baudRate);
		break;
	case SPI_2:
		SPI2_CTAR0 |= SPI_CTAR_BR(baudRate);
		break;
	default:
		break;
	}
}
/*It selects if MSB or LSM bits is first transmitted*/
void SPI_mSBFirst(SPI_ChannelType channel, SPI_LSMorMSBType msb)
{
	switch(channel)
	{
	case SPI_0:
		SPI0_CTAR0 &= ~(SPI_CTAR_LSBFE_MASK);
		SPI0_CTAR0 |= SPI_CTAR_LSBFE(msb);

		break;
	case SPI_1:
		SPI1_CTAR0 &= ~(SPI_CTAR_LSBFE_MASK);
		SPI1_CTAR0 |= SPI_CTAR_LSBFE(msb);
		break;
	case SPI_2:
		SPI2_CTAR0 &= ~(SPI_CTAR_LSBFE_MASK);
		SPI2_CTAR0 |= SPI_CTAR_LSBFE(msb);
		break;
	default:
		break;
	}
}
/*It stars the SPI transmission by modifying the value of HALT bit*/
void SPI_startTranference(SPI_ChannelType channel)
{
	switch(channel)
	{
	case SPI_0:
		SPI0_MCR &= ~(SPI_MCR_HALT_MASK);
		break;
	case SPI_1:
		SPI1_MCR &= ~(SPI_MCR_HALT_MASK);
		break;
	case SPI_2:
		SPI2_MCR &= ~(SPI_MCR_HALT_MASK);
		break;
	default:
		break;
	}
}
/*It stops the SPI transmission by modifying the value of HALT bit*/
void SPI_stopTranference(SPI_ChannelType channel)
{
	switch(channel)
	{
	case SPI_0:
		SPI0_MCR |= SPI_MCR_HALT_MASK;
		break;
	case SPI_1:
		SPI1_MCR |= SPI_MCR_HALT_MASK;
		break;
	case SPI_2:
		SPI2_MCR |= SPI_MCR_HALT_MASK;
		break;
	default:
		break;
	}
}
/*It transmits the information contained in data*/
void SPI_sendOneByte(uint8 Data)
{
	SPI0_PUSHR = (Data);
	while(0 == (SPI0_SR & SPI_SR_TCF_MASK));
	SPI0_SR |= SPI_SR_TCF_MASK;
}
/*It transmits the information contained in data*/
void SPI_sendOneByteGenerico(SPI_ChannelType channel, uint8 Data)
{
	switch(channel)
	{
	case SPI_0:
		SPI0_PUSHR = (Data);
		while(0 == (SPI0_SR & SPI_SR_TCF_MASK));
		SPI0_SR |= SPI_SR_TCF_MASK;
		break;
	case SPI_1:
		SPI1_PUSHR = (Data);
		while(0 == (SPI1_SR & SPI_SR_TCF_MASK));
		SPI1_SR |= SPI_SR_TCF_MASK;
		break;
	case SPI_2:
		SPI2_PUSHR = (Data);
		while(0 == (SPI2_SR & SPI_SR_TCF_MASK));
		SPI2_SR |= SPI_SR_TCF_MASK;
		break;
	default:
		break;
	}
}
/*It configures the SPI for transmission, this function as arguments receives a pointer to a constant structure where are all
 * the configuration parameters*/
void SPI_init(const SPI_ConfigType* SPI_Config)
{
	SPI_clk(SPI_Config->SPI_Channel);
	GPIO_clockGating(SPI_Config->GPIOForSPI.GPIO_portName);
	GPIO_pinControlRegister(SPI_Config->GPIOForSPI.GPIO_portName, SPI_Config->GPIOForSPI.SPI_clk,&(SPI_Config->pinConttrolRegisterPORTD));
	GPIO_pinControlRegister(SPI_Config->GPIOForSPI.GPIO_portName, SPI_Config->GPIOForSPI.SPI_Sout,&(SPI_Config->pinConttrolRegisterPORTD));
	SPI_setMaster(SPI_Config->SPI_Channel, SPI_Config->SPI_Master);
	SPI_fIFO(SPI_Config->SPI_Channel, SPI_Config->SPI_EnableFIFO);
	SPI_enable(SPI_Config->SPI_Channel);
	SPI_clockPolarity(SPI_Config->SPI_Channel, SPI_Config->SPI_Polarity);
	SPI_frameSize(SPI_Config->SPI_Channel, SPI_Config->frameSize);
	SPI_clockPhase(SPI_Config->SPI_Channel, SPI_Config->SPI_Phase);
	SPI_baudRate(SPI_Config->SPI_Channel, SPI_Config->baudrate);
	SPI_mSBFirst(SPI_Config->SPI_Channel, SPI_MSB);

}


