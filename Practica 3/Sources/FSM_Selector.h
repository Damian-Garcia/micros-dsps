/*
 * FSM_Selector.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_FSM_SELECTOR_H_
#define SOURCES_FSM_SELECTOR_H_

#include "DataTypeDefinitions.h"
#include "Eventos.h"
#include "UARTDriver.h"

typedef enum{
	FSM_SEL_IDLE,
	FSM_SEL_SEND_MAIN_MENU,
	FSM_SEL_IN_MAIN_MENU,
	FSM_SEL_SEND_MENU_1,
	FSM_SEL_SEND_MENU_2,
	FSM_SEL_SEND_MENU_3,
	FSM_SEL_SEND_MENU_4,
	FSM_SEL_SEND_MENU_5,
	FSM_SEL_SEND_MENU_6,
	FSM_SEL_SEND_MENU_7,
	FSM_SEL_SEND_MENU_8,
	FSM_SEL_SEND_MENU_9,
	FSM_SEL_ENUM_MAX,
}FSM_Selector_StateEnumType;

typedef struct  {
	FSM_Selector_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
	void *Cliente;
}FSM_Selector_InstanceType;

void FSM_Selector_init( FSM_Selector_InstanceType *, UART_ChannelType, void *);
void FSM_Selector_eval( FSM_Selector_InstanceType *, FSM_SEL_EVT_Type);

#endif /* SOURCES_FSM_SELECTOR_H_ */
