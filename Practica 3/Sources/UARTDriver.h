/*
 * UARTDriver.h
	\file
	\brief
		This is the header file of the UART driver. It contains the different functions needed
		to initialize and configure a Uart channel of the Kinetis. It also contains the structures
		that facilitate the reading of the data that is received by the UART.
	\author Erick Ortega and Damian Garcia Serrano.
	\date	11/2/2016
 */

#ifndef SOURCES_UARTDRIVER_H_
#define SOURCES_UARTDRIVER_H_

#include "MK64F12.h"
#include "DataTypeDefinitions.h"

/**
 * \brief A mail box type definition for serial port to detect when there is data ready to
 * be readen from the UART buffer.
 */
typedef struct{
	uint8 flag; /** Flag to indicate that there is new data*/
	uint8 mailBox; /** it contains the received data*/
} UART_MailBoxType;


/**
 * \brief This enum define the UART port to be used.
 */
typedef enum {UART_0,UART_1,UART_2,UART_3,UART_4,UART_5}UART_ChannelType;

/**
 * \brief It defines some common transmission baud rates
 */
typedef enum {BD_4800=4800,BD_9600=9600,BD_5600=5600, BD_115200=115200}UART_BaudRateType;

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This is the interrupt services routing (ISR).
 	 \param[in]  void.
 	 \return void
 */
void UART0_RX_TX_IRQHandler(void);
void UART1_RX_TX_IRQHandler(void);
void UART2_RX_TX_IRQHandler(void);
void UART3_RX_TX_IRQHandler(void);
void UART4_RX_TX_IRQHandler(void);
void UART5_RX_TX_IRQHandler(void);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This is the definition of a structure that can be used to configure a UART port.
 	 Any structure of this kind can be sent to the initializer method that is inside this
 	 header.
 */

typedef struct UartStruct
{
	UART_ChannelType uartChannel;
	uint32 systemClk;
	UART_BaudRateType baudRate;
}uartStruct;
//
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It configures the UART to be used. The structure that it receives contains the
 	 UART channel, the systemclock and the baudrate that is going to be configure in the I2C.
 	 \return void
 */
void UART_init(uartStruct* uartConfigStruct);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 enables the RX UART interrupt). It is to guaranty that the incoming data is complete
 	 when reception register is read. For more details see chapter 52 in the kinetis reference manual.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \return void
 */
void UART_interruptEnable(UART_ChannelType uartChannel);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It sends one character through the serial port. This function should include the next sentence:
 	 while(!(UART0_S1 & UART_S1_TC_MASK)). It is to guaranty that before to try to transmit a byte, the previous
 	 one was transmitted. In other word, to avoid to transmit data while the UART is busy transmitting information.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \param[in]  character to be transmitted.
 	 \return void
 */

void UART_putChar (UART_ChannelType uartChannel, uint8 character);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It sends a string character through the serial port.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \param[in]  string pointer to the string to be transmitted.
 	 \return void
 */
void UART_putString(UART_ChannelType uartChannel, sint8* string);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function enables the clock gating of the specified UART channel.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \return void
 */
void UART_ClockGating(UART_ChannelType uartChannel);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It returns the value ofthe mailbox flag of the specified uart channel.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \return void
 */
BooleanType UART_getMailboxFlag(UART_ChannelType uartChannel);

/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 It returns the character that is stored in the mailbox of the specified UART channel.
 	 \param[in]  uartChannel indicates the UART channel.
 	 \return void
 */
uint8 UART_getMailboxContent(UART_ChannelType uartChannel);

#endif /* SOURCES_UARTDRIVER_H_ */
