/**	 \file ADC.c
	 \brief
	 	 Este es el controlador del ADC de la tarjta kinetis. Contiene la definicion de las
	 	 funciones que permiten realizar de manera sencilla la configuracion del ADC de
	 	 la tarjeta Kinetis.
	 	 Tambien contiene enumeradores que facilitan la inicializacion del adc y su configuracion.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	11/2/2016
 */
#include "ADC.h"
#include "NVIC.h"
static double adc0value = 0;
void ADC_SetHardwareAverageSampleNumber(ADC_NameType adcName, ADC_HardwareAverageSelectType hardwareAverageSelect)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC3 &= ~ADC_SC3_AVGS_MASK;
		ADC0_SC3 |= ADC_SC3_AVGS(hardwareAverageSelect);
		break;
	case ADC_1:
		ADC1_SC3 &= ~ADC_SC3_AVGS_MASK;
		ADC1_SC3 |= ADC_SC3_AVGS(hardwareAverageSelect);
		break;
	default:
		break;
	}
}
void ADC_SetHardwareAverageEnable(ADC_NameType adcName, ADC_HardwareAverageEnable hardwareAverage)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC3 &= ~ADC_SC3_AVGE_MASK;
		ADC0_SC3 |= ADC_SC3_AVGE(hardwareAverage);
		break;
	case ADC_1:
		ADC1_SC3 &= ~ADC_SC3_AVGE_MASK;
		ADC1_SC3 |= ADC_SC3_AVGE(hardwareAverage);
		break;
	default:
		break;
	}
}
void ADC_SetContinuousConvertType(ADC_NameType adcName, ADC_ContinuousConvertType continuousConvert)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC3 &= ~ADC_SC3_ADCO_MASK;
		ADC0_SC3 |= ADC_SC3_ADCO(continuousConvert);
		break;
	case ADC_1:
		ADC1_SC3 &= ~ADC_SC3_ADCO_MASK;
		ADC1_SC3 |= ADC_SC3_ADCO(continuousConvert);
		break;
	default:
		break;
	}
}
void ADC_SetVoltageRefType(ADC_NameType adcName, ADC_VoltageRefType voltageRef)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC2 &= ~ADC_SC2_REFSEL_MASK;
		ADC0_SC2 |= ADC_SC2_REFSEL(voltageRef);
		break;
	case ADC_1:
		ADC1_SC2 &= ~ADC_SC2_REFSEL_MASK;
		ADC1_SC2 |= ADC_SC2_REFSEL(voltageRef);
		break;
	default:
		break;
	}
}
void ADC_SetDMA(ADC_NameType adcName, ADC_DMA adcDma)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC2 &= ~ADC_SC2_DMAEN_MASK;
		ADC0_SC2 |= ADC_SC2_DMAEN(adcDma);
		break;
	case ADC_1:
		ADC1_SC2 &= ~ADC_SC2_DMAEN_MASK;
		ADC1_SC2 |= ADC_SC2_DMAEN(adcDma);
		break;
	default:
		break;
	}
}
void ADC_SetFunctionRange(ADC_NameType adcName, ADC_CompareFunctionRange functionRange)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC2 &= ~ADC_SC2_ACREN_MASK;
		ADC0_SC2 |= ADC_SC2_ACREN(functionRange);
		break;
	case ADC_1:
		ADC1_SC2 &= ~ADC_SC2_ACREN_MASK;
		ADC1_SC2 |= ADC_SC2_ACREN(functionRange);
		break;
	default:
		break;
	}
}
void ADC_SetCompareFunctionGreaterThan(ADC_NameType adcName, ADC_CompareFunctionGreaterThan functionGreaterThanConfig)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC2 &= ~ADC_SC2_ACFGT_MASK;
		ADC0_SC2 |= ADC_SC2_ACFGT(functionGreaterThanConfig);
		break;
	case ADC_1:
		ADC1_SC2 &= ~ADC_SC2_ACFGT_MASK;
		ADC1_SC2 |= ADC_SC2_ACFGT(functionGreaterThanConfig);
		break;
	default:
		break;
	}
}
void ADC_SetCompareFunction(ADC_NameType adcName, ADC_CompareFunction compareFunction)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC2 &= ~ADC_SC2_ACFE_MASK;
		ADC0_SC2 |= ADC_SC2_ACFE(compareFunction);
		break;
	case ADC_1:
		ADC1_SC2 &= ~ADC_SC2_ACFE_MASK;
		ADC1_SC2 |= ADC_SC2_ACFE(compareFunction);
		break;
	default:
		break;
	}
}
void ADC_SetConversionTriggerSelect(ADC_NameType adcName, ADC_ConversionTriggerSelectType convTriggerSelect)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC2 &= ~ADC_SC2_ADTRG_MASK;
		ADC0_SC2 |= ADC_SC2_ADTRG(convTriggerSelect);
		break;
	case ADC_1:
		ADC1_SC2 &= ~ADC_SC2_ADTRG_MASK;
		ADC1_SC2 |= ADC_SC2_ADTRG(convTriggerSelect);
		break;
	default:
		break;
	}
}
void ADC_SetAsyncOutputClock(ADC_NameType adcName, ADC_AsyncOutputClock asyncOutputClk)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG2 &= ~ADC_CFG2_ADACKEN_MASK;
		ADC0_CFG2 |= ADC_CFG2_ADACKEN(asyncOutputClk);
		break;
	case ADC_1:
		ADC1_CFG2 &= ~ADC_CFG2_ADACKEN_MASK;
		ADC1_CFG2 |= ADC_CFG2_ADACKEN(asyncOutputClk);
		break;
	default:
		break;
	}
}
void ADC_SetMuxType(ADC_NameType adcName, ADC_MuxType muxType)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG2 &= ~ADC_CFG2_MUXSEL_MASK;
		ADC0_CFG2 |= ADC_CFG2_MUXSEL(muxType);
		break;
	case ADC_1:
		ADC1_CFG2 &= ~ADC_CFG2_MUXSEL_MASK;
		ADC1_CFG2 |= ADC_CFG2_MUXSEL(muxType);
		break;
	default:
		break;
	}
}
void ADC_ClockSourceSelection(ADC_NameType adcName, ADC_ClockSource clockSrc)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG1 &= ~ADC_CFG1_ADICLK_MASK;
		ADC0_CFG1 |= ADC_CFG1_ADICLK(clockSrc);
		break;
	case ADC_1:
		ADC1_CFG1 &= ~ADC_CFG1_ADICLK_MASK;
		ADC1_CFG1 |= ADC_CFG1_ADICLK(clockSrc);
		break;
	default:
		break;
	}
}
void ADC_SetResolution(ADC_NameType adcName, ADC_ResolutionMode resMode)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG1 &= ~ADC_CFG1_MODE_MASK;
		ADC0_CFG1 |= ADC_CFG1_MODE(resMode);
		break;
	case ADC_1:
		ADC1_CFG1 &= ~ADC_CFG1_MODE_MASK;
		ADC1_CFG1 |= ADC_CFG1_MODE(resMode);
		break;
	default:
		break;
	}
}
void ADC_SetSampleType(ADC_NameType adcName, ADC_SampleType sampleType)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG1 &= ~ADC_CFG1_ADLSMP_MASK;
		ADC0_CFG1 |= ADC_CFG1_ADLSMP(sampleType);
		break;
	case ADC_1:
		ADC1_CFG1 &= ~ADC_CFG1_ADLSMP_MASK;
		ADC1_CFG1 |= ADC_CFG1_ADLSMP(sampleType);
		break;
	default:
		break;
	}
}
void ADC_SetDivRatio(ADC_NameType adcName, ADC_DivRatioType divRatio)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG1 &= ~ADC_CFG1_ADIV_MASK;
		ADC0_CFG1 |= ADC_CFG1_ADIV(divRatio);
		break;
	case ADC_1:
		ADC1_CFG1 &= ~ADC_CFG1_ADIV_MASK;
		ADC1_CFG1 |= ADC_CFG1_ADIV(divRatio);
		break;
	default:
		break;
	}
}
void ADC_SetLowPowerConfiguration(ADC_NameType adcName, ADC_LowPowerConfiguration lwpConfig)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_CFG1 &= ~ADC_CFG1_ADLPC_MASK;
		ADC0_CFG1 |= ADC_CFG1_ADLPC(lwpConfig);
		break;
	case ADC_1:
		ADC1_CFG1 &= ~ADC_CFG1_ADLPC_MASK;
		ADC1_CFG1 |= ADC_CFG1_ADLPC(lwpConfig);
		break;
	default:
		break;
	}
}

void ADC_ClockGatingDisable(ADC_NameType adcName)
{
	switch (adcName)
	{
	case ADC_0:
		SIM_SCGC6 &= ~SIM_SCGC6_ADC0_MASK;
		break;
	case ADC_1:
		SIM_SCGC3 &= ~SIM_SCGC3_ADC1_MASK;
		break;
	default:
		break;
	}
}
void ADC_clockGatingEnable(ADC_NameType adcName)
{
	switch (adcName)
	{
	case ADC_0:
		SIM_SCGC6 |= SIM_SCGC6_ADC0_MASK;
		break;
	case ADC_1:
		SIM_SCGC3 |= SIM_SCGC3_ADC1_MASK;
		break;
	default:
		break;
	}
}
void ADC_ChannelSelect(ADC_NameType adcName, ADC_ChannelType adcChannel)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC1A&= ~ADC_SC1_ADCH_MASK;
		ADC0_SC1A|= ADC_SC1_ADCH(adcChannel);
		break;
	case ADC_1:
		ADC1_SC1A&= ~ADC_SC1_ADCH_MASK;
		ADC1_SC1A|= ADC_SC1_ADCH(adcChannel);
		break;
	default:
		break;
	}
}
void ADC_ConfigConversionInterrupt(ADC_NameType adcName, ADC_ConversionInterrupt convInterrupt)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC1A&= ~ADC_SC1_AIEN_MASK;
		ADC0_SC1A|= ADC_SC1_AIEN(convInterrupt);
		if(convInterrupt)
		{
			NVIC_enableInterruptAndPriotity(ADC0_IRQ,PRIORITY_8);
		}
		break;
	case ADC_1:
		ADC1_SC1A&= ~ADC_SC1_AIEN_MASK;
		ADC1_SC1A|= ADC_SC1_AIEN(convInterrupt);
		if(convInterrupt)
		{
			NVIC_enableInterruptAndPriotity(ADC1_IRQ,PRIORITY_8);
		}
		break;
	default:
		break;
	}
}
void ADC_ModeSelect(ADC_NameType adcName, ADC_Mode adcMode)
{
	switch(adcName)
	{
	case ADC_0:
		ADC0_SC1A&= ~ADC_SC1_DIFF_MASK;
		ADC0_SC1A|= ADC_SC1_DIFF(adcMode);
		break;
	case ADC_1:
		ADC1_SC1A&= ~ADC_SC1_DIFF_MASK;
		ADC1_SC1A|= ADC_SC1_DIFF(adcMode);
		break;
	default:
		break;
	}
}
void ADC_initializeAdc(const ADC_Config* adcConfig)
{
	ADC_clockGatingEnable(adcConfig->adcName);
	ADC_ChannelSelect(adcConfig->adcName, adcConfig->adcChannel);
	ADC_ConfigConversionInterrupt(adcConfig->adcName, adcConfig->convInterrupt);
	ADC_ModeSelect(adcConfig->adcName, adcConfig->adcMode);
	ADC_ClockSourceSelection(adcConfig->adcName, adcConfig->clockSrc);
	ADC_SetResolution(adcConfig->adcName, adcConfig->resMode);
	ADC_SetSampleType(adcConfig->adcName, adcConfig->sampleType);
	ADC_SetDivRatio(adcConfig->adcName, adcConfig->divRatio);
	ADC_SetLowPowerConfiguration(adcConfig->adcName, adcConfig->lwpConfig);

	ADC_SetHardwareAverageSampleNumber(adcConfig->adcName, adcConfig->hardwareAverageSelect);
	ADC_SetHardwareAverageEnable(adcConfig->adcName, adcConfig->hardwareAverage);
	ADC_SetContinuousConvertType(adcConfig->adcName, adcConfig->continuousConvert);
	ADC_SetVoltageRefType(adcConfig->adcName, adcConfig->voltageRef);
	ADC_SetDMA(adcConfig->adcName, adcConfig->adcDma);
	ADC_SetFunctionRange(adcConfig->adcName, adcConfig->functionRange);
	ADC_SetCompareFunctionGreaterThan(adcConfig->adcName, adcConfig->functionGreaterThanConfig);
	ADC_SetCompareFunction(adcConfig->adcName, adcConfig->compareFunction);
	ADC_SetConversionTriggerSelect(adcConfig->adcName, adcConfig->convTriggerSelect);
	ADC_SetAsyncOutputClock(adcConfig->adcName, adcConfig->asyncOutputClk);
	ADC_SetMuxType(adcConfig->adcName, adcConfig->muxType);
}
void ADC_writeAdc0Value(unsigned int value)
{
	adc0value = ((((double)value)*(3.3))/65535.0)/0.01;;
}

double ADC_getAdc0Value()
{
	return adc0value;
}
void ADC_enableSingleRead(ADC_ChannelType adcChannel)
{
	ADC0_SC1A &= ((~(ADC_SC1_ADCH_MASK))| ADC_SC1_ADCH(adcChannel));
}
unsigned int ADC_readUnsignedData(ADC_NameType adcName, ADC_ChannelType adcChannel)
{

	switch(adcName)
	{
	case ADC_0:
		ADC0_SC1A &= ~(ADC_SC1_ADCH_MASK)| ADC_SC1_ADCH(adcChannel);
		while(ADC0_SC2 & ADC_SC1_ADCH_MASK);
		while(!(ADC0_SC1A & ADC_SC1_COCO_MASK));
		return ADC0_RA;
		break;
	case ADC_1:
		ADC1_SC1A &= ~(ADC_SC1_ADCH_MASK)| ADC_SC1_ADCH(adcChannel);
		while(ADC1_SC2 & ADC_SC1_ADCH_MASK);
		while(!(ADC1_SC1A & ADC_SC1_COCO_MASK));
		return ADC1_RA;
		break;
	default:
		return 0;
		break;
	}
}
signed int ADC_readSignedData(ADC_NameType adcName)
{
	return 0;
}
