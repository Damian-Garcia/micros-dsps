/*
 * ControlPantalla.h
 *
 *  Created on: Nov 2, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_CONTROLPANTALLA_H_
#define SOURCES_CONTROLPANTALLA_H_


#include "DataTypeDefinitions.h"
#include "Eventos.h"
#include "UARTDriver.h"

typedef enum{
	FSM_ECO_IDLE,
	FSM_ECO_WAIT_CHAR,
	FSM_ECO_WRITE_CHAR,
	FSM_ECO_END,
}FSM_Eco_StateEnumType;

typedef enum{
	FSM_PANTALLA_IDLE,
	FSM_PANTALLA_ESPERAR_USUARIO,

	FSM_PANTALLA_EDITAR_FECHA,
	FSM_PANTALLA_ESPERAR_FECHA,
	FSM_PANTALLA_GUARDAR_FECHA,

	FSM_PANTALLA_EDITAR_HORA,
	FSM_PANTALLA_ESPERAR_HORA,
	FSM_PANTALLA_GUARDAR_HORA,

	FSM_PANTALLA_INC_HORA,
	FSM_PANTALLA_INC_MIN,
	FSM_PANTALLA_INC_SEG,

	FSM_PANTALLA_INC_DIA,
	FSM_PANTALLA_INC_MES,
	FSM_PANTALLA_INC_ANO,

	FSM_PANTALLA_ENUM_MAX
}FSM_ControlPantalla;

void ControlPantalla_init();
void ControlPantalla_PrintCurrent();
BooleanType ControlPantalla_isBusy();
BooleanType ControlPantalla_isEnabled();

void ControlPantalla_setLocalMode();
void ControlPantalla_setEcoMode(UART_ChannelType uart);

void ControlPantalla_evalEco(FSM_Eco_LCD_EVT_Type c);
void ControlPantalla_evalLocal();
void ControlPantalla_addEvent(FSM_Pantalla_EVT_Type evt);

void ControlPantalla_addEcoChar(uint8);
void ControlPantalla_clearScreen();
void ControlPantalla_setReady();

void ControlPantalla_clearBusy();
#endif /* SOURCES_CONTROLPANTALLA_H_ */
