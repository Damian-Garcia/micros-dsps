/*
 * ReadTime.c
 *
 *  Created on: Oct 30, 2016
 *      Author: Damian Garcia
 */


#include "ReadTime.h"
#include "RTC7940M.h"
typedef void (*action) (void);

FSM_ReadTime_InstanceType fsm;
BooleanType busyFlag;

struct FSM_ReadTime_StateType {
	action a;
	const FSM_ReadTime_StateEnumType *nextList;
};

static void Idle() {

}

static void Read() {
	//static uint8 arreglo[] = "1234\r\n";
	UART_putChar(fsm.uart, ' ');
	static uint8 arrayOfTime[10] = {0};
	arrayOfTime[7] = RTC_getSecondsOnesAscii();
	arrayOfTime[6] =RTC_getSecondsTensAscii();
	arrayOfTime[5] = ':';
	arrayOfTime[4] = RTC_getMinutesOnesAscii();
	arrayOfTime[3] = RTC_getMinutesTensAscii();
	arrayOfTime[2] = ':';
	arrayOfTime[1] = RTC_getHoursOnesAscii();
	arrayOfTime[0] = RTC_getHoursTensAscii();
	arrayOfTime[8] = '\r';
	arrayOfTime[9] = '\n';
	if(FALSE == getErrorRtcFlag()){
		UART_putString(fsm.uart, arrayOfTime);
		if(TRUE == RTC_isAMPMenabled())
		{
			if(TRUE == RTC_isPMactive())
			{
				UART_putString(fsm.uart," PM ");

			}
			else
			{
			    UART_putString(fsm.uart, "AM ");
			}
		}
	}else{
		UART_putString(fsm.uart, "Error de lectura de hora");
	}
	fsm.current = FSM_READTIME_WAITING;


}

static void Error() {
	static uint8 arreglo[] = "mensaje de error en lectura\r\n";
}

static void Waiting() {

}

static void End() {
	fsm.enable = FALSE;
	busyFlag = FALSE;
}

static FSM_ReadTime_StateEnumType IdleNext[FSM_READTIME_ENUM_MAX] = {
	FSM_READTIME_READ,
	FSM_READTIME_READ,
	FSM_READTIME_READ,
	FSM_READTIME_READ,
	FSM_READTIME_READ,

};

static FSM_ReadTime_StateEnumType ReadNext[FSM_READTIME_ENUM_MAX] = {
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
};

static FSM_ReadTime_StateEnumType ErrorNext[FSM_READTIME_ENUM_MAX] = {
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
};

static FSM_ReadTime_StateEnumType WaitingNext[FSM_READTIME_ENUM_MAX] = {
	FSM_READTIME_END,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
	FSM_READTIME_WAITING,
};

static FSM_ReadTime_StateEnumType EndNext[FSM_READTIME_ENUM_MAX] = {
	FSM_READTIME_IDLE,
	FSM_READTIME_IDLE,
	FSM_READTIME_IDLE,
	FSM_READTIME_IDLE,
	FSM_READTIME_IDLE,

};


static const struct FSM_ReadTime_StateType Estados[] = {
	{ Idle, IdleNext },
	{ Read, ReadNext },
	{ Error, ErrorNext },
	{ Waiting, WaitingNext },
	{ End, EndNext },
};

FSM_ReadTime_InstanceType* ReadTime_getInstance() {
	return &fsm;
}

void ReadTime_init(UART_ChannelType uart) {
	fsm.current = FSM_READTIME_IDLE;
	fsm.uart = uart;
	fsm.enable = TRUE;
	busyFlag = TRUE;
}

BooleanType ReadTime_isBusy() {
	return busyFlag;
}

BooleanType ReadTime_isEnabled() {
	return fsm.enable;
}

void ReadTime_eval(FSM_Leer_Hora_EVT_Type evt) {
	//fsm.current = fsm.current
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}

void ReadTime_clearBusy(){
	busyFlag = FALSE;
}
