/*
 * ControlPantalla.c
 *
 *  Created on: Nov 1, 2016
 *      Author: Damian Garcia
 */

#include "ControlPantalla.h"
#include "LCDNokia5110.h"
#include "Formatter.h"
#include "RTC7940M.h"
#include "Formatter.h"
#include "I2C.h"

typedef void(*action)(void);

typedef enum{
	PANTALLA_IMPRIMIR_TIME,
	PANTALLA_ECO,
	PANTALLA_MODIFICAR_HORA,
	PANTALLA_MODIFICAR_FECHA,
}PantallasEnumType;

struct FSM_ControlPantalla_StateType {
	action a;
	const FSM_ControlPantalla *nextList;
};

static BooleanType busyFlag;
static BooleanType readyFlag = FALSE;
static BooleanType enable;
static PantallasEnumType pantallaActual;

static FSM_Eco_StateEnumType currentEco;
static UART_ChannelType currentUart;

static uint8 lastChar;
static uint8 currentX;
static uint8 currentY;

static FSM_ControlPantalla currentControlPantalla;
static uint32 tempSeg, tempMin, tempHour;
static uint32 tempDay, tempMon, tempYear;
FSM_Pantalla_EVT_Type lastEvt;

static uint8 buffSec[9];
static uint8 buffMin[9];
static uint8 buffHour[9];
static uint8 buffYear[9];
static uint8 buffMon[9];
static uint8 buffDay[9];

static void printPrincipal(){
	//TODO hacer lectura
	if(pantallaActual ==  PANTALLA_IMPRIMIR_TIME ){
		tms(300);


		static uint8 arrayOfTime[10] = {0};
		static uint8 arrayOfDate[12];
		arrayOfDate[0] = RTC_getDayTensAscii();
		if(FALSE == getErrorRtcFlag())
		{
		arrayOfDate[1] = RTC_getDayOnesAscii();
		arrayOfDate[2] = '/';
		arrayOfDate[3] = RTC_getMonthTensAscii();
		arrayOfDate[4] = RTC_getMonthOnesAscii();
		arrayOfDate[5] = '/';
		arrayOfDate[6] = RTC_getYearCentsTensAscii();
		arrayOfDate[7] = RTC_getYearCentsOnesAscii();
		arrayOfDate[8] = RTC_getYearTensAscii();
		arrayOfDate[9] = RTC_getYearOnesAscii();
		arrayOfTime[10] = '\0';
		arrayOfTime[7] = RTC_getSecondsOnesAscii();
		arrayOfTime[6] = RTC_getSecondsTensAscii();
		arrayOfTime[5] = ':';
		arrayOfTime[4] = RTC_getMinutesOnesAscii();
		arrayOfTime[3] = RTC_getMinutesTensAscii();
		arrayOfTime[2] = ':';
		arrayOfTime[1] = RTC_getHoursOnesAscii();
		arrayOfTime[0] = RTC_getHoursTensAscii();
		arrayOfTime[8] = '\0';
		}
		else
		{

		}

		//RTC_getSecondsOnesAscii();
		if(TRUE == getErrorRtcFlag())
		{
			LCDNokia_gotoXY(0,0);
			ControlPantalla_clearScreen();
			//LCDNokia_sendString("HORA");
			LCDNokia_sendString(" Error de lectura de hora");
		}
		else if(FALSE == getErrorRtcFlag()){
			LCDNokia_gotoXY(0,0);
			ControlPantalla_clearScreen();
			LCDNokia_sendString("HORA ");
			LCDNokia_sendString(arrayOfTime);
			if(TRUE == RTC_isAMPMenabled())
			{
				if(TRUE == RTC_isPMactive())
				{
					LCDNokia_sendString(" pm ");
				}
				else
				{
					LCDNokia_sendString(" am ");
				}
			}
			LCDNokia_sendString("Fecha: ");
			LCDNokia_sendString(arrayOfDate);

			//tms(150);
			//LCDNokia_gotoXY(0,0);

		}
		//TODO: llamar RTC

	}else{

	}
}

static void Idle(){}

static void esperarUsuario(){
	pantallaActual =  PANTALLA_IMPRIMIR_TIME;
}

static void printEditarHora(){
	formatUnsignedInteger32(tempSeg,buffSec,-1) ;
	formatUnsignedInteger32(tempMin,buffMin,-1) ;
	formatUnsignedInteger32(tempHour,buffHour,-1) ;
	LCDNokia_clear();
	LCDNokia_sendString("Modificar Hora: ");
	LCDNokia_gotoXY(0,2);
	LCDNokia_sendString(buffHour);
	LCDNokia_sendChar(':');
	LCDNokia_sendString(buffMin);
	LCDNokia_sendChar(':');
	LCDNokia_sendString(buffSec);
}

static void printEditarFecha(){
	formatUnsignedInteger32(tempDay,buffDay,-1) ;
	formatUnsignedInteger32(tempMon,buffMon,-1) ;
	formatUnsignedInteger32(tempYear,buffYear,-1) ;
	LCDNokia_clear();

	LCDNokia_sendString("Modificar Fecha: ");
	LCDNokia_sendString(buffDay);
	LCDNokia_sendChar('/');
	LCDNokia_sendString(buffMon);
	LCDNokia_sendChar('/');
	LCDNokia_sendString(buffYear);
}



static void FechaHora(){

}

static void EditarHora(){
	pantallaActual = PANTALLA_MODIFICAR_HORA;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_HORA;
	tempSeg = 0, tempMin = 0, tempHour = 0;
}

static void incSeg(){
	tempSeg++ ;
	tempSeg = tempSeg % 60;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_HORA;
}

static void incMin(){
	tempMin++;
	tempMin = tempMin % 60;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_HORA;
}

static void incHour(){
	tempHour = (++tempHour) % 11;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_HORA;
}

static void esperarHora(){

}

static void actualizarHora(){
	///TODO: llamar a RTC
	currentControlPantalla = FSM_PANTALLA_ESPERAR_USUARIO;
}

static void EditarFecha(){
	pantallaActual = PANTALLA_MODIFICAR_FECHA;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_FECHA;
	tempDay = 1, tempMon = 1, tempYear = 2000;
}

static void incDay(){
	tempDay =  (tempDay++) % 31 + 1;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_FECHA;
}

static void incMon(){
	tempMon = (tempMon++) % 12 + 1;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_FECHA;
}

static void incYear(){
	tempYear++;
	currentControlPantalla = FSM_PANTALLA_ESPERAR_FECHA;
}

static void esperarFecha(){

}

static void actualizarFecha(){


	currentControlPantalla = FSM_PANTALLA_ESPERAR_USUARIO;
}

static void guardarFecha(){
	//TODO: llamar RTC
	uint16 dateArrayTemp[4];
	dateArrayTemp[0] = (IntegerToHex(tempDay,2));//Creamos el primer bcd
	dateArrayTemp[1] = (IntegerToHex(tempMon,2));//Creamos el primer bcd
	uint16 bcdyeartemp1 = IntegerToHex(tempYear,4);
	dateArrayTemp[2] = ((0x00FF & bcdyeartemp1));//Creamos el primer bcd de los anios parte baja
	dateArrayTemp[3] = (( (0xFF00 & bcdyeartemp1)>>8 ));//Creamos el primer bcd de los anios parte baja
	RTC_setFullDate(dateArrayTemp);//Se envia el nuevo array con las horas
	if(FALSE == getErrorRtcFlag()){
		LCDNokia_sendString("La fecha ha sido cambiada\r\n");
	}else{
		LCDNokia_sendString("La fecha NO PUDO ser cambiada\r\n");
	}
	tms(1000);
	currentControlPantalla = FSM_PANTALLA_ESPERAR_USUARIO;
}

static void guardarHora(){

	uint8 hoursArray[3] = {0};
	hoursArray[0] = IntegerToHex(tempHour,2);//Creamos el primer bcd
	hoursArray[1] = IntegerToHex(tempMin,2);//Creamos el primer bcd
	hoursArray[2] = IntegerToHex(tempSeg,2);//Creamos el primer bcd
	RTC_setFullHour(hoursArray);//Se envia el nuevo array con las horas
	if(FALSE == getErrorRtcFlag()){
		LCDNokia_sendString("La hora ha sido cambiada\r\n");
	}else{
		LCDNokia_sendString("La hora NO PUDO ser cambiada\r\n");
	}
	tms(1000);

	currentControlPantalla = FSM_PANTALLA_ESPERAR_USUARIO;
}

FSM_ControlPantalla IdleNext[FSM_Pantalla_EVT_ENUM_MAX] = {
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
};



FSM_ControlPantalla esperarUsuarioNext[FSM_Pantalla_EVT_ENUM_MAX] = {
	FSM_PANTALLA_EDITAR_FECHA,
	FSM_PANTALLA_EDITAR_HORA,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,
	FSM_PANTALLA_ESPERAR_USUARIO,

};

FSM_ControlPantalla esperarHoraNext[FSM_Pantalla_EVT_ENUM_MAX] = {
		FSM_PANTALLA_ESPERAR_HORA,
		FSM_PANTALLA_ESPERAR_HORA,
		FSM_PANTALLA_INC_SEG,
		FSM_PANTALLA_INC_MIN,
		FSM_PANTALLA_INC_HORA,
		FSM_PANTALLA_GUARDAR_HORA,

		FSM_PANTALLA_ESPERAR_HORA,

};

FSM_ControlPantalla esperarFechaNext[FSM_Pantalla_EVT_ENUM_MAX] = {
		FSM_PANTALLA_ESPERAR_FECHA,
		FSM_PANTALLA_ESPERAR_FECHA,
		FSM_PANTALLA_INC_DIA,
		FSM_PANTALLA_INC_MES,
		FSM_PANTALLA_INC_ANO,
		FSM_PANTALLA_GUARDAR_FECHA,
		FSM_PANTALLA_ESPERAR_FECHA,

};



struct FSM_ControlPantalla_StateType Estados[] = {
		{Idle, IdleNext},
		{esperarUsuario, esperarUsuarioNext},
		{EditarFecha, IdleNext},
		{esperarFecha, esperarFechaNext},
		{guardarFecha, IdleNext},
		{EditarHora, IdleNext},
		{esperarHora, esperarHoraNext},
		{guardarHora, IdleNext},
		{incHour, IdleNext},
		{incMin, IdleNext},
		{incSeg, IdleNext},
		{incDay, IdleNext},
		{incMon, IdleNext},
		{incYear, IdleNext},
};

void ControlPantalla_init(){
	busyFlag = FALSE;
	readyFlag = TRUE;
	pantallaActual  = PANTALLA_IMPRIMIR_TIME;
}

void ControlPantalla_PrintCurrent(){
	if(readyFlag){
		readyFlag = FALSE;
		switch(pantallaActual){
		case PANTALLA_IMPRIMIR_TIME:
			printPrincipal();
			break;
		case PANTALLA_MODIFICAR_FECHA:
			printEditarFecha();
			break;;
		case PANTALLA_MODIFICAR_HORA:
			printEditarHora();
			break;
		}
	}
}

BooleanType ControlPantalla_isBusy(){
	return busyFlag;
}

BooleanType ControlPantalla_isEnabled()
{
	return enable;
}

void ControlPantalla_setLocalMode(){
	pantallaActual = PANTALLA_IMPRIMIR_TIME;
}

void ControlPantalla_setEcoMode(UART_ChannelType uart){
	pantallaActual = PANTALLA_ECO;
	currentUart = uart;
	ControlPantalla_clearScreen();
	busyFlag = TRUE;
	enable = TRUE;
}

void ControlPantalla_evalEco(FSM_Eco_LCD_EVT_Type c){
	if(enable){
		if(FSM_ECO_LCD_ESC != c && FSM_ECO_LCD_NOP != c){
			LCDNokia_sendChar(lastChar);
			UART_putChar(currentUart, lastChar);

		}else if(FSM_ECO_LCD_ESC == c){
			enable = FALSE;
			busyFlag = FALSE;
			LCDNokia_clear();
			pantallaActual = PANTALLA_IMPRIMIR_TIME;
		}
	}
}

void ControlPantalla_evalLocal(){
	if(!busyFlag){
		currentControlPantalla = Estados[currentControlPantalla].nextList[lastEvt];
		lastEvt = FSM_Pantalla_EVT_NOP;
		Estados[currentControlPantalla].a();
	}
	}

void ControlPantalla_addEvent(FSM_Pantalla_EVT_Type evt){
		lastEvt = evt;
}

void ControlPantalla_addEcoChar(uint8 c){
	lastChar = c;
}
void ControlPantalla_clearScreen(){
	LCDNokia_clear();
}

void ControlPantalla_setReady(){
	readyFlag = TRUE;
}

void ControlPantalla_clearBusy(){
	busyFlag = FALSE;
}
