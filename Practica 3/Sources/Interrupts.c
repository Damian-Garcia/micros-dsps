/*
 * Interrupts.c
 *
 *  Created on: Nov 2, 2016
 *      Author: Damian Garcia
 */

#include "GPIO.h"
#include "Eventos.h"
#include "ControlPantalla.h"
#include "MK64F12.h"
#include "NVIC.h"

void PORTC_IRQHandler(){
	GPIO_clearInterrupt(GPIOC);
	NVIC_DisableIRQ(PORTC_IRQn);
	uint32 data = GPIO_readPORT(GPIOC);
	NVIC_EnableIRQ(PORTC_IRQn);
	data = (~data)&0x3F;
	if(data == 1){
		ControlPantalla_addEvent(FSM_Pantalla_EVT_BTN0);
	}else if(data == 2){
		ControlPantalla_addEvent(FSM_Pantalla_EVT_BTN1);
	}else if(data == 4){
		ControlPantalla_addEvent(FSM_Pantalla_EVT_BTN2);
	}else if(data == 8){
		ControlPantalla_addEvent(FSM_Pantalla_EVT_BTN3);
	}else if(data == 16){
		ControlPantalla_addEvent(FSM_Pantalla_EVT_BTN4);
	}else if(data == 32){
		ControlPantalla_addEvent(FSM_Pantalla_EVT_BTN5);
	}else{
		ControlPantalla_addEvent(FSM_Pantalla_EVT_NOP);
	}
}
