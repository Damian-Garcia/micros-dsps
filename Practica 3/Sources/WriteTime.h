/*
 * WriteTime.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_WRITETIME_H_
#define SOURCES_WRITETIME_H_

#include "DataTypeDefinitions.h"
#include "Eventos.h"
#include "UARTDriver.h"

typedef enum{
	FSM_WRITETIME_IDLE,
	FSM_WRITETIME_WAIT_NUMBER,
	FSM_WRITETIME_STORE_NUMBER,
	FSM_WRITETIME_WRITE_RTC,
	FSM_WRITETIME_ERROR,
	FSM_WRITETIME_WAIT_ESC,
	FSM_WRITETIME_END,
	FSM_WRITETIME_ENUM_MAX,
}FSM_WriteTime_StateEnumType;

typedef struct  {
	FSM_WriteTime_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_WriteTime_InstanceType;

FSM_WriteTime_InstanceType* WriteTime_getInstance();
void WriteTime_init(UART_ChannelType);
BooleanType WriteTime_isBusy();
BooleanType WriteTime_isEnabled();
void WriteTime_eval(FSM_Establecer_Hora_EVT_Type);
void WriteTime_addChar(uint8);

void WriteTime_clearBusy();

#endif /* SOURCES_WRITETIME_H_ */
