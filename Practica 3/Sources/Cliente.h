/*
 * Cliente.h
 *
 *  Created on: Oct 29, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_CLIENTE_H_
#define SOURCES_CLIENTE_H_

#include "FSM_Selector.h"
#include "UARTDriver.h"

typedef enum{
	MAIN_MENU,
	LEER_MEMORIA,
	ESCRIBIR_MEMORIA,
	ESTABLECER_HORA,
	ESTABLECER_FECHA,
	FORMATO_HORA,
	LEER_HORA,
	LEER_FECHA,
	COMUNICACION_TERMINAL,
	ECO_LCD
}Process;

typedef struct{
	FSM_Selector_InstanceType Selector;
	void* currentFSM;
	Process currentProcess;
	uint8 currentEvent;
	BooleanType eventReady;
	UART_ChannelType uart;
}Cliente;

void Cliente_evaluate(Cliente *);
void Cliente_init(Cliente *, UART_ChannelType);
void addClientEvent(Cliente *, uint8 evt);

#endif /* SOURCES_CLIENTE_H_ */
