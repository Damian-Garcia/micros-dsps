/*
 * WriteFormat.h
 *
 *  Created on: Nov 1, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_WRITEFORMAT_H_
#define SOURCES_WRITEFORMAT_H_

#include "DataTypeDefinitions.h"
#include "UARTDriver.h"
#include "Eventos.h"

typedef enum{
	FSM_WRITEFORMAT_IDLE,
	FSM_WRITEFORMAT_CURRENT,
	FSM_WRITEFORMAT_WAIT_CHAR,
	FSM_WRITEFORMAT_STORE_CHAR,
	FSM_WRITEFORMAT_WRITE_RTC,
	FSM_WRITEFORMAT_ERROR,
	FSM_WRITEFORMAT_WAIT_ESC,
	FSM_WRITEFORMAT_END,
	FSM_WRITEFORMAT_ENUM_MAX,
}FSM_WriteFormat_StateEnumType;

typedef struct  {
	FSM_WriteFormat_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_WriteFormat_InstanceType;

FSM_WriteFormat_InstanceType* WriteFormat_getInstance();
void WriteFormat_init(UART_ChannelType);
BooleanType WriteFormat_isBusy();
BooleanType WriteFormat_isEnabled();
void WriteFormat_eval(FSM_Formato_Hora_EVT_Type);
void WriteFormat_addChar(uint8);

void WriteFormat_clearBusy();

#endif /* SOURCES_WRITEFORMAT_H_ */
