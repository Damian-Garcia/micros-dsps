/*
 * WriteDate.c
 *
 *  Created on: Nov 1, 2016
 *      Author: Damian Garcia
 */

#include "WriteDate.h"
#include "RTC7940M.h"
#include "Formatter.h"
typedef void(*action)(void);

struct FSM_WriteDate_StateType {
	action a;
	const FSM_WriteDate_StateEnumType *nextList;
};

static FSM_WriteDate_InstanceType fsm;
static BooleanType busyFlag;
static uint8 lengthFecha;
static uint8 lastChar;
static uint8 bufferFecha[10];

static void idle(){

}

static void waitNumber(){

}

static void storeNumber(){
	UART_putChar(fsm.uart, lastChar);

	if('0' <= lastChar && '9' >= lastChar){
		bufferFecha[lengthFecha++] = lastChar;
		bufferFecha[lengthFecha] = 0;
	}
	fsm.current = FSM_WRITEDATE_WAIT_NUMBER;
}

static void writeRTC(){

	uint16 dateArray[4] = {0};
	dateArray[0] = (convertAsciiDigitToBinary(bufferFecha[0])<<4) | (convertAsciiDigitToBinary(bufferFecha[1]));//Creamos el primer bcd
	dateArray[1] = (convertAsciiDigitToBinary(bufferFecha[2])<<4) | (convertAsciiDigitToBinary(bufferFecha[3]));//Creamos el primer bcd
	dateArray[2] = (convertAsciiDigitToBinary(bufferFecha[6])<<4) | (convertAsciiDigitToBinary(bufferFecha[7]));//Creamos el primer bcd
	dateArray[3] = (convertAsciiDigitToBinary(bufferFecha[4])<<4) | (convertAsciiDigitToBinary(bufferFecha[5])<<0); //Arreglo ascii de cent
	RTC_setFullDate(dateArray);//Se envia el nuevo array con las horas

	if(FALSE == getErrorRtcFlag()){
		UART_putString(fsm.uart, "La fecha ha sido cambiada\r\n");
		fsm.current = FSM_WRITEDATE_WAIT_ESC;
	}else{
		fsm.current = FSM_WRITEDATE_WAIT_ESC; //FSM_WRITETIME_ERROR;
	}

}

static void waitEsc(){

}

static void error(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
	lengthFecha = 0;
}

static void end(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
	lengthFecha = 0;
}

static FSM_WriteDate_StateEnumType idleNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {
	FSM_WRITEDATE_WAIT_NUMBER,
	FSM_WRITEDATE_WAIT_NUMBER,
	FSM_WRITEDATE_WAIT_NUMBER,
	FSM_WRITEDATE_WAIT_NUMBER,
	FSM_WRITEDATE_WAIT_NUMBER,
};

static const FSM_WriteDate_StateEnumType waitNumberNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {
	FSM_WRITEDATE_STORE_NUMBER,
	FSM_WRITEDATE_END,
	FSM_WRITEDATE_WRITE_RTC,
	FSM_WRITEDATE_ERROR,
	FSM_WRITEDATE_WAIT_NUMBER,
};

static const FSM_WriteDate_StateEnumType storeNumberNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {

};

static const FSM_WriteDate_StateEnumType writeRTCNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {

};

static const FSM_WriteDate_StateEnumType errorNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {

};

static const FSM_WriteDate_StateEnumType waitEscNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {
		FSM_WRITEDATE_WAIT_ESC,
		FSM_WRITEDATE_END,
		FSM_WRITEDATE_WAIT_ESC,
		FSM_WRITEDATE_WAIT_ESC,
		FSM_WRITEDATE_WAIT_ESC,
};

static const FSM_WriteDate_StateEnumType endNextList[FSM_ESTABLECER_FECHA_EVT_ENUM_MAX] = {

};

static const struct FSM_WriteDate_StateType Estados[FSM_WRITEDATE_ENUM_MAX] = {
	{idle, idleNextList},
	{waitNumber, waitNumberNextList},
	{storeNumber, storeNumberNextList},
	{writeRTC, writeRTCNextList},
	{error, errorNextList},
	{waitEsc, waitEscNextList},
	{end, endNextList},
};

FSM_WriteDate_InstanceType* WriteDate_getInstance(){
	return &fsm;
}

void WriteDate_init(UART_ChannelType uart){
	busyFlag = FALSE;
	lengthFecha = 0;
	lastChar = 0;
	fsm.uart = uart;
	fsm.current = FSM_WRITEDATE_IDLE;
	fsm.enable = TRUE;
	busyFlag = TRUE;
}
BooleanType WriteDate_isBusy(){
	return busyFlag;
}

BooleanType WriteDate_isEnabled(){
	return fsm.enable;
}

void WriteDate_eval(FSM_Establecer_Fecha_EVT_Type evt){
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}

void WriteDate_addChar(uint8 c){
	lastChar = c;
}

void WriteDate_clearBusy(){
	busyFlag = FALSE;
}
