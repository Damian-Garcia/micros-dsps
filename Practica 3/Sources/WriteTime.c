/*
 * WriteTime.c
 *
 *  Created on: Nov 1, 2016
 *      Author: Damian Garcia
 */
#include "WriteTime.h"
#include "UARTDriver.h"
#include "Formatter.h"
#include "RTC7940M.h"

typedef void(*action)(void);

struct FSM_WriteTime_StateType {
	action a;
	const FSM_WriteTime_StateEnumType *nextList;
};

static FSM_WriteTime_InstanceType fsm;
static BooleanType busyFlag;
static uint8 lengthHora;
static uint8 lastChar;
static uint8 bufferFecha[10];

static void idle(){

}

static void waitNumber(){

}

static void storeNumber(){
	UART_putChar(fsm.uart, lastChar);

	if('0' <= lastChar && '9' >= lastChar){
		bufferFecha[lengthHora++] = lastChar;
		bufferFecha[lengthHora] = 0;
	}
	fsm.current = FSM_WRITETIME_WAIT_NUMBER;
}

static void writeRTC(){
	//UART_putString(fsm.uart,"c");
	//uint8 hoursArray[3] = {0x11,0x30,0x1};
	//writeStreamofBytesToMemoryAddress(bufferMensaje, address, lengthMensaje);
	uint8 hoursArray[3] = {0};
	hoursArray[0] = (convertAsciiDigitToBinary(bufferFecha[0])<<4) | (convertAsciiDigitToBinary(bufferFecha[1]));//Creamos el primer bcd
	hoursArray[1] = (convertAsciiDigitToBinary(bufferFecha[2])<<4) | (convertAsciiDigitToBinary(bufferFecha[3]));//Creamos el primer bcd
	hoursArray[2] = (convertAsciiDigitToBinary(bufferFecha[4])<<4) | (convertAsciiDigitToBinary(bufferFecha[5]));//Creamos el primer bcd
	RTC_setFullHour(hoursArray);//Se envia el nuevo array con las horas


	if(FALSE == getErrorRtcFlag()){
		UART_putString(fsm.uart, "La hora ha sido cambiada\r\n");
		fsm.current = FSM_WRITETIME_WAIT_ESC;
	}else{
		fsm.current = FSM_WRITETIME_WAIT_ESC; //FSM_WRITETIME_ERROR;
	}
}

static void waitEsc(){

}

static void error(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
	lengthHora = 0;
}

static void end(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
	lengthHora = 0;
}

static FSM_WriteTime_StateEnumType idleNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {
	FSM_WRITETIME_WAIT_NUMBER,
	FSM_WRITETIME_WAIT_NUMBER,
	FSM_WRITETIME_WAIT_NUMBER,
	FSM_WRITETIME_WAIT_NUMBER,
	FSM_WRITETIME_WAIT_NUMBER,
};

static const FSM_WriteTime_StateEnumType waitNumberNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {
	FSM_WRITETIME_STORE_NUMBER,
	FSM_WRITETIME_END,
	FSM_WRITETIME_WRITE_RTC,
	FSM_WRITETIME_ERROR,
	FSM_WRITETIME_WAIT_NUMBER,
};

static const FSM_WriteTime_StateEnumType storeNumberNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {

};

static const FSM_WriteTime_StateEnumType writeRTCNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {

};

static const FSM_WriteTime_StateEnumType errorNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {

};

static const FSM_WriteTime_StateEnumType waitEscNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {
		FSM_WRITETIME_WAIT_ESC,
		FSM_WRITETIME_END,
		FSM_WRITETIME_WAIT_ESC,
		FSM_WRITETIME_WAIT_ESC,
		FSM_WRITETIME_WAIT_ESC,
};

static const FSM_WriteTime_StateEnumType endNextList[FSM_ESTABLECER_HORA_ENUM_MAX] = {

};

static const struct FSM_WriteTime_StateType Estados[FSM_WRITETIME_ENUM_MAX] = {
	{idle, idleNextList},
	{waitNumber, waitNumberNextList},
	{storeNumber, storeNumberNextList},
	{writeRTC, writeRTCNextList},
	{error, errorNextList},
	{waitEsc, waitEscNextList},
	{end, endNextList},
};

FSM_WriteTime_InstanceType* WriteTime_getInstance(){
	return &fsm;
}

void WriteTime_init(UART_ChannelType uart){
	busyFlag = TRUE;
	lengthHora = 0;
	lastChar = 0;
	fsm.uart = uart;
	fsm.current = FSM_WRITETIME_IDLE;
	fsm.enable = TRUE;
}
BooleanType WriteTime_isBusy(){
	return busyFlag;
}

BooleanType WriteTime_isEnabled(){
	return fsm.enable;
}

void WriteTime_eval(FSM_Establecer_Hora_EVT_Type evt){
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}

void WriteTime_addChar(uint8 c){
	lastChar = c;
}

void WriteTime_clearBusy(){
	busyFlag = FALSE;
}
