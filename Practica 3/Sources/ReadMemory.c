/*
 * ReadMemory.c
 *
 *  Created on: Oct 30, 2016
 *      Author: Damian Garcia
 */
#include "ReadMemory.h"
#include "EEPROM24LC256.h"
#include "Formatter.h"
typedef void(*action)(void);

struct FSM_ReadMemory_StateType {
	action a;
	const FSM_ReadMemory_StateEnumType *nextList;
};

static FSM_ReadMemory_InstanceType fsm;
static BooleanType busyFlag;
static uint8 lengthDireccion;
static uint8 lastChar;
static uint8 bufferDireccion[10];
static uint8 bufferLongitud[10];
static uint8 bufferDatosLeidos[100] = {0};
static uint8 lengthLongitud;

static void Idle(){

}

static void waitAddress(){

}

static void caracterDireccion(){
	bufferDireccion[lengthDireccion++] = lastChar;
	bufferDireccion[lengthDireccion]  = 0;
	fsm.current = FSM_READMEMORY_WAIT_ADDRESS;
	UART_putChar(fsm.uart,lastChar);
}

static void mensajeLongitud(){
	UART_putString(fsm.uart, "Cantidad de bytes a leer: ");
	fsm.current = FSM_READMEMORY_WAIT_LONGITUD;
}

static void waitLongitud(){

}

static void caracterLongitud(){
	bufferLongitud[lengthLongitud++] = lastChar;
	bufferLongitud[lengthLongitud] = 0;
	fsm.current = FSM_READMEMORY_WAIT_LONGITUD;
	UART_putChar(fsm.uart,lastChar);
}

static void leerMemoria(){
	UART_putString(fsm.uart,"c");
	uint16 address = convertAsciiAddressToHexadecimalBCDWithNumberOfDigits(bufferDireccion, lengthDireccion);
	uint16 numberOfReads= convertAsciiBufferToOneUnsignedInt(bufferLongitud, lengthLongitud);
	readStreamOfBytesFromMemoryToArray(bufferDatosLeidos, address, numberOfReads);
	if( FALSE == getErrorMemoryFlag() ){
		UART_putString(fsm.uart, "El contenido de la memoria es:\n");
		for(uint8 k = 0; k<numberOfReads; k++)
		{
			UART_putChar(fsm.uart, bufferDatosLeidos[k]);
		}
		fsm.current = FSM_READMEMORY_WAIT_ESC;
	}else{
		UART_putString(fsm.uart, "La lectura de la memoria fallo :( pero no fue su culpa :) (creo)\n");
		fsm.current = FSM_READMEMORY_WAIT_ESC;
	}
}

static void error(){

}

static void waitEsc(){

}

static void end(){
	fsm.enable = FALSE;
	busyFlag = FALSE;
	lengthDireccion = 0;
	lengthLongitud = 0;
}


static const FSM_ReadMemory_StateEnumType IdleNextList[FSM_READMEMORY_ENUM_MAX] = {
		FSM_READMEMORY_WAIT_ADDRESS, //char
		FSM_READMEMORY_WAIT_ADDRESS, //esc
		FSM_READMEMORY_WAIT_ADDRESS, //enter
		FSM_READMEMORY_WAIT_ADDRESS, //desconexion
		FSM_READMEMORY_WAIT_ADDRESS, //nop
};

static const FSM_ReadMemory_StateEnumType WaitAddressNextList[FSM_READMEMORY_ENUM_MAX] = {
		FSM_READMEMORY_ADDRESS, 		//
		FSM_READMEMORY_END,	//
		FSM_READMEMORY_MENSAJE_LONGITUD,		//
		FSM_READMEMORY_ERROR,		//
		FSM_READMEMORY_WAIT_ADDRESS,
};

static const FSM_ReadMemory_StateEnumType addressNextList[FSM_READMEMORY_ENUM_MAX] = {
		
};

static const FSM_ReadMemory_StateEnumType mensajelongitudNextList[FSM_READMEMORY_ENUM_MAX] = {
};

static const FSM_ReadMemory_StateEnumType WaitLongitudNextList[FSM_READMEMORY_ENUM_MAX] = {
		FSM_READMEMORY_LEN,
		FSM_READMEMORY_END,
		FSM_READMEMORY_LEER,
		FSM_READMEMORY_ERROR,
		FSM_READMEMORY_WAIT_LONGITUD,
};

static const FSM_ReadMemory_StateEnumType longitudNextList[FSM_READMEMORY_ENUM_MAX] = {
};

static const FSM_ReadMemory_StateEnumType leerNextList[FSM_READMEMORY_ENUM_MAX] = {
		FSM_READMEMORY_WAIT_ESC,		//
		FSM_READMEMORY_WAIT_ESC,		//
		FSM_READMEMORY_WAIT_ESC,		//
		FSM_READMEMORY_WAIT_ESC,		//
		FSM_READMEMORY_WAIT_ESC,		//
};

static const FSM_ReadMemory_StateEnumType errorNextList[FSM_READMEMORY_ENUM_MAX]  = {
		FSM_READMEMORY_WAIT_ESC,			//
		FSM_READMEMORY_WAIT_ESC,			//
		FSM_READMEMORY_WAIT_ESC,			//
		FSM_READMEMORY_WAIT_ESC,			//
		FSM_READMEMORY_WAIT_ESC,			//
};

static const FSM_ReadMemory_StateEnumType waitEscNextList[FSM_READMEMORY_ENUM_MAX]  = {
		FSM_READMEMORY_WAIT_ESC,	//
		FSM_READMEMORY_END,			//
		FSM_READMEMORY_WAIT_ESC,	//
		FSM_READMEMORY_WAIT_ESC,	//
		FSM_READMEMORY_WAIT_ESC,	//
};

static const FSM_ReadMemory_StateEnumType endNextList[FSM_READMEMORY_ENUM_MAX] = {
	FSM_READMEMORY_IDLE,	//
	FSM_READMEMORY_IDLE,	//
	FSM_READMEMORY_IDLE,	//
	FSM_READMEMORY_IDLE,	//
	FSM_READMEMORY_IDLE,	//
};

static const struct FSM_ReadMemory_StateType Estados[FSM_READMEMORY_ENUM_MAX] = {
	{Idle, IdleNextList},
	{waitAddress, WaitAddressNextList},
	{caracterDireccion, addressNextList},
	{mensajeLongitud, mensajelongitudNextList},
	{waitLongitud, WaitLongitudNextList},
	{caracterLongitud, longitudNextList},
	{leerMemoria, leerNextList},
	{error, errorNextList},
	{waitEsc, waitEscNextList},
	{end, endNextList}
};

FSM_ReadMemory_InstanceType* ReadMemory_getInstance(){
	return &fsm;
}

void ReadMemory_init(UART_ChannelType uart){
	busyFlag = TRUE;
	lengthDireccion = 0;
	lengthLongitud = 0;
	fsm.uart = uart;
	fsm.current = FSM_READMEMORY_IDLE;
	fsm.enable = TRUE;
}

BooleanType ReadMemory_isBusy(){
	return busyFlag;
}

BooleanType ReadMemory_isEnabled(){
	return fsm.enable;
}

void ReadMemory_eval(FSM_Leer_Memoria_EVT_Type evt){
	if(fsm.enable){
		fsm.current = Estados[fsm.current].nextList[evt];
		Estados[fsm.current].a();
	}
}

void ReadMemory_addChar(uint8 c){
	lastChar = c;
}

void ReadMemory_clearBusyFlag(){
	busyFlag = FALSE;
}
