//TODO: change header comment
#include "MK64F12.h"

#include "SPI.h"
#include "LCDNokia5110.h"
#include "UARTDriver.h"
#include "NVIC.h"
#include "FSM_Selector.h"
#include "Cliente.h"
#include "ControlPantalla.h"
#include "ModuleConfigurations.h"
#include "PIT.h"
#include "Chat.h"

typedef uint8 const * (*af[])(uint8* const);

int main(void)
{
	    UART_ChannelType TeraTerm;

	    initializeMCGHighFrequence();
		PinConfiguration_initializeI2CModules();
		PinConfiguration_initializeUartModules();
		/**Enables the UART 0 interrupt*/
		UART_interruptEnable(UART_0);
		UART_interruptEnable(UART_4);

		/**Enables the UART 0 interrupt in the NVIC*/
		NVIC_enableInterruptAndPriotity(UART0_IRQ, PRIORITY_10);
		NVIC_enableInterruptAndPriotity(UART4_IRQ, PRIORITY_10);

		PinConfig_BtnInputPinsInitialization();
	    PinConfig_BtnInterruptPortInitialization();
	    PIT_TimerType pit = PIT_1;
	    PIT_ClockGatingEnable();
	    float sysClock = 60000000;
	    float period = 0.99;

	    PIT_setTimeDelay(pit, sysClock, period);
	    PIT_MCRConfig pitMCR = {PIT_DISABLE, PIT_DISABLE};
	    PIT_TCRConfig pitTCRConfig = { pit, NOTCHAINED, PIT_ENABLE, PIT_ENABLE};
	    NVIC_enableInterruptAndPriotity(PIT_CH1_IRQ, PRIORITY_10);
	    EnableInterrupts;
	    PIT_configureMCRRegister(pitMCR);
	    PIT_configureTimmerControlRegister(pitTCRConfig);

	    FSM_Selector_InstanceType ControlTerminal1;
	    FSM_Selector_InstanceType ControlTerminal2;

	    PinConfig_ScreenInitialization();
	    LCDNokia_init();

	    Cliente terminal1;
	    Cliente terminal2;

	    Cliente_init(&terminal1, UART_0);
	    Cliente_init(&terminal2, UART_4);

	    Chat_addCliente(&terminal1);
	    Chat_addCliente(&terminal2);


    for (;;) {
        if(UART_getMailboxFlag(terminal1.uart)){
            addClientEvent(&terminal1, UART_getMailboxContent(terminal1.uart));
        }
        if(UART_getMailboxFlag(terminal2.uart)){
                    addClientEvent(&terminal2, UART_getMailboxContent(terminal2.uart));
        }

        Cliente_evaluate(&terminal1);
        Cliente_evaluate(&terminal2);
        ControlPantalla_evalLocal();
        ControlPantalla_PrintCurrent();
    }
    /* Never leave main */
    return 0;
}
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
