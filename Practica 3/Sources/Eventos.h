/*
 * Eventos.h
 *
 *  Created on: Oct 28, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_EVENTOS_H_
#define SOURCES_EVENTOS_H_

#include "DataTypeDefinitions.h"

typedef enum{
	FSM_SEL_EVT_1,
	FSM_SEL_EVT_2,
	FSM_SEL_EVT_3,
	FSM_SEL_EVT_4,
	FSM_SEL_EVT_5,
	FSM_SEL_EVT_6,
	FSM_SEL_EVT_7,
	FSM_SEL_EVT_8,
	FSM_SEL_EVT_9,
	FSM_SEL_EVT_ESC,
	FSM_SEL_EVT_ENTER,
	FSM_SEL_EVT_NOP,
	FSM_SEL_EVT_ENUM_MAX
}FSM_SEL_EVT_Type;


typedef enum{
	FSM_LEER_MEMORIA_CHAR,
	FSM_LEER_MEMORIA_ESC,
	FSM_LEER_MEMORIA_ENTER,
	FSM_LEER_MEMORIA_DESCONEXION,
	FSM_LEER_MEMORIA_NOP,
	FSM_LEER_MEMORIA_ENUM_MAX
}FSM_Leer_Memoria_EVT_Type;

typedef enum{
	FSM_ESCRIBIR_MEMORIA_CHAR,
	FSM_ESCRIBIR_MEMORIA_ESC,
	FSM_ESCRIBIR_MEMORIA_ENTER,
	FSM_ESCRIBIR_MEMORIA_ERROR,
	FSM_ESCRIBIR_MEMORIA_NOP,
	FSM_ESCRIBIR_MEMORIA_ENUM_MAX
}FSM_Escribir_Memoria_EVT_Type;

typedef enum{
	FSM_ESTABLECER_FECHA_EVT_NUM,
	FSM_ESTABLECER_FECHA_EVT_ESC,
	FSM_ESTABLECER_FECHA_EVT_ENTER,
	FSM_ESTABLECER_FECHA_EVT_ERROR,
	FSM_ESTABLECER_FECHA_EVT_NOP,
	FSM_ESTABLECER_FECHA_EVT_ENUM_MAX,
}FSM_Establecer_Fecha_EVT_Type;

typedef enum{
	FSM_ESTABLECER_HORA_NUM,
	FSM_ESTABLECER_HORA_ESC,
	FSM_ESTABLECER_HORA_ENTER,
	FSM_ESTABLECER_HORA_ERROR,
	FSM_ESTABLECER_HORA_NOP,
	FSM_ESTABLECER_HORA_ENUM_MAX,
}FSM_Establecer_Hora_EVT_Type;

typedef enum{
	FSM_FORMATO_HORA_CHAR,
	FSM_FORMATO_HORA_ESC,
	FSM_FORMATO_HORA_ENTER,
	FSM_FORMATO_HORA_ERROR,
	FSM_FORMATO_HORA_NOP,
	FSM_FORMATO_HORA_ENUM_MAX,
}FSM_Formato_Hora_EVT_Type;

typedef enum{
	CHAT_INIT_EVT,
	CHAT_CHAR_EVT,
	CHAT_ENTER_EVT,
	CHAT_ESC_EVT,
	CHAT_NOP
}Chat_EVT_EnumType;

typedef enum{
	FSM_ECO_LCD_CHAR,
	FSM_ECO_LCD_ESC,
	FSM_ECO_LCD_ENTER,
	FSM_ECO_LCD_NOP,
	FSM_ECO_LCD_MAX_ENUM,
}FSM_Eco_LCD_EVT_Type;

typedef enum{
	FSM_LEER_HORA_ESC,
	FSM_LEER_HORA_ENTER,
	FSM_LEER_HORA_ERROR,
	FSM_LEER_HORA_NOP,
	FSM_LEER_HORA_MAX_ENUM,
}FSM_Leer_Hora_EVT_Type;

typedef enum{
	FSM_LEER_FECHA_ESC,
	FSM_LEER_FECHA_ENTER,
	FSM_LEER_FECHA_ERROR,
	FSM_LEER_FECHA_NOP,
	FSM_LEER_FECHA_MAX_ENUM,
}FSM_Leer_Fecha_EVT_Type;

typedef enum{
	FSM_Pantalla_EVT_BTN0,
	FSM_Pantalla_EVT_BTN1,
	FSM_Pantalla_EVT_BTN2,
	FSM_Pantalla_EVT_BTN3,
	FSM_Pantalla_EVT_BTN4,
	FSM_Pantalla_EVT_BTN5,
	FSM_Pantalla_EVT_NOP,
	FSM_Pantalla_EVT_ENUM_MAX
}FSM_Pantalla_EVT_Type;

void addProcessEvent(uint8);
uint8 getProcessEvent(uint8);

FSM_SEL_EVT_Type decodeMainMenuEvent(uint8 evt);
FSM_Escribir_Memoria_EVT_Type decodeLeerMemoriaMenuEvent(uint8 evt);
FSM_Leer_Memoria_EVT_Type decodeEscribirMemoriaMenuEvent(uint8 evt);
FSM_Establecer_Hora_EVT_Type decodeEstablecerHoraEvent(uint8 evt);
FSM_Establecer_Fecha_EVT_Type decodeEstabelcerFechaEvent(uint8 evt);
FSM_Formato_Hora_EVT_Type decodeFormatoHoraEvent(uint8 evt);
FSM_Leer_Hora_EVT_Type decodeLeerHoraEvent(uint8 evt);
FSM_Leer_Fecha_EVT_Type decodeLeerFechaEvent(uint8 evt);
void decodeComunicacionTerminal(uint8 evt);
Chat_EVT_EnumType decodeChatEvent(uint8 evt);

FSM_Eco_LCD_EVT_Type decodeEcoLCDEvent(uint8 evt);

#endif /* SOURCES_EVENTOS_H_ */
