/*
 * ReadDate.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_READDATE_H_
#define SOURCES_READDATE_H_

#include  "DataTypeDefinitions.h"
#include "Eventos.h"
#include "UARTDriver.h"

typedef enum{
	FSM_READDATE_IDLE,
	FSM_READDATE_READ,
	FSM_READDATE_ERROR,
	FSM_READDATE_WAITING,
	FSM_READDATE_END,
	FSM_READDATE_NOP,
	FSM_READDATE_ENUM_MAX,
}FSM_ReadDate_StateEnumType;

typedef struct  {
	FSM_ReadDate_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_ReadDate_InstanceType;

FSM_ReadDate_InstanceType* ReadDate_getInstance();
void ReadDate_init(UART_ChannelType uart);
BooleanType ReadDate_isBusy();
BooleanType ReadDate_isEnabled();
void ReadDate_addEvent();
void ReadDate_eval(FSM_Leer_Fecha_EVT_Type);

void ReadDate_clearBusyFlag();

#endif /* SOURCES_READDATE_H_ */
