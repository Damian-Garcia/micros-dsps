/*
 * EEPROM24LC256.c
 *
 *  Created on: Nov 1, 2016
 *      Author: operi
 */
#include "EEPROM24LC256.h"


#define SYSTEMCLK 21000000
#define CONTROLCODE 1010
#define MEMADDRESS 0001
#define ADDRSCONTROL 0b10100000
#define TMSDELAY 90
static BooleanType errorMemoryFlag = FALSE;
#include "PIT.h"

/*
 * Estructura local que contiene la configuiracion de la memoria
 */
static I2CStructure memoryStruct=
{
		I2C_0,
		I2C_MASTER,
		I2C_TX_MODE,
		TRUE,
		SYSTEMCLK,
		100961,
		0x15,
		0x2
};

BooleanType getErrorMemoryFlag()
{
	return errorMemoryFlag;
}

void configureMemoryConfiguration()
{
	I2C_initAsMaster(&memoryStruct);
}
void writeByteToMemoryAddress(uint8 byteData, uint16 memoryAddress)
{
	/*
	 * Limpieza de la bandera de error de la escritura de datos en memoria
	 * y tiempo de retardo al comienzo pequeno para evitar problemas al realizar
	 * multiples escrituras y multiples lecturas de la memoria
	 */
	errorMemoryFlag = FALSE;
	tms(TMSDELAY);
	/*
	 * Escritura del control de escritura de datos en la memoria ------------------------------------------
	 * En este bloque de codigo siguiente lo que sucede es que se envia a la memoria
	 * una instruccion de control con la direccion fisica del dispositivo e indicandole que
	 * sera una escritura.
	 * */
	uint8 addressByte = ADDRSCONTROL;
	I2C_start(memoryStruct.channel);
	I2C_write_Byte(memoryStruct.channel, addressByte);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));//Mientras nos de verdadero nos quedamos aqui
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(memoryStruct.channel);
		return;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la direccion alta. ---------------------------------------------------------------------
	 */
	I2C_write_Byte(memoryStruct.channel, memoryAddress>>8);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(memoryStruct.channel);
		return;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la direccion baja de la memoria.-----------------------------------------------
	 */
	I2C_write_Byte(memoryStruct.channel, memoryAddress&0x0FF);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(memoryStruct.channel);
		return;
	}
	else
	{
		PIT_clearEnable(I2CPIT);

	}
	/*
	 * Escritura de los datos en memoria-----------------------------------------------
	 */
	I2C_write_Byte(memoryStruct.channel, byteData);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(memoryStruct.channel);
		return;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	//Detenemos la transmision de datos.
	I2C_stop(memoryStruct.channel);
}

uint8 readByteFromMemoryAddress( uint16 memoryAddress)
{
	errorMemoryFlag = FALSE;
	tms(TMSDELAY);
	I2C_TX_RX_Mode(memoryStruct.channel, I2C_TX_MODE);
	uint8 datosleidos = 0;
	uint8 addressByte1 = ADDRSCONTROL;
	uint8 addressByte2 = addressByte1|1;
	/*
	 * Escritura del control de escritura de datos en la memoria ------------------------------------------
	 */
	I2C_start(memoryStruct.channel);
	I2C_write_Byte(memoryStruct.channel, addressByte1);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(memoryStruct.channel);
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la direccion alta. ---------------------------------------------------------------------
	 */
	I2C_write_Byte(memoryStruct.channel, memoryAddress>>8);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la direccion baja de la memoria.-----------------------------------------------
	 */
	I2C_write_Byte(memoryStruct.channel, memoryAddress&0x0FF);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento.
		I2C_stop(memoryStruct.channel);
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}
	/*
	 * Escritura de la instruccion para la realizacion del control---------------------------------
	 */
	I2C_repeted_Start(memoryStruct.channel);
	I2C_write_Byte(memoryStruct.channel, addressByte2);
	I2C_wait(memoryStruct.channel);
	while(I2C_get_ACK(memoryStruct.channel));
	if(PIT_getInterruptionFlag(I2CPIT))//Si se habilito la bandera de error entonces nos salimos de la escritura
	{
		errorMemoryFlag = TRUE;// Encendemos la bandera de error o agregamos algun evento
		I2C_stop(memoryStruct.channel);
		return 0;
	}
	else
	{
		PIT_clearEnable(I2CPIT);
	}

	/*
	 * Lectura del datos ----------------------------------------------------------------------------
	 */
	I2C_TX_RX_Mode(memoryStruct.channel, I2C_RX_MODE);
	datosleidos= I2C_read_Byte(memoryStruct.channel);
	I2C_NACK(memoryStruct.channel);
	I2C_wait(memoryStruct.channel);
	I2C_stop(memoryStruct.channel);
	datosleidos= I2C_read_Byte(memoryStruct.channel);
	return datosleidos;

}

void writeStreamofBytesToMemoryAddress(uint8* buffer, uint16 startAddress, uint8 size)
{
	for(uint8 i=0; i<size; i++)
	{
		writeByteToMemoryAddress( buffer[i],  startAddress+i);
		if(errorMemoryFlag == TRUE)// Si la bandera de error se habilito dentro del write entonces terminamos de escribir
		{
			break;
		}
	}
}

void readStreamOfBytesFromMemoryToArray(uint8* buffer, uint16 startAddress, uint8 size)
{
	for(uint8 i=0; i<size; i++)
	{
		buffer[i] = readByteFromMemoryAddress(startAddress+i);
		if(errorMemoryFlag == TRUE)// Si la bandera de error se habilito dentro del write entonces terminamos de escribir
		{
			break;
		}
	}
}

