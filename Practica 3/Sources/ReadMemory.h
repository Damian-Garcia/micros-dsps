/*
 * ReadMemory.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Damian Garcia
 */

#ifndef SOURCES_READMEMORY_H_
#define SOURCES_READMEMORY_H

#include "UARTDriver.h"
#include "DataTypeDefinitions.h"
#include "Eventos.h"

typedef enum{
	FSM_READMEMORY_IDLE,
	FSM_READMEMORY_WAIT_ADDRESS,
	FSM_READMEMORY_ADDRESS,
	FSM_READMEMORY_MENSAJE_LONGITUD,
	FSM_READMEMORY_WAIT_LONGITUD,
	FSM_READMEMORY_LEN,
	FSM_READMEMORY_LEER,
	FSM_READMEMORY_ERROR,
	FSM_READMEMORY_WAIT_ESC,
	FSM_READMEMORY_END,
	FSM_READMEMORY_ENUM_MAX,
}FSM_ReadMemory_StateEnumType;

typedef struct  {
	FSM_ReadMemory_StateEnumType current;
	BooleanType enable;
	UART_ChannelType uart;
}FSM_ReadMemory_InstanceType;

FSM_ReadMemory_InstanceType* ReadMemory_getInstance();
void ReadMemory_init(UART_ChannelType);
BooleanType ReadMemory_isBusy();
BooleanType ReadMemory_isEnabled();
void ReadMemory_eval(FSM_Leer_Memoria_EVT_Type);
void ReadMemory_addChar(uint8);

void ReadMemory_clearBusyFlag();
#endif /* SOURCES_READMEMORY_H_ */
