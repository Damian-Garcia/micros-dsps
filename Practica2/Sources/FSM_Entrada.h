/**	 \file FSM_Entrada.h
	 \brief
	 This is the header file for the FSM logic.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */

#ifndef FSM_ENTRADA_H_
#define FSM_ENTRADA_H_

#include "Eventos.h"

/*!
 * \typedef Estado_FSM_Entrada
 * \brief This enum type contains all the possible states for the FSM.
 */
typedef enum{
	FSM_ENTRADA_PRINCIPAL,     //!< FSM_ENTRADA_PRINCIPAL

	FSM_ENTRADA_OPCIONES,      //!< FSM_ENTRADA_OPCIONES

	FSM_ENTRADA_ALARMA,        //!< FSM_ENTRADA_ALARMA
	FSM_ENTRADA_INC_ALARMA,    //!< FSM_ENTRADA_INC_ALARMA
	FSM_ENTRADA_DEC_ALARMA,    //!< FSM_ENTRADA_DEC_ALARMA
	FSM_ENTRADA_OK_ALARMA,     //!< FSM_ENTRADA_OK_ALARMA

	FSM_ENTRADA_FORMATO,       //!< FSM_ENTRADA_FORMATO
	FSM_ENTRADA_SET_CELSIUS,   //!< FSM_ENTRADA_SET_CELSIUS
	FSM_ENTRADA_SET_FAHRENHEI, //!< FSM_ENTRADA_SET_FAHRENHEI
	FSM_ENTRADA_OK_FORMATO,    //!< FSM_ENTRADA_OK_FORMATO

	FSM_ENTRADA_PORCENTAJE,    //!< FSM_ENTRADA_PORCENTAJE
	FSM_ENTRADA_INC_PORCENTAJE,//!< FSM_ENTRADA_INC_PORCENTAJE
	FSM_ENTRADA_DEC_PORCENTAJE,//!< FSM_ENTRADA_DEC_PORCENTAJE
	FSM_ENTRADA_OK_PORCENTAJE, //!< FSM_ENTRADA_OK_PORCENTAJE

	FSM_ENTRADA_MANUAL,        //!< FSM_ENTRADA_MANUAL
	FSM_ENTRADA_SET_MANUAL,    //!< FSM_ENTRADA_SET_MANUAL
	FSM_ENTRADA_CLEAR_MAANUAL, //!< FSM_ENTRADA_CLEAR_MAANUAL
	FSM_ENTRADA_OK_MANUAL,     //!< FSM_ENTRADA_OK_MANUAL
	FSM_ENTRADA_INC_MANUAL,    //!< FSM_ENTRADA_INC_MANUAL
	FSM_ENTRADA_DEC_MANUAL,    //!< FSM_ENTRADA_DEC_MANUAL

	FSM_ENTRADA_FRECUENCIMETRO,//!< FSM_ENTRADA_FRECUENCIMETRO

	FSM_ENTRADA_ENUM_MAX       //!< FSM_ENTRADA_ENUM_MAX
}Estado_FSM_Entrada;

/**
 * \brief FSM initialization
 * This functions sets the default values for the FSM.
 * \return void
 */
void FSM_Entrada_init();

/**
 * \brief Current state/event evaluation
 * This functions takes the current event, determines the next state and executes its action.
 * \param Evento_Boton
 */
void FSM_Entrada_eval(Evento_Boton);

#endif /* FSM_ENTRADA_H_ */
