/**	 \file FSM_Entrada.h
	 \brief
	 This is the source file for the IRQ handles of PortC
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */

#include "MK64F12.h"
#include "NVIC.h"
#include "GPIO.h"
#include "PIT.h"
#include "Eventos.h"

void PORTC_IRQHandler(){
	GPIO_clearInterrupt(GPIOC);
	NVIC_DisableIRQ(PORTC_IRQn);
	uint32 data = GPIO_readPORT(GPIOC);
	NVIC_EnableIRQ(PORTC_IRQn);
	data = (~data)&0x3F;
	if(data == 1){
		addEventoBoton(EVT_BTN_0);
	}else if(data == 2){
		addEventoBoton(EVT_BTN_1);
	}else if(data == 4){
		addEventoBoton(EVT_BTN_2);
	}else if(data == 8){
		addEventoBoton(EVT_BTN_3);
	}else if(data == 16){
		addEventoBoton(EVT_BTN_4);
	}else if(data == 32){
		addEventoBoton(EVT_BTN_5);
	}else{

	}
	PIT_setEnable(PIT_2);

}
