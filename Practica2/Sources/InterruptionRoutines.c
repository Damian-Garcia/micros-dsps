/*
 * InterruptionRoutines.c
 *
 *  Created on: Oct 1, 2016
 *      Author: operi
 */

#include "MK64F12.h"
#include "GPIO.h"
#include "ADC.h"
#include "NVIC.h"
#include "FlexTimer.h"
#include "PinConfiguration.h"
#include "ControlPantalla.h"

/**
 * Tamano de array temporal de valores del contador.
 */
#define FRECARRAYSIZE 95
/**
 * Ancho de banda donde se detectara el signo de la derivada (razon de cambio)
 */
#define FRECPASOMIN 539000
#define FRECPASOMAX 560000
/**
 * Offset para que se solo se tomen en cuenta diferencias mayores a 1000
 */
#define OFFSET 1000
/**
 * Frecuencia del reloj.
 */
static double clockPeriod = 1.0/(21000000.0);
/**
 * Valor de la frecuencia alta previamente asignada (antes de asignar una nueva frecuencia alta)
 */
static double tempHighFrecBeforeValue = 0;
/**
 * Contador utilizado para saber cuando actualizar el valor de la frecuencia (habilitar el frecuencimetro)
 */
static uint16_t frequenceUpdateCounter = 0;
/**
 * Bandera que indica si la frecuencia que se esta muestreando es de un valor bastante grande.
 */
static uint16_t highFrequenceFlag = FALSE;
/**
 * Variable auxiliar que indica cuando comenzar a disminuir la actualizacion de la frecuencia.
 */
static uint32_t tempvalue = 120000;
/**
 * Contador que es utilizado para saber si en realidad nos fuimos a una frecuencia baja o simplemente fueron valores erroneos.
 */
static uint32_t counterLowFrequence = 0;
/**
 * Arreglo donde se almacenaran
 */
static double tempHighFrecArray[FRECARRAYSIZE] = {0.0};
static uint16_t numberOfFrecCaptured = 0;
static uint16_t banderaPaso = FALSE;

typedef struct frequence_Structure
{
	uint32_t channelValue[2];
	uint8_t captureOneFlag :1;
	uint16_t n;
	uint8_t iterator;
	double frequence;

}freqStruct;

static freqStruct frequenceModule = {
		{0,0},
		FALSE,
		0,
		0,
		0.0
};

void FTM0_IRQHandler()// Para el muestreo del ADC.
{
	FTM0_SC &= ~FTM_SC_TOF_MASK;
	ADC_enableSingleRead(0);
	if(highFrequenceFlag == TRUE)
	{
		frequenceUpdateCounter++;
		if(frequenceUpdateCounter >0x20)
		{
			frequenceUpdateCounter = 0;
			NVIC_enableInterruptAndPriotity(FTM3_IRQ,PRIORITY_2);
		}
		else
		{
			NVIC_disableInterrupt(FTM3_IRQ);
		}
	}
	else
	{
		NVIC_enableInterruptAndPriotity(FTM3_IRQ,PRIORITY_2);
	}
}

void updateFrequence()
{
	frequenceModule.frequence = 1.0/(clockPeriod*((65535.0)*frequenceModule.n - frequenceModule.channelValue[0]+ frequenceModule.channelValue[1]));
	uint32_t tempFrequenceValue = ((uint32_t)frequenceModule.frequence) ;
	if( tempvalue < tempFrequenceValue)
	{
		highFrequenceFlag = TRUE;
		if(numberOfFrecCaptured >= FRECARRAYSIZE)
		{
			numberOfFrecCaptured = 0;
			int maxValueIndex = 0;
			for(int i=0;i<FRECARRAYSIZE;i++)
			{
				if(((uint32_t)tempHighFrecArray[i])>((uint32_t)tempHighFrecArray[maxValueIndex]))
				{
					maxValueIndex = i;
				}
			}
			tempHighFrecBeforeValue = getFrequenceValue();
			if(tempHighFrecArray[maxValueIndex] > FRECPASOMIN && tempHighFrecArray[maxValueIndex] < FRECPASOMAX)//si paso por el rango
			{
				if(tempHighFrecArray[maxValueIndex]> (tempHighFrecBeforeValue + OFFSET))
				{
					banderaPaso = TRUE;
				}
				else if(tempHighFrecArray[maxValueIndex]< (tempHighFrecBeforeValue - OFFSET))
				{
					banderaPaso = FALSE;
				}
			}
			if(banderaPaso)
			{
				setFrequenceValue(tempHighFrecArray[maxValueIndex]*2);
			}
			else
			{
				setFrequenceValue(tempHighFrecArray[maxValueIndex]);
			}
		}
		else
		{
			tempHighFrecArray[numberOfFrecCaptured++] = frequenceModule.frequence;

		}
//		if(305000 < tempFrequenceValue)
//		{
//			setFrequenceValue(frequenceModule.frequence);
//		}
//		else
//		{
//			setFrequenceValue(frequenceModule.frequence);
//		}
		//setFrequenceValue(frequenceModule.frequence);

		counterLowFrequence = 0;
	}
	else
	{
		if(highFrequenceFlag)
		{
			counterLowFrequence++;
			if(counterLowFrequence >0xF)
			{
				counterLowFrequence = 0;
				highFrequenceFlag = FALSE;
				setFrequenceValue(frequenceModule.frequence);
			}
		}
		else
		{
			setFrequenceValue(frequenceModule.frequence);
		}
	}
}
void FTM2_IRQHandler()//Para el ADC.
{
	/**Clearing the overflow interrupt flag*/
	FTM2_SC &= ~FTM_SC_TOF_MASK;

}
void FTM3_IRQHandler()//Para el frecuenciometrol Canal 4, timer 3
{
	if(FTM3_C4SC & FTM_CnSC_CHF_MASK)//Si detecto flancos de subida lo que hace es
	{
		frequenceModule.captureOneFlag = TRUE;
		frequenceModule.channelValue[frequenceModule.iterator++] = FTM_CnV_VAL_MASK & FTM3_C4V;
		if(frequenceModule.iterator == 2)
		{
			frequenceModule.iterator = 0;
			updateFrequence();
			frequenceModule.n=0;//Reiniciamos el contador de n
			frequenceModule.captureOneFlag = FALSE;
		}
		FTM3_C4SC &=~FTM_CnSC_CHF_MASK;
		return;
	}
	FTM3_SC &= ~FTM_SC_TOF_MASK;// Apagamos la bandera e incrementamos el valor de n
	if(frequenceModule.captureOneFlag)
	{
		frequenceModule.n++;
	}

}

void ADC0_IRQHandler()
{
	unsigned int value = ADC0_RA;
	ADC_writeAdc0Value(value);
}



/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
/*!
 	 \brief	 This function is used as a delay for debouncing the keyboard.
 	 \param[in]  ms value.
 */
static void tms(unsigned int ms) {
	volatile unsigned char i, k;
	volatile unsigned int ms2;
	ms2 = ms;
	while (ms2) {
		for (i = 0; i <= 140; i++) {
			k++;
		}
		ms2--;
		k++;
	}
}

