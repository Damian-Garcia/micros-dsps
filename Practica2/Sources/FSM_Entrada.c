/**	 \file FSM_Entrada.c
	 \brief
	 This is the source file for the FSM logic.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#include "FSM_Entrada.h"
#include "ControlPantalla.h"
#include "ControlAlarma.h"
#include "ControlMotor.h"

typedef void(*action)(void);
static Estado_FSM_Entrada actual;

typedef struct{
	action stateAction;
	const Estado_FSM_Entrada *nextStateList;
}FSM_Entrada_StateType;

static void Principal(){
	//cuando se esta en el menu principal se cancelan los cambios no hechos y se selecciona la pantalla
	ControlPantalla_changeActual(SCRN_PRINCIPAL);
	ControlPantalla_cancelChanges();
	ControlAlarma_cancelChanges();
	ControlMotor_cancelChanges();
}

static void Opciones(){
	// Solo se muestran las opciones
	ControlPantalla_changeActual(SCRN_OPCIONES);
}

static void Alarma(){
	// Se selecciona el menu de alarma
	ControlPantalla_changeActual(SCRN_ALARMA);
}

static void IncAlarma(){
	// Se incrementa el threshold de la alarma
	ControlAlarma_incrementValue();
	//actual = FSM_ENTRADA_ALARMA;
}

static void DecAlarma(){
	// Se decrementa el threshold de la alarma
	ControlAlarma_decrementValue();
	//actual = FSM_ENTRADA_ALARMA;
}

// El valor real de la alarma no cambia hasta que el usuario presiona OK
static void OkAlarma(){
	// Se fija el nuevo valor de la alarma
	ControlAlarma_updateValue();
}

static void Formato(){
	// Selecciona el menu de formato
	ControlPantalla_changeActual(SCRN_FORMATO);
}

static void SetCelsius(){
	// Selecciona el formato de centigrados
	ControlPantalla_changeFormat(FRMT_CELSIUS);
}

static void SetFahrenheit(){
	// Selecciona el formato fahrenheit
	ControlPantalla_changeFormat(FRMT_FAHRENHEIT);
}

// El formato real no cambio hasta que el usuario Presiona ok
static void OkFormato(){
	// Se actualiza el el formato de la pantalla.
	ControlPantalla_update();
}

static void Porcentaje(){
	// Selecciona la pantalla de cambio de incrementos
	ControlPantalla_changeActual(SCRN_PORCENTAJE);
}

static void IncPorcentaje(){
	// Se incrementa el paso del motor
	ControlMotor_incrementStep();
}

static void DecPorcentaje(){
	// Se decrementa el paso del motor
	ControlMotor_decrementStep();
}

//El incremento del paso del motor no surte efecto sino hasta que se presiona ok
static void OkPorcentaje(){
	// Se actualiza el paso del motor
	ControlMotor_updateValue();
}

static void Manual(){
	// Selecciona la pantalla de control manual
	ControlPantalla_changeActual(SCRN_MANUAL);
}

static void IncManual(){
	// Realiza un incremento manual
	if(!ControlAlarma_isActive()){
		ControlMotor_incrementValue();
		ControlMotor_updateValue();
		ControlMotor_updatePotenciaBase();
	}
}
static void DecManual(){
	// Realiza un decremento manual
	if(!ControlAlarma_isActive()){
		ControlMotor_decrementValue();
		ControlMotor_updateValue();
		ControlMotor_updatePotenciaBase();
	}
}

static void SetManual(){
	// Set
	ControlAlarma_clearActive();
}

static void ClearManual(){
	ControlAlarma_setActive();
}

static void OkManual(){
	ControlAlarma_updateValue();
}

static void Frecuencimetro(){
	ControlPantalla_changeActual(SCRN_FRECUENCIOMETRO);
}
const Estado_FSM_Entrada FSM_Entrada_Transiciones[FSM_ENTRADA_ENUM_MAX][EVT_BTN_ENUM_MAX]=
{
	// boton 0				boton 1				boton2 				boton 3					boton4 					boton5						nop
	{FSM_ENTRADA_OPCIONES, FSM_ENTRADA_ALARMA, FSM_ENTRADA_FORMATO, FSM_ENTRADA_PORCENTAJE,  FSM_ENTRADA_MANUAL, FSM_ENTRADA_FRECUENCIMETRO, FSM_ENTRADA_PRINCIPAL}, //principal

	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_ALARMA, FSM_ENTRADA_FORMATO, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_MANUAL, FSM_ENTRADA_FRECUENCIMETRO, FSM_ENTRADA_OPCIONES}, //opciones

	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_DEC_ALARMA, FSM_ENTRADA_INC_ALARMA, FSM_ENTRADA_OK_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA}, // alarma
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA,  FSM_ENTRADA_ALARMA}, // inc alarma
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA }, ////dec alarma
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA, FSM_ENTRADA_ALARMA }, //ok alarma

	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_SET_CELSIUS, FSM_ENTRADA_SET_FAHRENHEI, FSM_ENTRADA_OK_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO}, // formato
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO}, // celsius
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO},	// fahrenheit
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO, FSM_ENTRADA_FORMATO},	// ok formato

	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_DEC_PORCENTAJE, FSM_ENTRADA_INC_PORCENTAJE, FSM_ENTRADA_OK_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE},	// porcentaje
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE}, // inc porcentaje
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE}, // dec porcentaje
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE, FSM_ENTRADA_PORCENTAJE}, // ok porcentaje

	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_SET_MANUAL, FSM_ENTRADA_CLEAR_MAANUAL, FSM_ENTRADA_OK_MANUAL, FSM_ENTRADA_DEC_MANUAL, FSM_ENTRADA_INC_MANUAL, FSM_ENTRADA_MANUAL },	// manual
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL}, // on manual
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL}, // off manual
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL}, // ok manual
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL}, // inc manual
	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL, FSM_ENTRADA_MANUAL}, // dec manual

	{FSM_ENTRADA_PRINCIPAL, FSM_ENTRADA_FRECUENCIMETRO, FSM_ENTRADA_FRECUENCIMETRO,FSM_ENTRADA_FRECUENCIMETRO,FSM_ENTRADA_FRECUENCIMETRO,FSM_ENTRADA_FRECUENCIMETRO, FSM_ENTRADA_FRECUENCIMETRO}, //frecuencia
};

const FSM_Entrada_StateType Estados[FSM_ENTRADA_ENUM_MAX]= {
	{ Principal, FSM_Entrada_Transiciones[FSM_ENTRADA_PRINCIPAL] },				// principal
	{ Opciones, FSM_Entrada_Transiciones[FSM_ENTRADA_OPCIONES] },				// opciones
	{ Alarma, FSM_Entrada_Transiciones[FSM_ENTRADA_ALARMA] },					// alarma
	{ IncAlarma, FSM_Entrada_Transiciones[FSM_ENTRADA_INC_ALARMA] },			// inc alarma
	{ DecAlarma, FSM_Entrada_Transiciones[FSM_ENTRADA_DEC_ALARMA] },			// dec Alarma
	{ OkAlarma, FSM_Entrada_Transiciones[FSM_ENTRADA_OK_ALARMA] },				// ok Alarma
	{ Formato, FSM_Entrada_Transiciones[FSM_ENTRADA_FORMATO] },					// Formato
	{ SetCelsius, FSM_Entrada_Transiciones[FSM_ENTRADA_SET_CELSIUS] },			// Set Celsius
	{ SetFahrenheit, FSM_Entrada_Transiciones[FSM_ENTRADA_SET_FAHRENHEI] },		// Set Fahrenheit
	{ OkFormato, FSM_Entrada_Transiciones[FSM_ENTRADA_OK_FORMATO] },			// ok Formato
	{ Porcentaje, FSM_Entrada_Transiciones[FSM_ENTRADA_PORCENTAJE] },			// Porcentaje
	{ IncPorcentaje, FSM_Entrada_Transiciones[FSM_ENTRADA_INC_PORCENTAJE] },	// Inc Porcentaje
	{ DecPorcentaje, FSM_Entrada_Transiciones[FSM_ENTRADA_DEC_PORCENTAJE] }, 	// Dec Porcentaje
	{ OkPorcentaje, FSM_Entrada_Transiciones[FSM_ENTRADA_OK_PORCENTAJE] },		// Ok porcentaje
	{ Manual, FSM_Entrada_Transiciones[FSM_ENTRADA_MANUAL] },					// Manual
	{ SetManual, FSM_Entrada_Transiciones[FSM_ENTRADA_SET_MANUAL] },			// Set Manual
	{ ClearManual, FSM_Entrada_Transiciones[FSM_ENTRADA_CLEAR_MAANUAL] },		// clear Manual
	{ OkManual, FSM_Entrada_Transiciones[FSM_ENTRADA_OK_MANUAL] },				// Ok Manual
	{ IncManual, FSM_Entrada_Transiciones[FSM_ENTRADA_INC_MANUAL] },			// INC Manual
	{ DecManual, FSM_Entrada_Transiciones[FSM_ENTRADA_DEC_MANUAL] },			// 	Dec Manual
	{ Frecuencimetro, FSM_Entrada_Transiciones[FSM_ENTRADA_FRECUENCIMETRO] },	// Frecuencimetro
};

void FSM_Entrada_init(){
	actual = FSM_ENTRADA_PRINCIPAL;
}

void FSM_Entrada_eval(Evento_Boton evt){
	actual = Estados[actual].nextStateList[evt];
	Estados[actual].stateAction();
}
