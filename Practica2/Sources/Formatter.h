/**	 \file Formater.h
	 \brief
	 	 This is the header file for the string conversions for printing.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */

#ifndef SOURCES_FORMATTER_H_
#define SOURCES_FORMATTER_H_

#include "DataTypeDefinitions.h"
/**
 * \brief This function stores an ASCII representation of an unsigned 32 bit integer.
 * \param number
 * \param buffer
 * \param digits
 */
void formatUnsignedInteger32(uint32 number, uint8 *buffer, sint8 digits);
/**
 * \brief This function stores an ASCII representation of a 64 bit floating point number.
 * \param number
 * \param buffer
 * \param decimalDigits
 */
void formatDouble(double number, uint8 *buffer, uint8 decimalDigits);

#endif /* SOURCES_FORMATTER_H_ */
