/**	 \file ControlPantalla.h
	 \brief
	 	 This is the header file for LCD display control and printing.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#ifndef SOURCES_CONTROLPANTALLA_H_
#define SOURCES_CONTROLPANTALLA_H_

#include "DataTypeDefinitions.h"
/**
 * \typedef Screen
 * \brief type enum of the screens
 */
typedef enum{
	SCRN_PRINCIPAL,      //!< SCRN_PRINCIPAL
	SCRN_OPCIONES,       //!< SCRN_OPCIONES
	SCRN_ALARMA,         //!< SCRN_ALARMA
	SCRN_FORMATO,        //!< SCRN_FORMATO
	SCRN_PORCENTAJE,     //!< SCRN_PORCENTAJE
	SCRN_MANUAL,         //!< SCRN_MANUAL
	SCRN_FRECUENCIOMETRO,//!< SCRN_FRECUENCIOMETRO
	SCRN_ENUM_MAX        //!< SCRN_ENUM_MAX
}Screen;

/**
 * \typedef TempFormat
 * \brief Type enum for the available formats.
 */
typedef enum{
	FRMT_CELSIUS,  //!< FRMT_CELSIUS
	FRMT_FAHRENHEIT//!< FRMT_FAHRENHEIT
}TempFormat;

/**
 * \brief This function returns the current printed frequence
 * \return double
 */
double getFrequenceValue();
/**
 * \brief This functions converts a celsius temperature value to its fahrenheit equivalent
 * \param[in] double
 * \return double
 */
double fahrenheitToCelsius(double);
/**
 * \brief This functions converts a fahrenheit temperature value to its celsius equivalent
 * \param[in] double
 * \return double
 */
double celsiusToFahrenheit(double);
/**
 * \brief Initializes the default values for the temperature control logic.
 */
void ControlPantalla_init();
/**
 * \brief Prints the selected screen in the LCD screen.
 */
void ControlPantalla_printActual();
/**
 * \brief Changes the current screen that will be printed.
 * \param[in] Screen
 */
void ControlPantalla_changeActual(Screen);
/**
 * \brief Changes the next format of the temperature
 * @param
 */
void ControlPantalla_changeFormat(TempFormat);
/**
 * \brief Sets the next temperature format to the current format.
 */
void ControlPantalla_update();
/**
 * \brief Deletes the changes made to the next temperature format.
 */
void ControlPantalla_cancelChanges();
/**
 * \brief This function returns TRUE if the screen is ready to print
 * \return BooleanType
 */
BooleanType ControlPantalla_hasToPrint();
/**
 * \brief Enables the print function of the screen.
 */
void ControlPantalla_enablePrint();
/**
 * \brief Changes the frequency value to be printed.
 * \param double
 */
void setFrequenceValue(double value);

#endif /* SOURCES_CONTROLPANTALLA_H_ */
