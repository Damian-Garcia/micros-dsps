/**	 \file ControlPantalla.c
	 \brief
	 	 This is the source file for LCD display control and printing.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#include "ControlPantalla.h"
#include "DataTypeDefinitions.h"
#include "ControlMotor.h"
#include "ControlAlarma.h"
#include "LCDNokia5110.h"
#include "Formatter.h"
#include "ADC.h"

static Screen currentScreen;
static TempFormat currentFormat;
static TempFormat newFormat;
static BooleanType hasToPrint;
static double frequenceValue = 1;

static const uint8 s_principal1[] = "Velocidad";
static const uint8 s_principal2[] = "Temperatura";

static const uint8 s_opciones1[] = "1) Alarma";
static const uint8 s_opciones2[] = "2) Formato temp";
static const uint8 s_opciones3[] = "3) % de inc";
static const uint8 s_opciones4[] = "4) Cntrl manual";
static const uint8 s_opciones5[] = "5) Frecuencimetro";

static const uint8 s_alarma1[] = "Alarma";
static const uint8 s_alarma2[] = "(-)B1 (+)B2 (ok)B3";

static const uint8 s_formato1[] = "Formato Temp";
static const uint8 s_formato2[] = "Temp = ";
static const uint8 s_formato3[] = "(C)B1 (F)B2 (ok) B3";

static const uint8 s_porcentaje1[] = "% de inc";
static const uint8 s_porcentaje2[] = "(-)B1 (+)B2 (ok)B3";


static const uint8 s_manual1[] = "Cntrl manual";
static const uint8 s_manual2[] = "(on)B1 (of)B2 (ok)B3";
static const uint8 s_manual3[] = "(-)B4 (+)B5";

static const uint8 s_frecuencimetro1[] = "Frequencia";
static const uint8 s_frecuencimetro2[] = "(Hz)";

static void Principal() {
	uint32 velocidad;
	uint32 temperatura;

	velocidad = ControlMotor_getPotencia();
	uint8 bufferVelocidad[10];

	formatUnsignedInteger32(velocidad, bufferVelocidad, -1);

	double temp;

	temp = ADC_getAdc0Value();//ControlAlarma_getTemperatura();
	if(FRMT_FAHRENHEIT == currentFormat){
		temp = celsiusToFahrenheit(temp);
	}
	uint8 bufferPi[10];
	formatDouble(temp,bufferPi,2);

	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_principal1); /*! It print a string stored in an array*/

	LCDNokia_gotoXY(0,1);
	LCDNokia_sendString(bufferVelocidad);

	LCDNokia_gotoXY(0, 3); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_principal2); /*! It print a string stored in an array*/

	LCDNokia_gotoXY(0,4);
	LCDNokia_sendString(bufferPi);

}

static void Opciones() {
	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_opciones1); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 1); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_opciones2); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 2); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_opciones3); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 3); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_opciones4); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 4); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_opciones5); /*! It print a string stored in an array*/

}

static void Alarma() {
	uint32 valorAlarma;
	valorAlarma = ControlAlarma_getNewAlarmValue();
	uint8 bufferAlarma[10];
	formatUnsignedInteger32(valorAlarma, bufferAlarma, -1);
	//TODO: complete
	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_alarma1); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 1); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(bufferAlarma); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 2); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_alarma2); /*! It print a string stored in an array*/

}

static void Formato(){
	//TODO: complete
	double temp;
	char letraFormato = 'C';
	uint8 bufferTemp[10];
	temp = ControlAlarma_getTemperatura();
	if( newFormat == FRMT_FAHRENHEIT){
		temp = celsiusToFahrenheit(temp);
		letraFormato  = 'F';
	}
	formatDouble(temp,bufferTemp, 2);
	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_formato1); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 1); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(bufferTemp); /*! It print a string stored in an array*/
	LCDNokia_sendChar(letraFormato);
	LCDNokia_gotoXY(0, 2); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_formato3); /*! It print a string stored in an array*/
}

static void Porcentaje() {
	//TODO: complete
	uint32 porcentaje;
	uint8 bufferPorcentaje[10];
	porcentaje = ControlMotor_getNuevoMotorStep();
	formatUnsignedInteger32(porcentaje, bufferPorcentaje, -1);

	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_porcentaje1); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 1); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(bufferPorcentaje); /*! It print a string stored in an array*/
	LCDNokia_sendChar('%');
	LCDNokia_gotoXY(0, 2); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_porcentaje2); /*! It print a string stored in an array*/

}

static void Manual() {
	uint32 potencia;
	uint8 bufferPotencia[10];
	potencia = ControlMotor_getPotencia();
	formatUnsignedInteger32(potencia, bufferPotencia, -1);
	//TODO: complete
	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_manual1); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 1); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(bufferPotencia); /*! It print a string stored in an array*/
	LCDNokia_sendChar('%');
	LCDNokia_gotoXY(0, 2); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_manual2); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 3); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_manual3); /*! It print a string stored in an array*/

}
double getFrequenceValue()
{
	return frequenceValue;
}
void setFrequenceValue(double value)
{
	frequenceValue = value;
}
static void Frecuenciometro() {
	//TODO: complete

	uint8 bufferFrecuencia[10];
	formatDouble(frequenceValue,bufferFrecuencia,2);
	LCDNokia_clear();
	LCDNokia_gotoXY(0, 0); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_frecuencimetro1); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 1); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(bufferFrecuencia); /*! It print a string stored in an array*/
	LCDNokia_gotoXY(0, 2); /*! It establishes the position to print the messages in the LCD*/
	LCDNokia_sendString(s_frecuencimetro2); /*! It print a string stored in an array*/
}

void ControlPantalla_init() {
	currentFormat = FRMT_CELSIUS;
	newFormat = currentFormat;
	currentScreen = SCRN_PRINCIPAL;
	//TODO: complete
}

void ControlPantalla_printActual() {
	switch (currentScreen) {
	case SCRN_PRINCIPAL:
		Principal();
		break;
	case SCRN_OPCIONES:
		Opciones();
		break;
	case SCRN_ALARMA:
		Alarma();
		break;
	case SCRN_FORMATO:
		Formato();
		 break;
	case SCRN_PORCENTAJE:
		Porcentaje();
		break;
	case SCRN_MANUAL:
		Manual();
		break;
	case SCRN_FRECUENCIOMETRO:
		Frecuenciometro();
		break;
	}

	hasToPrint = FALSE;
}

void ControlPantalla_changeActual(Screen sc) {
	currentScreen = sc;
}

void ControlPantalla_changeFormat(TempFormat tmf) {
	newFormat = tmf;
}

void ControlPantalla_update() {
	currentFormat = newFormat;
}

void ControlPantalla_cancelChanges() {
	newFormat = currentFormat;
}

BooleanType ControlPantalla_hasToPrint() {
	return hasToPrint;
}

void ControlPantalla_enablePrint() {
	hasToPrint = TRUE;
}

double fahrenheitToCelsius(double t){
	return (t- 32)/1.8;
}

double celsiusToFahrenheit(double t){
	return t*1.8 + 32;
}

TempFormat ControlPantalla_currentFormat(){
	return currentFormat;
}
