/**	 \file Eventos.c
 *	 \brief
 *	 This is the source file for the abstract control of the button events.
 *	 \author Damian Garcia Serrano and Erick Ortega Prudencio
 *	 \date	Oct 17 2016
 */
#include "Eventos.h"
#include "DataTypeDefinitions.h"

/**
 * This anonymous union contains 6 bits, one for each button.
 */
static union {
	uint8 AllButtons :6;
	struct {
		BIT_ON_OFF_Type btn0 :1;
		BIT_ON_OFF_Type btn1 :1;
		BIT_ON_OFF_Type btn2 :1;
		BIT_ON_OFF_Type btn3 :1;
		BIT_ON_OFF_Type btn4 :1;
		BIT_ON_OFF_Type btn5 :1;
	} Buttons;
} Entrada;

void addEventoBoton(Evento_Boton evt) {
	switch (evt) {
	case EVT_BTN_0:
		Entrada.Buttons.btn0 = BIT_ON;
		break;
	case EVT_BTN_1:
		Entrada.Buttons.btn1 = BIT_ON;
		break;
	case EVT_BTN_2:
		Entrada.Buttons.btn2 = BIT_ON;
		break;
	case EVT_BTN_3:
		Entrada.Buttons.btn3 = BIT_ON;
		break;
	case EVT_BTN_4:
		Entrada.Buttons.btn4 = BIT_ON;
		break;
	case EVT_BTN_5:
		Entrada.Buttons.btn5 = BIT_ON;
		break;
	default:
		break;
	}
}

/// In case of two buttons been pressed at the same time, the function returns the higher priority button
Evento_Boton getEventoBoton() {
	Evento_Boton evt;
	if(Entrada.Buttons.btn0){
		evt = EVT_BTN_0;
		Entrada.AllButtons = 0;
	}else if(Entrada.Buttons.btn1){
		evt = EVT_BTN_1;
		Entrada.AllButtons = 0;
	}else if(Entrada.Buttons.btn2){
		evt = EVT_BTN_2;
		Entrada.AllButtons = 0;
	}else if(Entrada.Buttons.btn3){
		evt = EVT_BTN_3;
		Entrada.AllButtons = 0;
	}else if(Entrada.Buttons.btn4){
		evt = EVT_BTN_4;
		Entrada.AllButtons = 0;
	}else if(Entrada.Buttons.btn5){
		evt = EVT_BTN_5;
		Entrada.AllButtons = 0;
	}else{
		evt = EVT_BTN_NOP;
	}
	return evt;
}
