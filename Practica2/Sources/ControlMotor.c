/**	 \file ControlMotor.c
	 \brief
	 	 This is the source file for the Motor
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#include "ControlMotor.h"
#include "DataTypeDefinitions.h"
#include "PinConfiguration.h"

static uint32 Potencia;
static uint32 nuevaPotencia;
static uint32 MotorStep;
static uint32 nuevoMotorStep;
static uint32 PotenciaBase;

void ControlMotor_init(){
	Potencia = 85;
	nuevaPotencia = 85;
	MotorStep = 15;
	nuevoMotorStep = 15;
	PotenciaBase = 85;
	// TODO: complete
}

void ControlMotor_incrementValue(){
	if(MotorMaxSpeed > nuevaPotencia){
		nuevaPotencia += MotorStep;
		if(MotorMaxSpeed < nuevaPotencia ){
			nuevaPotencia = MotorMaxSpeed;
		}
	}
}

void ControlMotor_decrementValue(){
	if(MotorMinSpeed < nuevaPotencia){
		if(MotorMinSpeed + MotorStep < nuevaPotencia){
			nuevaPotencia -= MotorStep;
		}else{
			nuevaPotencia = MotorMinSpeed;
		}
	}
}

void ControlMotor_incrementStep(){
	if(MotorMaxStep > nuevoMotorStep){
		nuevoMotorStep += MotorStepIncrements;
		if(MotorMaxStep < nuevoMotorStep){
			nuevoMotorStep = MotorMaxStep;
		}
	}
}

void ControlMotor_decrementStep(){
	if(MotorMinStep < nuevoMotorStep){
		if(MotorMinStep + MotorStepIncrements < nuevoMotorStep ){
			nuevoMotorStep -= MotorStepIncrements;
		}else{
			nuevoMotorStep = MotorMinStep;
		}
	}
}

void ControlMotor_updateValue(){
	Potencia = nuevaPotencia;
	MotorStep = nuevoMotorStep;
	setMotorFlexNewDutyCycle(Potencia);
}

void ControlMotor_cancelChanges(){
	nuevaPotencia = Potencia;
	nuevoMotorStep = MotorStep;
}

void ControlMotor_setValue(uint32 p){
	if(p<= MotorMaxSpeed){
		Potencia = p;
	}else{
		Potencia = MotorMinSpeed;
	}
	setMotorFlexNewDutyCycle(Potencia);

}

void ControlMotor_updatePotenciaBase(){
	PotenciaBase = Potencia;
}

uint32 ControlMotor_getPotencia(){
	return Potencia;
}

uint32 ControlMotor_getNuevaPotencia(){
	return nuevaPotencia;
}

uint32 ControlMotor_getNuevoMotorStep(){
	return nuevoMotorStep;
}

uint32 ControlMotor_getMotorStep(){
	return MotorStep;
}

uint32 ControlMotor_getPotenciaBase(){
	return PotenciaBase;
}
