/**	 \file ControlAlarma.c
	 \brief
	 	 This is the header file for Alarm and its actions over other modules
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#include "ControlAlarma.h"
#include "ADC.h"
#include "PinConfiguration.h"
#include "ControlMotor.h"

/// This static const represents the absolute limit of the alarm
static const double limite = 100;
/// This variable holds the current value of the alarm
static double valorAlarma;
/// This variable holds the next value of the alarm, if changed
static double nuevoValorAlarma;
/// This variable holds the current temperature value
static double valorTemperatura;

/// This variable is active if the Alarm is active
static BooleanType Activo;
/// This variable is holds the next Activo value, if changed.
static BooleanType nuevoActivo;

static BooleanType ready;

void ControlAlarma_init() {
	valorAlarma = AlarmInitialValue;
	nuevoValorAlarma = AlarmInitialValue;
	valorTemperatura = 25.0;
	Activo = TRUE;
}

void ControlAlarma_incrementValue() {
	double currentStep;
	currentStep = AlarmIncrementStep;
	if (AlarmUpperLimit > nuevoValorAlarma) {
		nuevoValorAlarma += currentStep;
		if (AlarmUpperLimit < nuevoValorAlarma) {
			nuevoValorAlarma = AlarmUpperLimit;
		}
	}
}

void ControlAlarma_decrementValue() {
	double currentStep;
	currentStep = AlarmIncrementStep;

	if (AlarmLowerLimit < nuevoValorAlarma) {
		nuevoValorAlarma -= currentStep;
		if(AlarmLowerLimit > nuevoValorAlarma){
			nuevoValorAlarma = AlarmLowerLimit;
		}
	}
}

void ControlAlarma_setActive() {
	nuevoActivo = TRUE;
}

void ControlAlarma_clearActive() {
	nuevoActivo = FALSE;
}

void ControlAlarma_updateValue() {
	valorAlarma = nuevoValorAlarma;
	Activo = nuevoActivo;
}

void ControlAlarma_cancelChanges() {
	nuevoActivo = Activo;
	nuevoValorAlarma = valorAlarma;
}

BooleanType ControlAlarma_isActive() {
	return Activo;
}

BooleanType ControlAlarma_isReady() {
	return ready;
}

void ControlAlarma_evaluateTemp() {
	valorTemperatura = ADC_getAdc0Value();
	if(ControlAlarma_isActive()){
		if(valorTemperatura > 25.0)
		{
			int n;
			//setMotorFlexNewDutyCycle(25);
			n = (valorTemperatura-25)/2;
			sint32 i = 0;
			sint32 potencia = ControlMotor_getPotenciaBase() - ControlMotor_getMotorStep() * n;
			ControlMotor_setValue(potencia);
		}else{
			ControlMotor_setValue(ControlMotor_getPotenciaBase());
		}
	}
	if(valorTemperatura > valorAlarma){
		PinConfig_setOutputAlarmValue(ALARM_ENABLED);
	}else{
		PinConfig_setOutputAlarmValue(ALARM_DISABLED);
	}
	ready = FALSE;
}

double ControlAlarma_getAlarmValue() {
	return valorAlarma;
}

double ControlAlarma_getNewAlarmValue() {
	return nuevoValorAlarma;
}

double ControlAlarma_getTemperatura() {
	return valorTemperatura;
}
