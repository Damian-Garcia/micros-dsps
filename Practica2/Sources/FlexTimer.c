/**
	\file 
	\brief 
		This is the starter file of FlexTimer. 
		In this file the FlexTimer is configured in overflow mode. 
	\author J. Luis Pizano Escalante, luispizano@iteso.mx
	\date	7/09/2014
	\todo
	    Add configuration structures.
 */

#include "FlexTimer.h"
#include "NVIC.h"


uint16_t FTM_getCHValue(FTM_NameType ftmName, FTM_TypeChannel channelType)
{
	switch(ftmName)
	{
	case FTM_0:
		return FTM0_CnV(channelType);
		break;
	case FTM_1:
		return FTM1_CnV(channelType);
		break;
	case FTM_2:
		return FTM2_CnV(channelType);
		break;
	case FTM_3:
		return FTM3_CnV(channelType);
		break;
	default:
		return 0;
		break;
	}
}
void FTM_updateCHValue(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_TypeCv cvType)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	case FTM_1:
		FTM1_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	case FTM_2:
		FTM2_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	case FTM_3:
		FTM3_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	default:
		break;
	}
}
void FTM_SetCVvalue(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_TypeCv cvType)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	case FTM_1:
		FTM1_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	case FTM_2:
		FTM2_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	case FTM_3:
		FTM3_CnV(channelType) = FTM_CnV_VAL(cvType);
		break;
	default:
		break;
	}
}
void FTM_SetChannelInterruptEnable(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_ChannelIEB channelIE)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_CnSC(channelType) &= ~FTM_CnSC_CHIE_MASK;
		FTM0_CnSC(channelType) |= FTM_CnSC_CHIE(channelIE);
		break;
	case FTM_1:
		FTM1_CnSC(channelType) &= ~FTM_CnSC_CHIE_MASK;
		FTM1_CnSC(channelType) |= FTM_CnSC_CHIE(channelIE);
		break;
	case FTM_2:
		FTM2_CnSC(channelType) &= ~FTM_CnSC_CHIE_MASK;
		FTM2_CnSC(channelType) |= FTM_CnSC_CHIE(channelIE);
		break;
	case FTM_3:
		FTM3_CnSC(channelType) &= ~FTM_CnSC_CHIE_MASK;
		FTM3_CnSC(channelType) |= FTM_CnSC_CHIE(channelIE);
		break;
	default:
		break;
	}
}
void FTM_SetMSB_MSA_Mode(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_MSB_ModeSelect msbModeSelect, FTM_MSA_ModeSelect msaModeSelect)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_CnSC(channelType) &= ~(FTM_CnSC_MSA_MASK|FTM_CnSC_MSB_MASK);
		FTM0_CnSC(channelType) |= FTM_CnSC_MSA(msaModeSelect)|FTM_CnSC_MSB(msbModeSelect);
		break;
	case FTM_1:
		FTM1_CnSC(channelType) &= ~(FTM_CnSC_MSA_MASK|FTM_CnSC_MSB_MASK);
		FTM1_CnSC(channelType) |= FTM_CnSC_MSA(msaModeSelect)|FTM_CnSC_MSB(msbModeSelect);
		break;
	case FTM_2:
		FTM2_CnSC(channelType) &= ~(FTM_CnSC_MSA_MASK|FTM_CnSC_MSB_MASK);
		FTM2_CnSC(channelType) |= FTM_CnSC_MSA(msaModeSelect)|FTM_CnSC_MSB(msbModeSelect);
		break;
	case FTM_3:
		FTM3_CnSC(channelType) &= ~(FTM_CnSC_MSA_MASK|FTM_CnSC_MSB_MASK);
		FTM3_CnSC(channelType) |= FTM_CnSC_MSA(msaModeSelect)|FTM_CnSC_MSB(msbModeSelect);
		break;
	default:
		break;
	}
}
void FTM_SetEdgeLevelSelect(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_ELSB_EdgeLevelSelect elsb, FTM_ELSA_EdgeLevelSelect elsa)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_CnSC(channelType) &= ~(FTM_CnSC_ELSA_MASK|FTM_CnSC_ELSB_MASK);
		FTM0_CnSC(channelType) |= FTM_CnSC_ELSA(elsa)|FTM_CnSC_ELSB(elsb);
		break;
	case FTM_1:
		FTM1_CnSC(channelType) &= ~(FTM_CnSC_ELSA_MASK|FTM_CnSC_ELSB_MASK);
		FTM1_CnSC(channelType) |= FTM_CnSC_ELSA(elsa)|FTM_CnSC_ELSB(elsb);
		break;
	case FTM_2:
		FTM2_CnSC(channelType) &= ~(FTM_CnSC_ELSA_MASK|FTM_CnSC_ELSB_MASK);
		FTM2_CnSC(channelType) |= FTM_CnSC_ELSA(elsa)|FTM_CnSC_ELSB(elsb);
		break;
	case FTM_3:
		FTM3_CnSC(channelType) &= ~(FTM_CnSC_ELSA_MASK|FTM_CnSC_ELSB_MASK);
		FTM3_CnSC(channelType) |= FTM_CnSC_ELSA(elsa)|FTM_CnSC_ELSB(elsb);
		break;
	default:
		break;
	}
}
void FTM_SetDMA(FTM_NameType ftmName, FTM_TypeChannel channelType, FTM_DMA ftmDma)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_CnSC(channelType) &= ~FTM_CnSC_DMA_MASK;
		FTM0_CnSC(channelType) |= FTM_CnSC_DMA(ftmDma);
		break;
	case FTM_1:
		FTM1_CnSC(channelType) &= ~FTM_CnSC_DMA_MASK;
		FTM1_CnSC(channelType) |= FTM_CnSC_DMA(ftmDma);
		break;
	case FTM_2:
		FTM2_CnSC(channelType) &= ~FTM_CnSC_DMA_MASK;
		FTM2_CnSC(channelType) |= FTM_CnSC_DMA(ftmDma);
		break;
	case FTM_3:
		FTM3_CnSC(channelType) &= ~FTM_CnSC_DMA_MASK;
		FTM3_CnSC(channelType) |= FTM_CnSC_DMA(ftmDma);
		break;
	default:
		break;
	}
}
void FTM_SetFaultieIE(FTM_NameType ftmName,FTM_FaultIEB faultieInterruptE)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_FAULTIE_MASK;
		FTM0_MODE |= FTM_MODE_FAULTIE(faultieInterruptE);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_FAULTIE_MASK;
		FTM1_MODE |= FTM_MODE_FAULTIE(faultieInterruptE);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_FAULTIE_MASK;
		FTM2_MODE |= FTM_MODE_FAULTIE(faultieInterruptE);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_FAULTIE_MASK;
		FTM3_MODE |= FTM_MODE_FAULTIE(faultieInterruptE);
		break;
	default:
		break;
	}
}
void FTM_SetTypeFaultControl(FTM_NameType ftmName, FTM_TypeFaultControl faultTypeControl)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_FAULTM_MASK;
		FTM0_MODE |= FTM_MODE_FAULTM(faultTypeControl);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_FAULTM_MASK;
		FTM1_MODE |= FTM_MODE_FAULTM(faultTypeControl);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_FAULTM_MASK;
		FTM2_MODE |= FTM_MODE_FAULTM(faultTypeControl);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_FAULTM_MASK;
		FTM3_MODE |= FTM_MODE_FAULTM(faultTypeControl);
		break;
	default:
		break;
	}
}
void FTM_SetCaptureTestEnable(FTM_NameType ftmName, FTM_CaptureTestEB captureTestE)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_CAPTEST_MASK;
		FTM0_MODE |= FTM_MODE_CAPTEST(captureTestE);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_CAPTEST_MASK;
		FTM1_MODE |= FTM_MODE_CAPTEST(captureTestE);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_CAPTEST_MASK;
		FTM2_MODE |= FTM_MODE_CAPTEST(captureTestE);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_CAPTEST_MASK;
		FTM3_MODE |= FTM_MODE_CAPTEST(captureTestE);
		break;
	default:
		break;
	}
}
void FTM_SetTypePWMSync(FTM_NameType ftmName,FTM_TypePWMSync pwmSyncType)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_PWMSYNC_MASK;
		FTM0_MODE |= FTM_MODE_PWMSYNC(pwmSyncType);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_PWMSYNC_MASK;
		FTM1_MODE |= FTM_MODE_PWMSYNC(pwmSyncType);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_PWMSYNC_MASK;
		FTM2_MODE |= FTM_MODE_PWMSYNC(pwmSyncType);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_PWMSYNC_MASK;
		FTM3_MODE |= FTM_MODE_PWMSYNC(pwmSyncType);
		break;
	default:
		break;
	}
}
void FTM_SetWriteProtectionDisableEnable(FTM_NameType ftmName, FTM_WriteProtectionDisableE writeProtectionDisableE)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_WPDIS_MASK;
		FTM0_MODE |= FTM_MODE_WPDIS(writeProtectionDisableE);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_WPDIS_MASK;
		FTM1_MODE |= FTM_MODE_WPDIS(writeProtectionDisableE);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_WPDIS_MASK;
		FTM2_MODE |= FTM_MODE_WPDIS(writeProtectionDisableE);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_WPDIS_MASK;
		FTM3_MODE |= FTM_MODE_WPDIS(writeProtectionDisableE);
		break;
	default:
		break;
	}
}
void FTM_SetInitChannelOutput(FTM_NameType ftmName, FTM_InitChOut initChOut)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_INIT_MASK;
		FTM0_MODE |= FTM_MODE_INIT(initChOut);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_INIT_MASK;
		FTM1_MODE |= FTM_MODE_INIT(initChOut);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_INIT_MASK;
		FTM2_MODE |= FTM_MODE_INIT(initChOut);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_INIT_MASK;
		FTM3_MODE |= FTM_MODE_INIT(initChOut);
		break;
	default:
		break;
	}
}
void FTM_SetTypeAvailableRegisters(FTM_NameType ftmName, FTM_TypeAvailableRegisters availableRegistersType)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MODE &= ~FTM_MODE_FTMEN_MASK;
		FTM0_MODE |= FTM_MODE_FTMEN(availableRegistersType);
		break;
	case FTM_1:
		FTM1_MODE &= ~FTM_MODE_FTMEN_MASK;
		FTM1_MODE |= FTM_MODE_FTMEN(availableRegistersType);
		break;
	case FTM_2:
		FTM2_MODE &= ~FTM_MODE_FTMEN_MASK;
		FTM2_MODE |= FTM_MODE_FTMEN(availableRegistersType);
		break;
	case FTM_3:
		FTM3_MODE &= ~FTM_MODE_FTMEN_MASK;
		FTM3_MODE |= FTM_MODE_FTMEN(availableRegistersType);
		break;
	default:
		break;
	}
}


void FTM_SetModValue(FTM_NameType ftmName, FTM_ModVal modVal)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_MOD &= ~FTM_MOD_MOD_MASK;
		FTM0_MOD |= FTM_MOD_MOD(modVal);
		break;
	case FTM_1:
		FTM1_MOD &= ~FTM_MOD_MOD_MASK;
		FTM1_MOD |= FTM_MOD_MOD(modVal);
		break;
	case FTM_2:
		FTM2_MOD &= ~FTM_MOD_MOD_MASK;
		FTM2_MOD |= FTM_MOD_MOD(modVal);
		break;
	case FTM_3:
		FTM3_MOD &= ~FTM_MOD_MOD_MASK;
		FTM3_MOD |= FTM_MOD_MOD(modVal);
		break;
	default:
		break;
	}
}
void FTM_SetOverflowInterrput(FTM_NameType ftmName, FTM_OverflowInterruptB overflowInterruptB)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_SC &= ~FTM_SC_TOIE_MASK;
		FTM0_SC |= FTM_SC_TOIE(overflowInterruptB);
		if(overflowInterruptB)
		{
			NVIC_enableInterruptAndPriotity(FTM0_IRQ,PRIORITY_1);
		}
		break;
	case FTM_1:
		FTM1_SC &= ~FTM_SC_TOIE_MASK;
		FTM1_SC |= FTM_SC_TOIE(overflowInterruptB);
		if(overflowInterruptB)
		{
			NVIC_enableInterruptAndPriotity(FTM1_IRQ,PRIORITY_5);
		}
		break;
	case FTM_2:
		FTM2_SC &= ~FTM_SC_TOIE_MASK;
		FTM2_SC |= FTM_SC_TOIE(overflowInterruptB);
		if(overflowInterruptB)
		{
			NVIC_enableInterruptAndPriotity(FTM2_IRQ,PRIORITY_5);
		}
		break;
	case FTM_3:
		FTM3_SC &= ~FTM_SC_TOIE_MASK;
		FTM3_SC |= FTM_SC_TOIE(overflowInterruptB);
		if(overflowInterruptB)
		{
			NVIC_enableInterruptAndPriotity(FTM3_IRQ,PRIORITY_2);
		}
		break;
	default:
		break;
	}
}
void FTM_SetCounterTypePWMS(FTM_NameType ftmName, FTM_CounterTypePWMS counterType)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_SC &= ~FTM_SC_CPWMS_MASK;
		FTM0_SC |= FTM_SC_CPWMS(counterType);
		break;
	case FTM_1:
		FTM1_SC &= ~FTM_SC_CPWMS_MASK;
		FTM1_SC |= FTM_SC_CPWMS(counterType);
		break;
	case FTM_2:
		FTM2_SC &= ~FTM_SC_CPWMS_MASK;
		FTM2_SC |= FTM_SC_CPWMS(counterType);
		break;
	case FTM_3:
		FTM3_SC &= ~FTM_SC_CPWMS_MASK;
		FTM3_SC |= FTM_SC_CPWMS(counterType);
		break;
	default:
		break;
	}
}
void FTM_SetTypeClockSource(FTM_NameType ftmName, FTM_TypeCLKsource sourceClk)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_SC &= ~FTM_SC_CLKS_MASK;
		FTM0_SC |= FTM_SC_CLKS(sourceClk);
		break;
	case FTM_1:
		FTM1_SC &= ~FTM_SC_CLKS_MASK;
		FTM1_SC |= FTM_SC_CLKS(sourceClk);
		break;
	case FTM_2:
		FTM2_SC &= ~FTM_SC_CLKS_MASK;
		FTM2_SC |= FTM_SC_CLKS(sourceClk);
		break;
	case FTM_3:
		FTM3_SC &= ~FTM_SC_CLKS_MASK;
		FTM3_SC |= FTM_SC_CLKS(sourceClk);
		break;
	default:
		break;
	}
}
void FTM_SetTypePrescalerFactor(FTM_NameType ftmName,FTM_TypePrescalerFactor prescaleFactor)
{
	switch(ftmName)
	{
	case FTM_0:
		FTM0_SC &= ~FTM_SC_PS_MASK;
		FTM0_SC |= FTM_SC_PS(prescaleFactor);
		break;
	case FTM_1:
		FTM1_SC &= ~FTM_SC_PS_MASK;
		FTM1_SC |= FTM_SC_PS(prescaleFactor);
		break;
	case FTM_2:
		FTM2_SC &= ~FTM_SC_PS_MASK;
		FTM2_SC |= FTM_SC_PS(prescaleFactor);
		break;
	case FTM_3:
		FTM3_SC &= ~FTM_SC_PS_MASK;
		FTM3_SC |= FTM_SC_PS(prescaleFactor);
		break;
	default:
		break;
	}
}

void FTM_ClockGatingEnable(FTM_NameType ftmName)
{
	switch(ftmName)
	{
	case FTM_0:
		SIM_SCGC6 |= SIM_SCGC6_FTM0_MASK;
		break;
	case FTM_1:
		SIM_SCGC6 |= SIM_SCGC6_FTM1_MASK;
		break;
	case FTM_2:
		SIM_SCGC6 |= SIM_SCGC6_FTM2_MASK;
		break;
	case FTM_3:
		SIM_SCGC3 |= SIM_SCGC3_FTM3_MASK;
		break;
	default:
		break;
	}
}
void FlexTimer_InitGeneralFlex(const FTM_GeneralConfiguration* flexTimerConfiguration)
{
	FTM_ClockGatingEnable(flexTimerConfiguration->ftmName);
	FTM_SetOverflowInterrput(flexTimerConfiguration->ftmName, flexTimerConfiguration->overflowInterruptB);
	FTM_SetCounterTypePWMS(flexTimerConfiguration->ftmName, flexTimerConfiguration->counterType);
	FTM_SetTypeClockSource(flexTimerConfiguration->ftmName, flexTimerConfiguration->sourceClk);
	FTM_SetTypePrescalerFactor(flexTimerConfiguration->ftmName,flexTimerConfiguration->prescaleFactor);

	FTM_SetModValue(flexTimerConfiguration->ftmName, flexTimerConfiguration->modVal);
	FTM_SetFaultieIE(flexTimerConfiguration->ftmName,flexTimerConfiguration->faultieInterruptE);
	FTM_SetTypeFaultControl(flexTimerConfiguration->ftmName, flexTimerConfiguration->faultTypeControl);
	FTM_SetCaptureTestEnable(flexTimerConfiguration->ftmName, flexTimerConfiguration->captureTestE);
	FTM_SetTypePWMSync(flexTimerConfiguration->ftmName,flexTimerConfiguration->pwmSyncType);
	FTM_SetWriteProtectionDisableEnable(flexTimerConfiguration->ftmName, flexTimerConfiguration->writeProtectionDisableE);
	FTM_SetInitChannelOutput(flexTimerConfiguration->ftmName, flexTimerConfiguration->initChOut);
	FTM_SetTypeAvailableRegisters(flexTimerConfiguration->ftmName, flexTimerConfiguration->availableRegistersType);

	FTM_SetChannelInterruptEnable(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->channelIE);
	FTM_SetMSB_MSA_Mode(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->msbModeSelect, flexTimerConfiguration->msaModeSelect);
	FTM_SetEdgeLevelSelect(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->elsb, flexTimerConfiguration->elsa);
	FTM_SetDMA(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->ftmDma);
	FTM_SetCVvalue(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->cvType);
	FTM_updateCHValue(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->cvType);
}
void FlexTimer_InitOverflow(const FTM_OverflowConfiguration* flexTimerConfiguration)
{
	FTM_ClockGatingEnable(flexTimerConfiguration->ftmName);
	FTM_SetWriteProtectionDisableEnable(flexTimerConfiguration->ftmName, FTM_ENABLE);

	FTM_SetOverflowInterrput(flexTimerConfiguration->ftmName, FTM_ENABLE);
	FTM_SetCounterTypePWMS(flexTimerConfiguration->ftmName, FTM_COUNTER_UP);
	FTM_SetTypeClockSource(flexTimerConfiguration->ftmName, flexTimerConfiguration->sourceClk);
	FTM_SetTypePrescalerFactor(flexTimerConfiguration->ftmName, flexTimerConfiguration->prescaleFactor);

	FTM_SetModValue(flexTimerConfiguration->ftmName, flexTimerConfiguration->modVal);
	FTM_SetFaultieIE(flexTimerConfiguration->ftmName, FTM_DISABLE);
	FTM_SetTypeFaultControl(flexTimerConfiguration->ftmName, FTM_FAULT_CONTROL_DISABLED);
	FTM_SetCaptureTestEnable(flexTimerConfiguration->ftmName, FTM_DISABLE);
	FTM_SetTypePWMSync(flexTimerConfiguration->ftmName, FTM_NO_RESTRICTIONS);
	FTM_SetInitChannelOutput(flexTimerConfiguration->ftmName, 0);
	FTM_SetTypeAvailableRegisters(flexTimerConfiguration->ftmName, FTM_ONLY_TPM_COMPATIBLE_REGISTERS_AVAILABLE);
}

void FlexTimer_InitPWM(const FTM_PWMConfiguration* flexTimerConfiguration)
{

	FTM_ClockGatingEnable(flexTimerConfiguration->ftmName);
	FTM_SetWriteProtectionDisableEnable(flexTimerConfiguration->ftmName, FTM_ENABLE);
	FTM_SetOverflowInterrput(flexTimerConfiguration->ftmName, FTM_ENABLE);
	FTM_SetCounterTypePWMS(flexTimerConfiguration->ftmName, FTM_COUNTER_UP);
	FTM_SetTypeClockSource(flexTimerConfiguration->ftmName, flexTimerConfiguration->sourceClk);
	FTM_SetTypePrescalerFactor(flexTimerConfiguration->ftmName,flexTimerConfiguration->prescaleFactor);
	FTM_SetModValue(flexTimerConfiguration->ftmName, flexTimerConfiguration->modVal);
	FTM_SetFaultieIE(flexTimerConfiguration->ftmName, FTM_DISABLE);
	FTM_SetTypeFaultControl(flexTimerConfiguration->ftmName, FTM_FAULT_CONTROL_DISABLED);
	FTM_SetTypeAvailableRegisters(flexTimerConfiguration->ftmName, FTM_ONLY_TPM_COMPATIBLE_REGISTERS_AVAILABLE);
	FTM_SetChannelInterruptEnable(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->channelIE);
	FTM_SetMSB_MSA_Mode(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->msbModeSelect, flexTimerConfiguration->msaModeSelect);
	FTM_SetEdgeLevelSelect(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->elsb, flexTimerConfiguration->elsa);
	FTM_SetCVvalue(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->cvType);

}

void FlexTimer_InitInputCaptureMode(const FTM_InputCaptureConfiguration* flexTimerConfiguration)
{
	FTM_ClockGatingEnable(flexTimerConfiguration->ftmName);
	NVIC_enableInterruptAndPriotity(FTM3_IRQ,PRIORITY_5);
	FTM3_SC=0x00;//DEVOLVER ESTA Y LA ANTERIOR A 2 PARA QUE FUNCIONE.
	FTM_SetWriteProtectionDisableEnable(flexTimerConfiguration->ftmName, FTM_ENABLE);
	FTM_SetOverflowInterrput(flexTimerConfiguration->ftmName, FTM_ENABLE);//todo reactivar
	FTM_SetTypeClockSource(flexTimerConfiguration->ftmName, flexTimerConfiguration->sourceClk);
	FTM_SetTypePrescalerFactor(flexTimerConfiguration->ftmName,flexTimerConfiguration->prescaleFactor);
	FTM_SetModValue(flexTimerConfiguration->ftmName, flexTimerConfiguration->modVal);
	FTM_SetTypeAvailableRegisters(flexTimerConfiguration->ftmName, FTM_ONLY_TPM_COMPATIBLE_REGISTERS_AVAILABLE);
	FTM_SetChannelInterruptEnable(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->channelIE);
	FTM_SetMSB_MSA_Mode(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->msbModeSelect, flexTimerConfiguration->msaModeSelect);
	FTM_SetEdgeLevelSelect(flexTimerConfiguration->ftmName, flexTimerConfiguration->channelType, flexTimerConfiguration->elsb, flexTimerConfiguration->elsa);
}


void FlexTimer_OutputCompareInit()
{

}


