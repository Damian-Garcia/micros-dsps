/**
	\file
	\brief
		This is the source file for the pin configuration which contains the definition of the ports and
		pins that are going to be used by the system and also defines some functions that are going to
		modify the values of the pins.
	\author Erick Ortega y Damian Garcia.
	\date	10/18/2016
 */
#include "Practica2.h"
#include "Eventos.h"
#include "FSM_Entrada.h"
#include "ControlAlarma.h"
#include "ControlMotor.h"
#include "ControlPantalla.h"
#include "SPI.h"
#include "PIT.h"
#include "LCDNokia5110.h"
#include "ADC.h"
#include "NVIC.h"
#include "FlexTimer.h"
#include "PinConfiguration.h"


struct FTM_Overflow_Configuration overflowConfig =
{
	FTM_0,
	FTM_SYSTEM_CLK,
	FTM_DIVIDED_128,
	0xFF,
};

const FTM_PWMConfiguration pwmConfig =
{
	FTM_2,
	FTM_SYSTEM_CLK,
	FTM_DIVIDED_128,
	FSM_MOT_MAX_CHVAL,
	FTM_CH0,
	FTM_DISABLE,
	0x1,//b
	0x0,//a
	0x1,//b
	0x0,//a
	FSM_MOT_MIN_CHVAL + (FSM_MOT_MAX_CHVAL-FSM_MOT_MIN_CHVAL)/2
};
const FTM_InputCaptureConfiguration inputCaptureConfig =
{
		//Modo edge align
	FTM_3,
	FTM_SYSTEM_CLK,
	FTM_DIVIDED_1,
	0xFFFF,
	FTM_CH4,
	FTM_ENABLE,
	0x0,//b
	0x0,//a
	0x0,//b
	0x1,//a
	0xFFFF
};

const ADC_Config adcConfig=
{
		ADC_0,// Adc name
		ADC_ENABLE,// Conversion interrupt
		ADC_SINGLE_MODE,// ADC Mode
		0x1F,//Channel type (se utilizara el cero al momento de realizar la lectura)
		ADC_DISABLE,//adc LWPWRCONF
		ADC_DIVRATIO_1,//ADC DIV ratio
		ADC_SHORT_SAMPLE,//ADC sample type
		ADC_S_16BIT_DIFF_16BIT,//Resolution mode
		ADC_BUSCLK,//Adc clockSrc
		ADC_ADXXA,// adc mux type
		ADC_DISABLE, // asuync output clock disable
		ADC_SOFTWARETRIGGER, //trigger type
		ADC_DISABLE,//Comparefunction
		ADC_LESSTHANTHRESHOLD_NOTINCLUSIVE,//function greater than
		ADC_DISABLE,// functionRange
		ADC_DISABLE,// adcDma;
		ADC_VOLTAGE_DEFAULT,// adc VOLTAGE;
		ADC_ONE_CONVERSION,// ADC_ContinuousConvertType
		ADC_ENABLE,//ADC_HardwareAverageEnable
		SAMPLE_32// average value ;
};

const SPI_ConfigType SPI_Config = { SPI_DISABLE_FIFO,
		SPI_LOW_POLARITY,
		SPI_LOW_PHASE,
		SPI_MSB,
		SPI_0,
		SPI_MASTER,
		GPIO_MUX2,
		SPI_BAUD_RATE_2,
		SPI_FSIZE_8,
		{ GPIOD, BIT1, BIT2 } };

void Practica2_init() {
	// TODO: btns init
	// TODO: pantalla init
	// TODO: motor init
	// TODO: init pin configurations
	SPI_init(&SPI_Config);
	LCDNokia_init();
	PinConfig_BtnInputPinsInitialization();

	NVIC_enableInterruptAndPriotity(PORTC_IRQ, PRIORITY_4);

	PIT_TimerType pit = PIT_0;
	PIT_TimerType pit1 = PIT_1;
	PIT_TimerType pit2 = PIT_2;

	PIT_ClockGatingEnable();
	float sysClock = 21000000;
	float period = 0.5;

	PIT_setTimeDelay(pit, sysClock, period);
	PIT_setTimeDelay(pit1, sysClock, period);
	PIT_setTimeDelay(pit2, sysClock, 0.3);

	PIT_MCRConfig pitMCR = { PIT_DISABLE, PIT_DISABLE };
	PIT_TCRConfig pitTCRConfig = { pit, NOTCHAINED, PIT_ENABLE, PIT_ENABLE };
	PIT_TCRConfig pitTCRConfig1 = { pit1, NOTCHAINED, PIT_ENABLE, PIT_ENABLE };
	PIT_TCRConfig pitTCRConfig2 = { pit2, NOTCHAINED, PIT_ENABLE, PIT_DISABLE };

	NVIC_setBASEPRI_threshold(PRIORITY_15);
	NVIC_enableInterruptAndPriotity(PIT_CH0_IRQ, PRIORITY_12);
	NVIC_enableInterruptAndPriotity(PIT_CH1_IRQ, PRIORITY_12);
	NVIC_enableInterruptAndPriotity(PIT_CH2_IRQ, PRIORITY_12);
/*
 * Inicializacion de los flex timmer y del ADC
 */
	/*
	 * SOLO PARA EL FRECUENCIOMETRO  FTM3 channel 4
	 */
		PinConfig_FrecInputPinsInitialization();
		FlexTimer_InitInputCaptureMode(&inputCaptureConfig);
		FlexTimer_InitOverflow(&overflowConfig);
	/*
	 * SOLO PARA EL ADC SENSOR DE TEMPERATURA FTM 0 channel 0*/
		ADC_initializeAdc(&adcConfig);

	/*PARA el PWM: FTM 2 Channel 0
	 *
	 */
		PinConfig_PWMOutputPinInitialization();
		FlexTimer_InitPWM(&pwmConfig);
		setMotorFlexNewDutyCycle(85);

	PinConfig_outputPinsInitialization();

	EnableInterrupts;
	PIT_configureMCRRegister(pitMCR);
	PIT_configureTimmerControlRegister(pitTCRConfig);
	//PIT_configureTimmerControlRegister(pitTCRConfig1);

	ControlAlarma_init();
	ControlMotor_init();
	ControlPantalla_init();
}

void Practica2_run() {
	// Obtener ultimo evento
	Evento_Boton evt;
	evt = getEventoBoton();
	FSM_Entrada_eval(evt);

	/* La alarma no evalua la temperatura siempre, solo cuando el PIT la despierte,
	 * para darle tiempo al ADC.
	 */
	ControlAlarma_evaluateTemp();


	/*
	 * La pantalla no imprime siempre, solo cuando un PIT la despieste
	 */
	if (ControlPantalla_hasToPrint()) {
		ControlPantalla_printActual();
	}

}
