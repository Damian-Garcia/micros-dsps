/**	 \file Formater.c
	 \brief
	 	 This is the source file for the string conversions for printing.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#include "Formatter.h"

uint32 tenPower(uint8 n){
	int r = 1;
	for(;n;n--){
		r *= 10;
	}
	return r;
}

uint8 integerlength(uint32 number){
	uint8 digits;
	for (digits = 0; number ;number /= 10, digits++);
	return (digits>0)? digits : 1;
}

void formatUnsignedInteger32(uint32 number, uint8 *buffer, sint8 digits) {
	uint8 i = 0;
	if(digits < 0){
		digits = integerlength(number);
	}

	for (i = 0; i < digits; i++) {
		buffer[digits - i - 1] = (number % 10) + '0';
		number /= 10;
	}
	buffer[digits] = '\0';
}

void formatDouble(double number, uint8 *buffer, uint8 decimalDigits) {
	uint32 integerPart;
	integerPart = number;
	uint8 digits = integerlength(integerPart);
	formatUnsignedInteger32(number, buffer, digits);
	buffer[digits] = '.';
	number -= integerPart;
	number *= tenPower(decimalDigits);
	uint32 decimalPart = number;
	formatUnsignedInteger32(number, buffer+digits+1, decimalDigits);
}
