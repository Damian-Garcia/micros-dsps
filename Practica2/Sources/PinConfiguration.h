/**
	\file
	\brief
		This is the header file for the pin configuration which contains the definition of the ports and
		pins that are going to be used by the system and also defines some functions that are going to
		modify the values of the pins.
	\author Erick Ortega y Damian Garcia.
	\date	10/18/2016
 */
#ifndef SOURCES_PINCONFIGURATION_H_
#define SOURCES_PINCONFIGURATION_H_
#include "MK64F12.h"
#include "NVIC.h"
#include "DataTypeDefinitions.h"
#include "GPIO.h"
#include "FlexTimer.h"


/*!
 	 \def FSM_PORT_BTN0
 	 \brief Constant that indicate the port of the button 0
 */
#define FSM_PORT_BTN0 GPIOC
/*!
 	 \def FSM_PIN_BTN0
 	 \brief Constant that indicate the pin number of the button 0
 */
#define FSM_PIN_BTN0 0

/*!
 	 \def FSM_PORT_BTN1
 	 \brief Constant that indicate the port of the button 1
 */
#define FSM_PORT_BTN1 GPIOC
/*!
 	 \def FSM_PIN_BTN1
 	 \brief Constant that indicate the pin number of the button 1
 */
#define FSM_PIN_BTN1 1

/*!
 	 \def FSM_PORT_BTN2
 	 \brief Constant that indicate the port of the button 2
 */
#define FSM_PORT_BTN2 GPIOC
/*!
 	 \def FSM_PIN_BTN2
 	 \brief Constant that indicate the pin number of the button 2
 */
#define FSM_PIN_BTN2 2

/*!
 	 \def FSM_PORT_BTN3
 	 \brief Constant that indicate the port of the button 3
 */
#define FSM_PORT_BTN3 GPIOC
/*!
 	 \def FSM_PIN_BTN3
 	 \brief Constant that indicate the pin number of the button 3
 */
#define FSM_PIN_BTN3 3

/*!
 	 \def FSM_PORT_BTN4
 	 \brief Constant that indicate the port of the button 4
 */
#define FSM_PORT_BTN4 GPIOC
/*!
 	 \def FSM_PIN_BTN4
 	 \brief Constant that indicate the pin number of the button 4
 */
#define FSM_PIN_BTN4 4

/*!
 	 \def FSM_PORT_BTN5
 	 \brief Constant that indicate the port of the button 5
 */
#define FSM_PORT_BTN5 GPIOC

/*!
 	 \def FSM_PIN_BTN5
 	 \brief Constant that indicate the pin number of the button 5
 */
#define FSM_PIN_BTN5 5

/*
 * Puertos y pines de los perifericos ----------------------------------------------------------------------
 */
/*!
 	 \def FSM_MOT_PORT_OUT
 	 \brief Constant that indicate the port of the motor output.
 */
#define FSM_MOT_PORT_OUT GPIOB

/*!
 	 \def FSM_MOT_PIN_OUT
 	 \brief Constant that indicate the pin of the motor output.
 */
#define FSM_MOT_PIN_OUT 18

/*!
 	 \def FSM_FREC_PORT_IN
 	 \brief Constant that indicate the port of the frecuentiometer input.
 */
#define FSM_FREC_PORT_IN GPIOC//GPIOB
/*!
 	 \def FSM_FREC_PIN_IN
 	 \brief Constant that indicate the pin of the frecuentiometer input.
 */
#define FSM_FREC_PIN_IN 8//19

/*!
 	 \def FSM_ALARM_PORT_OUT
 	 \brief Constant that indicate the output port of the alarm buzzer.
 */
#define FSM_ALARM_PORT_OUT GPIOE
/*!
 	 \def FSM_ALARM_PIN_OUT
 	 \brief Constant that indicate the output pin of the alarm buzzer.
 */
#define FSM_ALARM_PIN_OUT 25
/*
 * Puertos y pines de la pantalla --------------------------------------
 */

/*!
 	 \def SCREEN_PORT_RST
 	 \brief Constant that indicate the output port of the screen reset.
 */
#define SCREEN_PORT_RST GPIOD
/*!
 	 \def SCREEN_PIN_RST
 	 \brief Constant that indicate the output pin of the screen reset.
 */
#define SCREEN_PIN_RST 0
/*!
 	 \def SCREEN_PORT_DC
 	 \brief Constant that indicate the output port of the screen dc.
 */

#define SCREEN_PORT_DC GPIOD
/*!
 	 \def SCREEN_PIN_DC
 	 \brief Constant that indicate the output pin of the screen dc.
 */
#define SCREEN_PIN_DC 3

/*!
 	 \def SCREEN_PORT_DIN
 	 \brief Constant that indicate the output port of the screen din.
 */
#define SCREEN_PORT_DIN GPIOD
/*!
 	 \def SCREEN_PIN_DIN
 	 \brief Constant that indicate the output pin of the screen din.
 */
#define SCREEN_PIN_DIN 2
/*!
 	 \def SCREEN_PORT_CLK
 	 \brief Constant that indicate the output port of the screen clk.
 */
#define SCREEN_PORT_CLK GPIOD
/*!
 	 \def SCREEN_PIN_CLK
 	 \brief Constant that indicate the output pin of the screen clk.
 */
#define SCREEN_PIN_CLK 1


/*!
 	 \def FSM_MOT_MAX_CHVAL
 	 \brief Constant that indicate the maximum value of the channel value of the flex timer that the motor is using.
 */
#define FSM_MOT_MAX_CHVAL 0x014A
/*!
 	 \def FSM_MOT_MIN_CHVAL
 	 \brief Constant that indicate the minimum value of the channel value of the flex timer that the motor is using.
 */
#define FSM_MOT_MIN_CHVAL 0x0

/*!
 	 \def FSM_MOT_STEP_SIZE
 	 \brief Constant that indicates a one percent step of the pwm duty cycle of the motor.
 */
#define FSM_MOT_STEP_SIZE (FSM_MOT_MAX_CHVAL-FSM_MOT_MIN_CHVAL)/100

/*! \typedef FSM_MotorOutputValue
 *	\brief Constants defined to specify the value of the motor output pin value.
*/
typedef enum
{
	MOTOR_DISABLED, /*!< Definition of the value to disable (turn off) the motor*/
	MOTOR_ENABLED /*!< Definition of the value to enable the motor*/
}FSM_MotorOutputValue;

/*! \typedef FSM_AlarmOutputValue
 *	\brief Constants defined to specify the value of the motor output pin value.
*/
typedef enum
{
	ALARM_DISABLED, /*!< Definition of the value to disable (turn off) the alarm*/
	ALARM_ENABLED /*!< Definition of the value to enable the alarm*/
}FSM_AlarmOutputValue;

/*
 * Functions that interact with the device drivers and other things.
 */
double getMotorFlexTimerActualPercent();

/*!
 	 \brief	 This function configures increments. the value that is going to be assigned to the flex timer.
 	 \return void
 */
void incrementMotorFlexTimerFrequenceValue();//Incrementa un paso el valor que se actualizara al flex timer (1%)
/*!
 	 \brief	 This function configures decrements the value that is going to be assigned to the flex timer.
 	 \return void
 */
void decrementMotorFlexTimerFrequenceValue();//Decrementa en un paso el valor que se actualizara en el flex timer (-1%)

/*!
 	 \brief	 This function configures the frequence value gradually. (not used).
 	 \return void
 */
void setMotorFlexNewFrequence();
/*!
 	 \brief	 This function configures the duty cycle indicatated (it receives it in percentage).
 	 \return void
 */
void setMotorFlexNewDutyCycle(uint16_t dutyCycle);
/*!
 	 \brief	 This function configures the frecuenciometer input pin as an input capture of a flex timer.
 	 \return void
 */
void PinConfig_FrecInputPinsInitialization();
/*!
 	 \brief	 This function configures the pins of the push buttons connected (6 push buttons.).
 	 \return void
 */
void PinConfig_BtnInputPinsInitialization();

/*!
 	 \brief	 This function configures the interruption of the port where the push buttons are connected.
 	 \return void
 */
void PinConfig_BtnInterruptPortInitialization();

/*!
 	 \brief	 This function configures the pins of the alarm as output pins.  (setting the initial value of each
 	 pin in the turned off value.
 	 \return void
 */
void PinConfig_outputPinsInitialization();
/*!
 	 \brief	 This function sets the value of the alarm in the received value.
 	  \param[in]  value Value that is going to be assigned to the alarm output pin.
 	 \return void
 */
void PinConfig_setOutputAlarmValue(FSM_AlarmOutputValue value);

/*!
 	 \brief	 This function configures the pwm output pin as a pwm pin.
 	 \return void
 */
void PinConfig_PWMOutputPinInitialization();
/*!
 	 \brief	 This function sets the value of the motor in the received value.
 	  \param[in]  value Value that is going to be assigned to the motor output pin.
 	 \return void
 */
void PinConfig_setOutputMotorValue(FSM_MotorOutputValue value);


#endif /* SOURCES_PINCONFIGURATION_H_ */
