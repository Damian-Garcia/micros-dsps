/**	 \file ControlAlarma.h
	 \brief
	 	 This is the header file for Alarm and its actions over other modules
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */

#ifndef SOURCES_CONTROLALARMA_H_
#define SOURCES_CONTROLALARMA_H_

#include "DataTypeDefinitions.h"
#include "ControlPantalla.h"

/** Value of the Alarm at Startup */
#define AlarmInitialValue 30
/** Increment/Decrement step of the Alarm */
#define AlarmIncrementStep 1
/** Max configurable Alarm Value */
#define AlarmUpperLimit	50
/** Min configurable Alarm Value */
#define AlarmLowerLimit 1

/**
 * \brief This function configures the initial condition of the alarm
 * \return void
 */
void ControlAlarma_init();
/**
 * \brief This function increments the next value of the alarm
 * \return void
 */
void ControlAlarma_incrementValue();
/**
 * \brief This function decrements the next value of the alarm
 * \return void
 */
void ControlAlarma_decrementValue();
/**
 * \brief This function activates the control of the alarm over the Motor and the buzzer
 * \return void
 */
void ControlAlarma_setActive();
/**
 * \brief This function deactivates the control of the alarm over the Motor and the buzzer
 * \return void
 */
void ControlAlarma_clearActive();
/**
 * \brief This function sets the next value of the alarm as the current value
 * \return void
 */
void ControlAlarma_updateValue();
/**
 * \brief This function deletes the next value of the alarm an sets it to the current value
 * \return true
 */
void ControlAlarma_cancelChanges();
/**
 * \brief This function returns TRUE if the alarm controls the buzzer and the buzzer
 * \return BooleanType
 */
BooleanType ControlAlarma_isActive();
/**
 *\brief This function returns TRUE if the alarm is ready to process the next temperature value
 *\deprecated
 *\return BooleanType
 */
BooleanType ControlAlarma_isReady();
/**
 * \brief This function reads the stored temp value, calculates the speed of the motor and activates/deactivates the buzzer
 * \return void
 */
void ControlAlarma_evaluateTemp();
/**
 * \brief Returns the current value of the alarm
 * \return double
 */
double ControlAlarma_getAlarmValue();
/**
 * \brief Returns the next value of the alarm
 * return double
 */
double ControlAlarma_getNewAlarmValue();
/**
 * \brief Returns the current temperature as a double
 * \return double
 */
double ControlAlarma_getTemperatura();

#endif /* SOURCES_CONTROLALARMA_H_ */
