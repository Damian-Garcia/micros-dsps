/*
 * PinConfiguration.c
 *
 *  Created on: Sep 25, 2016
 *      Author: operi
 */

#ifndef SOURCES_PINCONFIGURATION_C_
#define SOURCES_PINCONFIGURATION_C_
#include "PinConfiguration.h"
static uint16_t motorFlexTimerChv = FSM_MOT_MIN_CHVAL + (FSM_MOT_MAX_CHVAL-FSM_MOT_MIN_CHVAL)/2;

double getMotorFlexTimerActualPercent()
{
	return motorFlexTimerChv*100/FSM_MOT_MAX_CHVAL;
}
void setMotorFlexNewDutyCycle(uint16_t dutyCycle)
{
	motorFlexTimerChv = FSM_MOT_MIN_CHVAL + (FSM_MOT_MAX_CHVAL-FSM_MOT_MIN_CHVAL)* ((double)dutyCycle)/100;
	FTM_updateCHValue(FTM_2, FTM_CH0, motorFlexTimerChv);
}

void incrementMotorFlexTimerFrequenceValue()//Incrementa un paso el valor que se actualizara al flex timer (1%)
{
	if(motorFlexTimerChv == FSM_MOT_MAX_CHVAL)
	{
		setMotorFlexNewFrequence();
		return;
	}
	motorFlexTimerChv+= FSM_MOT_STEP_SIZE;
	setMotorFlexNewFrequence();
}
void decrementMotorFlexTimerFrequenceValue()//Decrementa en un paso el valor que se actualizara en el flex timer (-1%)
{
	if(motorFlexTimerChv == FSM_MOT_MIN_CHVAL)
	{
		return;
	}
	motorFlexTimerChv-= FSM_MOT_STEP_SIZE;//Decrementamos hasta que comienze el valor.
}
void setMotorFlexNewFrequence()// Incrementa gradualmente hasta llegar al porcentaje
{
	FTM_updateCHValue(FTM_2, FTM_CH0, motorFlexTimerChv);
}

void PinConfig_FrecInputPinsInitialization()
{
	/*
	 * todo needs to configure to use a timer to measure the frequence of the signal.
	 */
	GPIO_clockGating(FSM_FREC_PORT_IN);
	GPIO_pinControlRegisterType pcrFSM_Frec = GPIO_MUX3;
	GPIO_pinControlRegister(FSM_FREC_PORT_IN, FSM_FREC_PIN_IN, &pcrFSM_Frec);
	//GPIO_dataDirectionPIN(FSM_FREC_PORT_IN,GPIO_INPUT,FSM_FREC_PIN_IN);

}
void PinConfig_BtnInputPinsInitialization()
{
	GPIO_clockGating(FSM_PORT_BTN0);
	GPIO_pinControlRegisterType pcrFSM_Btn0 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN0, FSM_PIN_BTN0, &pcrFSM_Btn0);
	GPIO_dataDirectionPIN(FSM_PORT_BTN0,GPIO_INPUT,FSM_PIN_BTN0);

	GPIO_clockGating(FSM_PORT_BTN1);
	GPIO_pinControlRegisterType pcrFSM_Btn1 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN1, FSM_PIN_BTN1, &pcrFSM_Btn1);
	GPIO_dataDirectionPIN(FSM_PORT_BTN1,GPIO_INPUT,FSM_PIN_BTN1);

	GPIO_clockGating(FSM_PORT_BTN2);
	GPIO_pinControlRegisterType pcrFSM_Btn2 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN2, FSM_PIN_BTN2, &pcrFSM_Btn2);
	GPIO_dataDirectionPIN(FSM_PORT_BTN2,GPIO_INPUT,FSM_PIN_BTN2);

	GPIO_clockGating(FSM_PORT_BTN3);
	GPIO_pinControlRegisterType pcrFSM_Btn3 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN3, FSM_PIN_BTN3, &pcrFSM_Btn3);
	GPIO_dataDirectionPIN(FSM_PORT_BTN3,GPIO_INPUT,FSM_PIN_BTN3);

	GPIO_clockGating(FSM_PORT_BTN4);
	GPIO_pinControlRegisterType pcrFSM_Btn4 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN4, FSM_PIN_BTN4, &pcrFSM_Btn4);
	GPIO_dataDirectionPIN(FSM_PORT_BTN4,GPIO_INPUT,FSM_PIN_BTN4);

	GPIO_clockGating(FSM_PORT_BTN5);
	GPIO_pinControlRegisterType pcrFSM_Btn5 = GPIO_MUX1|GPIO_PE|GPIO_PS|INTR_FALLING_EDGE;
	GPIO_pinControlRegister(FSM_PORT_BTN5, FSM_PIN_BTN5, &pcrFSM_Btn5);
	GPIO_dataDirectionPIN(FSM_PORT_BTN5,GPIO_INPUT,FSM_PIN_BTN5);

}
void PinConfig_BtnInterruptPortInitialization()
{
	NVIC_enableInterruptAndPriotity(FSM_PORT_BTN5, PRIORITY_5);
	EnableInterrupts;
}

void PinConfig_outputPinsInitialization()
{
	/*
	 * Alarm output pin initialization
	 */
	GPIO_clockGating(FSM_ALARM_PORT_OUT);
	GPIO_pinControlRegisterType pcrFSM_AlarmOut = GPIO_MUX1|GPIO_PE|GPIO_PS;
	GPIO_pinControlRegister(FSM_ALARM_PORT_OUT, FSM_ALARM_PIN_OUT, &pcrFSM_AlarmOut);
	GPIO_setPIN(FSM_ALARM_PORT_OUT, FSM_ALARM_PIN_OUT); // La alarma es activa en alto
	GPIO_dataDirectionPIN(FSM_ALARM_PORT_OUT,GPIO_OUTPUT,FSM_ALARM_PIN_OUT);

}
void PinConfig_PWMOutputPinInitialization()
{
	GPIO_clockGating(FSM_MOT_PORT_OUT);
	GPIO_pinControlRegisterType pcrFSM_MotOut = GPIO_MUX3;
	GPIO_pinControlRegister(FSM_MOT_PORT_OUT, FSM_MOT_PIN_OUT, &pcrFSM_MotOut);
}
void PinConfig_setOutputAlarmValue(FSM_AlarmOutputValue value)
{
	GPIO_writePIN(FSM_ALARM_PORT_OUT, value, FSM_ALARM_PIN_OUT);
}
void PinConfig_setOutputMotorValue(FSM_MotorOutputValue value)
{
	GPIO_writePIN(FSM_MOT_PORT_OUT, value, FSM_MOT_PIN_OUT);
}

#endif /* SOURCES_PINCONFIGURATION_C_ */
