/**	 \file Eventos.h
	 \brief
	 This is the header file for the abstract control of the button events.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */

#ifndef SOURCES_EVENTOS_H_
#define SOURCES_EVENTOS_H_

/**
 * \typedef Evento_Boton
 * \brief This enum type contains the possible button events for the control FSM
 */
typedef enum {
	EVT_BTN_0,
	EVT_BTN_1,
	EVT_BTN_2,
	EVT_BTN_3,
	EVT_BTN_4,
	EVT_BTN_5,
	EVT_BTN_NOP,

	EVT_BTN_ENUM_MAX
} Evento_Boton;

/*! \brief This function adds an event to be processed
 * 	\param[in] Evento_Boton
 *	\return void
 */
void addEventoBoton(Evento_Boton);

/**
 * \brief This function pops an event to be processed
 * \return Evento_Boton
 */
Evento_Boton getEventoBoton();

#endif
