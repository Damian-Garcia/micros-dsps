/**	 \file Practica2.h
	 \brief
	 This is the header file for project configuration and behavior.
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */

#ifndef PRACTICA2_H_
#define PRACTICA2_H_

/**
 * \brief
 * 		This function configures the ports and logic of the circuit.
 * 	\return void
 */
void Practica2_init(void);

/** \brief
 * 		This function evaluates the current conditions and takes actions if necessary
 * 	\return void
 */
void Practica2_run(void);

#endif /* PRACTICA2_H_ */
