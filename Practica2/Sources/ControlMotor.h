/**	 \file ControlMotor.h
	 \brief
	 	 This is the header file for the Motor
	 \author Damian Garcia Serrano and Erick Ortega Prudencio
	 \date	Oct 17 2016
 */
#ifndef SOURCES_CONTROLMOTOR_H_
#define SOURCES_CONTROLMOTOR_H_

#include "DataTypeDefinitions.h"

#define MotorInitialSpeed 85
#define MotorMaxSpeed 100
#define MotorMinSpeed 0

#define MotorInitialStep 15
#define MotorStepIncrements 5
#define MotorMaxStep 100
#define MotorMinStep 5

/**
 * \brief Configures the initial values of the motor control.
 */
void ControlMotor_init();

/**
 * \brief Increments the next value of power in the motor
 */
void ControlMotor_incrementValue();
/**
 * \brief Decrements the next value of power in the motor.
 */
void ControlMotor_decrementValue();
/**
 * \brief Increments the value of the step
 */
void ControlMotor_incrementStep();
/**
 * \brief Decrements the value of the step
 */
void ControlMotor_decrementStep();
/**
 * \brief Updates the current power and step values to the new selected values
 */
void ControlMotor_updateValue();
/**
 * \brief Cancels the changes made to the next power and step values.
 */
void ControlMotor_cancelChanges();
/**
 * \brief Sets an specific value to the power. This change is made immediately
 */
void ControlMotor_setValue();
/**
 * \brief Changes the base power of the motor to the value of the current power.
 * 		The decrements made by the alarm will start from this value
 * 	\return void
 */
void ControlMotor_updatePotenciaBase();
/**
 * \brief returns the current power value
 * \return uint32
 */
uint32 ControlMotor_getPotencia();
/**
 * \brief returns the next power value.
 * @return uint32
 */
uint32 ControlMotor_getNuevaPotencia();
/**
 * \brief returns the next power value.
 * @return uint32
 */
uint32 ControlMotor_getNuevoMotorStep();
/**
 * \brief returns the current increment/decrement step.
 * \return uint32
 */
uint32 ControlMotor_getMotorStep();
/**
 * \brief returns the current base power.
 * \return uint32
 */
uint32 ControlMotor_getPotenciaBase();
#endif /* SOURCES_CONTROLMOTOR_H_ */
