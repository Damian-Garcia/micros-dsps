/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK64F12.h"
#include "GPIO.h"
#include "NVIC.h"


static int i = 0;

void FTM0_IRQHandler(){
	GPIO_tooglePIN(GPIOC,1);
	FTM0_SC;
	FTM0_SC &= (~FTM_SC_TOF_MASK);
}

int main(void)
{
	SIM_SCGC6 |= SIM_SCGC6_FTM0(1); /** Bit 10 of SIM_SCGC5 is set*/

	GPIO_clockGating(GPIOC);
	GPIO_pinControlRegisterType pcrFSM_MatC0 = GPIO_MUX1;
	GPIO_pinControlRegister(GPIOC, 1, &pcrFSM_MatC0);
	GPIO_setPIN(GPIOC, 1);
	GPIO_dataDirectionPIN(GPIOC,GPIO_OUTPUT,1 );

	FTM0_MODE = FTM_MODE_WPDIS_MASK ;//| FTM_MODE_FTMEN_MASK;
	FTM0_MOD = 32811;
	FTM0_SC = FTM_SC_TOIE_MASK | FTM_SC_CLKS(1) | FTM_SC_CPWMS_MASK | FTM_SC_PS(5) ;

	FTM0_CNTIN =0;

	//FTM0_PWMLOAD = FTM_PWMLOAD_LDOK_MASK;
	NVIC_enableInterruptAndPriotity(FTM0_IRQ, PRIORITY_4);
	EnableInterrupts;

    for (;;) {
       // i++;
    }
    /* Never leave main */
    return 0;
}
